/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.provider;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.crater.mentorz.exception.CustomException;

@Provider
public class CustomExceptionMapper implements ExceptionMapper<CustomException> {

	public CustomExceptionMapper() {
	}

	@Override
	public Response toResponse(CustomException e) {
		return Response.status(e.getStatus().value())
				.entity(e.getEntity() != null ? e.getEntity() : e.getMessage())
				.type(MediaType.APPLICATION_JSON).build();
	}

}
