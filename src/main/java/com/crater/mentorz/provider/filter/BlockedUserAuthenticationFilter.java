package com.crater.mentorz.provider.filter;

import java.io.IOException;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.crater.mentorz.annotation.Blocked;
import com.crater.mentorz.provider.UsersCache;


@Service
public class BlockedUserAuthenticationFilter implements ContainerRequestFilter {

	private final Logger _logger = LoggerFactory.getLogger(BlockedUserAuthenticationFilter.class);

	@Context
	private ResourceInfo resourceInfo;

	@Context
	UriInfo uriInfo;

	final private UsersCache userCache;

	@Autowired
	public BlockedUserAuthenticationFilter(UsersCache userCache) {
		this.userCache = userCache;
	}

	@Override
	public void filter(final ContainerRequestContext requestContext) throws IOException {
		if (resourceInfo.getResourceMethod().isAnnotationPresent(Blocked.class)) {
			String blockedUserId = null;
			final String userId = uriInfo.getPathParameters().getFirst("userId");
			final String mentorId = uriInfo.getPathParameters().getFirst("mentorId");
			final String menteeId = uriInfo.getPathParameters().getFirst("menteeId");
			final String ratedUserId = uriInfo.getPathParameters().getFirst("ratedUserId");
			final String friendId = uriInfo.getPathParameters().getFirst("friendId");
			
			if(mentorId !=null){
				blockedUserId=mentorId;
			} else if(menteeId != null){
				blockedUserId=menteeId;
			} else if(ratedUserId != null){
				blockedUserId=ratedUserId;
			} else if (friendId != null) {
				blockedUserId=friendId;
			}
			
			if (userCache.isBlocked(Long.valueOf(userId), Long.valueOf(blockedUserId))) {
				_logger.info("User blocked");
				requestContext.abortWith(Response.status(Response.Status.PRECONDITION_FAILED).build());
			}
		}
	}

}
