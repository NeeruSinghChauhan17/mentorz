package com.crater.mentorz.provider.filter;

import java.io.IOException;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.crater.mentorz.annotation.PostBlocked;
import com.crater.mentorz.dao.PostDAO;
import com.crater.mentorz.provider.UsersCache;
import com.google.common.base.Optional;

@Service
public class BlockedPostAuthenticationFilter implements ContainerRequestFilter{

	private final Logger _logger = LoggerFactory.getLogger(BlockedUserAuthenticationFilter.class);

	@Context
	private ResourceInfo resourceInfo;

	@Context
	UriInfo uriInfo;

	final private UsersCache userCache;
	
	final private PostDAO postDao;
	
	@Autowired
	public BlockedPostAuthenticationFilter(UsersCache userCache, final PostDAO postDao) {
		this.userCache = userCache;
		this.postDao=postDao;
	}
	
	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		if (resourceInfo.getResourceMethod().isAnnotationPresent(PostBlocked.class)) {
			String postId = uriInfo.getPathParameters().getFirst("postId");
			Optional<Long> friendId=postDao.getUserIdByPost(Long.parseLong(postId));
			String userId = uriInfo.getPathParameters().getFirst("userId");
			if(friendId.isPresent()){
			if (userCache.isBlocked(Long.valueOf(userId), friendId.get())) {
				_logger.info("User  blocked");
				requestContext.abortWith(Response.status(Response.Status.PRECONDITION_FAILED).build());
			}
			}
		}
		
	}

}
