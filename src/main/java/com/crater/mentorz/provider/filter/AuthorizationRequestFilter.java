/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.provider.filter;

import java.io.IOException;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.crater.mentorz.annotation.Auth;
import com.crater.mentorz.resources.Handler;
import com.crater.mentorz.service.AuthorizationService;

@Service
public class AuthorizationRequestFilter implements ContainerRequestFilter {
	 
	private final Logger _logger = LoggerFactory.getLogger(AuthorizationRequestFilter.class);
	
	/*@Inject
	ResourceInfo resourceInfo;
	
	@Inject
	UriInfo uriInfo;*/
	
	@Context
	private ResourceInfo resourceInfo;

	@Context
	UriInfo uriInfo;
	
	/*@Autowired
	AuthorizationService authorizationService;*/
	
	private final AuthorizationService authorizationService;
	//private static final Logger LOGGER = LoggerFactory.getLogger(AuthorizationRequestFilter.class);
	
	@Autowired
	public AuthorizationRequestFilter(AuthorizationService authorizationService) {
		this.authorizationService = authorizationService;
	}
	
    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
    	if(resourceInfo.getResourceMethod().isAnnotationPresent(Auth.class)) {
    		String userId = uriInfo.getPathParameters().getFirst("userId");
    		
    		if(userId == null || !authorizationService.authorize(userId,
    				requestContext.getHeaderString(Handler.HEADER_PARAM_OAUTH_TOKEN),
    				requestContext.getHeaderString(Handler.HEADER_PARAM_USER_AGENT))) {
    			_logger.info("Unauthorized for token " 
    				+ requestContext.getHeaderString(Handler.HEADER_PARAM_OAUTH_TOKEN)
    				+ " UserAgent " + requestContext.getHeaderString(Handler.HEADER_PARAM_USER_AGENT));
				requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
			}
    	}
    }
}
