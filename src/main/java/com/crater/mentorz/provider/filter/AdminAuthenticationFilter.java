package com.crater.mentorz.provider.filter;

import java.io.IOException;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.springframework.stereotype.Service;

import com.crater.mentorz.annotation.Admin;
import com.crater.mentorz.resources.Handler;

/**
 *
 * @Auther Harish Anuragi
 */

@Service
public class AdminAuthenticationFilter implements ContainerRequestFilter{

	@Context
	private ResourceInfo resourceInfo;
	
	@org.springframework.beans.factory.annotation.Value("${admin.auth.token}")
	  private String adminAuthToken;
	
	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		if (resourceInfo.getResourceMethod().isAnnotationPresent(Admin.class)) {
			if (!(requestContext.getHeaderString(Handler.HEADER_PARAM_OAUTH_TOKEN)).equals(adminAuthToken)) {
				requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
			}
		}
	}
}

