package com.crater.mentorz.provider;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.crater.mentorz.dao.UserDAO;



@Service
public class UsersCache {

	private static Map<Long, HashSet<Long>> BLOCKED_USERS = new HashMap<>();
	private final UserDAO userDao;

	@Autowired
	public UsersCache(final UserDAO userDao) {
		this.userDao = userDao;
		/*List<Map<Long, Long>> blockedUsers = this.userDao.getBlockedUsersIds();
		for (Map<Long, Long> users : blockedUsers) {
		    for (Map.Entry<Long, Long> entry : users.entrySet()) {
		       addToBlockedUser(entry.getKey(), entry.getValue());
		    }
		}*/
	}

	public void addToBlockedUser(final Long userId, final Long blockedUserId) {
		blockUser(userId, blockedUserId);
		// blockUser(blockedUserId, userId);
	}

	private void blockUser(final Long userId, final Long blockedUserId) {
		if (BLOCKED_USERS.containsKey(userId)) {
			BLOCKED_USERS.get(userId).add(blockedUserId);
		} else {
			BLOCKED_USERS.put(userId, new HashSet<Long>() {
				{
					add(blockedUserId);
				}
			});
		}
	}

	private void unblockUser(final Long userId, final Long blockedUserId) {
		if (BLOCKED_USERS.containsKey(userId)) {
			BLOCKED_USERS.get(userId).remove(blockedUserId);
			
		}
	}
	
	public void removedFromBlockedUser(final Long userId, final Long blockedUserId) {
		unblockUser(userId, blockedUserId);
		// unblockUser(blockedUserId, userId);
	}
	public Boolean isBlocked(final Long userId, final Long blockedUserId) {
		if(BLOCKED_USERS.containsKey(blockedUserId) && BLOCKED_USERS.containsKey(userId))
		{
			if (BLOCKED_USERS.get(blockedUserId).contains(userId) || BLOCKED_USERS.get(userId).contains(blockedUserId)) {
				return true;
			}
		}
		if (BLOCKED_USERS.containsKey(blockedUserId)) {
			if (BLOCKED_USERS.get(blockedUserId).contains(userId)) {
				return true;
			}
		} else if (BLOCKED_USERS.containsKey(userId)) {
			if (BLOCKED_USERS.get(userId).contains(blockedUserId)) {
				return true;
			}
		}
		return false;
	}
	
}
