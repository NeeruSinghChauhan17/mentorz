package com.crater.mentorz.utils;

import java.util.HashMap;
import java.util.LinkedHashMap;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.lang.StringUtils;

import com.crater.mentorz.models.user.Countries;
import com.crater.mentorz.models.user.Country;


public class CountryCodeUtil {
	
	final static HashMap<String, Country> countryWithCode=new LinkedHashMap<>();

 static {
	try{
		//ClassLoader classLoader = new CountryCodeUtil().getClass().getClassLoader();
		final JAXBContext jaxbContext = JAXBContext.newInstance(Countries.class);
		final Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		final Countries countries = (Countries) jaxbUnmarshaller.unmarshal( 
				new CountryCodeUtil().getClass().getResourceAsStream(("/util/country-code.xml")));
		for(Country country : countries.getCountries())
		{
			countryWithCode.put(country.getPhoneCode().toString(), country);
		}
	}catch(Exception e ){
		e.printStackTrace();
	}
  }
 
 public static String getCountryAreaCodeByCC(final String cc){
	 if(countryWithCode.get(cc) != null){
		 return countryWithCode.get(cc).getCode();
	 }
	 return StringUtils.EMPTY;
 }
 
 /*public static void main(String[] args) {
	System.out.println(getCountryAreaCodeByCC("891"));
}*/

}


