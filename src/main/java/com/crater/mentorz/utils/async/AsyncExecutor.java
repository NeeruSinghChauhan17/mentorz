package com.crater.mentorz.utils.async;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class AsyncExecutor {
	
	private static final int NUMBER_OF_THREADS = 10;

	private static AsyncExecutor instance = null;
	private ExecutorService executorService = null;
	
	private AsyncExecutor() {
		
	}
	
	public static AsyncExecutor getInstance() {
		if (instance == null) {
			instance = new AsyncExecutor();
		}
		if (instance.executorService == null) {
			instance.executorService = Executors.newFixedThreadPool(NUMBER_OF_THREADS);
		}
		return instance;
	}
	
	public void execute(Runnable job) {
		instance.executorService.execute(job);
	}
	
}
