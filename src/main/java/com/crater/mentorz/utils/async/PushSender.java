package com.crater.mentorz.utils.async;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.crater.mentorz.http.HttpCallMediator;
import com.crater.mentorz.models.push.Notification;

public class PushSender  implements Runnable{

	
	private final Notification notification;
	private final HttpCallMediator httpCallMediator;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(HttpCallMediator.class);
	public PushSender(Notification notification, HttpCallMediator httpCallMediator) {
		this.notification = notification;
		this.httpCallMediator = httpCallMediator;
	}




	@Override
	public void run() {
		
		httpCallMediator.sendPush(notification);
		
		
		
	}

}
