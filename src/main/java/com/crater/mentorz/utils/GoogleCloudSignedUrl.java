/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.time.Instant;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.NotFoundException;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Harish Anuragi<br> 
 *         <p>
 *         This util is used to get signed url with the help of this url user
 *         can perform operation on google cloud storage, like:- upload content, download
 *         content, remove content
 * 
 */
public class GoogleCloudSignedUrl {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(GoogleCloudSignedUrl.class);
	
	final static String SERVICE_ACCOUNT_EMAIL = "735337429890-compute@developer.gserviceaccount.com";
	final static String SERVICE_ACCOUNT_PKCS12_FILE_PATH = "/home/ubuntu/Wedoshoes-66697a900602.p12";
	//final static long expiration = System.currentTimeMillis() / 1000 + 3600;
	static long expiration;
	final static String BUCKET_NAME = "before_after_images";

	private static final String RESUMEABLE_UPLOAD_URL_FORMAT = "https://www.googleapis.com/upload/storage/v1/b/%s/o?uploadType=resumable&name=%s";
	/*public static void main(String[] args) throws Exception {
		System.out.println(expiration);
		HttpServletRequest request = null;
		HttpServletResponse response = null;
		RequestDispatcher rd = request.getRequestDispatcher(getSigningURL("GET","ImageTest2"));
		rd.forward(request, response);
		//ResourceType rs = new ResourceType().getMethodOrResource().
		//System.out.println(getLocationForResumeableUpload("ImageTest2", "image/jpeg", "image/jpeg", 0l));
	}*/
	
	public static String getLocationForResumeableUpload(String filename, String resourceType, String contentType, Long contentLength) throws Exception {
	    String resumeableUploadURL = String.format(RESUMEABLE_UPLOAD_URL_FORMAT,
	    		BUCKET_NAME, filename);

	    try {
	    	
	        URL uploadURL = new URL(resumeableUploadURL);
	        HttpsURLConnection connection = (HttpsURLConnection) uploadURL.openConnection();
	        connection.setSSLSocketFactory(getFactory(new File(SERVICE_ACCOUNT_PKCS12_FILE_PATH), "notasecret".toCharArray()));
	        connection.setRequestMethod("POST");
	        connection.setRequestProperty("X-Upload-Content-Type", contentType);
	        connection.setRequestProperty("X-Upload-Content-Length", "0");
	        connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
	        connection.setRequestProperty("Content-Length", "0");
	        connection.setDoOutput(true);
            connection.setFixedLengthStreamingMode(0);
	        //List<String> scopes = new ArrayList<>();
	        //scopes.add("https://www.googleapis.com/auth/devstorage.full_control");
	        /*AppIdentityService.GetAccessTokenResult accessToken =
	                AppIdentityServiceFactory.getAppIdentityService().getAccessToken(scopes);
	        connection.setRequestProperty("Authorization", "Bearer " + accessToken.getAccessToken());*/
	        connection.connect();

	        if (connection.getResponseCode() >= 400) {
	        	LOGGER.debug("connection failed with accessToken: " + getKeyFromP12());
	        	LOGGER.debug(connection.getResponseMessage());
	            throw new NotFoundException("Object at url: " + resumeableUploadURL +
	                    " returns response code: " + connection.getResponseCode() + " response message: " + connection.getResponseMessage());
	        }

	        String responseMessage = connection.getResponseMessage();
	        String locationHeader = connection.getHeaderField("Location");
	        LOGGER.debug("Upload request response " + connection.getResponseCode() + " : " + responseMessage + " Location header: " + locationHeader);
	        return locationHeader;

	    } catch (MalformedURLException e) {
	    	LOGGER.error("URL Malformed: " + resumeableUploadURL + " unable to fetch resumeable upload url", e);
	        throw new InternalServerErrorException("Unable to complete upload request", e);
	    } catch (IOException e) {
	    	LOGGER.error("Unable to fetch upload URL from GCS: " + resumeableUploadURL, e);
	        throw new InternalServerErrorException("Unable to complete upload request", e);
	    }
	}
	
	private static javax.net.ssl.SSLSocketFactory getFactory( File pKeyFile, char[] cs ) throws Exception {
		  KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance("SunX509");
		  KeyStore keyStore = KeyStore.getInstance("PKCS12");

		  InputStream keyInput = new FileInputStream(pKeyFile);
		  keyStore.load(keyInput, cs);
		  keyInput.close();

		  keyManagerFactory.init(keyStore, cs);

		  SSLContext context = SSLContext.getInstance("SSL");
		  context.init(keyManagerFactory.getKeyManagers(), null, new SecureRandom());

		  return context.getSocketFactory();
		}
	
	/**
	 * This method is used to get signed url for upload content on google cloud.
	 * It expires in default specified time
	 * 
	 * @param httpRequestType
	 * @param objectName
	 * @return signed url
	 * @throws Exception
	 */
	public static String getSigningURL(String httpRequestType, String objectName) throws Exception {
		expiration = Instant.now().toEpochMilli() / 1000 + 3600;
		String url_signature = signString(httpRequestType + "\n\n\n" + expiration + "\n" + "/" + BUCKET_NAME + "/"
				+ objectName, getKeyFromP12());
		String signed_url = "https://storage.googleapis.com/" + BUCKET_NAME + "/" + objectName + "?GoogleAccessId="
				+ SERVICE_ACCOUNT_EMAIL + "&Expires=" + expiration + "&Signature="
				+ URLEncoder.encode(url_signature, "UTF-8");
		return signed_url;
	}

	private static PrivateKey getKeyFromP12() throws Exception {
		return loadKeyFromPkcs12(SERVICE_ACCOUNT_PKCS12_FILE_PATH, "notasecret".toCharArray());
	}

	private static PrivateKey loadKeyFromPkcs12(String filename, char[] password) throws Exception {
		FileInputStream fis = new FileInputStream(filename);
		KeyStore ks = KeyStore.getInstance("PKCS12");
		ks.load(fis, password);
		return (PrivateKey) ks.getKey("privatekey", password);
	}

	private static String signString(String stringToSign, PrivateKey key) throws Exception {
		if (key == null)
			throw new Exception("Private Key not initalized");
		Signature signer = Signature.getInstance("SHA256withRSA");
		signer.initSign(key);
		signer.update(stringToSign.getBytes("UTF-8"));
		byte[] rawSignature = signer.sign();
		return new String(Base64.encodeBase64(rawSignature, false), "UTF-8");
	}
}
