package com.crater.mentorz.utils.aes;

public class MD5util {

	
	public static String  getMD5(byte [] bytes) {
		if(bytes == null) {
			return null;
		}
		return org.apache.commons.codec.digest.DigestUtils.md5Hex(bytes);
	}
	
	public static String  getMD5(String string) {
		String key = "craterzone"; 
		if(string == null) {
			return null;
		}
		return org.apache.commons.codec.digest.DigestUtils.md5Hex(string + key);
	}
	public static String  getMD5(Long string) {
		String key = "craterzone"; 
		if(string == null) {
			return null;
		}
		return org.apache.commons.codec.digest.DigestUtils.md5Hex(string + key);
	}
}
