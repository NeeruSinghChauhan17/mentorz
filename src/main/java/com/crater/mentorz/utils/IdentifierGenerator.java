/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.utils;

import java.math.BigInteger;
import java.security.SecureRandom;

/**
 * @author Harish Anuragi
 * 
 */
public final class IdentifierGenerator {
	private static SecureRandom random = new SecureRandom();

	public static String nextRandomId() {
		return new BigInteger(130, random).toString(32);
	}
}
