/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.utils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.twilio.sdk.auth.AccessToken;
import com.twilio.sdk.auth.IpMessagingGrant;

/**
 * @author Harish Anuragi
 *
 */
public class TwilioTokenGenerator {

	public String getToken(Long userId) throws IOException{
		 InputStream inputStream;
		 Properties prop = new Properties();
		 String propFileName = "twiliocredentials.properties";
		 inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);		 
			if (inputStream != null) {
				prop.load(inputStream);
			} else {
				throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
			}						
		    // Required for all types of tokens
		    String twilioAccountSid = prop.getProperty("TWILIO_ACCOUNT_SID");
		    String twilioApiKey = prop.getProperty("TWILIO_API_KEY");
		    String twilioApiSecret = prop.getProperty("TWILIO_API_SECRET");

		    // Required for IP Messaging
		    String ipmServiceSid = prop.getProperty("TWILIO_IPM_SERVICE_SID");
		    //String deviceId = "someiosdevice";
		    String identity = userId.toString();
		    String appName = "MentorzAPP";
		    //String endpointId = appName + ":" + identity + ":" + deviceId;
		    String endpointId = appName + ":" + identity;
		      
		    // Create IP messaging grant
		    IpMessagingGrant grant = new IpMessagingGrant();
		    grant.setEndpointId(endpointId);
		    grant.setServiceSid(ipmServiceSid);
		    
		    // Create access token
		    AccessToken token = new AccessToken.Builder(
		      twilioAccountSid,
		      twilioApiKey,
		      twilioApiSecret
		    ).identity(identity).grant(grant).build();

		   // System.out.println(token.toJWT());
            return token.toJWT();
	 }
	
	 public String generateToken(Long userId, String deviceId){
		 
	        String identity =userId.toString();
	        		
	        // Create an endpoint ID which uniquely identifies the user on their current device
	        String appName = "MentorzAPP";
	        String endpointId = appName + ":" + identity + ":" + deviceId;
	        
	        // Create IP messaging grant
	        IpMessagingGrant grant = new IpMessagingGrant();  
	        grant.setEndpointId(endpointId);    
	        grant.setServiceSid("IS5dfe624432404159aa405780850d775a");
	        grant.setPushCredentialSid("CR9d358ede4b69c475f6742134706b0f94");
	        
	        // Create access token
	        AccessToken token = new AccessToken.Builder(
	          "ACa3901c8c5619c81bf003962b41e4baee",
	          "SK8889a2b84aaba84b3ddb95a51856419b",
	          "0xUgihepJ1Qa2fxTlNeUBMju0upvxUgQ"
	        ).identity(identity).grant(grant).build();
	        
			return token.toJWT();
	    } 
	
}
