/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */

package com.crater.mentorz.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This Class perform various operation on String
 * 
 *  @author raks
 *
 */
public class StringUtil {
	
	private static final Logger Log = LoggerFactory.getLogger(StringUtil.class);
	
	
	private static final String UTF_8="UTF-8";
	/**
	 * This method check if a <Code>String<Code> is null or empty
	 * @param text
	 * @return true if String is null or has length 0 otherwise false
	 */
	public static boolean isEmpty(String text) {
		if(text == null || text.isEmpty()) {
			return true;
		}
		return false;
	}
	
	public static String getStringAlternateCharacters(String string) {
		StringBuilder stringWithAlternateCharacters = new StringBuilder();
		char[] chars = string.toCharArray();
		for (int i=0;i<=chars.length - 1;i++) {
			if (i%2==0) {
				continue;
			}
			stringWithAlternateCharacters.append(chars[i]);
		}
		return stringWithAlternateCharacters.toString();
	}
	
	public static String textEncoderUTF8(String text) {
		try {
			if (text == null || text.length() <= 0) {
				return text;
			}
			return java.net.URLEncoder.encode(text, UTF_8);
		} catch (Exception e) {
			Log.error("error while encoding ",e);
		}
		return text;
	}

	/**
	 * This Function is used for decoding the encodded STring in UTF-8 Standard
	 * 
	 * @param text
	 * @return
	 */
	public static String textDecoderUTF8(String text) {
		try {
			if (text == null || text.length() <= 0) {
				return text;
			}
			return java.net.URLDecoder.decode(text, UTF_8);
		} catch (Exception e) {
			Log.error("error while decoding ",e);
		}
		return text;
	}
	
	public static boolean validURL(String url) {
		try{
			new java.net.URI(url);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
}
