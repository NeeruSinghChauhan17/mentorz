/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.utils;

/**
 * @author Harish Anuragi
 *
 */


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

/**
 * Wrapper around a <code>javax.servlet.http.HttpServletRequest
 * utilized when <code>InvokerServlet processes the initial request
 * for an invoked servlet.  Subsequent requests will be mapped directly
 * to the servlet, because a new servlet mapping will have been created.
 * 
 */

public class InvokerHttpRequest extends HttpServletRequestWrapper {

	// ----------------------------------------------------------- Constructors

	/**
	 * Construct a new wrapped request around the specified servlet request.
	 * 
	 * @param request
	 *            The servlet request being wrapped
	 */
	public InvokerHttpRequest(HttpServletRequest request,String path) {

		super(request);
		this.pathInfo = path;
		this.pathTranslated = path;
		this.requestURI = path;
		this.requestURL = path;
		this.servletPath = path;

	}

	// ----------------------------------------------------- Instance Variables

	/**
	 * Descriptive information about this implementation.
	 */
	protected static final String info = "org.apache.catalina.servlets.InvokerHttpRequest/1.0";

	/**
	 * The path information for this request.
	 */
	protected String pathInfo = null;

	/**
	 * The translated path information for this request.
	 */
	protected String pathTranslated = null;

	/**
	 * The request URI for this request.
	 */
	protected String requestURI = null;
	
	/**
	 * The request URL for this request.
	 */
	protected String requestURL = null;

	/**
	 * The servlet path for this request.
	 */
	protected String servletPath = null;

	// --------------------------------------------- HttpServletRequest Methods

	/**
	 * Override the <code>getPathInfo() method of the wrapped request.
	 */
	public String getPathInfo() {

		return (this.pathInfo);

	}

	/**
	 * Override the <code>getPathTranslated() method of the
	 * wrapped request.
	 */
	public String getPathTranslated() {

		return (this.pathTranslated);

	}

	/**
	 * Override the <code>getRequestURI() method of the wrapped request.
	 */
	public String getRequestURI() {

		return (this.requestURI);

	}

	/**
	 * Override the <code>getRequestURL() method of the wrapped request.
	 */
	public StringBuffer getRequestURL() {
		return (new StringBuffer(this.requestURL));

	}

	/**
	 * Override the <code>getServletPath() method of the wrapped
	 * request.
	 */
	public String getServletPath() {

		return (this.servletPath);

	}

	// -------------------------------------------------------- Package Methods

	/**
	 * Return descriptive information about this implementation.
	 */
	public String getInfo() {

		return (info);

	}

	/**
	 * Set the path information for this request.
	 * 
	 * @param pathInfo
	 *            The new path info
	 */
	void setPathInfo(String pathInfo) {

		this.pathInfo = pathInfo;

	}

	/**
	 * Set the translated path info for this request.
	 * 
	 * @param pathTranslated
	 *            The new translated path info
	 */
	void setPathTranslated(String pathTranslated) {

		this.pathTranslated = pathTranslated;

	}

	/**
	 * Set the request URI for this request.
	 * 
	 * @param requestURI
	 *            The new request URI
	 */
	void setRequestURI(String requestURI) {

		this.requestURI = requestURI;

	}

	/**
	 * Set the servlet path for this request.
	 * 
	 * @param servletPath
	 *            The new servlet path
	 */
	void setServletPath(String servletPath) {

		this.servletPath = servletPath;

	}

}
