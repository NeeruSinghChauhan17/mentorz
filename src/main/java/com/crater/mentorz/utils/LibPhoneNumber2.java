package com.crater.mentorz.utils;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;

public class LibPhoneNumber2 {

	public static String[] getInternationalNumber(String number) {
	  if(!number.contains("+")){
	     number="+"+number;	  
	  }
		String[] arr = new String[2];
		PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
		 try {
		    // phone must begin with '+'
		    PhoneNumber numberProto = phoneUtil.parse(number, "");
		   int countryCode = numberProto.getCountryCode();
		    long no=numberProto.getNationalNumber();
		   
		    
			arr[0]=String.valueOf(countryCode);
		    arr[1]=String.valueOf(no);
		} catch (NumberParseException e) {
		    System.err.println("NumberParseException was thrown: " + e.toString());
		}
		return arr;

	}

}
