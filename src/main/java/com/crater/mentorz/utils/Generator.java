package com.crater.mentorz.utils;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public class Generator {

	public static String hashCode(String msg) {
		String digest = null;
		final String algo = "HmacSHA1";
		try {
			final SecretKeySpec key = new SecretKeySpec(
					("key").getBytes("UTF-8"), algo);
			final Mac mac = Mac.getInstance(algo);
			mac.init(key);

			final byte[] bytes = mac.doFinal(msg.getBytes("ASCII"));

			final StringBuffer hash = new StringBuffer();
			for (int i = 0; i < bytes.length; i++) {
				String hex = Integer.toHexString(0xFF & bytes[i]);
				if (hex.length() == 1) {
					hash.append('0');
				}
				hash.append(hex);
			}
			digest = hash.toString();
		} catch (UnsupportedEncodingException e) {
		} catch (InvalidKeyException e) {
		} catch (NoSuchAlgorithmException e) {
		}
		return digest;
	}
	
	public static String generateToken() {
		final StringBuilder buf = new StringBuilder();
		final SecureRandom sr = new SecureRandom();
		for (int i = 0; i < 10; i++) {// log2(52^6)=34.20... so, this is about// 32bit strong.
			final boolean upper = sr.nextBoolean();
			char ch = (char) (sr.nextInt(26) + 'a');
			if (upper)
				ch = Character.toUpperCase(ch);
			buf.append(ch);
		}
		return buf.toString();
	}
}

