package com.crater.mentorz.enums;

public enum AccountStatus {

	ACTIVE(0),

	INACTIVE(1);
	
	private int statusCode;
	
	private AccountStatus(int statusCode) {
		this.statusCode = statusCode;
	}

	

	public  int getStatusCode() {
		return statusCode;
	}



	public static AccountStatus getStatus(int statusCode) {
		switch (statusCode) {

		case 0:
			return ACTIVE;

		case 1:
			return INACTIVE;
		}
		return null;
	}
}

