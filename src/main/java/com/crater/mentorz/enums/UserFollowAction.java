package com.crater.mentorz.enums;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.google.common.collect.Maps;

public enum UserFollowAction {
	
	FOLLOW(1, "follow"),
	UN_FOLLOW(2, "unfollow");

	

	private static final Map<Integer, UserFollowAction> CODE_TO_CATEGORY = Maps
			.newHashMap();

	private static final Map<String, UserFollowAction> DISPLAY_NAME_TO_CATEGORY = Maps
			.newHashMap();

	static {
		for (final UserFollowAction category : UserFollowAction.values()) {
			CODE_TO_CATEGORY.put(category.getCode(), category);
		}
		for (UserFollowAction category : UserFollowAction.values()) {
			DISPLAY_NAME_TO_CATEGORY.put(category.getDisplayName(), category);
		}
	}

	private final Integer code;

	private final String displayName;

	private UserFollowAction(final Integer code, final String displayName) {
		this.code = code;
		this.displayName = displayName;
	}

	public Integer getCode() {
		return code;
	}

	public String getDisplayName() {
		return displayName;
	}

	@JsonValue
	public String toValue() {
		return getDisplayName();
	}

	@JsonCreator
	public static UserFollowAction nameToType(final String displayName) {
		return DISPLAY_NAME_TO_CATEGORY.get(displayName);
	}

	public static UserFollowAction codeToCategory(final Integer code) {
		return CODE_TO_CATEGORY.get(code);
	}
}
