package com.crater.mentorz.enums;

public enum NotificationMessageType {

	HAS_FOLLOWING(1),

	SHARE(2),

	COMMENTED_ON_POST(3),

	BLOCK(4),

	LIKED_POST(5),
	
	SEND_REQUEST(6),

	ACCEPT_REQUEST(7),
	
	IS_LIVE(8),
	
	IS_WATCHING(9),
	
	CONTACT_ON_MENTORZ(10);
	
	
	private int notificationCode;

	private NotificationMessageType(int notificationCode) {
		this.notificationCode = notificationCode;
	}

	public int getNotificationCode() {
		return notificationCode;
	}

	public static NotificationMessageType getNotification(int notificationCode) {
		switch (notificationCode) {

		case 1:
			return HAS_FOLLOWING;

		case 2:
			return SHARE;

		case 3:
			return COMMENTED_ON_POST;

		case 4:
			return BLOCK;

		case 5:
			return LIKED_POST;

		case 6:
			return SEND_REQUEST;

		case 7:
			return ACCEPT_REQUEST;
		
		case 10:
			return CONTACT_ON_MENTORZ;
		}
		return null;
	}
	public enum NotificationType{
		YOU, LIVESTREAMS;
	}
}
