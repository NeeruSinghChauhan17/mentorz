/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.exception;

import org.springframework.http.HttpStatus;


public class GoneException extends CustomException {

	private static final long serialVersionUID = 1L;

	public static class Builder {
		
		private HttpStatus status = HttpStatus.GONE;
		private String message = "Resource Gone";
		private Throwable cause;
		private ErrorEntity entity;
		
		public Builder() {
			
		}
		
		public Builder message( String message) {
			this.message = message;
			return this;
		}
		
		public Builder entity( ErrorEntity entity) {
			this.entity = entity;
			return this;
		}
		
		public Builder throwable( Throwable cause ) {
			this.cause = cause;
			return this;
		}
		
		public GoneException build() {
			return new GoneException(this);
		}
	}

	
	private GoneException( Builder builder) {
		super(builder.message, builder.status, builder.cause , builder.entity);
	}
}
