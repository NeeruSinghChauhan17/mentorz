/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.exception;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonProperty;


public class CustomException extends Exception {

	private static final long serialVersionUID = 1L;
	
	/**
	 * status : HttpStatus Code in Exception
	 */
	private final HttpStatus status ;
	
	private final ErrorEntity entity;

	public CustomException(String message, HttpStatus status, Throwable cause, ErrorEntity entity) {
		super(message, cause);
		this.status = status;
		this.entity = entity;
	}

	@JsonProperty("status")
	public HttpStatus getStatus() {
		return status;
	}

	@JsonProperty("entity")
	public ErrorEntity getEntity() {
		return entity;
	}
}
