/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.exception;

import org.springframework.http.HttpStatus;

public class NotFoundException extends CustomException {
	
	private static final long serialVersionUID = 1L;

	public static class Builder {
		
		private HttpStatus status = HttpStatus.NOT_FOUND;
		private String message = "Bad Request";
		private Throwable cause;
		private ErrorEntity entity;
		
		public Builder() {
			
		}
		
		public Builder message( String message) {
			this.message = message;
			return this;
		}
		
		public Builder entity( ErrorEntity entity) {
			this.entity = entity;
			return this;
		}
		public Builder throwable( Throwable cause ) {
			this.cause = cause;
			return this;
		}
		
		public NotFoundException build() {
			return new NotFoundException(this);
		}
	}

	public NotFoundException( Builder builder) {
		super(builder.message, builder.status, builder.cause, builder.entity );
	}

}
