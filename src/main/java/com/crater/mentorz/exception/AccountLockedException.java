/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.exception;

import org.springframework.http.HttpStatus;


public class AccountLockedException extends CustomException {
	
	private static final long serialVersionUID = 1L;

	public static class Builder {
		
		private HttpStatus status = HttpStatus.LOCKED;
		private String message = "account locked";
		private Throwable cause;
		private ErrorEntity entity;
		
		public Builder() {
			
		}
		
		public Builder message( String message) {
			this.message = message;
			return this;
		}
		
		public Builder entity( ErrorEntity entity) {
			this.entity = entity;
			return this;
		}
		public Builder throwable( Throwable cause ) {
			this.cause = cause;
			return this;
		}
		
		public AccountLockedException build() {
			return new AccountLockedException(this);
		}
		
	}

	public AccountLockedException( Builder builder) {
		super(builder.message, builder.status, builder.cause, builder.entity );
	}

}
