/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.exception;

import org.springframework.http.HttpStatus;


/**
 * @author Neeru singh
 *
 */
public class ConflitException extends CustomException {
	
	private static final long serialVersionUID = 1L;

	public static class Builder {
		
		private HttpStatus status = HttpStatus.CONFLICT;
		private String message = "Conflit";
		private Throwable cause;
		private ErrorEntity entity;
		
		public Builder() {
			
		}
		
		public Builder message( String message) {
			this.message = message;
			return this;
		}
		
		public Builder entity( ErrorEntity entity) {
			this.entity = entity;
			return this;
		}
		
		public Builder throwable( Throwable cause ) {
			this.cause = cause;
			return this;
		}
		
		public ConflitException build() {
			return new ConflitException(this);
		}
	}

	
	private ConflitException( Builder builder) {
		super(builder.message, builder.status, builder.cause , builder.entity);
	}
}
