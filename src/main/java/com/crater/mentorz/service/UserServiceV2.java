/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.service;

import java.io.IOException;
import java.security.SignatureException;
import java.security.cert.CertificateException;
import java.util.Collection;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.crater.mentorz.exception.BadRequestException;
import com.crater.mentorz.exception.ConflitException;
import com.crater.mentorz.exception.ForbiddenException;
import com.crater.mentorz.exception.NotFoundException;
import com.crater.mentorz.exception.UnAuthorizedException;
import com.crater.mentorz.models.Rating;
import com.crater.mentorz.models.mentor.MentorSearch;
import com.crater.mentorz.models.payment.Balance;
import com.crater.mentorz.models.payment.Payment;
import com.crater.mentorz.models.payment.Redeem;
import com.crater.mentorz.models.post.Post;
import com.crater.mentorz.models.user.DeviceInfo;
import com.crater.mentorz.models.user.Feedback;
import com.crater.mentorz.models.user.ForgetPassword;
import com.crater.mentorz.models.user.Interest;
import com.crater.mentorz.models.user.InterestList;
import com.crater.mentorz.models.user.InternationalNumber;
import com.crater.mentorz.models.user.Login;
import com.crater.mentorz.models.user.NewPassowrd;
import com.crater.mentorz.models.user.PhoneContact;
import com.crater.mentorz.models.user.Profile;
import com.crater.mentorz.models.user.ProfileImage;
import com.crater.mentorz.models.user.RequestInfo;
import com.crater.mentorz.models.user.SocialInterest;
import com.crater.mentorz.models.user.SocialSource;
import com.crater.mentorz.models.user.SocialUser;
import com.crater.mentorz.models.user.UpdatePasswordV2;
import com.crater.mentorz.models.user.User;
import com.crater.mentorz.models.user.UserData;
import com.crater.mentorz.models.user.Value;
import com.cz.googlecontentapi.exception.CustomException;
import com.google.common.base.Optional;


public interface UserServiceV2 {
	void register(final User user, final String userAgent) throws ConflitException;

	void registerSocialUser(final SocialUser socialUser, final String userAgent) throws ConflitException;

	void verifyEmail(final Long userId) throws NotFoundException;

	User login(final Login login, final String userAgent) throws UnAuthorizedException,com.crater.mentorz.exception.CustomException;

	void updateValues(final Long userId, final List<Value> values);

	void updateInterests(final Long userId, final InterestList interest);

	List<Value> getValues(final Long userId, final Integer pageNo);

	List<Interest> getMyInterests(final Long userId, final Integer pageNo);

	User socialLogin(final SocialUser socialUser, final String userAgent) throws UnAuthorizedException,com.crater.mentorz.exception.CustomException;

	User getProfile(final Long userId) throws NotFoundException;

	Boolean follow(final Long userId, final Long followId) throws ForbiddenException,com.crater.mentorz.exception.CustomException;

	Long getFollowers(final Long userId);

	Double getRating(final Long userId);

	Profile getProfileImage(final Long userId);

	List<Post> getBoard(final Long userId, final Integer pageNo);

	@Transactional
	Boolean updateProfile(final Long userId, final Profile profile);

	@Transactional
	Boolean rate(final Long userId, final Long ratedUserId, final Rating rating) throws ForbiddenException;

	List<Interest> getInterestsList(final Integer pageNo,final Long userId);
	
	List<Interest> getInterestsById(final Integer interestId);
	
	List<Interest> getInterestsByParentId(final Integer parentId,final Long userId);

	List<Value> getValuesList(final Integer pageNo);

	Integer isFollowing(final Long userId, final Long followingUserId);

	String getSignedUrl(final String httpRequestType, final String objectName) throws IllegalArgumentException, SignatureException,
			IOException, CustomException;

	String getSessionUrl(final String objectName, final String contentType) throws CertificateException, IOException, CustomException;

	Boolean updateProfileImage(final Long userId, final ProfileImage profileImage);

	Boolean sendRequest(final Long userId, final Long mentorId, final RequestInfo info) throws BadRequestException, ForbiddenException;

	String getAccessToken(final Long userId, final String deviceId);

	Optional<List<User>> getMyMentor(final Long userId, final Integer pageNo);

	Optional<List<User>> getMyMentee(final Long userId, final Integer pageNo);

	void deleteAndAddUserSocialInterests(final Long userId, final List<SocialInterest> interestList, final SocialSource socialSource);

	Boolean credit(final Payment payment);

	Boolean debit(final Payment payment, final Long mentorId);

	Boolean redeem(final Redeem redeem);

	Optional<Balance> getBalance(final Long userId, final Long mentorId);

	void feedback(final Long userId, final Feedback feedback);

	Integer blockUser(final Long userId, final Long blockUserId);

	void unFollow(final Long userId, final Long unfollowUserId);

	Optional<List<User>> getBlockedUsers(final Long userId, final Integer pageNo);

	Integer unBlockUser(final Long userId, final Long unBlockUserId);

	Optional<Collection<User>> getReviews(final Long userId, final Integer pageNo);
	
	void updateDeviceInfo(final Long userId,final DeviceInfo deviceInfo);
	
	Boolean logout(final Long userId,final String userAgent );
	
	Boolean forgetPassword(final ForgetPassword forgetPassword) throws com.crater.mentorz.exception.CustomException ;
	
	Boolean resetPassword(final NewPassowrd newPassowrd);
	
	void deleteDevice(final Long userId);
	
	void deleteToken(final Long userId);
	 
	Boolean getAccountStatus(final Long userId);

	List<Interest> syncInterest(final Long time);

    Collection<User> findFollowing(final Long userId);

	void saveUserDocument();

	Optional<List<User>> listFollowers(final Long userId, final Integer pageNo);

	Optional<List<User>> listFollowing(final Long userId,final Integer pageNo);

	void skipMentor(final Long userId,final Long mentorId);
																												
	List<MentorSearch> searchMentorByString(final Long userId,final String mentorName);

	void addContacts(final List<PhoneContact> contacts,final  Long userId,final Short isDelta) throws  BadRequestException, UnAuthorizedException;

    void verifyUser(final InternationalNumber number) throws  NotFoundException;

	void updateContact(InternationalNumber updatecontact, Long userId) throws ConflitException;

	List<PhoneContact> getContactsToInvite(Long userId);

	void updatePassword(UpdatePasswordV2 updatepassword) throws UnAuthorizedException,  NotFoundException;

	boolean isNoExists(InternationalNumber number);

	UserData getUser(Long userId) throws IllegalArgumentException, SignatureException, IOException, CustomException;

	
}
