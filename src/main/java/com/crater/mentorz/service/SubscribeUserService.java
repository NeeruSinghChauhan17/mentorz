package com.crater.mentorz.service;

import com.crater.mentorz.models.user.SubscribeUser;

public interface SubscribeUserService {

	Boolean subscribeUser(final SubscribeUser subscribeUser);
}
