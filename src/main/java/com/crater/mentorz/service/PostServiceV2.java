/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.service;

import java.util.Collection;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.crater.mentorz.exception.ForbiddenException;
import com.crater.mentorz.models.post.AbuseType;
import com.crater.mentorz.models.post.Comment;
import com.crater.mentorz.models.post.Post;
import com.crater.mentorz.models.post.PostV2;
import com.google.common.base.Optional;


public interface PostServiceV2 {

	@Transactional
	PostV2 addPost(final Long userId, final PostV2 post);

	@Transactional
	Boolean like(final Long postId, final Long userId) throws ForbiddenException;

	@Transactional
	Boolean view(final Long postId, final Long userId) throws ForbiddenException;

	@Transactional
	Boolean share(final Long postId, final Long userId) throws ForbiddenException;

	@Transactional
	Comment comment(final Long userId, final Long postId, final Comment comment) throws ForbiddenException;

	List<Post> getPostByUserId(final Long userId, final Integer pageNo);

	List<Comment> getCommentByPostId(final Long postId, final Integer pageNo);

	List<Post> getBoard(final Integer pageNo, final Long userId);

	@Transactional
	void deleteComment(final Long userId, final Long postId, final Long commentId) throws ForbiddenException;

	List<Post> getFriendPostByUserId(final Long userId, final Long friendId, final Integer pageNo);

	void abusePost(final Long userId, final Long postId, final AbuseType abuseType) throws ForbiddenException;

	Optional<List<Post>> searchPost(final Long userId, final List<Integer> interest, final Integer pageNo);
	
	Optional<Comment>  getCommentByCommentId(final Long commentId);
	
	Optional<Post>  getPostByPostIdInShareOut(final Long postId);

	Optional<Collection<PostV2>> getPostByPostId(Long postId);

}
