package com.crater.mentorz.service;

import java.util.List;

import com.crater.mentorz.exception.UnAuthorizedException;
import com.crater.mentorz.models.admin.Action;
import com.crater.mentorz.models.admin.Rank;
import com.crater.mentorz.models.admin.User;
import com.crater.mentorz.models.post.Post;
import com.crater.mentorz.models.user.Feedback;
import com.crater.mentorz.models.user.Interest;
import com.google.common.base.Optional;


public interface AdminService {

	User login(final User user) throws UnAuthorizedException;

	Optional<List<com.crater.mentorz.models.user.User>> getUsers(final Integer pageNo);
	
	List<com.crater.mentorz.models.user.User> getMentors();

	Optional<List<Feedback>> getFeedback(final Integer pageNo);

	Integer addInterest(final Interest interest);

	Optional<List<com.crater.mentorz.models.user.User>> getBlockedUsers(final Integer pageNo);

	Boolean updateUserAccountStatus(final Long userId, final Action action);

	Optional<List<Feedback>> searchFeedbackByUserName(final String userName, final Integer pageNo);

	Optional<List<com.crater.mentorz.models.user.User>> searchUsersByUserName(final String userName, final Integer pageNo);

	Optional<List<Post>> searchAbusedUsersByUserName(final String userName, final Integer pageNo);

	Optional<List<com.crater.mentorz.models.user.User>> searchBlockedUsersByUserName(final String userName, final Integer pageNo);

	Optional<List<Post>> getAbusedPost(final Integer pageNo);

	void updatePostStatus(final Long userId, final Long postId, final Action action);
	
	Integer getTotalUser();
	
	Float getAvgOfValuesPerMentor();
	
	Float getAvgOfExpertisePerMentor();
	
	Integer getTotalMentor();
	
	List<Interest> getInterestsList();
	
    List<Rank> getExpertisesRank();
	
	List<Rank> getValuesRank();

	List<Rank> getInterestsRank();
 
	
}

