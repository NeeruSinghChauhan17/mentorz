package com.crater.mentorz.service.impl;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.crater.mentorz.dao.AdminDAO;
import com.crater.mentorz.exception.UnAuthorizedException;
import com.crater.mentorz.models.admin.Action;
import com.crater.mentorz.models.admin.Rank;
import com.crater.mentorz.models.admin.User;
import com.crater.mentorz.models.post.Post;
import com.crater.mentorz.models.user.Feedback;
import com.crater.mentorz.models.user.Interest;
import com.crater.mentorz.service.AdminService;
import com.google.common.base.Optional;

/**
 *
 * @Auther Harish Anuragi
 */

@Service
public class AdminServiceImpl implements AdminService{

	private final AdminDAO adminDao;
	
	@Autowired
	public AdminServiceImpl(final AdminDAO adminDao) {
		this.adminDao = adminDao;		
	}
	
	@Override
	public User login(final User user) throws UnAuthorizedException {
		final String password = adminDao.login(user);
		if(user.getPassword().equals(password)) {
			return new User(user.getName(), null, "test_token");
		}
		/*throw new UnAuthorizedException.Builder().entity(new ErrorEntity(401, "Unable to login please check your credentials")).build();*/
		return null;
	}

	@Override
	public Optional<List<com.crater.mentorz.models.user.User>> getUsers(final Integer pageNo) {
		return adminDao.getUsers(pageNo);
	}

	@Override
	public Optional<List<Feedback>> getFeedback(final Integer pageNo) {
		return adminDao.getFeedback(pageNo);
	}

	@Override
	public Integer addInterest(final Interest interest) {
		return adminDao.saveInterest(interest);
	}

	@Override
	public Optional<List<com.crater.mentorz.models.user.User>> getBlockedUsers(final Integer pageNo) {
		return adminDao.getBlockedUsers(pageNo);
	}

	@Override
	public Boolean updateUserAccountStatus(final Long userId, final Action action) {
		return adminDao.updateUserAccountStatus(userId, action);
	}

	@Override
	public Optional<List<Feedback>> searchFeedbackByUserName(final String userName, final Integer pageNo) {
		return adminDao.searchFeedbackByUserName(userName, pageNo);
	}

	@Override
	public Optional<List<com.crater.mentorz.models.user.User>> searchUsersByUserName(final String userName, final Integer pageNo) {
		return adminDao.searchUsersByUserName(userName, pageNo);
	}

	@Override
	public Optional<List<Post>> searchAbusedUsersByUserName(final String userName, final Integer pageNo) {
		return adminDao.searchAbusedUsersByUserName(userName, pageNo);
	}

	@Override
	public Optional<List<com.crater.mentorz.models.user.User>> searchBlockedUsersByUserName(final String userName, final Integer pageNo) {
		return adminDao.searchBlockedUsersByUserName(userName, pageNo);
	}

	@Override
	public Optional<List<Post>> getAbusedPost(final Integer pageNo) {
		return adminDao.getAbusedPost(pageNo);
	}

	@Override
	public void updatePostStatus(final Long userId, final Long postId, final Action action) {
		adminDao.updatePostStatus(userId, postId, action);
	}

	@Override
	public Integer getTotalUser() {
		return adminDao.getTotalUser();
	}

	@Override
	public Integer getTotalMentor() {
		return adminDao.getTotalMentor();
	}

	@Override
	public List<Interest> getInterestsList() {
		return adminDao.getInterestsList();
	}

	@Override
	public List<com.crater.mentorz.models.user.User> getMentors() {
	
		return adminDao.getMentors();
	}
	
	private static Float round(Float d) {
	    BigDecimal bd = new BigDecimal(Float.toString(d));
	    bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
	    return bd.floatValue();
	}

	@Override
	public Float getAvgOfValuesPerMentor() {
		
		return round((float)adminDao.getTotalValues()/adminDao.getTotalUser());
	}

	@Override
	public Float getAvgOfExpertisePerMentor() {
		return round((float)adminDao.getTotalExpertise()/adminDao.getTotalMentor());
	}

	@Override
	public List<Rank> getExpertisesRank() {
		return adminDao.getExpertisesRank();
	}

	@Override
	public List<Rank> getValuesRank() {
		return adminDao.getValuesRank();
	}

	@Override
	public List<Rank> getInterestsRank() {
		return adminDao.getInterestsRank();
	}
}

