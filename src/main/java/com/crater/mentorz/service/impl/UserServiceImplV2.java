/*

 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.service.impl;

import java.io.IOException;
import java.security.SignatureException;
import java.security.cert.CertificateException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executor;

import javax.ws.rs.BadRequestException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.crater.mentorz.dao.UserDAOV2;
import com.crater.mentorz.elasticsearch.model.req.Source;
import com.crater.mentorz.enums.NotificationMessageType;
import com.crater.mentorz.exception.BadRequestException.Builder;
import com.crater.mentorz.exception.ConflitException;
import com.crater.mentorz.exception.ErrorEntity;
import com.crater.mentorz.exception.ForbiddenException;
import com.crater.mentorz.exception.NotFoundException;
import com.crater.mentorz.exception.UnAuthorizedException;
import com.crater.mentorz.http.HttpCallMediator;
import com.crater.mentorz.mail.EmailServiceImpl;
import com.crater.mentorz.models.Rating;
import com.crater.mentorz.models.mentor.Expertise;
import com.crater.mentorz.models.mentor.MentorSearch;
import com.crater.mentorz.models.payment.Balance;
import com.crater.mentorz.models.payment.Payment;
import com.crater.mentorz.models.payment.Redeem;
import com.crater.mentorz.models.post.Post;
import com.crater.mentorz.models.push.Notification;
import com.crater.mentorz.models.user.DeviceInfo;
import com.crater.mentorz.models.user.Feedback;
import com.crater.mentorz.models.user.ForgetPassword;
import com.crater.mentorz.models.user.Interest;
import com.crater.mentorz.models.user.InterestList;
import com.crater.mentorz.models.user.InternationalNumber;
import com.crater.mentorz.models.user.Login;
import com.crater.mentorz.models.user.NewPassowrd;
import com.crater.mentorz.models.user.PhoneContact;
import com.crater.mentorz.models.user.Profile;
import com.crater.mentorz.models.user.ProfileImage;
import com.crater.mentorz.models.user.RequestInfo;
import com.crater.mentorz.models.user.SocialInterest;
import com.crater.mentorz.models.user.SocialSource;
import com.crater.mentorz.models.user.SocialUser;
import com.crater.mentorz.models.user.UpdatePasswordV2;
import com.crater.mentorz.models.user.User;
import com.crater.mentorz.models.user.UserData;
import com.crater.mentorz.models.user.Value;
import com.crater.mentorz.service.AuthorizationService;
import com.crater.mentorz.service.MentorService;
import com.crater.mentorz.service.PostService;
import com.crater.mentorz.service.UserServiceV2;
import com.crater.mentorz.utils.EncryptDecrypt;
import com.crater.mentorz.utils.Generator;
import com.crater.mentorz.utils.LibPhoneNumber2;
import com.crater.mentorz.utils.StringUtil;
import com.crater.mentorz.utils.TwilioTokenGenerator;
import com.crater.mentorz.utils.async.PushSender;
import com.cz.googlecontentapi.exception.CustomException;
import com.cz.googlecontentapi.model.CloudObjectInfo;
import com.cz.googlecontentapi.model.GoogleCloudCredential;
import com.cz.googlecontentapi.service.impl.GoogleContentServiceImpl;
import com.google.common.base.Optional;

@Component
public class UserServiceImplV2 implements UserServiceV2 {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImplV2.class);
	private static final String NULL = null;
	private Integer OFFSET = 0;
	private final Integer LIMIT = 2560;

	private final UserDAOV2 userDao;
	private final AuthorizationService oAuthService;
	private final EmailServiceImpl emailService;
	final private PostService postService;
	final private MentorService mentorService;
	private final HttpCallMediator httpCallMediator;
	private final Executor taskExecutor;
	private final String GOOGLE_CLOUD_SERVICE_ACCOUNT_EMAIL = "276892747020-compute@developer.gserviceaccount.com";
	private final String GOOGLE_CLOUD_BUCKET_NAME = "mentorz_content";

	@Autowired
	private ElasticSearchHelper esHelper;

	@org.springframework.beans.factory.annotation.Value("${${mode}.storage.p12.path}")
	private String storageFilePath;

	@org.springframework.beans.factory.annotation.Value("${${mode}.email.verification.url}")
	private String emailVerificationUrl;

	@Autowired
	public UserServiceImplV2(final UserDAOV2 userDao, final AuthorizationService oAuthService,
			final EmailServiceImpl emailService, final PostService postService, final MentorService mentorService,
			final HttpCallMediator httpCallMediator, final Executor taskExecutor) {
		this.userDao = userDao;
		this.oAuthService = oAuthService;
		this.emailService = emailService;
		this.postService = postService;
		this.mentorService = mentorService;
		this.taskExecutor = taskExecutor;
		this.httpCallMediator = httpCallMediator;
	}

	@Override
	@Transactional
	public void register(final User user, final String userAgent) throws ConflitException {

		// user is searched for its existance on the basis of (i) cc ,phone,email id  (2) email id (3) cc,phone 
		final Optional<User> usser = userDao.isExists(user.getEmailId(), user.getNumber());

		if (usser.isPresent() && usser.get().getPassword() != null) {
			throw new ConflitException.Builder().build();
		} else if (usser.isPresent() && usser.get().getPassword() == null) {
			userDao.updateUser(usser.get().getUserId(), User.createWithDeviceInfoAndUserAgent(user, userAgent), false);
			// sendEmail(usser.get().getUserId(), usser.get().getEmailId());
			// //removed in v2
			return;
		}
		final User usr = saveUser(user, userAgent);

		/**
		 * Save user At ES
		 */
		esHelper.addUserDocument(usr);
		// sendEmail(usr.getUserId(), usr.getEmailId()); //removed in v2

		/**
		 * Send Notifications-This guyz on Mentorz
		 */
		sendNotifications(usr);
	}

	private void sendNotifications(final User user) {
		List<User> users = userDao.getListPhoneContacts(user); 
		final NotificationMessageType type = NotificationMessageType.CONTACT_ON_MENTORZ;
		String name = user.getProfile().getName();
		final Map<String, Object> customData = new HashMap<String, Object>();
		customData.put("userId", user.getUserId());
		customData.put("userName", name);
		customData.put("pushType", type);
		// List<Long> to = new ArrayList<Long>();
		Long from = user.getUserId();
		for (User us : users) {
			Long to = us.getUserId();
			taskExecutor
					.execute(new PushSender(getNotificationInfo1(from, to, name, type, customData), httpCallMediator));
		}

	}

	private Notification getNotificationInfo1(final Long from, final Long to, String name,
			final NotificationMessageType type, final Map<String, Object> customData) {
		return new Notification(from, Arrays.asList(to), Instant.now().toEpochMilli(), type,
				StringUtil.textDecoderUTF8(name), customData,true);
	}

	private void sendEmail(final Long userId, final String emailId) {
		String url = emailVerificationUrl + userId + "/verify";
		emailService.sendWelcomeMail(emailId, url);
	}

	@Transactional
	public User saveUser(final User user, final String userAgent) throws ConflitException {
		final User usr = userDao.save(User.createWithDeviceInfoAndUserAgent(user, userAgent));
		final String authToken = oAuthService.getOrCreateOAuthToken("" + usr.getUserId(), userAgent);
		return User.createWithAuthToken(authToken, usr);
	}

	@Override
	public void verifyEmail(final Long userId) throws NotFoundException {
		userDao.verifyEmail(userId);
	}

	@Override
	public User login(final Login login, final String userAgent) throws com.crater.mentorz.exception.CustomException {

		final Optional<User> usr = getUserDetails(login);
		// final Optional<User> usr = userDao.getUserDetails(login);

		if (usr.isPresent()) {
			if ((login.getNumber().getPhoneNumber() != null) || (login.getNumber().getPhoneNumber() != "")) {
				if (userDao.isSocialUserWithUserId(usr.get().getUserId())) {
					throw new NotFoundException.Builder()
							.entity(new ErrorEntity(2, "  user not found-registered with social id")).build();
				}
			}
          if (usr.get().getPassword().equals(login.getPassword())) {
				if (!(userDao.checkAccountStatus(login.getEmailId(), login.getNumber(), login.getPassword()))) {
					final DeviceInfo deviceInfo = DeviceInfo.createDeviceInfoWithUserAgentAndDeviceType(userAgent,
							login.getDeviceInfo().getDeviceType(), login.getDeviceInfo());
					userDao.updateDeviceInfo(usr.get().getUserId(), deviceInfo);

					final String authToken = oAuthService.getOrCreateOAuthToken("" + usr.get().getUserId(), userAgent);
					return User.createWithAuthToken(authToken, usr.get());
				} else {
					LOGGER.error("============== ACCOUNT SATATUS IS NOT ACTIVE=============");
					throw new UnAuthorizedException.Builder().entity(new ErrorEntity(2, "account is not activated"))
							.build();
				}
			} else {
				LOGGER.error("==============INCORRECT PASSWORD============");
				throw new UnAuthorizedException.Builder().entity(new ErrorEntity(2, "invalid password")).build();
			}
		}
		LOGGER.error("==============ACCOUNT IS NOT VERIFIED=============.");
		throw new ForbiddenException.Builder().entity(new ErrorEntity(2, "  account is not verified")).build();
	}

	private Optional<User> getUserDetails(Login login) throws NotFoundException {
		if (!(login.getEmailId() == "") && !(login.getEmailId() == null)) {
			LOGGER.info("==============USER LOGIN WITH  Email=============." + login.getEmailId());
			return userDao.getUserDetailsByEmail(login);
		} else if ((login.getNumber().getPhoneNumber() != null) || (login.getNumber().getPhoneNumber() != "")) {
			// if no not exists

			// if no exists
			LOGGER.info("==============USER LOGIN WITH PHONE NO=============" + login.getNumber().getPhoneNumber());
			Optional<User> user = userDao.getUserDetailsByPhone(login.getNumber().getPhoneNumber());

			if (!user.isPresent()) {
				LOGGER.info("==============USER LOGIN WITH PHONE NO(cc+no)=============."
						+ login.getNumber().getPhoneNumber());
				String[] number = LibPhoneNumber2.getInternationalNumber(login.getNumber().toString());
				Optional<User> usr = userDao.getUserDetailsByPhone(number[1]);
				if (usr.isPresent()) {
					return usr;
				}
			}
			if (!userDao.isNoExists(login.getNumber())) // if no not exists-then
														// false to true
			{
				LOGGER.info("==============USER NOT EXISTS==================" + login.getNumber().getPhoneNumber());
				throw new NotFoundException.Builder().entity(new ErrorEntity(2, "  user not exists ")).build();
			}

			return user;
		}
		return Optional.absent();

	}

	@Override
	public void updateValues(final Long userId, final List<Value> values) {
		userDao.updateValues(userId, values);
	}

	@Override
	public void updateInterests(final Long userId, final InterestList interestList) {
		userDao.deleteInterests(userId);
		for (final Interest interest : interestList.getInterests()) {
			final List<Interest> interests = new ArrayList<>();
			interests.add(interest);
			taskExecutor.execute(new Runnable() {
				@Override
				public void run() {
					userDao.updateInterests(userId, getMyInterest(interest, interests));
				}
			});

		}

	}

	private List<Interest> getMyInterest(final Interest interest, final List<Interest> interests) {
		if (interests.contains(interest.getInterestId())) {
			return interests;
		} else {
			if (interest.getHasChildren()) {
				List<Interest> childrens = userDao.getInterestsByParentId(interest.getInterestId());
				for (Interest children : childrens) {
					interests.add(children);
					getMyInterest(children, interests);
				}
			}
		}
		return interests;
	}

	@Override
	public List<Interest> getInterestsById(final Integer interestId) {
		return userDao.getInterestsById(interestId);
	}

	@Override
	public List<Interest> getInterestsByParentId(final Integer parentId, final Long userId) {
		final List<Interest> childs = userDao.getInterestsByParentId(parentId);
		final List<Interest> myInterests = userDao.getMyInterests(userId);
		final List<Interest> interests = new ArrayList<Interest>();
		for (Interest interest2 : childs) {
			final Interest interest = changeIsMyInterest(myInterests, interest2);
			if (interest != null) {
				interests.add(interest);
			} else {
				interests.add(interest2);
			}
		}
		return interests;
	}

	private Interest changeIsMyInterest(final List<Interest> childs, final Interest interest) {
		for (Interest inst : childs) {
			if (inst.getInterestId() == interest.getInterestId()) {
				return Interest.createInterestWithMyInterest(interest, true);
			}
		}
		return null;
	}

	@Override
	public List<Value> getValues(final Long userId, final Integer pageNo) {
		return userDao.getValues(userId, pageNo);
	}

	@Override
	public List<Interest> getMyInterests(final Long userId, final Integer pageNo) {
		return userDao.getMyInterests(userId);
	}

	@Override
	@Transactional
	public void registerSocialUser(final SocialUser socialUser, final String userAgent) throws ConflitException {
		if (isValidSocialSource(socialUser.getSocialSource())) {
			if (userDao.checkUserExistanceSocialId(socialUser.getSocialId(), socialUser.getSocialSource())) {
				LOGGER.debug("===========USER WITH SOCIALID ALREADY REGISTERED==========");
				throw new ConflitException.Builder().entity(new ErrorEntity(2, "socialId already registered")).build();
			}
			if ((socialUser.getNumber().getPhoneNumber() != null) || socialUser.getNumber().getPhoneNumber() != "") {
				if (userDao.checkUserExistancePhoneNo(socialUser)) {
					LOGGER.debug("===========USER WITH PHONE NO ALREADY REGISTERED==========");
					throw new ConflitException.Builder().entity(new ErrorEntity(2, "phone no already registered"))
							.build();
				}
			}

			final Optional<User> usser = userDao.isExists(socialUser.getEmailId(), socialUser.getNumber());
			Boolean isExist = false;
			Boolean isHavePassword;
			if (usser.isPresent()) {
				isExist = userDao.isAlreadySocial(usser.get().getUserId());
			}
			// when user is alredy registered with email_id and also have social
			// account
			if (usser.isPresent() && isExist) {
				//// isHavePassword = usser.get().getPassword().length() == 0 ?
				//// false //social users have no password
				// : true;
				userDao.updateUser(usser.get().getUserId(),
						User.createWithDeviceInfoAndUserAgent(socialUser, userAgent), false);
				userDao.updateSocialDetails(usser.get().getUserId(), socialUser.getSocialId(),
						socialUser.getSocialSource());
				return;
			}

			if (usser.isPresent() && !isExist) {
				isHavePassword = usser.get().getPassword().length() == 0 ? false : true;
				userDao.updateUser(usser.get().getUserId(),
						User.createWithDeviceInfoAndUserAgent(socialUser, userAgent), isHavePassword);
				userDao.addSocialInfo(usser.get().getUserId(), socialUser.getSocialId(), socialUser.getSocialSource());
				return;
			}

			final User usr = saveUser(socialUser, userAgent);
			userDao.addSocialInfo(usr.getUserId(), socialUser.getSocialId(), socialUser.getSocialSource());
			/**
			 * save user at ES
			 */

			esHelper.addUserDocument(usr);
			return;
		}
		throw new BadRequestException();
	}

	@Override
	public User socialLogin(final SocialUser socialUser, final String userAgent)
			throws com.crater.mentorz.exception.CustomException {

		if (isValidSocialSource(socialUser.getSocialSource())) {
			/*
			 * //check social id existance-found or not
			 * if(userDao.checkUserExistanceSocialId(socialUser)){
			 * LOGGER.info("==============USER NOT EXISTS==================");
			 * throw new NotFoundException.Builder().entity(new ErrorEntity(2,
			 * "  user not exists ")).build(); }
			 */

			final Optional<Long> optionalUserId = userDao.getUserIdBySocialId(socialUser.getSocialId(),
					socialUser.getSocialSource());

			if (optionalUserId.isPresent()) {
				if (!userDao.checkSocialAccountStatus(socialUser.getSocialId(), socialUser.getPassword(), true,
						socialUser.getSocialSource())) {

					final String authToken = oAuthService.getOrCreateOAuthToken("" + optionalUserId.get(), userAgent);
					final DeviceInfo deviceInfo = DeviceInfo.createDeviceInfoWithUserAgentAndDeviceType(userAgent,
							socialUser.getDeviceInfo().getDeviceType(), socialUser.getDeviceInfo());
					// userDao.updateSocialInfo(optionalUserId.get(),
					// socialUser.getProfile());
					userDao.updateDeviceInfo(optionalUserId.get(), deviceInfo);
					return User.createWithAuthToken(authToken,
							userDao.getSocialUserDetails(optionalUserId.get()).get());
				}
				throw new com.crater.mentorz.exception.CustomException("Account is not active",
						HttpStatus.NOT_ACCEPTABLE, null, null);
			}
			throw new NotFoundException.Builder().entity(new ErrorEntity(2, "  user not exists ")).build();

		}
		throw new BadRequestException();
	}

	@Override
	// @Timed(timingNotes = "first testing")
	public User getProfile(final Long userId) throws NotFoundException {
		final Optional<User> user = userDao.getProfile(userId);

		if (user.isPresent()) {
			return User.createWithExpertise(
					User.createWithProfile(user.get(),
							Profile.createWithRating(user.get().getProfile(),
									new Rating(userDao.getRating(userId), null, null))),
					mentorService.getExpertise(userId));

		}
		throw new NotFoundException.Builder().message("user not exists").build();
	}

	@Override
	@Transactional
	public Boolean follow(final Long userId, final Long followId) throws com.crater.mentorz.exception.CustomException {
		try {
			final NotificationMessageType type = NotificationMessageType.HAS_FOLLOWING;

			userDao.followUser(userId, followId);
			final Map<String, Object> customData = new HashMap<String, Object>();
			customData.put("userId", userId);
			customData.put("pushType", NotificationMessageType.HAS_FOLLOWING);
			customData.put("userName", userDao.getNameByUserId(userId));
			taskExecutor
					.execute(new PushSender(getNotificationInfo(userId, followId, type, customData), httpCallMediator));
		} catch (DuplicateKeyException e) {
			return false;
		}
		return true;
	}

	@Override
	public Long getFollowers(final Long userId) {
		return userDao.getFollowers(userId);
	}

	@Override
	public Double getRating(final Long userId) {
		return userDao.getRating(userId);
	}

	@Override
	public Profile getProfileImage(final Long userId) {
		final Optional<Profile> profile = userDao.getProfileImage(userId);
		if (profile.isPresent()) {
			return profile.get();
		}
		return null;
	}

	@Override
	public List<Post> getBoard(final Long userId, final Integer pageNo) {
		// final List<Integer> interests = userDao.getInterestIds(userId);
		return postService.getBoard(pageNo, userId);
	}

	@Override
	@Transactional
	public Boolean updateProfile(final Long userId, final Profile profile) {
		final int updated = userDao.updateProfile(userId, profile);
		if (updated > 0) {
			return true;
		}
		return false;
	}

	@Override
	@Transactional
	public Boolean rate(final Long userId, final Long ratedUserId, final Rating rating) throws ForbiddenException {
		try {
			userDao.rate(userId, ratedUserId, rating);
		} catch (DuplicateKeyException e) {
			return false;
		}
		return true;
	}

	@Override
	public List<Interest> getInterestsList(final Integer pageNo, final Long userId) {
		LOGGER.info("get default interest list");
		final List<Interest> rootInterests = userDao.getRootInterests(pageNo);
		final List<Interest> myInterests = userDao.getMyInterests(userId);
		final List<Interest> interests = new ArrayList<Interest>();
		for (Interest interest2 : rootInterests) {
			final Interest interest = changeIsMyInterest(myInterests, interest2);
			if (interest != null) {
				interests.add(interest);
			} else {
				interests.add(interest2);
			}
		}
		return interests;

	}

	@Override
	public List<Value> getValuesList(final Integer pageNo) {
		LOGGER.info("get default value list");
		return userDao.getValuesList(pageNo);
	}

	@Override
	public Integer isFollowing(final Long userId, final Long followingUserId) {
		return userDao.isFollowing(userId, followingUserId);
	}

	@Override
	public String getSignedUrl(final String httpRequestType, final String objectName) throws IllegalArgumentException,
			SignatureException, IOException, com.cz.googlecontentapi.exception.CustomException {
		final GoogleCloudCredential credential = new GoogleCloudCredential(GOOGLE_CLOUD_SERVICE_ACCOUNT_EMAIL,
				storageFilePath);
		// /home/mentorzcoreapi01/Mentorz-2fff03eace95.p12
		final CloudObjectInfo cloudObj = new CloudObjectInfo(objectName, GOOGLE_CLOUD_BUCKET_NAME);
		final GoogleContentServiceImpl contentImpl = new GoogleContentServiceImpl();
		return contentImpl.getSigningURL(credential, cloudObj, httpRequestType,
				Instant.now().toEpochMilli() / 1000 + 3600 * 24 * 92);
	}

	@Override
	public String getSessionUrl(final String objectName, final String contentType)
			throws CertificateException, IOException, com.cz.googlecontentapi.exception.CustomException {
		final GoogleCloudCredential credential = new GoogleCloudCredential(GOOGLE_CLOUD_SERVICE_ACCOUNT_EMAIL,
				storageFilePath);
		final CloudObjectInfo cloudObj = new CloudObjectInfo(objectName, GOOGLE_CLOUD_BUCKET_NAME);
		final GoogleContentServiceImpl contentImpl = new GoogleContentServiceImpl();
		return contentImpl.getResumeableUploadURL(credential, cloudObj, contentType, 0l);
	}

	private Boolean isValidSocialSource(final String socialSource) {
		if (socialSource.equals("fb") || socialSource.equals("linkedin")) {
			return true;
		}
		return false;
	}

	@Override
	public Boolean updateProfileImage(final Long userId, final ProfileImage profileImage) {
		final int updated = userDao.updateProfileImage(userId, profileImage);
		return updated > 0 ? true : false;
	}

	@Override
	public Boolean sendRequest(final Long userId, final Long mentorId, final RequestInfo info)
			throws com.crater.mentorz.exception.BadRequestException, ForbiddenException {
		// TODO need to send mail or sms using info

		if (userId == mentorId) {
			throw new com.crater.mentorz.exception.BadRequestException(
					new Builder().entity(new ErrorEntity(4, "mentor and mentee id's cant be same")));
		}
		final NotificationMessageType type = NotificationMessageType.SEND_REQUEST;
		final Map<String, Object> customData = new HashMap<String, Object>();
		customData.put("userId", userId);
		customData.put("userName", userDao.getNameByUserId(userId));
		customData.put("pushType", type);
		taskExecutor.execute(new PushSender(getNotificationInfo(userId, mentorId, type, customData), httpCallMediator));

		if (userDao.sendRequest(userId, mentorId) > 0 ? true : false) {
			/**
			 * add mentee request to mentor At Es
			 */
			esHelper.updateMenteeRequestToMentor(userId, mentorId);
			return true;
		} else {
			return false;
		}

	}

	private Notification getNotificationInfo(final Long from, final Long to, final NotificationMessageType type,
			final Map<String, Object> customData) {
		return new Notification(from, Arrays.asList(to), Instant.now().toEpochMilli(), type,
				StringUtil.textDecoderUTF8(userDao.getNameByUserId(from)), customData,true);
	}
	
	@Override
	public String getAccessToken(final Long userId, final String deviceId) {
		return new TwilioTokenGenerator().generateToken(userId, deviceId);
	}

	@Override
	public Optional<List<User>> getMyMentor(final Long userId, final Integer pageNo) {
		return userDao.getMyMentor(userId, pageNo);
	}

	@Override
	public Optional<List<User>> getMyMentee(final Long userId, final Integer pageNo) {
		return userDao.getMyMentee(userId, pageNo);
	}

	@Override
	public Optional<List<User>> listFollowers(final Long userId, final Integer pageNo) {
		return userDao.listFollowers(userId, pageNo);
	}

	@Override
	public Optional<List<User>> listFollowing(final Long userId, final Integer pageNo) {
		return userDao.listFollowing(userId, pageNo);
	}

	@Override
	public void deleteAndAddUserSocialInterests(final Long userId, final List<SocialInterest> interestList,
			final SocialSource socialSource) {
		userDao.deleteAndAddUserSocialInterests(userId, interestList, socialSource);
	}

	@Override
	public Boolean credit(final Payment payment) {
		return userDao.credit(payment);
	}

	@Override
	public Boolean debit(final Payment payment, final Long mentorId) {
		final Optional<Balance> balance = userDao.getBalance(payment.getUserId(), mentorId);
		if (balance.isPresent()) {
			Boolean hasCredits = false;
			final Double total = balance.get().getAvilableBalance() + balance.get().getCredit();
			if (total < payment.getBalance()) {
				return false;
			}
			// If balance is less than requesting amount then check credits
			if (balance.get().getAvilableBalance() < payment.getBalance()) {
				// If balance and credits both less than requesting amount then
				// check total of both
				if (balance.get().getCredit() >= payment.getBalance()) {
					/*
					 * if(( total >= payment.getBalance())){ //First deduct from
					 * balancer then credits Boolean
					 * isDebit=userDao.debit(balance
					 * .get(),payment.getUserId(),payment.getBalance());
					 * if(isDebit){
					 * userDao.creditIntoMentorAccount(mentorId,payment
					 * .getBalance()); } //Set true if credits is greater than
					 * requesting amount return isDebit; }
					 */
					hasCredits = true;
				} else {
					if ((total >= payment.getBalance())) {
						// First deduct from balance then credits
						Boolean isDebit = userDao.debit(balance.get(), payment.getUserId(), payment.getBalance());
						if (isDebit) {
							userDao.creditIntoMentorAccount(mentorId, payment.getBalance());
						}
						// Set true if credits is greater than requesting amount
						return isDebit;
					}
				}
			}
			// Deduct if hascredits true from credits else from amount
			Boolean isDebit = userDao.debit(payment, hasCredits);
			if (isDebit) {
				userDao.creditIntoMentorAccount(mentorId, payment.getBalance());
			}
			return isDebit;
		}
		return false;
	}

	@Override
	public Optional<Balance> getBalance(final Long userId, final Long mentorId) {
		return userDao.getBalance(userId, mentorId);
	}

	@Override
	public Boolean redeem(final Redeem redeem) {
		emailService.sendTransactionMail(redeem.getPaypalId(), redeem.getBalance());
		return true;
	}

	@Override
	public void feedback(final Long userId, final Feedback feedback) {
		userDao.feedback(userId, feedback);
	}

	@Override
	public Integer blockUser(final Long userId, final Long blockUserId) {
		final Integer result = userDao.blockUser(userId, blockUserId);
		if (result > 0) {
			/**
			 * addBlocked User At Es
			 */
			esHelper.addBlockedUserAtEs(userId, blockUserId);
			userDao.deleteFromMenteeRequest(userId, blockUserId);
			userDao.deleteFromPendingRequest(userId, blockUserId);
			userDao.deleteFromMyMentee(userId, blockUserId);
			userDao.deleteFromMyMentor(userId, blockUserId);

			final NotificationMessageType type = NotificationMessageType.BLOCK;
			final Map<String, Object> customData = new HashMap<>();
			customData.put("pushType", type);
			customData.put("userId", userId);
			taskExecutor.execute(
					new PushSender(getNotificationInfo(userId, blockUserId, type, customData), httpCallMediator));
		}
		return result;
	}

	@Override
	public void unFollow(final Long userId, final Long unfollowUserId) {
		if (userDao.unFollow(userId, unfollowUserId) > 0) {
			userDao.deceraseFollowingCount(userId);
			userDao.deceraseFollowerCount(unfollowUserId);
		}
	}

	@Override
	public Optional<List<User>> getBlockedUsers(final Long userId, final Integer pageNo) {
		return userDao.getBlockedUsers(userId, pageNo);
	}

	@Override
	public Integer unBlockUser(final Long userId, final Long unBlockUserId) {
		return userDao.unBlockUser(userId, unBlockUserId);

	}

	@Override
	public Optional<Collection<User>> getReviews(final Long userId, final Integer pageNo) {
		return userDao.getReviews(userId, pageNo);
	}

	@Override
	public void updateDeviceInfo(final Long userId, final DeviceInfo deviceInfo) {
		userDao.updateDeviceInfo(userId, deviceInfo);

	}

	@Override
	public Boolean logout(final Long userId, final String userAgent) {
		return userDao.logout(userId, userAgent);

	}

	@Override
	public Boolean forgetPassword(final ForgetPassword forgetPassword)
			throws com.crater.mentorz.exception.CustomException {
		Optional<User> user = userDao.isExists(forgetPassword.getEmailId(), null); // change
																					// code
																					// in
																					// v2
		if (user.isPresent()) {
			final String token = Generator.generateToken();
			emailService.sendResetToken(forgetPassword.getEmailId(), token);
			userDao.saveAndUpdateVerificationToken(forgetPassword, token);
			return true;
		}
		throw new com.crater.mentorz.exception.CustomException("Email not exists", HttpStatus.NOT_FOUND, null, null);
	}

	@Override
	public Boolean resetPassword(final NewPassowrd newPassowrd) {
		final String emailId = userDao.verifyToken(newPassowrd.getToken());
		if (emailId != null) {
			userDao.deleteToken(emailId);
			if (userDao.resetPassword(newPassowrd.getPassword(), emailId)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public void deleteDevice(final Long userId) {
		userDao.deleteDevice(userId);

	}

	@Override
	public void deleteToken(final Long userId) {
		userDao.deleteToken(userId);

	}

	@Override
	public Boolean getAccountStatus(final Long userId) {
		return userDao.getAccountStatus(userId);
	}

	@Override
	public List<Interest> syncInterest(final Long time) {
		return userDao.syncInterest(time);
	}

	@Override
	public Collection<User> findFollowing(final Long userId) {
		return userDao.findFollowing(userId);
	}

	@Transactional
	@Override
	public void saveUserDocument() {
		esHelper.deleteOldIndex();
		saveUserDocumentAtEs(OFFSET, LIMIT);
		saveMentorsExpertise(OFFSET, LIMIT);
		saveUserBlockedUsers();
		saveUserRating();
		saveUserAverageRating();
		saveUserRequestSentMentors();
		saveSkipMentors();
		LOGGER.info("ELASTICSEARCH:  ===============USERS UPLOADED IN ES ============= ");
	}

	private void saveMentorsExpertise(final Integer offset, final Integer limit) {
		Collection<Source> users = getMentorExpertise(offset, limit);
		/*
		 * if(users.size()==0 || users==null){ return; }
		 */
		for (Source source : users) {
			saveExpertises(source.getUserId(), source);
			saveParentExpertises(source.getUserId(), source.getExpertise(), new HashSet<Expertise>(),
					new HashSet<Integer>());
		}
		// saveMentorsExpertise( offset+limit+1, limit);

	}

	private void saveExpertises(final Long userId, final Source source) {
		esHelper.addExpertises(userId, new HashSet<Expertise>(source.getExpertise()));
		esHelper.addExpertisesIds(userId, source.getExpertiseIds());
	}

	private void saveParentExpertises(final Long userId, final List<Expertise> expertises,
			final Set<Expertise> parentExpertises, final Set<Integer> parentExpertiseIds) {
		Set<Integer> childExpertises = new HashSet<Integer>();

		if (!(expertises.size() == 0)) {
			for (Expertise exp : expertises) {
				if (exp.getParentId() == 0) {
					if (!parentExpertiseIds.contains(exp.getExpertiseId())) {
						parentExpertises.add(exp);
						parentExpertiseIds.add(exp.getExpertiseId());
					}
				} else {
					childExpertises.add(exp.getParentId());
				}
			}
			if (childExpertises.isEmpty()) {
				esHelper.updateParentExpertises(userId, parentExpertises);
				esHelper.updateParentExpertiseIds(userId, parentExpertiseIds);
			} else {
				saveParentExpertises(userId, new ArrayList<Expertise>(userDao.getParentExpertises(childExpertises)),
						parentExpertises, parentExpertiseIds);
			}
		}
	}

	private Collection<Source> getMentorExpertise(final Integer offset, final Integer limit) {
		return userDao.getMentorExpertises(offset, limit);
	}

	private Collection<Source> getUserDocument(final Integer offset, final Integer limit) {
		return userDao.getUserDocument(offset, limit);
	}

	private void saveSkipMentors() {
		Collection<Source> users = userDao.getSkippedMentors();
		/**
		 * save user skipped mentors At Es
		 */
		esHelper.saveSkipMentors(users);
	}

	private void saveUserRequestSentMentors() {
		Collection<Source> users = userDao.saveUserRequestSentMentors();
		/**
		 * save user request send mentors and my mentors At Es
		 */
		esHelper.saveUserRequestSentMentors(users);
	}

	private void saveUserAverageRating() {
		Collection<Source> users = userDao.getUserAverageRating();
		/**
		 * save user average rating At Es
		 */
		esHelper.saveUserAverageRating(users);
	}

	private void saveUserRating() {
		Collection<Source> users = userDao.getUserRating();
		/**
		 * save User rating At Es
		 */
		esHelper.saveUserRating(users);
	}

	private void saveUserBlockedUsers() {
		Collection<Source> users = userDao.getBlockedUsers();
		/**
		 * save User Blocked Users At Es
		 */
		esHelper.saveUserBlockedUsers(users);
	}

	private void saveUserDocumentAtEs(final Integer offset, final Integer limit) {
		Collection<Source> users = getUserDocument(offset, limit);
		if (users.size() == 0 || users == null) {
			return;
		}
		for (Source source : users) {
			esHelper.addDocument((Source) source);
		}
		saveUserDocumentAtEs(offset + limit + 1, limit);
	}

	@Override
	public void skipMentor(final Long userId, final Long mentorId) {
		if (userDao.skipMentor(userId, mentorId)) {
			/**
			 * skip mentor at ES
			 */
			esHelper.skipMentor(userId, mentorId);
		}
	}

	@Override
	public List<MentorSearch> searchMentorByString(final Long userId, final String mentorName) {
		return esHelper.searchMentorByStringV2(userId, mentorName);
	}

	@Override
	public void addContacts(final List<PhoneContact> contacts, final Long userId, final Short isDelta)
			throws  com.crater.mentorz.exception.BadRequestException, UnAuthorizedException {
		if (isDelta == 0) {
			/**
			 * 
			 * isDelta=1 - update contacts isDelta=0 - upload contacts
			 */
			userDao.deleteOldContacts(userId);
		}
		userDao.addContacts(contacts, userId);
	}

	@Override
	public void verifyUser(final InternationalNumber number) throws NotFoundException {
		if (userDao.isNoExists(number)) {
			userDao.verifyUserWithPhoneNumer(number);
			return;
		}
		throw new NotFoundException.Builder().entity(new ErrorEntity(2, "user not  exists")).build();
	}

	@Override
	public void updateContact(InternationalNumber updatecontact, Long userId) throws ConflitException {
		if (!userDao.isNoExists(updatecontact)) {
			userDao.updateContact(updatecontact, userId);
		} else {
			throw new ConflitException.Builder().entity(new ErrorEntity(2, "no already exists")).build();
		}
	}

	@Override
	public List<PhoneContact> getContactsToInvite(Long userId) {
		return userDao.getContactsToInvite(userId);
	}

	@Override
	public void updatePassword(UpdatePasswordV2 password) throws UnAuthorizedException, NotFoundException {
		String encrStr = EncryptDecrypt.getEncryptedString(password.getCode(), password.getPassword());
		if (userDao.isNoExists(password.getNumber())) { // true-if no exists
			if (encrStr.equals(password.getEncrytString())) {
				userDao.updatePassword(password);
			} else {
				LOGGER.error("================ ENCRYTED STRING DOES NOT MATCHED =====================" + encrStr);
				throw new UnAuthorizedException.Builder().entity(new ErrorEntity(2, "code not matched")).build();
			}
		} else {
			LOGGER.error("================ USER NOT EXISTS WITH THIS PHONE NO=====================");
			throw new NotFoundException.Builder().entity(new ErrorEntity(2, "  user not exists ")).build();
		}
	}

	@Override
	public boolean isNoExists(InternationalNumber number) {
		if (userDao.isNoExists(number)) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public UserData getUser(Long userId)
			throws IllegalArgumentException, SignatureException, IOException, CustomException {
		Optional<User> us = userDao.getUser(userId);    //contains lres-id and hres_id
		User user = null ;
		String url = "";
		if (us.isPresent()) {
			 user = us.get();
			
			if (user.getProfile().getLresId() != NULL && user.getProfile().getLresId() != "") {

				String object = user.getProfile().getLresId();
				
				if (object != null || object != "") {
					
					url = getSignedUrl("GET", object);
				}
			}
			LOGGER.info("================== user info=====================");
			
			return UserData.createWithProfileImage(user, url);
		}
		return null;

			
		
	
	}

}
