package com.crater.mentorz.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.crater.mentorz.dao.SubscribeUserDao;
import com.crater.mentorz.mail.EmailServiceImpl;
import com.crater.mentorz.models.user.SubscribeUser;
import com.crater.mentorz.service.SubscribeUserService;
import com.google.common.base.Optional;

@Service
public class SubscribeUserSeviceImpl implements SubscribeUserService {

	private final SubscribeUserDao subscribeUserDao;

	private final EmailServiceImpl emailService;

	@Autowired
	public SubscribeUserSeviceImpl(final SubscribeUserDao subscribeUserDao, final EmailServiceImpl emailService) {
		this.subscribeUserDao = subscribeUserDao;
		this.emailService = emailService;
	}

	@Override
	public Boolean subscribeUser(final SubscribeUser subscribeUser) {
		final Optional<SubscribeUser> exitingSubscriber = subscribeUserDao.findUserByEmail(subscribeUser.getEmail());
		if (exitingSubscriber.isPresent()) {
			// NEED TO DELETE SUBSCRIBER. Will do in future now we are sending
			// each time.

			new Thread("subscriptionConfirmationMail") {
				public void run() {
					emailService.sendSubscribeMail(subscribeUser);
				}
			}.start();

		} else {
			subscribeUserDao.saveSubscribedUser(subscribeUser);
			new Thread("subscriptionConfirmationMail") {
				public void run() {
					emailService.sendSubscribeMail(subscribeUser);
				}
			}.start();
		}
		return true;
	}

}
