/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.service.impl;

import java.time.Instant;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crater.mentorz.dao.PostDAO;
import com.crater.mentorz.dao.UserDAO;
import com.crater.mentorz.enums.NotificationMessageType;
import com.crater.mentorz.exception.ForbiddenException;
import com.crater.mentorz.http.HttpCallMediator;
import com.crater.mentorz.models.post.AbuseType;
import com.crater.mentorz.models.post.Comment;
import com.crater.mentorz.models.post.Post;
import com.crater.mentorz.models.push.Notification;
import com.crater.mentorz.service.PostService;
import com.crater.mentorz.utils.StringUtil;
import com.crater.mentorz.utils.async.PushSender;
import com.google.common.base.Optional;

@Service
public class PostServiceImpl implements PostService {

	// private static final Logger LOGGER =
	// LoggerFactory.getLogger(PostServiceImpl.class);

	private final PostDAO postDao;
	private final HttpCallMediator httpCallMediator;
	private final Executor taskExecutor;
	private final UserDAO userDAO;
	
	@Autowired
	public PostServiceImpl(final PostDAO postDao,final HttpCallMediator httpCallMediator,final Executor taskExecutor,final UserDAO userDAO) {
		this.postDao = postDao;
		this.httpCallMediator=httpCallMediator;
		this.taskExecutor=taskExecutor;
		this.userDAO=userDAO;
	}

	@Override
	@Transactional
	public Post addPost(final Long userId, final Post post) {
		if (post.getContent() != null) {
			return postDao.addPostWithContent(userId, post);
		} else {
			return postDao.addPostWithOutContent(userId, post);
		}
	}

	@Override
	@Transactional
	public Boolean like(final Long postId, final Long userId) throws ForbiddenException {
		try {
			postDao.like(postId, userId);
			final Optional<Long> to=postDao.getUserIdByPost(postId);
			if(!to.equals(userId)){
				
				final NotificationMessageType type=NotificationMessageType.LIKED_POST;
				final Map<String, Object> customData=new HashMap<String, Object>();
				customData.put("postId", postId);
				customData.put("userId", userId);
				customData.put("userName",userDAO.getNameByUserId(userId));
				customData.put("pushType",type );
				taskExecutor.execute(new PushSender(getNotificationInfo(userId, to.get(), type, customData), 
						httpCallMediator));
			}
		} catch (DuplicateKeyException e) {
			return false;
		}
		return true;
	}
	
	private Notification getNotificationInfo(final Long from,final Long to,final NotificationMessageType 
			type,final Map<String, Object> customData ){
		return new Notification(from, Arrays.asList(to), 
				Instant.now().toEpochMilli(),type ,StringUtil.textDecoderUTF8( 
						userDAO.getNameByUserId(from)),customData,!customData.isEmpty()?true:false);
	}

	@Override
	@Transactional
	public Boolean view(final Long postId, final Long userId) throws ForbiddenException {
		try {
			postDao.view(postId, userId);
		} catch (DuplicateKeyException e) {
			return false;
		}
		return true;
	}

	@Override
	@Transactional
	public Boolean share(final Long postId, final Long userId) throws ForbiddenException {
		try {
			postDao.share(postId, userId);
			final Optional<Long> to = postDao.getUserIdByPost(postId);
			if(to.isPresent()){
			if(!to.equals(userId)) {
				final NotificationMessageType type = NotificationMessageType.SHARE;
				final Map<String, Object> customData = new HashMap<String, Object>();
				customData.put("postId", postId);
				customData.put("userId", userId);
				customData.put("userName", userDAO.getNameByUserId(userId));
				customData.put("pushType", type);
				taskExecutor.execute(new PushSender(getNotificationInfo(userId,
						to.get(), type, customData), httpCallMediator));
			}
			}
		} catch (DuplicateKeyException e) {
			return false;
		}
		return true;
	}

	@Override
	@Transactional
	public Comment comment(final Long userId, final Long postId, final Comment comment) throws ForbiddenException {
		final Comment cmnt = postDao.addComment(userId, postId, comment);
		if (cmnt.getCommentId() != 0) {
			final Optional<Long> to = postDao.getUserIdByPost(postId);
			
			
			if(!to.equals(userId)){
				
				final NotificationMessageType type=NotificationMessageType.COMMENTED_ON_POST;
				final Map<String, Object> customData=new HashMap<String, Object>();
				customData.put("postId", postId);
				customData.put("userId", userId);
				customData.put("userName", userDAO.getNameByUserId(userId));
				customData.put("commentId", comment.getCommentId());
				customData.put("pushType",type );
				taskExecutor.execute(new PushSender(getNotificationInfo(userId, to.get(), type, customData),
						httpCallMediator));
			}
			return cmnt;
		}
		
		throw new ForbiddenException.Builder().build();
	}

	@Override
	public List<Post> getPostByUserId(final Long userId, final Integer pageNo) {
		return postDao.getPostByUserId(userId, pageNo).get();
	}

	@Override
	public List<Comment> getCommentByPostId(final Long postId, final Integer pageNo) {
		return postDao.getCommentByPostId(postId, pageNo).get();
	}

	@Override
	public List<Post> getBoard(final Integer pageNo, final Long userId) {
		return postDao.getBoard(pageNo, userId).get();
	}

	@Override
	@Transactional
	public void deleteComment(final Long userId, final Long postId, final Long commentId) throws ForbiddenException {
		if (postDao.deleteComment(userId, postId, commentId) <= 0) {
			throw new ForbiddenException.Builder().message("comment not exists or you are not authorised").build();
		}
	}

	@Override
	public List<Post> getFriendPostByUserId(final Long userId, final Long friendId, final Integer pageNo) {
		return postDao.getFriendPostByUserId(userId, friendId, pageNo).get();
	}

	@Override
	public void abusePost(final Long userId, final Long postId, final AbuseType abuseType) throws ForbiddenException {
		postDao.abusePost(userId, postId, abuseType);
	}

	@Override
	public Optional<List<Post>> searchPost(final Long userId, final List<Integer> interest, final Integer pageNo) {
		return postDao.searchPost(userId, interest, pageNo);
	}

	@Override
	public Optional<Comment> getCommentByCommentId(final Long commentId) {
		return postDao.getCommentByCommentId(commentId);
	}

	@Override
	public Optional<Post>  getPostByPostId(final Long postId) {
		return postDao.getPostByPostId(postId);
	}
}
