/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.service.impl;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crater.mentorz.dao.MentorDAOV3;
import com.crater.mentorz.dao.UserDAOV3;
import com.crater.mentorz.enums.NotificationMessageType;
import com.crater.mentorz.exception.ForbiddenException;
import com.crater.mentorz.http.HttpCallMediator;
import com.crater.mentorz.models.mentee.MenteeRequestTimeStamp;
import com.crater.mentorz.models.mentor.BeAMentor;
import com.crater.mentorz.models.mentor.Expertise;
import com.crater.mentorz.models.mentor.MentorFilter;
import com.crater.mentorz.models.push.Notification;
import com.crater.mentorz.models.user.Interest;
import com.crater.mentorz.models.user.User;
import com.crater.mentorz.service.MentorServiceV3;
import com.crater.mentorz.utils.StringUtil;
import com.crater.mentorz.utils.async.PushSender;
import com.google.common.base.Optional;


@Service
public class MentorServiceImplV3 implements MentorServiceV3 {

	private final MentorDAOV3 mentorDao;
	private final Executor taskExecutor;
	private final UserDAOV3 userDAO;
	private final HttpCallMediator httpCallMediator;
	private final Integer ADDED = 1;
	private final Integer NOT_ADDED = 0;
	private static final Logger LOGGER = LoggerFactory.getLogger(MentorServiceImplV3.class);

	@Autowired
	private ElasticSearchHelper esHelper;


	@Autowired
	public MentorServiceImplV3(final MentorDAOV3 mentorDao, final HttpCallMediator httpCallMediator,
			final Executor taskExecutor, final UserDAOV3 userDAO) {
		this.mentorDao = mentorDao;
		this.taskExecutor = taskExecutor;
		this.userDAO = userDAO;
		this.httpCallMediator = httpCallMediator;
	}

	@Override
	@Transactional
	public Boolean beAMentor(final Long userId, final BeAMentor mentor) {
		try {
			mentorDao.addMentor(userId, mentor);
			final List<Expertise> expertises = new ArrayList<>();
			List<Expertise> myExpertises = new ArrayList<>();

			for (Expertise expertise : mentor.getExperties()) {
				myExpertises = getMyExpertise(expertise, expertises);

			}
			mentorDao.deleteExpertises(userId);
			mentorDao.updateExperties(userId, mentor.getExperties(), ADDED);
			mentorDao.updateExperties(userId, myExpertises, NOT_ADDED);

			/**
			 * add mentor atEs
			 */

			
			esHelper.addMentorDocument(userId, mentor, myExpertises);
			
			taskExecutor.execute(new Runnable() {
			    @Override
				public void run() {
		     	saveParentExpertises(userId, mentor.getExperties(), new HashSet<Expertise>(), new HashSet<Integer>());
			    esHelper.removeSkippedMentor(userId);
			    }});

			return true;
		} catch (ForbiddenException e) {
			return false;
		}

	}

	private void saveParentExpertises(Long userId, List<Expertise> expertises, Set<Expertise> parentExpertises,
			Set<Integer> expertiseIds) {
		
		Set<Integer> childExpertises = new HashSet<Integer>();
		for (Expertise exp : expertises) {
			if (exp.getParentId() == 0) {
				
				parentExpertises.add(exp);
				expertiseIds.add(exp.getExpertiseId());
			} else {
				
				childExpertises.add(exp.getParentId());
			}
		}
		if (childExpertises.isEmpty()) {
			
			esHelper.updateParentExpertises(userId, parentExpertises);
			esHelper.updateParentExpertiseIds(userId, expertiseIds);
		} else {
			saveParentExpertises(userId, mentorDao.getParentExpertises(childExpertises), parentExpertises,
					expertiseIds);
		}

	}

	private List<Expertise> getMyExpertise(Expertise expertise, List<Expertise> expertises) {
		// expertises.add(expertise);
		if (expertise.getHasChildren()) {
			List<Expertise> childrens = mentorDao.getExpertiseByParentId(expertise.getExpertiseId());
			for (Expertise children : childrens) {
				expertises.add(children);
				getMyExpertise(children, expertises);
			}
		}
		return expertises;
	}

	@Override
	public Optional<List<User>> getMentor(final Long userId, final MentorFilter filter, final Integer offset,
			final Integer limit) {
		/*
		 * final Optional<List<User>> user = mentorDao.getMentor(userId, filter,
		 * pageNo); if (user.isPresent()) { return user.get(); } return null;
		 */

		// =============================================================================
		// Elastic search implementation

		Optional<Set<Integer>> myInterestParents = getParentInterests(filter.getInterests(), new HashSet<Integer>());   
		if (myInterestParents.isPresent()) {
			List<User> users = esHelper.getMentor(userId, filter, myInterestParents.get(), offset, limit);
			return Optional.fromNullable(users);
		} else {
			return Optional.absent();
		}
	}

   	private Optional<Set<Integer>> getParentInterests(List<Interest> interests, Set<Integer> parentExpertiseIds) {
		Set<Integer> childExpertises = new HashSet<Integer>();
		try{
		for (Interest intr : interests) {
			if (intr.getParentId() == 0) {
			  parentExpertiseIds.add(intr.getInterestId());
			} else {
				childExpertises.add(intr.getParentId());
			}
		}
		if (!childExpertises.isEmpty()) {
			getParentInterests(mentorDao.getParentInterestIds(childExpertises), parentExpertiseIds);
		}
		}
		catch(Exception e){
			LOGGER.error("Exception -parent id is not present ===="+e);	
		}
      return Optional.fromNullable(parentExpertiseIds);
     }

	@Override
	public List<Expertise> getExpertise(final Long userId) {
		List<Expertise> expertises = mentorDao.getMyExpertise(userId);
		return expertises;
	}

	@Override
	public Optional<BeAMentor> getPublicBio(final Long userId) {
		return mentorDao.getPublicBio(userId);
	}

	@Override
	public Optional<Collection<MenteeRequestTimeStamp>> getMenteeRequests(final Long userId, final Integer pageNo) {
		return mentorDao.getMenteeRequests(userId, pageNo);
	}

	@Override
	@Transactional
	public Boolean addMentee(final Long userId, final Long menteeId) throws ForbiddenException {

		if (!(mentorDao.isValidMentee(userId, menteeId))) {
			return false;
		}

		try {
			if (userDAO.isBlockedUserOrBlockedBy(userId, menteeId)) {

			}
			mentorDao.addMentee(userId, menteeId);

			/**
			 * Update mentorCount and MenteeCount At Es
			 */

			taskExecutor.execute(new Runnable() {
			@Override
			public void run() {
			esHelper.updateMentorCount(menteeId);
			esHelper.updateMenteeCount(userId);
			    }
			});

			final NotificationMessageType type = NotificationMessageType.ACCEPT_REQUEST;
			final Map<String, Object> customData = new HashMap<String, Object>();
			customData.put("userId", userId);
			customData.put("pushType", type);
			customData.put("userName", userDAO.getNameByUserId(userId));
			taskExecutor
					.execute(new PushSender(getNotificationInfo(userId, menteeId, type, customData), httpCallMediator));
		} catch (DuplicateKeyException e) {
			return false;
		}
		return true;
	}

	private Notification getNotificationInfo(final Long from, final Long to, final NotificationMessageType type,
			final Map<String, Object> customData) {
		return new Notification(from, Arrays.asList(to), Instant.now().toEpochMilli(), type,
				StringUtil.textDecoderUTF8(userDAO.getNameByUserId(from)), customData,!customData.isEmpty()?true:false);
	}

	@Override
	@Transactional
	public Map<String, Boolean> getRequestStatus(final Long userId, final Long mentorId) {
		Map<String, Boolean> status = new HashMap<String, Boolean>();
		status.put("isAlreadySent", mentorDao.isRequestAlreadySent(userId, mentorId));
		status.put("isMyMentor", mentorDao.isMyMentor(userId, mentorId));
		return status;
	}

	@Override
	public Boolean deleteRequest(final Long userId, final Long menteeId) {
		
		Integer id=mentorDao.getRequestSentMappingId(menteeId,userId);
		mentorDao.deleteFromUsersInterestsWithMentors(id);
		if(mentorDao.deleteRequest(userId, menteeId) > 0 ){
			/**
			 * Delete request at Es
			 */
			esHelper.deleteAndCancelRequest(menteeId, userId);
			return true;
		}
		return false;
		
	}

	@Override
	public Optional<Collection<MenteeRequestTimeStamp>> getPendingRequests(final Long userId, final Integer pageNo) {
		return mentorDao.getPendingRequests(userId, pageNo);
	}

	@Override
	public Boolean cancelPendingRequest(final Long userId, final Long mentorId) {
		/**
		 * Cancel request at Es
		 */
		
		/*esHelper.deleteAndCancelRequest(userId, mentorId);
		return mentorDao.cancelPendingRequest(userId, mentorId) > 0 ? true : false;*/
		
		Integer id=mentorDao.getRequestSentMappingId(userId,mentorId);
		mentorDao.deleteFromUsersInterestsWithMentors(id);
		if(mentorDao.cancelPendingRequest(userId, mentorId) > 0 ){
			esHelper.deleteAndCancelRequest(userId, mentorId);
			return true;
			
		}
		return false;
	}

	@Override
	public List<Expertise> getExpertiseById(final Long mentorId, final Integer expertiseId) {
		return mentorDao.getExpertiseById(mentorId, expertiseId);
	}

	@Override
	public List<Expertise> getExpertiseByParentId(final Integer parentId, final Long userId) {
		final List<Expertise> childs = mentorDao.getExpertiseByParentId(parentId);
		final List<Expertise> myExpertise = mentorDao.getMyExpertise(userId);
		final List<Expertise> expertises = new ArrayList<Expertise>();
		for (Expertise expertise : childs) {
			final Expertise expertise1 = changeIsMyExpertise(myExpertise, expertise);
			if (expertise1 != null) {
				expertises.add(expertise1);
			} else {
				expertises.add(expertise);
			}
		}
		return expertises;
	}

	private Expertise changeIsMyExpertise(List<Expertise> childs, Expertise expertise) {
		for (Expertise exts : childs) {
			if (exts.getExpertiseId() == expertise.getExpertiseId()) {
				return Expertise.createExpertiseWithMyInterest(exts, true);
			}
		}
		return null;
	}

	@Override
	public List<Expertise> getExpertiseList(final Long mentorId) {
		return mentorDao.getExpertiseList(mentorId);
	}

	@Override
	public List<Expertise> getExpertiseListByParentId(Integer parentId, Long mentorId) {
		final List<Expertise> childs = mentorDao.getExpertiseListByParentId(parentId, mentorId);
		final List<Expertise> myExpertise = mentorDao.getMyExpertise(mentorId);
		final List<Expertise> expertises = new ArrayList<Expertise>();
		for (Expertise expertise : childs) {
			final Expertise expertise1 = changeIsMyExpertise(myExpertise, expertise);
			if (expertise1 != null) {
				expertises.add(expertise1);
			} else {
				expertises.add(expertise);
			}
		}
		return expertises;
	}

}
