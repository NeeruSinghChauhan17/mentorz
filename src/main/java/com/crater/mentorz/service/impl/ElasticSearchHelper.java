package com.crater.mentorz.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.crater.mentorz.elasticsearch.ElasticSearchHttpClient;
import com.crater.mentorz.elasticsearch.model.req.Bool;
import com.crater.mentorz.elasticsearch.model.req.ChargePrice;
import com.crater.mentorz.elasticsearch.model.req.ElasticSearch;
import com.crater.mentorz.elasticsearch.model.req.Experience;
import com.crater.mentorz.elasticsearch.model.req.Filter;
import com.crater.mentorz.elasticsearch.model.req.Must;
import com.crater.mentorz.elasticsearch.model.req.MustNot;
import com.crater.mentorz.elasticsearch.model.req.Params;
import com.crater.mentorz.elasticsearch.model.req.Query;
import com.crater.mentorz.elasticsearch.model.req.QueryString;
import com.crater.mentorz.elasticsearch.model.req.Range;
import com.crater.mentorz.elasticsearch.model.req.Rating;
import com.crater.mentorz.elasticsearch.model.req.RatingRange;
import com.crater.mentorz.elasticsearch.model.req.Script;
import com.crater.mentorz.elasticsearch.model.req.Source;
import com.crater.mentorz.elasticsearch.model.req.Term;
import com.crater.mentorz.elasticsearch.model.req.Terms;
import com.crater.mentorz.models.mentor.BeAMentor;
import com.crater.mentorz.models.mentor.Expertise;
import com.crater.mentorz.models.mentor.MentorFilter;
import com.crater.mentorz.models.mentor.MentorSearch;
import com.crater.mentorz.models.user.Profile;
import com.crater.mentorz.models.user.ProfileImage;
import com.crater.mentorz.models.user.User;

@Component
public class ElasticSearchHelper {

	private static final Logger LOG = LoggerFactory.getLogger(ElasticSearchHelper.class);

	@Autowired
	private ElasticSearchHttpClient esHttpClient;

	private static final String UPDATE_USER_PROFILE_LRES_IMAGE = "ctx._source.lres_id=lres_id";
	private static final String UPDATE_USER_PROFILE_HRES_IMAGE = "ctx._source.hres_id=hres_id";
	private static final Integer ACTIVE_STATUS = 0;
	private static final String ADD_MENTOR_CHARGE_PRICE = "ctx._source.charge_price=charge_price";
	private static final String ADD_MENTOR_ORG = "ctx._source.org=org";
	private static final String ADD_MENTOR_LOCATION = "ctx._source.located_in=located_in";
	private static final String ADD_MENTOR_POSITION = "ctx._source.position=position";
	private static final String ADD_MENTOR_EXPERIENCE = "ctx._source.exp=exp";
	private static final String ADD_MENTOR_EXPERTISES = "ctx._source.expertises=expertises";
	private static final String UPDATE_MENTOR_COUNT = "ctx._source.mentors+=1";
	private static final String UPDATE_MENTEE_COUNT = "ctx._source.mentees+=1";
	private static final String INCREASE_FOLLOW_COUNT = "ctx._source.followers+=1";
	private static final String INCREASE_FOLLOWING_COUNT = "ctx._source.following+=1";
	private static final String DECREASE_FOLLOW_COUNT = "ctx._source.followers-=1";
	private static final String DECREASE_FOLLOWING_COUNT = "ctx._source.following-=1";
	private static final String ADD_BLOCKED_USER = "ctx._source.blocked_users +=blocked_users";
	private static final String UNBLOCK_USER = "ctx._source.blocked_users-=blocked_users";
	private static final String UPDATE_USER_INFO = "ctx._source.basic_info=basic_info";
	private static final String UPDATE_USER_NAME = "ctx._source.name=name";
	private static final String UPDATE_USER_VIDEO_BIOLRES = "ctx._source.video_bio_lres=video_bio_lres";
	private static final String UPDATE_USER_VIDEO_BIOHRES = "ctx._source.video_bio_hres=video_bio_hres";
	private static final String UPDATE_USER_RATING = "ctx._source.ratings+=ratings";
	private static final String UPDATE_USER_AVERAGE_RATING = "ctx._source.avg_rating=avg_rating";
	private static final String ADD_USER_RATING = "ctx._source.ratings +=ratings";
	private static final String DELETE_USER_OLD_RATING = "ctx._source.ratings.remove(ratings)";
	private static final String ADD_USER_AVERAGE_RATING = "ctx._source.avg_rating=avg_rating";
	private static final String ADD_MENTOR_EXPERTISE_IDS = "ctx._source.expertise_ids +=expertise_ids";
	private static final String UPDATE_USER_FACEBOOK_URL = "ctx._source.facebook_url =facebook_url";
	private static final String UPDATE_USER_LINKEDIN_URL = "ctx._source.linkedin_url =linkedin_url";
	private static final String ADD_SKIP_MENTOR = "ctx._source.skipped_mentors +=skipped_mentors";
	private static final String REMOVE_SKIP_MENTOR = "ctx._source.skipped_mentors -=skipped_mentors";
	private static final String SAVE_REQUEST_SENT_MENTORS = "ctx._source.request_sent_mentors +=request_sent_mentors";
	private static final String CANCEL_DELETE_REQUEST = "ctx._source.request_sent_mentors -=request_sent_mentors";
	private static final String ADD_MENTOR_PARENT_EXPERTISES = "ctx._source.parent_expertises +=parent_expertises";
	private static final String ADD_MENTOR_PARENT_EXPERTISE_IDS="ctx._source.parent_expertise_ids +=parent_expertise_ids";

	public List<User> getMentor(final Long userId,final  MentorFilter filter,final Set<Integer> myInterests, final Integer offset,final  Integer limit) {
       List<User> source = null;
		try {
			List<String> list = new ArrayList<>();
			for (int i = 0; i < filter.getInterests().size(); i++) {
				list.add(Integer.toString(filter.getInterests().get(i).getInterestId()));
			}
       	if (filter.getMaxExp() != 0 && filter.getMinExp() >= 0 && filter.getMinRating() >= 0
					&& filter.getMaxRating() != 0 && filter.getMinPrice() != 0 && filter.getMaxPrice() != 0) {
                source = getMentorWithRatingAndExpAndPriceParamMap(filter, list,myInterests, userId, offset, limit);
           } else if (filter.getMaxExp() != 0 && filter.getMinExp() >= 0 && filter.getMinRating() >= 0
					&& filter.getMaxRating() != 0) {
                source = getMentorWithRatingAndExpParamMap(filter, list,myInterests, userId, offset, limit);
           } else if (filter.getMinRating() >= 0 && filter.getMaxRating() != 0 && filter.getMinPrice() >= 0
					&& filter.getMaxPrice() != 0) {
                source = getMentorWithRatingAndPriceParamMap(filter, list,myInterests, userId, offset, limit);
           } else if (filter.getMaxExp() != 0 && filter.getMinExp() >= 0 && filter.getMinPrice() >= 0
					&& filter.getMaxPrice() != 0) {
        	    source = getMentorWithExpAndPriceParamMap(filter, list,myInterests, userId, offset, limit);
            } else if (filter.getMaxExp() != 0 && filter.getMinExp() >= 0) {
                source = getMentorWithExpParamMap(filter, list, userId,myInterests, offset, limit);
            } else if (filter.getMinRating() >= 0 && filter.getMaxRating() != 0) {

				source = getMentorWithRatingParamMap(filter, list,myInterests, userId, offset, limit);
			} else if (filter.getMinPrice() >= 0 && filter.getMaxPrice() != 0) {
                source = getMentorWithPriceParamMap(filter, list, myInterests,userId, offset, limit);
            } else {
                 source = getMentorWithInterestParamMap(list, myInterests,userId, offset, limit);
			}
		} catch (Exception e) {
			LOG.error("ElasticSearch : Error While Search [Mentor] ", e);
          }
		return source;
	}

	private List<User> getMentorWithInterestParamMap(final List<String> list,final Set<Integer> myInterests,final  Long userId,final  Integer pageNo,
			final Integer limit) {
	ElasticSearch es=getMentorWithInterestParamMapQuery( list, userId);
        List<Source> sources = esHttpClient.getResponse(es, pageNo, limit, "Search [Mentor , Intersest]");
        List<User> users=getUsersFromSources( myInterests,sources);
		return users;
	}

    private ElasticSearch getMentorWithInterestParamMapQuery(final List<String> list,final Long userId) {
	    Set<Long> skippedMentors=null;
		Set<Long> myblockedUsers=null;
		Set<Long> requestSentMentors=null;
		
		final List<Must> must = new ArrayList<>();
		final List<MustNot> mustnot = new ArrayList<>();
		
		Optional<Source> source=esHttpClient.getUserDocument(userId);
		if(source.isPresent()){
		     skippedMentors = source.get().getSkippedMentors();
		     myblockedUsers = source.get().getBlockedIds();
		     requestSentMentors = source.get().getRequestSentMentors();
		}
		must.add(Must.createMustWithTerm(Term.withStatus(ACTIVE_STATUS)));
		Filter filter = Filter.withTerms(Terms.withExpertises(list));
		if (myblockedUsers != null) {
			for (Long id : myblockedUsers) {
				mustnot.add(MustNot.withTerm(Term.withUserId(id)));
			}
		}
		if (skippedMentors != null) {
			for (Long id : skippedMentors) {
				mustnot.add(MustNot.withTerm(Term.withUserId(id)));
			}
		}

		if (requestSentMentors != null) {
			for (Long id : requestSentMentors) {
				mustnot.add(MustNot.withTerm(Term.withUserId(id)));
			}
		}
		List<Source> Users=usersWhoBlockedMe(userId);
		if(Users!=null){
			for (Source user : Users) {
				mustnot.add(MustNot.withTerm(Term.withUserId(user.getUserId())));
			}	
		}
		mustnot.add(MustNot.withTerm(Term.withUserId(userId)));
        ElasticSearch es = ElasticSearch
				.withQuery(Query.withBool(Bool.withMustAndMustNotAndFilter(must, mustnot, filter)));
		return es;
	}

	private List<User> getMentorWithPriceParamMap(final MentorFilter filter,final  List<String> list, 
			final Set<Integer> myInterests,	final Long userId,final Integer pageNo,
			Integer limit) {
		ElasticSearch es=getMentorWithPriceParamMapQuery(filter, list, userId);
		List<Source> sources = esHttpClient.getResponse(es, pageNo, limit, "Search [Mentor , Price]");
		List<User> users=getUsersFromSources( myInterests,sources);
	    return users;
	}
	
	
	private ElasticSearch getMentorWithPriceParamMapQuery(final MentorFilter filter, final List<String> list,
			final Long userId) {
		Set<Long> skippedMentors=null;
		Set<Long> blockedUsers=null;
		Set<Long> requestSentMentors=null;
		List<Source> Users=usersWhoBlockedMe(userId);
		final List<Must> must = new ArrayList<>();
		final List<MustNot> mustnot = new ArrayList<>();
			
		Optional<Source> source=esHttpClient.getUserDocument(userId);
		if(source.isPresent()){
		     skippedMentors = source.get().getSkippedMentors();
		     blockedUsers = source.get().getBlockedIds();
		     requestSentMentors = source.get().getRequestSentMentors();
		}
	    must.add(Must.createMustWithTerm(Term.withStatus(ACTIVE_STATUS)));
		must.add(Must.createMustWithRange(Range.withChargePrice(ChargePrice.withMaxAndMin(filter.getMaxPrice(),
				filter.getMinPrice()))));
	
		if (blockedUsers != null) {
			for (Long id : blockedUsers) {
				mustnot.add(MustNot.withTerm(Term.withUserId(id)));
			}
		}
		if (skippedMentors != null) {
			for (Long id : skippedMentors) {
				mustnot.add(MustNot.withTerm(Term.withUserId(id)));
			}
		}
		
		
		if(Users!=null){
			for (Source user : Users) {
				mustnot.add(MustNot.withTerm(Term.withUserId(user.getUserId())));
			}	
		}
		
	  if (requestSentMentors != null) {
			for (Long id : requestSentMentors) {
				mustnot.add(MustNot.withTerm(Term.withUserId(id)));
			}
		}
		mustnot.add(MustNot.withTerm(Term.withUserId(userId)));
		Filter expFilter = Filter.withTerms(Terms.withExpertises(list));
		ElasticSearch es = ElasticSearch
				.withQuery(Query.withBool(Bool.withMustAndMustNotAndFilter(must, mustnot, expFilter)));
	 return es;
	}

	private List<Source> usersWhoBlockedMe(Long userId) {
		final List<Must> must = new ArrayList<>();
		must.add(Must.createMustWithTerm(Term.withBlockedId(userId)));
		ElasticSearch es = ElasticSearch
				.withQuery(Query.withBool(Bool.withMust(must)));
		return esHttpClient.getResponse1(es, "Search [Users , Who_blocked_me]");
	}

	private List<User> getMentorWithRatingParamMap(final MentorFilter filter,final  List<String> list,
			final Set<Integer> myInterests,	final Long userId,final  Integer pageNo,final Integer limit) {
		ElasticSearch es=getMentorWithRatingParamMapQuery(filter, list, userId);
		List<Source> sources = esHttpClient.getResponse(es, pageNo, limit, "Search [Mentor , Rating]");
		List<User> users=getUsersFromSources( myInterests,sources);
		return users;
	}
	
	private ElasticSearch getMentorWithRatingParamMapQuery(final MentorFilter filter,final  List<String> list,
			final Long userId) {
		Set<Long> skippedMentors=null;
		Set<Long> blockedUsers=null;
		Set<Long> requestSentMentors=null;
		final List<Must> must = new ArrayList<>();
		final List<MustNot> mustnot = new ArrayList<>();
		
		Optional<Source> source=esHttpClient.getUserDocument(userId);
		if(source.isPresent()){
		     skippedMentors = source.get().getSkippedMentors();
		     blockedUsers = source.get().getBlockedIds();
		     requestSentMentors = source.get().getRequestSentMentors();
		}
		must.add(Must.createMustWithTerm(Term.withStatus(ACTIVE_STATUS)));
		must.add(Must.createMustWithRange(
				Range.withAvgRating(RatingRange.withMaxAndMin(filter.getMaxRating(), filter.getMinRating()))));
		if (blockedUsers != null) {
			for (Long id : blockedUsers) {
				mustnot.add(MustNot.withTerm(Term.withUserId(id)));
			}
		}
      if (skippedMentors != null) {
			for (Long id : skippedMentors) {
				mustnot.add(MustNot.withTerm(Term.withUserId(id)));
			}
		}

		if (requestSentMentors != null) {
			for (Long id : requestSentMentors) {
				mustnot.add(MustNot.withTerm(Term.withUserId(id)));
			}
		}
		
		List<Source> Users=usersWhoBlockedMe(userId);
		if(Users!=null){
			for (Source user : Users) {
				mustnot.add(MustNot.withTerm(Term.withUserId(user.getUserId())));
			}	
		}
		mustnot.add(MustNot.withTerm(Term.withUserId(userId)));
		Filter expFilter = Filter.withTerms(Terms.withExpertises(list));
		ElasticSearch es = ElasticSearch
				.withQuery(Query.withBool(Bool.withMustAndMustNotAndFilter(must, mustnot, expFilter)));
	   return es;
	}

     private List<User> getMentorWithExpParamMap(final MentorFilter filter,final  List<String> list, final Long userId,
    		 final Set<Integer> myInterests,	final Integer pageNo,final Integer limit) {
	    ElasticSearch es=getMentorWithExpParamMapQuery(filter, list, userId);
		List<Source> sources = esHttpClient.getResponse(es, pageNo, limit, "Search [Mentor , Experience]");
		List<User> users=getUsersFromSources(myInterests,sources);
		return users;
	}
	
	
	private ElasticSearch getMentorWithExpParamMapQuery(final MentorFilter filter,final  List<String> list, 
			final Long userId) {
		Set<Long> skippedMentors=null;
		Set<Long> blockedUsers=null;
		Set<Long> requestSentMentors=null;
		final List<Must> must = new ArrayList<>();
		final List<MustNot> mustnot = new ArrayList<>();
		
		Optional<Source> source=esHttpClient.getUserDocument(userId);
		if(source.isPresent()){
		     skippedMentors = source.get().getSkippedMentors();
		     blockedUsers = source.get().getBlockedIds();
		     requestSentMentors = source.get().getRequestSentMentors();
		}
		must.add(Must.createMustWithTerm(Term.withStatus(ACTIVE_STATUS)));
		must.add(Must.createMustWithRange(
				Range.withExperience(Experience.withMaxAndMin(filter.getMaxExp(), filter.getMinExp()))));
		if (blockedUsers != null) {
			for (Long id : blockedUsers) {
				mustnot.add(MustNot.withTerm(Term.withUserId(id)));
			}
		}
        if (skippedMentors != null) {
			for (Long id : skippedMentors) {
				mustnot.add(MustNot.withTerm(Term.withUserId(id)));
			}
		}
       if (requestSentMentors != null) {
			for (Long id : requestSentMentors) {
				mustnot.add(MustNot.withTerm(Term.withUserId(id)));
			}
		}
   	   List<Source> Users=usersWhoBlockedMe(userId);
	    if(Users!=null){
		   for (Source user : Users) {
			mustnot.add(MustNot.withTerm(Term.withUserId(user.getUserId())));
		}	
	}
        mustnot.add(MustNot.withTerm(Term.withUserId(userId)));
		Filter expFilter = Filter.withTerms(Terms.withExpertises(list));
		ElasticSearch es = ElasticSearch
				.withQuery(Query.withBool(Bool.withMustAndMustNotAndFilter(must, mustnot, expFilter)));
	 return es;
	}
	


	private List<User> getMentorWithExpAndPriceParamMap(final MentorFilter filter,final  List<String> list,
			final Set<Integer> myInterests,	final Long userId,final Integer pageNo, final Integer limit) {
		ElasticSearch es=getMentorWithExpAndPriceParamMapQuery(filter, list, userId);
		List<Source> sources = esHttpClient.getResponse(es, pageNo, limit, "Search [Mentor  , Experience And Price]");
		List<User> users=getUsersFromSources( myInterests,sources);
		return users;
	}

	private ElasticSearch getMentorWithExpAndPriceParamMapQuery(final MentorFilter filter,final  List<String> list,
			final Long userId) {
		Set<Long> skippedMentors=null;
		Set<Long> blockedUsers=null;
		Set<Long> requestSentMentors=null;
		final List<Must> must = new ArrayList<>();
		final List<MustNot> mustnot = new ArrayList<>();
		
		Optional<Source> source=esHttpClient.getUserDocument(userId);
		if(source.isPresent()){
		     skippedMentors = source.get().getSkippedMentors();
		     blockedUsers = source.get().getBlockedIds();
		     requestSentMentors = source.get().getRequestSentMentors();
		}
		must.add(Must.createMustWithTerm(Term.withStatus(ACTIVE_STATUS)));
		must.add(Must.createMustWithRange(
				Range.withExperience(Experience.withMaxAndMin(filter.getMaxExp(), filter.getMinExp()))));
		must.add(Must.createMustWithRange(
				Range.withChargePrice(ChargePrice.withMaxAndMin(filter.getMaxPrice(), filter.getMinPrice()))));
		
		if (blockedUsers != null) {
			for (Long id : blockedUsers) {
				mustnot.add(MustNot.withTerm(Term.withUserId(id)));
			}
		}
		if (skippedMentors != null) {
			for (Long id : skippedMentors) {
				mustnot.add(MustNot.withTerm(Term.withUserId(id)));
			}
		}

		if (requestSentMentors != null) {
			for (Long id : requestSentMentors) {
				mustnot.add(MustNot.withTerm(Term.withUserId(id)));
			}
		}
		List<Source> Users=usersWhoBlockedMe(userId);
		if(Users!=null){
			for (Source user : Users) {
				mustnot.add(MustNot.withTerm(Term.withUserId(user.getUserId())));
			}	
		}
		mustnot.add(MustNot.withTerm(Term.withUserId(userId)));
		Filter expFilter = Filter.withTerms(Terms.withExpertises(list));
		ElasticSearch es = ElasticSearch
				.withQuery(Query.withBool(Bool.withMustAndMustNotAndFilter(must, mustnot, expFilter)));
		return es;
	}

	private List<User> getMentorWithRatingAndPriceParamMap(final MentorFilter filter,final  List<String> list,
			final Set<Integer> myInterests,	final Long userId,final Integer pageNo,final  Integer limit) {
	    ElasticSearch es=getMentorWithRatingAndPriceParamMapQuery(filter, list, userId);
		List<Source> sources = esHttpClient.getResponse(es, pageNo, limit, "Search [Mentor  ,  Rating And Price]");
		List<User> users=getUsersFromSources(myInterests, sources);
		return users;
	}
	

	private ElasticSearch getMentorWithRatingAndPriceParamMapQuery(final MentorFilter filter,final  List<String> list,
		final Long userId) {
	    Set<Long> skippedMentors=null;
		Set<Long> blockedUsers=null;
		Set<Long> requestSentMentors=null;
		final List<Must> must = new ArrayList<>();
		final List<MustNot> mustnot = new ArrayList<>();
		
		Optional<Source> source=esHttpClient.getUserDocument(userId);
		if(source.isPresent()){
		     skippedMentors = source.get().getSkippedMentors();
		     blockedUsers = source.get().getBlockedIds();
		     requestSentMentors = source.get().getRequestSentMentors();
		}
		must.add(Must.createMustWithTerm(Term.withStatus(ACTIVE_STATUS)));
		must.add(Must.createMustWithRange(
				Range.withAvgRating(RatingRange.withMaxAndMin(filter.getMaxRating(), filter.getMinRating()))));
		must.add(Must.createMustWithRange(
				Range.withChargePrice(ChargePrice.withMaxAndMin(filter.getMaxPrice(), filter.getMinPrice()))));
	  if (blockedUsers != null) {
			for (Long id : blockedUsers) {
				mustnot.add(MustNot.withTerm(Term.withUserId(id)));
			}
		}
		if (skippedMentors != null) {
			for (Long id : skippedMentors) {
				mustnot.add(MustNot.withTerm(Term.withUserId(id)));
			}
		}

		if (requestSentMentors != null) {
			for (Long id : requestSentMentors) {
				mustnot.add(MustNot.withTerm(Term.withUserId(id)));
			}
		}
		List<Source> Users=usersWhoBlockedMe(userId);
		if(Users!=null){
			for (Source user : Users) {
				mustnot.add(MustNot.withTerm(Term.withUserId(user.getUserId())));
			}	
		}
		mustnot.add(MustNot.withTerm(Term.withUserId(userId)));
		Filter expFilter = Filter.withTerms(Terms.withExpertises(list));
		ElasticSearch es = ElasticSearch
				.withQuery(Query.withBool(Bool.withMustAndMustNotAndFilter(must, mustnot, expFilter)));
	  return es;
	}



	private List<User> getMentorWithRatingAndExpParamMap(final MentorFilter filter,final  List<String> list,
			final Set<Integer> myInterests,	final  Long userId,
			Integer pageNo, Integer limit) {
		ElasticSearch es=getMentorWithRatingAndExpParamMapQuery(filter, list, userId);
		List<Source> sources = esHttpClient.getResponse(es, pageNo, limit, "Search [Mentor  ,  Rating And Experience]");
		List<User> users=getUsersFromSources(myInterests,sources);
		return users;
	}


	
	private ElasticSearch getMentorWithRatingAndExpParamMapQuery(final MentorFilter filter,final  List<String> list, 
			final Long userId) {
        Set<Long> skippedMentors=null;
		Set<Long> blockedUsers=null;
		Set<Long> requestSentMentors=null;
		final List<Must> must = new ArrayList<>();
		final List<MustNot> mustnot = new ArrayList<>();
		
		Optional<Source> source=esHttpClient.getUserDocument(userId);
		if(source.isPresent()){
		     skippedMentors = source.get().getSkippedMentors();
		     blockedUsers = source.get().getBlockedIds();
		     requestSentMentors = source.get().getRequestSentMentors();
		}
		must.add(Must.createMustWithTerm(Term.withStatus(ACTIVE_STATUS)));
		must.add(Must.createMustWithRange(
				Range.withAvgRating(RatingRange.withMaxAndMin(filter.getMaxRating(), filter.getMinRating()))));
		must.add(Must.createMustWithRange(
				Range.withExperience(Experience.withMaxAndMin(filter.getMaxExp(), filter.getMinExp()))));
		if (blockedUsers != null) {
			for (Long id : blockedUsers) {
				mustnot.add(MustNot.withTerm(Term.withUserId(id)));
			}
		}
		if (skippedMentors != null) {
			for (Long id : skippedMentors) {
				mustnot.add(MustNot.withTerm(Term.withUserId(id)));
			}
		}
		if (requestSentMentors != null) {
			for (Long id : requestSentMentors) {
				mustnot.add(MustNot.withTerm(Term.withUserId(id)));
			}
		}
		List<Source> Users=usersWhoBlockedMe(userId);
		if(Users!=null){
			for (Source user : Users) {
				mustnot.add(MustNot.withTerm(Term.withUserId(user.getUserId())));
			}	
		}
		mustnot.add(MustNot.withTerm(Term.withUserId(userId)));
		Filter expFilter = Filter.withTerms(Terms.withExpertises(list));
		ElasticSearch es = ElasticSearch
				.withQuery(Query.withBool(Bool.withMustAndMustNotAndFilter(must, mustnot, expFilter)));
       return es;
	}

	
	
	private List<User> getMentorWithRatingAndExpAndPriceParamMap(final MentorFilter filter, final List<String> list,
			final Set<Integer> myInterests,	final Long userId,final Integer pageNo,final  Integer limit) {
	    ElasticSearch es=getMentorWithRatingAndExpAndPriceParamMapQuery(filter, list, userId);
		List<Source> sources = esHttpClient.getResponse(es, pageNo, limit,
				"Search [Mentor  ,  Rating And Experience And Price]");
		List<User> users=getUsersFromSources(myInterests, sources);
		return users;
	}
	
	private ElasticSearch	getMentorWithRatingAndExpAndPriceParamMapQuery(MentorFilter filter, List<String> list, Long userId){
		
		Set<Long> skippedMentors=null;
		Set<Long> blockedUsers=null;
		Set<Long> requestSentMentors=null;
		final List<Must> must = new ArrayList<>();
		final List<MustNot> mustnot = new ArrayList<>();
		
		Optional<Source> source=esHttpClient.getUserDocument(userId);
		if(source.isPresent()){
		     skippedMentors = source.get().getSkippedMentors();
		     blockedUsers = source.get().getBlockedIds();
		     requestSentMentors = source.get().getRequestSentMentors();
		}
		must.add(Must.createMustWithTerm(Term.withStatus(ACTIVE_STATUS)));
		must.add(Must.createMustWithRange(
				Range.withAvgRating(RatingRange.withMaxAndMin(filter.getMaxRating(), filter.getMinRating()))));
		must.add(Must.createMustWithRange(
				Range.withExperience(Experience.withMaxAndMin(filter.getMaxExp(), filter.getMinExp()))));
		must.add(Must.createMustWithRange(
				Range.withChargePrice(ChargePrice.withMaxAndMin(filter.getMaxPrice(), filter.getMinPrice()))));
		if (blockedUsers != null) {
			for (Long id : blockedUsers) {
				mustnot.add(MustNot.withTerm(Term.withUserId(id)));
			}
		}
		if (skippedMentors != null) {
			for (Long id : skippedMentors) {
				mustnot.add(MustNot.withTerm(Term.withUserId(id)));
			}
		}
		if (requestSentMentors != null) {
			for (Long id : requestSentMentors) {
				mustnot.add(MustNot.withTerm(Term.withUserId(id)));
			}
		}
		List<Source> Users=usersWhoBlockedMe(userId);
		if(Users!=null){
			for (Source user : Users) {
				mustnot.add(MustNot.withTerm(Term.withUserId(user.getUserId())));
			}	
		}
		mustnot.add(MustNot.withTerm(Term.withUserId(userId)));
		Filter expFilter = Filter.withTerms(Terms.withExpertises(list));
		ElasticSearch es = ElasticSearch
				.withQuery(Query.withBool(Bool.withMustAndMustNotAndFilter(must, mustnot, expFilter)));	
		return es;
		}

	private List<User>  getUsersFromSources(final Set<Integer> myInterests,final List<Source> sources){
		List<User> users = new ArrayList<>();
		
		for (Source sour : sources) {
			List<Expertise> isMyexpertises=isMyExpertise(myInterests,sour.getParentExpertises());
			User user = new User(sour.getUserId(), sour.getEmailId(), null, null,null,
					new Profile(null, null, sour.getLresId(), sour.getHresId(), sour.getFollowers(),
							sour.getFollowing(), sour.getMentors(), sour.getMentees(), sour.getBasicInfo(),
							sour.getBirthDate(), sour.getChargePrice(), sour.getName(), 0, sour.getExp(),
							sour.getVideoBioLres(), sour.getVideoBioHres(),
							new com.crater.mentorz.models.Rating(sour.getAverageRating().doubleValue(), null, null), 0,
							sour.getFacebookUrl(), sour.getLinkedInURl()),
					null, isMyexpertises, null, null, null,null,null);
			users.add(user);
		}
		return users;
	}
	
	private List<Expertise> isMyExpertise(final Set<Integer> myExpertises,final  List<Expertise> expertises) {
		Set<Expertise> isMyExpertises=new HashSet<Expertise>();
		for(Expertise exp:expertises){
			if(myExpertises.contains(exp.getExpertiseId())){
			
	          Expertise exprtse=new Expertise(exp.getExpertiseId(),exp.getExpertise(),exp.getParentId(),exp.getHasChildren(),true);
				isMyExpertises.add(exprtse);
			}else{
				Expertise exprtse=new Expertise(exp.getExpertiseId(),exp.getExpertise(),exp.getParentId(),exp.getHasChildren(),false);
				isMyExpertises.add(exprtse);
			}
		}
	  return new ArrayList<Expertise>(isMyExpertises);
	}

	public Boolean deleteUserSource(final Long userId) {
		return esHttpClient.deleteUserDocument(userId);
	}

	public void addDocument(final Source source) {
		esHttpClient.addDocument(source);
	}

	public void updateUserProfileImageAtEs(final Long userId,final  ProfileImage profileImage) {
		esHttpClient.updateDocumentWithScript(userId,
				Script.withScriptAndParam(UPDATE_USER_PROFILE_LRES_IMAGE, Params.withLresId(profileImage.getLresId())),
				"[Update_profile_lres_image]");
		esHttpClient.updateDocumentWithScript(userId,
				Script.withScriptAndParam(UPDATE_USER_PROFILE_HRES_IMAGE, Params.withHresId(profileImage.getHresId())),
				"[Update_profile_hres_image");
	}

	
	public void addMentorDocument(final Long userId, final BeAMentor mentor, final List<Expertise> myExpertises) {   // for v1 users
	    List<Expertise> experties = mentor.getExperties();
		Set<Integer> expertiseIds = new HashSet<>();
		
		deleteOldMentorDocument(userId); // delete old mentor info
		for (Expertise expertise : experties) {
			expertiseIds.add(expertise.getExpertiseId());
		}
		for (Expertise expertise : myExpertises) {
			expertiseIds.add(expertise.getExpertiseId());
		}

		esHttpClient.updateDocumentWithScript(userId,
				Script.withScriptAndParam(ADD_MENTOR_CHARGE_PRICE, Params.withPrice(mentor.getPrice())),
				"[Update_mentor_price]");
		esHttpClient.updateDocumentWithScript(userId,
				Script.withScriptAndParam(ADD_MENTOR_ORG, Params.withOrg(mentor.getOrganisation())),
				"[Update_mentor_org]");
		esHttpClient.updateDocumentWithScript(userId,
				Script.withScriptAndParam(ADD_MENTOR_LOCATION, Params.withLocation(mentor.getLocation())),
				"[Update_mentor_location]");
		esHttpClient.updateDocumentWithScript(userId,
				Script.withScriptAndParam(ADD_MENTOR_POSITION, Params.withPosition(mentor.getDesignation())),
				"[Update_mentor_position]");
		esHttpClient.updateDocumentWithScript(userId,
				Script.withScriptAndParam(ADD_MENTOR_EXPERIENCE, Params.withExp(mentor.getExpYears())),
				"[Update_mentor_Experience]");
		esHttpClient.updateDocumentWithScript(userId,
				Script.withScriptAndParam(ADD_MENTOR_EXPERTISES, Params.withExpertise(experties)),
				"[Update_mentor_Expertises]");
		esHttpClient.updateDocumentWithScript(userId,
				Script.withScriptAndParam(ADD_MENTOR_EXPERTISE_IDS, Params.withExpertiseIds(expertiseIds)),
				"[Update_mentor_Expertise_ids]");
	}

	public void updateMentorCount(final Long menteeId) {
		esHttpClient.updateDocumentWithScript(menteeId, Script.withScript(UPDATE_MENTOR_COUNT),
				" [Increase_Mentor_Count]");
	}

	public void updateMenteeCount(final Long userId) {
		esHttpClient.updateDocumentWithScript(userId, Script.withScript(UPDATE_MENTEE_COUNT),
				" [Increase_Mentee_Count]");
	}

	public void updateFollowerCount(final Long followId) {
		esHttpClient.updateDocumentWithScript(followId, Script.withScript(INCREASE_FOLLOW_COUNT),
				"[Increase_Follower_Count]");
	}

	public void updateFollowingCount(final Long userId) {
		esHttpClient.updateDocumentWithScript(userId, Script.withScript(INCREASE_FOLLOWING_COUNT),
				"[Increase_Following_Count]");
	}

	public void decreaseFollowingCount(final Long userId) {
		esHttpClient.updateDocumentWithScript(userId, Script.withScript(DECREASE_FOLLOWING_COUNT),
				"[Decrease_Following_Count]");
	}

	public void decreaseFollowerCount(final Long unfollowUserId) {
		esHttpClient.updateDocumentWithScript(unfollowUserId, Script.withScript(DECREASE_FOLLOW_COUNT),
				"[Decrease_Follower_Count]");
	}

	public void addBlockedUserAtEs(final Long userId,final  Long blockUserId) {
		esHttpClient.updateDocumentWithScript(userId,
				Script.withScriptAndParam(ADD_BLOCKED_USER, Params.withBlockedId(blockUserId)), "[Add_blocked_user]");
	}

	public void unblockUserAtEs(final Long userId, final Long unBlockUserId) {
		esHttpClient.updateDocumentWithScript(userId,
				Script.withScriptAndParam(UNBLOCK_USER, Params.withBlockedId(unBlockUserId)), "[Unblock_user]");
	}

	public void addUserDocument(final User usr) {
		Source source = new Source(usr.getUserId(), usr.getEmailId(), usr.getProfile().getLresId(),
				usr.getProfile().getHresId(), 0, 0, 0, 0, usr.getProfile().getBasicInfo(),
				usr.getProfile().getBirthDate(), 0, "", "", "", 0, 0, usr.getProfile().getVideoBioLres(),
				usr.getProfile().getVideoBioHres(), new ArrayList<Rating>(), 0.0f, usr.getProfile().getName(), 0,
				new ArrayList<Expertise>(), new HashSet<Integer>(), new HashSet<Long>(), new HashSet<Long>(),
				new HashSet<Long>(), new ArrayList<Expertise>(),new ArrayList<Integer>(),"","");
		esHttpClient.addDocument(source);
	}

	public void updateProfile(final Long userId, final Profile profile) {
		esHttpClient.updateDocumentWithScript(userId,
				Script.withScriptAndParam(UPDATE_USER_PROFILE_LRES_IMAGE, Params.withLresId(profile.getLresId())),
				"[ Update_profile_lres_image ]");
		esHttpClient.updateDocumentWithScript(userId,
				Script.withScriptAndParam(UPDATE_USER_PROFILE_HRES_IMAGE, Params.withHresId(profile.getHresId())),
				"[ Update_profile_hres_image ]");
		esHttpClient.updateDocumentWithScript(userId,
				Script.withScriptAndParam(UPDATE_USER_INFO, Params.withBasicInfo(profile.getBasicInfo())),
				"[ Update_user_info ]");
		esHttpClient.updateDocumentWithScript(userId,
				Script.withScriptAndParam(UPDATE_USER_NAME, Params.withName(profile.getName())),
				"[ Update_user_name ]");
		if(profile.getVideoBioLres()!=null)
		{
			esHttpClient.updateDocumentWithScript(userId, Script.withScriptAndParam(UPDATE_USER_VIDEO_BIOLRES,
				Params.withVideoBioLres(profile.getVideoBioLres())), "[ Update_user_video_biolres ]");
		}
		if(profile.getVideoBioHres()!=null){
		esHttpClient.updateDocumentWithScript(userId, Script.withScriptAndParam(UPDATE_USER_VIDEO_BIOHRES,
				Params.withVedioBioHres(profile.getVideoBioHres())), "[ Update_user_video_biohres ]");
		}
		if(!(profile.getFacebookUrl()==null)){
		esHttpClient.updateDocumentWithScript(userId,
				Script.withScriptAndParam(UPDATE_USER_FACEBOOK_URL, Params.withFacebookURl(profile.getFacebookUrl())),
				"[ Update_user_facebook_url ]");
		}
		if(!(profile.getLinkedInURl()==null)){
		esHttpClient.updateDocumentWithScript(userId,
				Script.withScriptAndParam(UPDATE_USER_LINKEDIN_URL, Params.withLinkedInUrl(profile.getLinkedInURl())),
				"[ Update_user_linkedin_url ]");
		}
	}

	public void updateUserRating(final Long ratedId,final  Long userId,final  Integer rating) {
		Rating ra = new Rating(rating, ratedId);
		esHttpClient.updateDocumentWithScript(userId,
				Script.withScriptAndParam(UPDATE_USER_RATING, Params.withRatings(ra)), "[ Update_user_rating ]");
		addAverageRating(userId);
	}

	private void addAverageRating(final Long userId) {
		Optional<Source> user = esHttpClient.getUserDocument(userId);
		if (user.isPresent()) {
			List<Rating> rating = user.get().getRating();
			Float avg = 0.0f;
			Integer count = 0;
			for (Rating rat : rating) {
				avg = avg + rat.getRating();
				count++;
			}
			avg = avg / count;
			if(avg > 5){
				avg =(float) 5.0;
			}
			if (avg > 0) {
				esHttpClient.updateDocumentWithScript(userId,
						Script.withScriptAndParam(UPDATE_USER_AVERAGE_RATING, Params.withAvgRating(avg)),
						"[ Update_user_average_rating ]");
			}
		}
	}

	public void saveUserRating(final Collection<Source> users) {
		for (Source sour : users) {
			List<Rating> rating = sour.getRating();
			for (Rating rat : rating) {
				esHttpClient.updateDocumentWithScript(sour.getUserId(),
						Script.withScriptAndParam(ADD_USER_RATING, Params.withRatings(rat)), "[ Save_user_rating ]");
			}
		}
	}

	public void saveUserBlockedUsers(final Collection<Source> users) {
		for (Source sour : users) {
			Set<Long> blockedUsers = sour.getBlockedIds();
			for (Long bu : blockedUsers) {
				esHttpClient.updateDocumentWithScript(sour.getUserId(),
						Script.withScriptAndParam(ADD_BLOCKED_USER, Params.withBlockedId(bu)), "[ Save_blocked_users]");
			}
		}
	}

	public void deleteOldUserRating(final Long ratedUserId,final  Long userId, final Integer rating) {
		esHttpClient.updateDocumentWithScript(userId,
				Script.withScriptAndParam(DELETE_USER_OLD_RATING, Params.withRatings(new Rating(rating, ratedUserId))),
				"[ Delete_user_old_rating ]");
	}

	public void saveUserAverageRating(final Collection<Source> users) {
		for (Source sour : users) {
			esHttpClient.updateDocumentWithScript(sour.getUserId(),
					Script.withScriptAndParam(ADD_USER_AVERAGE_RATING, Params.withAvgRating(sour.getAverageRating())),
					"[ Save_user_avg_rating ]");
		}
	}

	public void skipMentor(final Long userId,final Long mentorId) {
		esHttpClient.updateDocumentWithScript(userId,
				Script.withScriptAndParam(ADD_SKIP_MENTOR, Params.withMentorID(mentorId)), "[ Add_skip_mentor ]");
		}

	public void removeSkippedMentor(final Long mentorId) {
		final List<Must> must = new ArrayList<>();
		must.add(Must.createMustWithTerm(Term.withSkippedMentor(mentorId)));
		ElasticSearch es = ElasticSearch.withQuery(Query.withBool(Bool.withMust(must)));
		List<Source> users = esHttpClient.getResponse1(es, "[ remove_skip_mentors]");
		for (Source user : users) {
			esHttpClient.updateDocumentWithScript(user.getUserId(),
					Script.withScriptAndParam(REMOVE_SKIP_MENTOR, Params.withMentorID(mentorId)),
					"[ Remove_skip_mentor ]");
		}
      }

	public void deleteOldMentorDocument(final Long userId) {
		Optional<Source> usr = esHttpClient.getUserDocument(userId);
		if (usr.isPresent()) {
			Source source = new Source(usr.get().getUserId(), usr.get().getEmailId(), usr.get().getLresId(),
					usr.get().getHresId(), usr.get().getFollowers(), usr.get().getFollowing(), usr.get().getMentors(),
					usr.get().getMentees(), usr.get().getBasicInfo(), usr.get().getBirthDate(), 0, "", "", "",
					usr.get().getRequests(), 0, usr.get().getVideoBioLres(), usr.get().getVideoBioHres(),
					usr.get().getRating(), usr.get().getAverageRating(), usr.get().getName(),
					usr.get().getAccountStatus(), new ArrayList<Expertise>(), new HashSet<Integer>(),
					usr.get().getBlockedIds(), usr.get().getSkippedMentors(), usr.get().getRequestSentMentors(),
					new ArrayList<Expertise>(),new ArrayList<Integer>(),usr.get().getFacebookUrl(),
					usr.get().getLinkedInURl());
			esHttpClient.addDocument(source);
		}
      }

	public void saveUserRequestSentMentors(final Collection<Source> users) {
		for (Source user : users) {
			Set<Long> mentors = user.getRequestSentMentors();
			for (Long mentor : mentors) {
				esHttpClient.updateDocumentWithScript(user.getUserId(),
						Script.withScriptAndParam(SAVE_REQUEST_SENT_MENTORS, Params.withRequestSentMentorID(mentor)),
						"[ Save_request_sent_mentors ]");
			}
		}
       }

	public void updateMenteeRequestToMentor(final Long userId,final Long mentorId) {
		esHttpClient.updateDocumentWithScript(userId,
				Script.withScriptAndParam(SAVE_REQUEST_SENT_MENTORS, Params.withRequestSentMentorID(mentorId)),
				"[ Save_request_sent_mentors ]");
          }

	public void deleteAndCancelRequest(final Long userId,final Long mentorId) {
		esHttpClient.updateDocumentWithScript(userId,
				Script.withScriptAndParam(CANCEL_DELETE_REQUEST, Params.withRequestSentMentorID(mentorId)),
				"[ Delete_and_cancel_request ]");
        }

	public void updateParentExpertises(final Long userId,final Set<Expertise> parentExpertises) {
		esHttpClient.updateDocumentWithScript(userId,
				Script.withScriptAndParam(ADD_MENTOR_PARENT_EXPERTISES, Params.withParentExpertises(parentExpertises)),
				"[Update_mentor_parent_Expertises]");
	    }

	public List<MentorSearch> searchMentorByStringV1(String mentorName) {
		mentorName="name:" + mentorName + "*";
		List<MentorSearch> users = new ArrayList<>();
		final Query query = Query.withBool(Bool.withMust(Arrays.asList(Must.
				createMustWithQueryString((QueryString.
						withQueryAndDefaultOperator(mentorName, "AND"))))));
		ElasticSearch es = ElasticSearch.withQuery(query);
		List<Source> sources = esHttpClient.getResponse1(es, "Search [mentor ,by_String]");
		for (Source sour : sources) {
			MentorSearch user = new MentorSearch(sour.getUserId(),sour.getName(), sour.getBasicInfo(),
					sour.getLresId(),sour.getHresId());
		    users.add(user);
		}
		return users;
        }
	
	public List<MentorSearch> searchMentorByStringV2(Long userId,String mentorName) {
		mentorName="name:" + mentorName + "*";
		List<MentorSearch> users = new ArrayList<>();
		Set<Long> blockedUsers=new HashSet<Long>();
        final List<MustNot> mustnot = new ArrayList<>();
		
		Optional<Source> source=esHttpClient.getUserDocument(userId);
		if(source.isPresent()){
		   blockedUsers = source.get().getBlockedIds();
		 }
		/**
		 * blocked users of user should not come in search mentor by string
		 */
		
		if (blockedUsers != null) {
			for (Long id : blockedUsers) {
				mustnot.add(MustNot.withTerm(Term.withUserId(id)));
			}
		}
		
		final Query query = Query.withBool(Bool.withMustAndMustNot(
		Arrays.asList(Must.createMustWithQueryString(QueryString.
				withQueryAndDefaultOperator(mentorName, "AND"))), mustnot));
		
		
		
		//final Query query = Query.withBool(Bool.withMust(Arrays.asList(Must.
				//createMustWithQueryString(QueryString.withQuery(mentorName)))));
		ElasticSearch es = ElasticSearch.withQuery(query);
		List<Source> sources = esHttpClient.getResponse1(es, "Search [mentor ,by_String]");
		for (Source sour : sources) {
			MentorSearch user = new MentorSearch(sour.getUserId(),sour.getName(), sour.getBasicInfo(),
					sour.getLresId(),sour.getHresId());
		    users.add(user);
		}
		return users;
        }

	public void saveSkipMentors(final Collection<Source> users) {
		for (Source user : users) {
			Set<Long> mentors = user.getSkippedMentors();
			for (Long mentor : mentors) {
				esHttpClient.updateDocumentWithScript(user.getUserId(),
						Script.withScriptAndParam(ADD_SKIP_MENTOR, Params.withMentorID(mentor)), "[ Save_skip_mentor ]");
			}}}

	public void updateParentExpertiseIds(Long userId, Set<Integer> expertiseIds) {
		esHttpClient.updateDocumentWithScript(userId,
				Script.withScriptAndParam(ADD_MENTOR_PARENT_EXPERTISE_IDS,
						Params.withParentExpertiseIds(expertiseIds)),
				"[Update_mentor_parent_Expertise_ids]");
		
	}

	public void addExpertises(Long userId, HashSet<Expertise> expertises) {
		esHttpClient.updateDocumentWithScript(userId,Script.withScriptAndParam(ADD_MENTOR_EXPERTISES, 
				Params.withExpertise(new ArrayList<Expertise>(expertises))),
		"[Update_mentor_Expertises]");

		
		
	}

	public void addExpertisesIds(Long userId, Set<Integer> expertiseIds) {
		esHttpClient.updateDocumentWithScript(userId,Script.withScriptAndParam(ADD_MENTOR_EXPERTISE_IDS, Params.withExpertiseIds(expertiseIds)),
		"[Update_mentor_Expertise_ids]");
		
	}

	public void deleteOldIndex() {
		if(esHttpClient.deleteOldIndex()){
			LOG.info("ELASTICSEARCH:=============== MENTORZ INDEX DELETED SUCCESSFULLY ==============" );
		}else{
			LOG.error("ELASTICSEARCH:=============== ERROR WHILE DELETING INDEX ==============");
		}
		
	}

}
