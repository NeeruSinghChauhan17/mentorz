/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.service;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.springframework.transaction.annotation.Transactional;

import com.crater.mentorz.exception.ForbiddenException;
import com.crater.mentorz.models.mentee.MenteeRequestTimeStamp;
import com.crater.mentorz.models.mentor.BeAMentor;
import com.crater.mentorz.models.mentor.Expertise;
import com.crater.mentorz.models.mentor.MentorFilter;
import com.crater.mentorz.models.user.User;
import com.google.common.base.Optional;

public interface MentorServiceV3 {

	@Transactional
	Boolean beAMentor(final Long userId, final BeAMentor mentor);

	Optional<List<User>> getMentor(final Long userId, final MentorFilter filter, final Integer offset,final Integer limit);

	List<Expertise> getExpertise(final Long userId);

	Optional<BeAMentor> getPublicBio(final Long userId);

	Optional<Collection<MenteeRequestTimeStamp>> getMenteeRequests(final Long userId, final Integer pageNo);

	Optional<Collection<MenteeRequestTimeStamp>> getPendingRequests(final Long userId, final Integer pageNo);

	@Transactional
	Boolean addMentee(final Long userId, final Long menteeId) throws ForbiddenException;

	@Transactional
	Map<String, Boolean> getRequestStatus(final Long userId, final Long mentorId);

	Boolean deleteRequest(final Long userId, final Long menteeId);

	Boolean cancelPendingRequest(final Long userId, Long mentorId);

	List<Expertise> getExpertiseById(final Long mentorId,final Integer expertiseId);

	List<Expertise> getExpertiseByParentId(final Integer parentId, final Long userId);

	List<Expertise> getExpertiseList(final Long mentorId);

	List<Expertise> getExpertiseListByParentId(Integer parentId, Long mentorId);

	
}
