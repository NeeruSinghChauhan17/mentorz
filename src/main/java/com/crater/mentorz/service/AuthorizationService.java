/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.service;

/**
 * @author Navrattan Yadav
 *
 */
public interface AuthorizationService {

	String getOrCreateOAuthToken(String userId, String userAgent);

	boolean authorize(String userId, String token, String userAgent);

	boolean deleteToken(String token, String userAgent);
}