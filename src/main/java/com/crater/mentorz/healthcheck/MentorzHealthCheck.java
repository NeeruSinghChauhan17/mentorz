package com.crater.mentorz.healthcheck;

import org.springframework.stereotype.Component;

import com.codahale.metrics.health.HealthCheck;

@Component
public class MentorzHealthCheck extends HealthCheck {

	@Override
	protected Result check() throws Exception {
		return Result.healthy();
	}
}
