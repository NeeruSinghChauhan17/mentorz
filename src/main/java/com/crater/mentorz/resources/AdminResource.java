package com.crater.mentorz.resources;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.hibernate.validator.constraints.NotBlank;

import com.crater.mentorz.annotation.Admin;
import com.crater.mentorz.exception.NotFoundException;
import com.crater.mentorz.exception.UnAuthorizedException;
import com.crater.mentorz.models.admin.Action;
import com.crater.mentorz.models.admin.SearchType;
import com.crater.mentorz.models.admin.User;
import com.crater.mentorz.models.user.Interest;



@Path("/mentorz/api/v1/admin")
public interface AdminResource extends Handler {

	@POST
	@Path("/login")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Admin
	Response login(@Valid @NotNull User user, @HeaderParam(HEADER_PARAM_USER_AGENT) String userAgent,
			 @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) String oauthToken) throws UnAuthorizedException;
	
	@GET
	@Path("/users")
	@Admin
	@Produces(MediaType.APPLICATION_JSON)
	Response allUsers(@DefaultValue("0") @QueryParam("pageNo") Integer pageNo, @HeaderParam(HEADER_PARAM_USER_AGENT) String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) String oauthToken);
	
	
	@GET
	@Path("/feedback")
	@Admin
	@Produces(MediaType.APPLICATION_JSON)
	Response feedbacks(@DefaultValue("0") @QueryParam("pageNo") Integer pageNo, @HeaderParam(HEADER_PARAM_USER_AGENT) String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) String oauthToken);
	


	@PUT
	@Admin
	@Path("/interest")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	Response interest(@NotNull Interest interest, @HeaderParam(HEADER_PARAM_USER_AGENT) String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) String oauthToken);
	

	
	
	@GET
	@Admin
	@Path("/blocked/users")
	@Produces(MediaType.APPLICATION_JSON)
	Response blockedUsers(@DefaultValue("0") @QueryParam("pageNo") Integer pageNo, @HeaderParam(HEADER_PARAM_USER_AGENT) String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) String oauthToken);
	
	
	@POST
	@Admin
	@Path("/update/user/{userId}/account")
	@Produces(MediaType.APPLICATION_JSON)
	Response updateUserAccountStatus(@PathParam("userId") Long userId, @NotNull @QueryParam("action") Action action, @HeaderParam(HEADER_PARAM_USER_AGENT) String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) String oauthToken);
	
	
	@GET
	@Admin
	@Path("/search/user")
	@Produces(MediaType.APPLICATION_JSON)
	Response searchUser(@NotNull @QueryParam("user_name") String userName, @NotNull @QueryParam("search_type") SearchType searchType, @DefaultValue("0") @QueryParam("pageNo") Integer pageNo, 
			@HeaderParam(HEADER_PARAM_USER_AGENT) String userAgent, @NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) String oauthToken);	
	
	@GET
	@Admin
	@Path("/abused/post")
	@Produces(MediaType.APPLICATION_JSON)
	Response abusedPost(@DefaultValue("0") @QueryParam("pageNo") Integer pageNo, @HeaderParam(HEADER_PARAM_USER_AGENT) String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) String oauthToken);
	
	
	
	@POST
	@Admin
	@Path("/update/user/{userId}/post/{postId}")
	@Produces(MediaType.APPLICATION_JSON)
	Response updatePostStatus(@PathParam("userId") Long userId, @PathParam("postId") Long postId, @NotNull @QueryParam("action") Action action, @HeaderParam(HEADER_PARAM_USER_AGENT) String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) String oauthToken);
	
	
	@GET
	@Admin
	@Path("/total/users")
	@Produces(MediaType.APPLICATION_JSON)
	Response noOfUsers( @HeaderParam(HEADER_PARAM_USER_AGENT) String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) String oauthToken);
	
	@GET
	@Admin
	@Path("/total/mentors")
	@Produces(MediaType.APPLICATION_JSON)
	Response noOfMentor(@HeaderParam(HEADER_PARAM_USER_AGENT) String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) String oauthToken);
	@GET
	@Admin
	@Path("/{userId}/user")
	@Produces(MediaType.APPLICATION_JSON)
	Response getUserById(@PathParam("userId") Long userId,
			@HeaderParam(HEADER_PARAM_USER_AGENT) String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) String oauthToken)  throws NotFoundException ;
	
	@GET
	@Admin
	@Path("/{postId}/post")
	@Produces(MediaType.APPLICATION_JSON)
	Response getPostById(@PathParam("postId") Long postId,
			@HeaderParam(HEADER_PARAM_USER_AGENT) String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) String oauthToken) ;
	
	@GET
	@Admin
	@Path("/mentors")
	@Produces(MediaType.APPLICATION_JSON)
	Response getAllMentors(
			@HeaderParam(HEADER_PARAM_USER_AGENT) String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) String oauthToken) ;
	
	@GET
	@Path("/all/interest")
	@Produces(MediaType.APPLICATION_JSON)
	Response allInterest();
	
	@GET
	@Path("/interest")
	@Produces(MediaType.APPLICATION_JSON)
	Response getInterestByParerntId(@QueryParam("parentId") Integer parentId);
	
	@GET
	@Admin
	@Path("/values/per/user")
	@Produces(MediaType.APPLICATION_JSON)
	Response getAvgOfValuesPerMentor(@PathParam("postId") Long postId,
			@HeaderParam(HEADER_PARAM_USER_AGENT) String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) String oauthToken) ;
	@GET
	@Admin
	@Path("/expertises/per/mentor")
	@Produces(MediaType.APPLICATION_JSON)
	Response getAvgOfExpertisePerMentor(@PathParam("postId") Long postId,
			@HeaderParam(HEADER_PARAM_USER_AGENT) String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) String oauthToken) ;
	
	@GET
	@Path("/expertises/rank")
	@Produces(MediaType.APPLICATION_JSON)
	Response getExpertisesRank();
	
	@GET
	@Path("/values/rank")
	@Produces(MediaType.APPLICATION_JSON)
	Response getValuesRank();

	@GET
	@Path("/interests/rank")
	@Produces(MediaType.APPLICATION_JSON)
	Response getInterestsRank();
	
	
}
