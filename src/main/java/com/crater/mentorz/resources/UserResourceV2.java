/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.resources;
import java.io.IOException;
import java.security.SignatureException;
import java.security.cert.CertificateException;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.stereotype.Component;

import com.crater.mentorz.annotation.Auth;
import com.crater.mentorz.annotation.Blocked;
import com.crater.mentorz.exception.BadRequestException;
import com.crater.mentorz.exception.ConflitException;
import com.crater.mentorz.exception.CustomException;
import com.crater.mentorz.exception.ForbiddenException;
import com.crater.mentorz.exception.NotFoundException;
import com.crater.mentorz.exception.UnAuthorizedException;
import com.crater.mentorz.models.Entity;
import com.crater.mentorz.models.Rating;
import com.crater.mentorz.models.payment.Payment;
import com.crater.mentorz.models.payment.Redeem;
import com.crater.mentorz.models.user.DeviceInfo;
import com.crater.mentorz.models.user.Feedback;
import com.crater.mentorz.models.user.ForgetPassword;
import com.crater.mentorz.models.user.InterestList;
import com.crater.mentorz.models.user.InternationalNumber;
import com.crater.mentorz.models.user.Login;
import com.crater.mentorz.models.user.NewPassowrd;
import com.crater.mentorz.models.user.PhoneContacts;
import com.crater.mentorz.models.user.Profile;
import com.crater.mentorz.models.user.ProfileImage;
import com.crater.mentorz.models.user.RequestInfo;
import com.crater.mentorz.models.user.SocialInterestList;
import com.crater.mentorz.models.user.SocialSource;
import com.crater.mentorz.models.user.SocialUser;
import com.crater.mentorz.models.user.UpdatePasswordV2;
import com.crater.mentorz.models.user.User;
import com.crater.mentorz.models.user.ValuesList;

@Component
@Path("/mentorz/api/v2/user")
@Consumes("application/json")
@Produces("application/json")
public interface UserResourceV2 extends Handler {
    
	@PUT
	Response register(@Valid @NotNull final User user, @HeaderParam("user-agent") final  String userAgent)
			throws ConflitException;
    
	@POST
	@Path("/login")
	Response login(@Valid @NotNull  final Login login, @HeaderParam("user-agent") String userAgent)
			throws UnAuthorizedException,com.crater.mentorz.exception.CustomException;

	@POST
	@Path("/sociallogin")
	Response socialLogin(@Valid @NotNull final SocialUser socialUser, @HeaderParam("user-agent") 
	final String userAgent) throws UnAuthorizedException,com.crater.mentorz.exception.CustomException;

	@PUT
	@Path("/social")
	Response registerSocialUser(@Valid @NotNull final SocialUser socialUser,
			@HeaderParam("user-agent") final String userAgent) throws ConflitException;

	@GET
	@Path("/{userId}/verify")
	Response verifyEmail(@PathParam("userId")final  Long userId) throws NotFoundException;

	@Auth
	@POST
	
	@Path("/{userId}/values")
	Response updateValues(@PathParam("userId") final Long userId, @Valid @NotNull final ValuesList values,
			@NotBlank @HeaderParam(HEADER_PARAM_USER_AGENT) final  String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) final String oauthToken);

	@Auth
	@POST
	@Path("/{userId}/interests")
	Response updateInterests(@PathParam("userId")final  Long userId, @Valid @NotNull final 
			InterestList interestList,@NotBlank @HeaderParam(HEADER_PARAM_USER_AGENT) final String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) final  String oauthToken);

	@Auth
	@GET
	@Path("/{userId}/values")
	Response getValues(@PathParam("userId") final  Long userId,
			@NotBlank @HeaderParam(HEADER_PARAM_USER_AGENT) final String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) final String oauthToken, 
			@DefaultValue("0") @QueryParam("pageNo")  final Integer pageNo) throws ForbiddenException;

	@Auth
	@GET
	@Path("/{userId}/interest")
	Response getInterests(@PathParam("userId") final Long userId,
			@NotBlank @HeaderParam(HEADER_PARAM_USER_AGENT) final String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) final String oauthToken, 
			@DefaultValue("0") @QueryParam("pageNo") final Integer pageNo) throws ForbiddenException;

	
	
	@Auth
	@GET
	@Path("/{userId}/profile")
    Response getProfile(@PathParam("userId") final Long userId,
			@NotBlank @HeaderParam(HEADER_PARAM_USER_AGENT) final String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) final String oauthToken) throws NotFoundException;

	@Auth
	@Blocked
	@POST
	@Path("/{userId}/{follow}/{friendId}")
	Response follow(@PathParam("userId") final  Long userId, @PathParam("friendId") final Long friendId,
			@PathParam("follow") final String follow,
			@NotBlank @HeaderParam(HEADER_PARAM_USER_AGENT) final  String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) final String oauthToken)
			throws ForbiddenException,com.crater.mentorz.exception.CustomException;

	
	
	@GET
	@Path("/{userId}/followers")
	Entity<Long> getFollowers(@PathParam("userId") final Long userId);

	@GET
	@Path("/{userId}/rating")
	Response getRating(@PathParam("userId") final  Long userId);

	@GET
	@Path("/{userId}/profile/image")
	Response getProfileImage(@PathParam("userId") final Long userId);

	@Auth
	@GET
	@Path("/{userId}/board")
	Response getUserBoard(@PathParam("userId") final  Long userId,
			@DefaultValue("0") @QueryParam("pageNo") final Integer pageNo,
			@NotBlank @HeaderParam(HEADER_PARAM_USER_AGENT) final String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) final String oauthToken);

	@Auth
	@POST
	@Path("/{userId}/update/profile")
	Response updateProfile(@PathParam("userId") final  Long userId, @Valid @NotNull  final Profile profile,
			@NotBlank @HeaderParam(HEADER_PARAM_USER_AGENT) final String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) final String oauthToken);

	@Auth
	@Blocked
	@POST
	@Path("/{userId}/rate/{ratedUserId}")
	Response rate(@PathParam("userId") final Long userId, @PathParam("ratedUserId") final Long ratedUserId,
			@NotNull @Valid  final Rating rating,
			@NotBlank @HeaderParam(HEADER_PARAM_USER_AGENT) final String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) final String oauthToken) throws ForbiddenException;

	@GET
	@Path("/values")
	Response valuesList(@DefaultValue("0") @QueryParam("pageNo") final Integer pageNo,
			@HeaderParam(HEADER_PARAM_USER_AGENT) final String userAgent,
			 @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) final String oauthToken);

	@GET
	@Path("/{userId}/interests")
	@Auth
	Response interestsList(@PathParam("userId") final Long userId,@DefaultValue("0") 
	@QueryParam("pageNo") final  Integer pageNo, @HeaderParam(HEADER_PARAM_USER_AGENT) final String userAgent,
			 @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) final String oauthToken,
			 @QueryParam("interestId") final  Integer id,@QueryParam("parentId") final Integer parentId);
	
	@Auth
	@GET
	@Path("/{userId}/following/{followingUserId}")
	Response following(@PathParam("userId") final  Long userId, @PathParam("followingUserId") final Long followingUserId,
			@NotBlank @HeaderParam(HEADER_PARAM_USER_AGENT) final String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) final  String oauthToken)
			throws ForbiddenException;
	
	//@Auth
	@GET
	@Path("/signed/geturl/object/{objectName}")
	Response getGETSignedUrl(@NotBlank @PathParam("objectName") final String objectName,
			@HeaderParam(HEADER_PARAM_USER_AGENT) final String userAgent,
			@HeaderParam(HEADER_PARAM_OAUTH_TOKEN) final String oauthToken) throws IllegalArgumentException, SignatureException, IOException, CustomException, com.cz.googlecontentapi.exception.CustomException;
	
	//@Auth
	@GET
	@Path("/signed/puturl/object/{objectName}")
	Response getPUTSignedUrl(@NotBlank @PathParam("objectName")final  String objectName,
			@HeaderParam(HEADER_PARAM_USER_AGENT) final String userAgent,
			@HeaderParam(HEADER_PARAM_OAUTH_TOKEN) final String oauthToken) throws IllegalArgumentException, SignatureException, IOException, CustomException, com.cz.googlecontentapi.exception.CustomException;
	
	//@Auth
	@GET
	@Path("/signed/deleteurl/object/{objectName}")
	Response getDELETESignedUrl(@NotBlank @PathParam("objectName") final String objectName,
			@HeaderParam(HEADER_PARAM_USER_AGENT) final String userAgent,
			@HeaderParam(HEADER_PARAM_OAUTH_TOKEN) final String oauthToken) throws IllegalArgumentException, SignatureException, IOException, CustomException, com.cz.googlecontentapi.exception.CustomException;
	
	//@Auth
	@GET
	@Path("/signed/sessionuri/object/{objectName}")
	Response getSessionUrl(@PathParam("objectName")final  String objectName,
			@NotBlank @HeaderParam("contentType") final String contentType,
			@HeaderParam(HEADER_PARAM_USER_AGENT) final String userAgent,
			@HeaderParam(HEADER_PARAM_OAUTH_TOKEN) final  String oauthToken) throws CertificateException, IOException, CustomException, com.cz.googlecontentapi.exception.CustomException;
	
	@Auth
	@POST
	@Path("/{userId}/update/profile/image")
	Response updateProfileImage(@PathParam("userId") final Long userId,@Valid @NotNull final 
			ProfileImage profileImage,
			@NotBlank @HeaderParam(HEADER_PARAM_USER_AGENT) final  String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) final  String oauthToken);
	
	@Auth
	@POST
	@Blocked
	@Path("/{userId}/send/request/mentor/{mentorId}")
	Response sendMentorRequest(@PathParam("userId") final Long userId, @PathParam("mentorId") final Long mentorId,
			@Valid @NotNull final RequestInfo info,
			@NotBlank @HeaderParam(HEADER_PARAM_USER_AGENT) final String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) final String oauthToken) throws BadRequestException, ForbiddenException;

    @Auth
    @GET
    @Path("/{userId}/accesstoken")
    Response getAccessToken(@PathParam("userId") final Long userId, @NotBlank @QueryParam("deviceId")
     final   String deviceId, @NotBlank @HeaderParam(HEADER_PARAM_USER_AGENT)  final  String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) final String oauthToken);
    
    
    @Auth
	@GET
	@Path("/{userId}/my/mentor")
	Response getMyMentor(@PathParam("userId") final  Long userId,
			@NotBlank @HeaderParam(HEADER_PARAM_USER_AGENT)  final String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) final String oauthToken,
			@QueryParam("friend_id") final  Long freindId,
			@DefaultValue("0") @QueryParam("pageNo") final Integer pageNo);
    
    @Auth
	@GET
	@Path("/{userId}/my/mentee")
	Response getMyMentee(@PathParam("userId") final  Long userId,
			@NotBlank @HeaderParam(HEADER_PARAM_USER_AGENT) final String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) final String oauthToken,
			@QueryParam("friend_id") final  Long freindId,
			@DefaultValue("0") @QueryParam("pageNo") final Integer pageNo);
    
    @Auth
	@GET
	@Path("/followers/{userId}")
	Response listFollowers(@PathParam("userId") final Long userId, 
			@NotBlank @HeaderParam(HEADER_PARAM_USER_AGENT) final String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) final  String oauthToken,
			@QueryParam("friend_id") final Long freindId,
			@DefaultValue("0") @QueryParam("pageNo") final Integer pageNo);
    
    @Auth
	@GET
	@Path("/following/{userId}")
	Response listFollowing(@PathParam("userId") final Long userId, 
			@NotBlank @HeaderParam(HEADER_PARAM_USER_AGENT) final String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) final String oauthToken, 
			@QueryParam("friend_id") final  Long freindId,
			@DefaultValue("0") @QueryParam("pageNo") final Integer pageNo);
    
	
    
    @PUT
    @Auth
    @Path("/{userId}/social/interests")
    Response addOrUpdateSocialInterest(@PathParam("userId") final Long userId, 
    		@NotBlank @HeaderParam(HEADER_PARAM_USER_AGENT) final  String userAgent,
    		@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) final String oauthToken,
    		@Valid @NotNull final SocialInterestList interestList, 
    		@DefaultValue("FACEBOOK") @QueryParam("social_source")  final SocialSource socialSource);
    
    @PUT
    @Auth
    @Path("/{userId}/credit/balance")
    Response credit(@PathParam("userId") final  Long userId,
    		@NotBlank @HeaderParam(HEADER_PARAM_USER_AGENT) final String userAgent,
    		@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) final String oauthToken,
    		@Valid @NotNull final Payment payment);
    
    @POST
    @Auth
    @Path("/{userId}/debit/balance/{mentorId}")
    Response debit(@PathParam("userId") final Long userId, 
    		@NotBlank @HeaderParam(HEADER_PARAM_USER_AGENT) final String userAgent,
    		@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) final String oauthToken,
    		@Valid @NotNull final Payment payment,@PathParam("mentorId") final  Long mentorId);
    
   
    @POST
	@Auth
	@Path("/{userId}/redeem/balance")
	Response redeem(@PathParam("mentorId") final Long userId,
			@NotBlank @HeaderParam(HEADER_PARAM_USER_AGENT) final String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) final String oauthToken,
			@Valid @NotNull final Redeem redeem);
    
    @GET
    @Auth
    @Path("/{userId}/balance/{mentorId}")
    Response getBalance(@PathParam("userId") final Long userId, 
    		@NotBlank @HeaderParam(HEADER_PARAM_USER_AGENT) final String userAgent,
    		@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) final String oauthToken,
    		@PathParam("mentorId") final Long mentorId);
    
    @POST
    @Auth
    @Path("/{userId}/feedback")
    Response feedback(@PathParam("userId") final Long userId,
    		@NotNull @Valid final Feedback feedback, 
    		@NotBlank @HeaderParam(HEADER_PARAM_USER_AGENT) final String userAgent,
    		@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) final String oauthToken);
    
    @POST
	@Auth
	@Path("/{userId}/block/{friendId}")
	Response blockUser(@PathParam("userId") final Long userId, 
			@PathParam("friendId") final Long friendId, 
			@NotBlank @HeaderParam(HEADER_PARAM_USER_AGENT) final String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) final String oauthToken);
	
    @GET
	@Auth
	@Path("/{userId}/blocked/users")
	Response getBlockedUsers(@PathParam("userId")  final Long userId, 
			@DefaultValue("0") @QueryParam("pageNo") final  Integer pageNo,
			@NotBlank @HeaderParam(HEADER_PARAM_USER_AGENT) final String userAgent, 
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) final String oauthToken);
	
	@POST
	@Auth
	@Path("/{userId}/unblock/{friendId}")
	Response unBlockUser(@PathParam("userId") final Long userId, 
			@PathParam("friendId") final Long friendId,
			@NotBlank @HeaderParam(HEADER_PARAM_USER_AGENT) final String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) final String oauthToken);
	
	@Auth
	@Blocked
	@GET
	@Path("/{userId}/friend/{friendId}/profile")
	Response getFriendProfile(@PathParam("userId") final  Long userId, 
			@PathParam("friendId") final Long friendId,
			@NotBlank @HeaderParam(HEADER_PARAM_USER_AGENT) final String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) final String oauthToken) throws NotFoundException;
	
	@GET
	@Auth
	@Path("/{userId}/review")
	Response getReviews(@PathParam("userId") final Long userId, 
			@DefaultValue("0") @QueryParam("pageNo") final Integer pageNo,
			@NotBlank @HeaderParam(HEADER_PARAM_USER_AGENT)  final String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) final String oauthToken);
	
	@POST
	@Auth
	@Path("/{userId}/update/device")
	Response updateDeviceInfo(@PathParam("userId") final Long userId,
			@Valid @NotNull final DeviceInfo deviceInfo,
			@NotBlank @HeaderParam(HEADER_PARAM_USER_AGENT) final String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN)  final String oauthToken);
	
	
	@DELETE
	@Path("/{userId}/logout")
	@Auth
	Response logout(@PathParam("userId") final Long userId,
			@NotBlank @HeaderParam(HEADER_PARAM_USER_AGENT) final String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) final  String oauthToken)
			throws UnAuthorizedException;
	
	@GET
	@Path("/{userId}/account/status")
	@Auth
	Response accountStatus(@PathParam("userId")final  Long userId,
			@NotBlank @HeaderParam(HEADER_PARAM_USER_AGENT) final String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) final  String oauthToken)
			throws UnAuthorizedException;
	
	@GET
	@Path("/{userId}/sync/interest")
	@Auth
	Response syncInterest(@PathParam("userId") final Long userId,
			 @DefaultValue("0")@QueryParam("time")  final Long time,
			@NotBlank @HeaderParam(HEADER_PARAM_USER_AGENT) final String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) final String oauthToken
			)
			throws UnAuthorizedException;
	
	
	@POST
	@Path("/forgot/password")
	Response forgetPassword(@Valid @NotNull final  ForgetPassword forgetPassword)
			throws  com.crater.mentorz.exception.CustomException ;
	
	@POST
	@Path("/reset/password")
	Response resetPassword(@Valid  @NotNull final  NewPassowrd newPassowrd);
	
	
	@GET
	@Path("/current/time")
	Response getCurrentTime();
	
	@GET
	@Path("/{userId}/findFollowing")
	Response findFollowing(@PathParam("userId")final  Long userId,
			@NotBlank @HeaderParam(HEADER_PARAM_USER_AGENT) final String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) final String oauthToken);
	
	
	@POST
	@Path("add/user/document/elasticsearch")
    Response saveUserAtEs(final User user);
	

	@Auth
	@POST
	@Path("/{userId}/mentor/{mentorId}/skip")
	Response skipMentor(@PathParam("userId")final  Long userId,@PathParam("mentorId")final  Long mentorId,
			@NotBlank @HeaderParam(HEADER_PARAM_USER_AGENT) final String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) final String oauthToken);
	
	@GET
	@Auth
	@Path("/{userId}/searchmentorbystring/{string}")
	Response searchMentorByString(@PathParam("userId")final  Long userId,@PathParam("string") @NotBlank  @NotNull final  String mentorName,
			@NotBlank @HeaderParam(HEADER_PARAM_USER_AGENT) final String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) final String oauthToken);
	
	@Auth
	@POST
	@Path("/{userId}/addcontacts/isdelta/{isDelta}")
	 Response addContacts(final PhoneContacts contacts,
			 @PathParam("userId")   @NotNull final Long userId,
			 @PathParam("isDelta")   @NotNull final Short isDelta) throws NotFoundException, UnAuthorizedException, BadRequestException;
	
	@POST
	@Path("/verifyuser")
	public Response verifyUser(@Valid @NotNull InternationalNumber number) throws  NotFoundException;
	
	
	
	@POST
	@Auth
	@Path("/{userId}/updatecontact")
	public Response updateContact(@Valid @NotNull InternationalNumber updateContact,
			@PathParam("userId") final Long userId,
			@NotBlank @HeaderParam(HEADER_PARAM_USER_AGENT) final String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) final String oauthToken ) 
					throws ConflitException;
	@GET
	@Auth
	@Path("/{userId}/invitablecontacts")
	public Response getContactList(@PathParam("userId") final Long userId,
			@NotBlank @HeaderParam(HEADER_PARAM_USER_AGENT) final String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) final String oauthToken);
	
    @POST
	@Path("/updatepassword")
	public Response updatePassword(@Valid @NotNull UpdatePasswordV2 updatepassword) throws UnAuthorizedException,  NotFoundException;   //v2 forget password
    
    
   
    @POST
    @Path("/checkno")
    public Response isNoExists(@Valid @NotNull InternationalNumber number);
    
    @GET
    @Path("/{userId}/data")
    public Response getUser(@PathParam("userId") final Long userId) throws IllegalArgumentException, SignatureException, IOException, com.cz.googlecontentapi.exception.CustomException;

}



