package com.crater.mentorz.resources;

import java.util.Collection;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.crater.mentorz.models.user.SubscribeUser;

@Path("/mentorz/api/v1/user/subscribe")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public interface SubscribeUserResource {

	@PUT
	Response subscribeUser(final SubscribeUser subscribeUser);

	@GET
	Collection<SubscribeUser> getAllSubscribedUser();
}