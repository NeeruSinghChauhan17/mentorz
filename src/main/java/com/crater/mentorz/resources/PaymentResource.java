package com.crater.mentorz.resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.crater.mentorz.annotation.Auth;


@Path("/mentorz/api/v1/{userId}/payment")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public interface PaymentResource {
	
	@Auth
	@POST
	@Path("/paypal_webhook")
	Response createWebook();

}
