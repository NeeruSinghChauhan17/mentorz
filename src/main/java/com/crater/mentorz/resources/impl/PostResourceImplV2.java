
package com.crater.mentorz.resources.impl;

import java.util.Collection;
import java.util.List;

import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.crater.mentorz.annotation.Auth;
import com.crater.mentorz.annotation.Blocked;
import com.crater.mentorz.annotation.PostBlocked;
import com.crater.mentorz.exception.ForbiddenException;
import com.crater.mentorz.models.post.AbuseType;
import com.crater.mentorz.models.post.Comment;
import com.crater.mentorz.models.post.CommentList;
import com.crater.mentorz.models.post.Post;
import com.crater.mentorz.models.post.PostList;
import com.crater.mentorz.models.post.PostV2;
import com.crater.mentorz.resources.PostResourceV2;
import com.crater.mentorz.service.PostServiceV2;
import com.google.common.base.Optional;


@Component
public class PostResourceImplV2 implements PostResourceV2 {

	final private PostServiceV2 postService;

	@Autowired
	public PostResourceImplV2(final PostServiceV2 postService) {
		this.postService = postService;
	}

	@Override
	@Auth
	public PostV2 addPost(final Long userId, final PostV2 post, final String userAgent, final String oauthToken) {
		return postService.addPost(userId, post);
	}

	@Override
	@Auth
	@PostBlocked
	public Response like(final Long userId, final Long postId, final String userAgent, final String oauthToken) throws ForbiddenException {
		if (postService.like(postId, userId)) {
			return Response.noContent().build();
		}
		return Response.notModified().build();
	}

	@Override
	@Auth
	@PostBlocked
	public Response view(final Long userId, final Long postId, final String userAgent, final String oauthToken) throws ForbiddenException {
		if (postService.view(postId, userId)) {
			return Response.noContent().build();
		}
		return Response.notModified().build();
	}

	@Override
	@Auth
	@PostBlocked
	public Response share(final Long userId, final Long postId, final String userAgent, final String oauthToken) throws ForbiddenException {
		if (postService.share(postId, userId)) {
			return Response.noContent().build();
		}
		return Response.notModified().build();
	}

	@Override
	@Auth
	@PostBlocked
	public Comment comment(final Long userId, final Long postId, final Comment comment, final String userAgent, final String oauthToken)
			throws ForbiddenException {
		return postService.comment(userId, postId, comment);
	}

	@Override
	public Response getPostByUserId(final Long userId, final Integer pageNo) {
		final List<Post> posts = postService.getPostByUserId(userId, pageNo);
		if (posts.isEmpty()) {
			return Response.noContent().build();
		}
		return Response.ok(new PostList(posts)).build();
	}

	@Override
	@Auth
	public Response getComments(final Long userId, final Long postId, final Integer pageNo, final String userAgent, final String oauthToken) {
		final List<Comment> comments = postService.getCommentByPostId(postId, pageNo);
		if (comments.isEmpty()) {
			return Response.noContent().build();
		}
		return Response.ok(new CommentList(comments)).build();
	}

	@Override
	@Auth
	public Response deleteComment(final Long userId, final Long postId, final Long commentId, final String userAgent,
			final String oauthToken) throws ForbiddenException {
		postService.deleteComment(userId, postId, commentId);
		return Response.ok("successfully deleted").build();
	}

	@Override
	@Auth
	@Blocked
	public Response getFriendPostByUserId(final Long userId, final Long friendId, final Integer pageNo) {
		final List<Post> posts = postService.getFriendPostByUserId(userId, friendId, pageNo);
		if (posts.isEmpty()) {
			return Response.noContent().build();
		}
		return Response.ok(new PostList(posts)).build();
	}

	@Override
	@Auth
	public Response abusePost(final Long userId, final Long postId, final AbuseType abuseType, final String userAgent, final String oauthToken) throws ForbiddenException {
		postService.abusePost(userId, postId, abuseType);
		return Response.noContent().build();
	}

	@Override
	@Auth
	public Response searchPostByInterest(final Long userId, final List<Integer> interest, final Integer pageNo, final String userAgent, final String oauthToken) {
		final List<Post> posts = postService.searchPost(userId, interest, pageNo).get();
		if (posts.isEmpty()) {
			return Response.noContent().build();
		}
		return Response.ok(new PostList(posts)).build();
	}

	@Override
	public Response getPostByPostIdInShareOut(final  Long postId) {
		final Optional<Post> post=postService.getPostByPostIdInShareOut(postId);
		if(post.isPresent()) {
			return Response.ok(post.get()).build();
		}
		return Response.noContent().build();
	}

	@Override
	@Auth
	public Response getCommentByCommentId(final Long userId,Long postId, final Long commentId,
			final String userAgent, final String oauthToken) {
		final Optional<Comment> comment=postService.getCommentByCommentId(commentId);
		if(comment.isPresent()) {
			return Response.ok(comment.get()).build();
		}
		return Response.noContent().build();
	}

	@Override
	@Auth
	public Response getPostByPostId(Long userId, Long postId) {
		final Optional<Collection<PostV2>> post=postService.getPostByPostId(postId);
		if(post.isPresent()) {
			return Response.ok(post.get()).build();
		}
		return Response.noContent().build();
	}
}
