/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.resources.impl;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.crater.mentorz.annotation.Auth;
import com.crater.mentorz.annotation.Blocked;
import com.crater.mentorz.exception.ForbiddenException;
import com.crater.mentorz.models.Entity;
import com.crater.mentorz.models.mentee.MenteeRequestTimeStamp;
import com.crater.mentorz.models.mentee.MenteeRequestTimeStampList;
import com.crater.mentorz.models.mentor.BeAMentor;
import com.crater.mentorz.models.mentor.Expertise;
import com.crater.mentorz.models.mentor.ExpertiseList;
import com.crater.mentorz.models.mentor.MentorFilter;
import com.crater.mentorz.models.user.User;
import com.crater.mentorz.models.user.UserList;
import com.crater.mentorz.resources.MentorResourceV3;
import com.crater.mentorz.service.MentorServiceV3;
import com.google.common.base.Optional;


@Component
@Path("/mentorz/api/v3/{userId}/mentor")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class MentorResourceImplV3 implements MentorResourceV3 {

	@Autowired
	private MentorServiceV3 mentorService;

	
	

	@Override
	@Auth
	public Response beAMentor(final Long userId, final BeAMentor mentor, final String userAgent, final String oauthToken) {
		if (mentorService.beAMentor(userId, mentor)) {
			return Response.ok(mentor).build();
		}
		return Response.notModified().build();
	}
	


	@Override
	@Auth
	public Response getMentor(final Long userId, final MentorFilter filter, final String userAgent, final String oauthToken, 
			final Integer offset,final Integer limit) {
		
		final Optional<List<User>> usr = mentorService.getMentor(userId, filter, offset,limit);
		if (usr.get()!= null &&  !usr.get().isEmpty()) {
			return Response.ok(new UserList(usr.get())).build();
		}
		return Response.noContent().build();
	}

	@Override
	////@Auth
	public Response getMentorExpertise(final Long userId, final String userAgent, final String oauthToken, final Long mentorId)
			throws ForbiddenException {
		final List<Expertise> expertise = mentorService.getExpertise(mentorId);
		if (expertise.isEmpty()) {
			return Response.noContent().build();
		}
		return Response.ok(new ExpertiseList(expertise)).build();
	}

	@Override
	@Auth
	public Response getPublicBio(final Long userId, final String userAgent, final String oauthToken) throws ForbiddenException {
		final Optional<BeAMentor> publicBio = mentorService.getPublicBio(userId);
		if (publicBio.isPresent()) {
			return Response.ok(publicBio.get()).build();
		}
		return Response.noContent().build();

	}

	@Override
	@Auth         
	public Response getMenteeRequests(final Long userId, final Integer pageNo, final String userAgent, final String oauthToken) {
		final Optional<Collection<MenteeRequestTimeStamp>> requests =  mentorService.getMenteeRequests(userId, pageNo);
		if (requests.isPresent() && !requests.get().isEmpty()) {
			return Response.ok(requests.get()).build();
		}
		return Response.noContent().build();
	}

	@Override
	@Auth
	@Blocked
	public Response addMentee(final Long userId, final Long menteeId, final String userAgent, final String oauthToken)
			throws ForbiddenException {
		
		if (mentorService.addMentee(userId, menteeId)) {
			return Response.noContent().build();
		}
		return Response.notModified().build();
	}

	@Auth
	@Override
	public Response getRequestStatus(final Long userId, final Long mentorId, final String userAgent, final String oauthToken) {
		return Response.ok(new Entity<Map<String, Boolean>>(mentorService.getRequestStatus(userId, mentorId))).build();
	}

	@Override
    @Auth
	public Response deleteRequest(final Long userId, final Long menteeId, final String userAgent, final String oauthToken) {
		if (mentorService.deleteRequest(userId, menteeId)) {
			
			return Response.noContent().build();
		}
		return Response.notModified().build();
	}

	@Override
	@Auth
	public Response getPendingRequests(final Long userId, final Integer pageNo, final String userAgent, final String oauthToken) {
		final Optional<Collection<MenteeRequestTimeStamp>> requests = mentorService.getPendingRequests(userId, pageNo);
		if (requests.isPresent() && !requests.get().isEmpty()) {
			return Response.ok(requests.get()).build();
		}
		return Response.noContent().build();
	}

	@Override
	@Auth
	public Response cancelPendingRequest(final Long userId, final Long mentorId, final String userAgent, final String oauthToken) {
		if (mentorService.cancelPendingRequest(userId, mentorId)) {
			return Response.noContent().build();
		}
		return Response.notModified().build();
	}

	//@Auth     //no need in v1
	@Override
	public Response expertiseList(final Long userId,final Long mentorId, final Integer pageNo,
			final String userAgent, final String oauthToken, final Integer expertiseId, final Integer parentId) {
		List<Expertise> expertises=null;
		if( expertiseId !=null){
			expertises=mentorService.getExpertiseById(mentorId,expertiseId);
		} else if(parentId !=null ){
			expertises = mentorService.getExpertiseListByParentId(parentId,mentorId);
		} else if( expertiseId ==null &&  parentId ==null){
			expertises = mentorService.getExpertiseList(mentorId);
		}
		if (expertises != null && expertises.isEmpty()) {
			return Response.noContent().build();
		}
		return Response.ok(new ExpertiseList(expertises)).build();
		//return Response.ok(expertises).build();
	}


	
}
