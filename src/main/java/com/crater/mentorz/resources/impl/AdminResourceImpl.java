package com.crater.mentorz.resources.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.crater.mentorz.annotation.Admin;
import com.crater.mentorz.exception.NotFoundException;
import com.crater.mentorz.exception.UnAuthorizedException;
import com.crater.mentorz.models.admin.Action;
import com.crater.mentorz.models.admin.Rank;
import com.crater.mentorz.models.admin.SearchType;
import com.crater.mentorz.models.admin.User;
import com.crater.mentorz.models.post.Post;
import com.crater.mentorz.models.user.Feedback;
import com.crater.mentorz.models.user.Interest;
import com.crater.mentorz.resources.AdminResource;
import com.crater.mentorz.service.AdminService;
import com.crater.mentorz.service.PostService;
import com.crater.mentorz.service.UserService;
import com.google.common.base.Optional;



@Component
public class AdminResourceImpl implements AdminResource{

	final private AdminService adminService;
	final private UserService userService;
	final private PostService postService;
	
	@Autowired
	public AdminResourceImpl(final AdminService adminService,final  UserService userService,final PostService postService) {
		this.adminService = adminService;
		this.userService=userService;
		this.postService=postService;
	}

	@Override
	@Admin
	public Response login(final User user, final String userAgent, final String oauthToken) throws UnAuthorizedException {
		final User usr=adminService.login(user);
		if(usr == null ){
			//return Response.ok(Status.UNAUTHORIZED).build();
			return Response.status(Status.UNAUTHORIZED).build();
		}
		return Response.ok(usr).build();
	}

	@Override
	@Admin
	public Response allUsers(final Integer pageNo, final String userAgent, final String oauthToken) {
		return Response.ok(adminService.getUsers(pageNo).get()).build();		 
	}
	
	
	@Override
	@Admin
	public Response feedbacks(final Integer pageNo, final String userAgent, final String oauthToken) {
		final List<Feedback> feedback = adminService.getFeedback(pageNo).get();
		if (feedback.isEmpty()) {
			return Response.noContent().build();
		}
		return Response.ok(feedback).build();
	}
	

	
	@Override
	@Admin
	public Response interest(final Interest interest, final String userAgent, final String oauthToken) {
		
		final Integer interestId=adminService.addInterest(interest);
		final Map<String, Integer> map=new HashMap<>();
		map.put("interestId", interestId);
		return Response.ok().entity(map).build();
	}

	@Override
	@Admin
	public Response blockedUsers(final Integer pageNo, final String userAgent, final String oauthToken) {
		final Optional<List<com.crater.mentorz.models.user.User>> blockedUsers = adminService.getBlockedUsers(pageNo);
		if (blockedUsers.get().isEmpty()) {
			return Response.noContent().build();
		}
		return Response.ok(blockedUsers.get()).build();
	}
	
	
	@Override
	@Admin
	public Response updateUserAccountStatus(final Long userId, final Action action, final String userAgent, final String oauthToken) {
		if(adminService.updateUserAccountStatus(userId, action)) {
			userService.deleteDevice(userId);
			userService.deleteToken(userId);
			return Response.noContent().build();
		}
		return Response.notModified().build();
	}
	
	@Override
	@Admin
	public Response searchUser(final String userName, final SearchType searchType, final Integer pageNo, final String userAgent, final String oauthToken) {
	   switch (searchType) {
	    	case ALL:	
		    	final List<com.crater.mentorz.models.user.User> users = adminService.searchUsersByUserName(userName, pageNo).get();
			    return users.isEmpty() ? Response.noContent().build() : Response.ok(users).build();
		    case ABUSED:			
			    final List<Post> abusedUsers = adminService.searchAbusedUsersByUserName(userName, pageNo).get();
			    return abusedUsers.isEmpty() ? Response.noContent().build() : Response.ok(abusedUsers).build();
		    case BLOCKED:			
			    final List<com.crater.mentorz.models.user.User> blockedUsers = adminService.searchBlockedUsersByUserName(userName, pageNo).get();
			    return blockedUsers.isEmpty() ? Response.noContent().build() : Response.ok(blockedUsers).build();
		    case FEEDBACK:
			    final List<Feedback> feedback = adminService.searchFeedbackByUserName(userName, pageNo).get();
			    return feedback.isEmpty() ? Response.noContent().build() : Response.ok(feedback).build();
	      }
		return Response.noContent().build();
	}

	@Override
	@Admin
	public Response abusedPost(final Integer pageNo,final String userAgent, final String oauthToken) {
		final List<Post> abusedPosts = adminService.getAbusedPost(pageNo).get();
		if (abusedPosts.isEmpty()) {
			return Response.noContent().build();
		}
		return Response.ok(abusedPosts).build();
	}
	
	@Override
	@Admin
	public Response updatePostStatus(final Long userId, final Long postId, final Action action,final String userAgent, final String oauthToken) {
		adminService.updatePostStatus(userId, postId, action);
	    return Response.noContent().build();
	}

	@Override
	@Admin
	public Response noOfUsers(final  String userAgent, final String oauthToken) {
		final Map<String,Integer> totalUser=new HashMap<String, Integer>();
		totalUser.put("total_users", adminService.getTotalUser());
		return Response.ok().entity(totalUser).build();
	}

	@Override
	@Admin
	public Response noOfMentor(final String userAgent, final String oauthToken) {
		final Map<String,Integer> totalMentor=new HashMap<String, Integer>();
		totalMentor.put("total_mentors", adminService.getTotalMentor());
		return Response.ok().entity(totalMentor).build();
	}

	@Override
	public Response allInterest() {
		return Response.ok().entity(adminService.getInterestsList()).build();
	}

	@Override
	public Response getInterestByParerntId(final Integer parentId) {
		
		return Response.ok().entity(userService.getInterestsByParentId(parentId, 0l)).build();
	}

	@Override
	@Admin
	public Response getUserById(final Long userId, final String userAgent, final String oauthToken) throws NotFoundException {
		
			return Response.ok().entity(userService.getProfile(userId)).build();
		
		
	}

	@Override
	public Response getPostById(final Long postId, final String userAgent,final String oauthToken) {
		Optional<Post> post=postService.getPostByPostId(postId);
		if(post.isPresent()){
			return Response.ok().entity(post.get()).build();
		}
		return Response.status(Status.NOT_FOUND).build();
	}

	@Override
	public Response getAllMentors(String userAgent, String oauthToken) {
		final List<com.crater.mentorz.models.user.User> mentors=adminService.getMentors();
		if(mentors.isEmpty()){
			return Response.noContent().build();
		}
		return Response.ok(mentors).build();
	}

	@Override
	public Response getAvgOfValuesPerMentor(Long postId, String userAgent,
			String oauthToken) {
		final Map<String,Float> totalMentor=new HashMap<String, Float>();
		totalMentor.put("values_per_user", adminService.getAvgOfValuesPerMentor());
		return Response.ok().entity(totalMentor).build();
	}

	@Override
	public Response getAvgOfExpertisePerMentor(Long postId, String userAgent,
			String oauthToken) {
		final Map<String,Float> totalMentor=new HashMap<String, Float>();
		totalMentor.put("expertises_per_mentor", adminService.getAvgOfExpertisePerMentor());
		return Response.ok().entity(totalMentor).build();
	}

	@Override
	public Response getExpertisesRank() {
		final List<Rank> ranks = adminService.getExpertisesRank();
		if (ranks.isEmpty()) {
			return Response.noContent().build();
		}
		return Response.ok(ranks).build();
	}

	@Override
	public Response getValuesRank() {
		final List<Rank> ranks = adminService.getValuesRank();
		if (ranks.isEmpty()) {
			return Response.noContent().build();
		}
		return Response.ok(ranks).build();
	}

	@Override
	public Response getInterestsRank() {
		final List<Rank> ranks = adminService.getInterestsRank();
		if (ranks.isEmpty()) {
			return Response.noContent().build();
		}
		return Response.ok(ranks).build();
	}
}

