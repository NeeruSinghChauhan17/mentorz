/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.resources.impl;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.security.SignatureException;
import java.security.cert.CertificateException;
import java.time.Instant;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.crater.mentorz.annotation.Auth;
import com.crater.mentorz.annotation.Blocked;
import com.crater.mentorz.enums.UserFollowAction;
import com.crater.mentorz.exception.BadRequestException;
import com.crater.mentorz.exception.ConflitException;
import com.crater.mentorz.exception.CustomException;
import com.crater.mentorz.exception.ForbiddenException;
import com.crater.mentorz.exception.NotFoundException;
import com.crater.mentorz.exception.NotModifiedException;
import com.crater.mentorz.exception.UnAuthorizedException;
import com.crater.mentorz.models.Entity;
import com.crater.mentorz.models.Rating;
import com.crater.mentorz.models.mentor.MentorSearch;
import com.crater.mentorz.models.payment.Balance;
import com.crater.mentorz.models.payment.Payment;
import com.crater.mentorz.models.payment.Redeem;
import com.crater.mentorz.models.post.Post;
import com.crater.mentorz.models.post.PostList;
import com.crater.mentorz.models.user.DeviceInfo;
import com.crater.mentorz.models.user.Feedback;
import com.crater.mentorz.models.user.ForgetPassword;
import com.crater.mentorz.models.user.Interest;
import com.crater.mentorz.models.user.InterestList;
import com.crater.mentorz.models.user.InternationalNumber;
import com.crater.mentorz.models.user.Login;
import com.crater.mentorz.models.user.NewPassowrd;
import com.crater.mentorz.models.user.PhoneContact;
import com.crater.mentorz.models.user.PhoneContacts;
import com.crater.mentorz.models.user.Profile;
import com.crater.mentorz.models.user.ProfileImage;
import com.crater.mentorz.models.user.SendRequest;
import com.crater.mentorz.models.user.SocialInterestList;
import com.crater.mentorz.models.user.SocialSource;
import com.crater.mentorz.models.user.SocialUser;
import com.crater.mentorz.models.user.UpdatePasswordV2;
import com.crater.mentorz.models.user.User;
import com.crater.mentorz.models.user.UserData;
import com.crater.mentorz.models.user.UserList;
import com.crater.mentorz.models.user.Value;
import com.crater.mentorz.models.user.ValuesList;
import com.crater.mentorz.provider.UsersCache;
import com.crater.mentorz.resources.UserResourceV3;
import com.crater.mentorz.service.UserServiceV3;
import com.google.common.base.Optional;

@Component
//@Path("/mentorz/api/v3/user")
//@Consumes(MediaType.APPLICATION_JSON)
//@Produces(MediaType.APPLICATION_JSON)
public class UserResourceImplV3 implements UserResourceV3 {

	@Autowired
	private UserServiceV3 userService;
	final private UsersCache userCache;

	private static final Logger LOGGER = LoggerFactory.getLogger(UserResourceImplV3.class);

	@Autowired
	public UserResourceImplV3(final UsersCache userCache) {
		// this.userService = userService;
		this.userCache = userCache;
	}

	@Override
	public Response register(final User user, final String userAgent) throws ConflitException {
		userService.register(user, userAgent);
		return Response.status(Status.CREATED).build();
	}

	@Override
	public Response login(final Login login, final String userAgent)
			throws UnAuthorizedException, com.crater.mentorz.exception.CustomException {
		
		return Response.ok(userService.login(login, userAgent)).build();
	}

	@Override
	public Response socialLogin(final SocialUser socialUser, final String userAgent)
			throws com.crater.mentorz.exception.CustomException {
		return Response.ok(userService.socialLogin(socialUser, userAgent)).build();
	}

	@Override
	public Response registerSocialUser(final SocialUser socialUser, final String userAgent) throws ConflitException {
		userService.registerSocialUser(socialUser, userAgent);
		return Response.created(null).build();
	}

	// TODO need to implement verification link expiration time
	@Override
	public Response verifyEmail(final Long userId) throws NotFoundException {
		userService.verifyEmail(userId);
		return Response.noContent().build();
	}

	@Override
	@Auth
	public Response updateValues(final Long userId, final ValuesList values, final String userAgent,
			final String oauthToken) {
		userService.updateValues(userId, values.getValues());
		return Response.noContent().build();
	}

	@Override
	@Auth
	public Response updateInterests(final Long userId, final InterestList interestList, final String userAgent,
			final String oauthToken) {
		userService.updateInterests(userId, interestList);
		return Response.noContent().build();
	}

	@Override
	@Auth
	public Response getValues(final Long userId, final String userAgent, final String oauthToken, final Integer pageNo)
			throws ForbiddenException {
		final List<Value> values = userService.getValues(userId, pageNo);
		if (values.isEmpty()) {
			return Response.noContent().build();
		}
		return Response.ok(new ValuesList(values)).build();
	}

	@Override
	@Auth
	public Response getInterests(final Long userId, final String userAgent, final String oauthToken,
			final Integer pageNo) throws ForbiddenException {
		final List<Interest> interests = userService.getMyInterests(userId, pageNo);
		if (interests.isEmpty()) {
			return Response.noContent().build();
		}
		return Response.ok(new InterestList(interests)).build();
	}

	@Override
	@Auth
	public Response getProfile(final Long userId, final String userAgent, final String oauthToken)
			throws NotFoundException {
		return Response.ok(userService.getProfile(userId)).build();
	}

	@Override
	@Auth
	@Blocked
	public Response follow(final Long userId, final Long friendId, final String follow, final String userAgent,
			final String oauthToken) throws com.crater.mentorz.exception.CustomException {
		if (UserFollowAction.codeToCategory(1).getDisplayName().equals(follow)) {
			if (userService.follow(userId, friendId)) {
				return Response.noContent().build();
			}
		} else if (UserFollowAction.codeToCategory(2).getDisplayName().equals(follow)) {
			userService.unFollow(userId, friendId);
			return Response.noContent().build();
		}
		return Response.status(Status.NOT_FOUND).build();
	}

	@Override
	public Entity<Long> getFollowers(final Long userId) {
		final Long followers = userService.getFollowers(userId);
		return new Entity<Long>(followers != null ? followers : 0);
	}

	@Override
	public Response getRating(final Long userId) {
		final Double rating = userService.getRating(userId);
		final Map<String, Double> map = new HashMap<>();
		map.put("rating", rating != null ? rating : 0);
		return Response.ok(map).build();
	}

	@Override
	public Response getProfileImage(final Long userId) {
		final Profile profile = userService.getProfileImage(userId);
		if (profile == null) {
			return Response.noContent().build();
		}
		return Response.ok(profile).build();
	}

	@Override
	@Auth
	public Response getUserBoard(final Long userId, final Integer pageNo, final String userAgent,
			final String oauthToken) {
		final List<Post> comments = userService.getBoard(userId, pageNo);
		if (comments.isEmpty()) {
			return Response.noContent().build();
		}
		return Response.ok(new PostList(comments)).build();
	}

	@Override
	@Auth
	public Response updateProfile(final Long userId, final Profile profile, final String userAgent,
			final String oauthToken) {
		if (userService.updateProfile(userId, profile)) {
			return Response.noContent().build();
		}
		return Response.notModified().build();
	}

	@Override
	@Auth
	@Blocked
	public Response rate(final Long userId, final Long ratedUserId, final Rating rating, final String userAgent,
			final String oauthToken) throws ForbiddenException {
		if (userService.rate(userId, ratedUserId, rating)) {

			return Response.noContent().build();
		}
		return Response.notModified().build();
	}

	@Override
	public Response valuesList(final Integer pageNo, final String userAgent, final String oauthToken) {
		final List<Value> values = userService.getValuesList(pageNo);
		if (values.isEmpty()) {
			return Response.noContent().build();
		}
		return Response.ok(new ValuesList(values)).build();
	}

	@Override
	@Auth
	public Response interestsList(final Long userId, final Integer pageNo, final String userAgent,
			final String oauthToken, Integer interestId, Integer parentId) {
		List<Interest> interests = null;
		if (interestId != null) {
			interests = userService.getInterestsById(interestId);
		} else if (parentId != null) {
			interests = userService.getInterestsByParentId(parentId, userId);
		} else if (interestId == null && parentId == null) {
			interests = userService.getInterestsList(pageNo, userId);
		}
		if (interests != null && interests.isEmpty()) {
			return Response.noContent().build();
		}
		return Response.ok(new InterestList(interests)).build();
	}

	@Override
	@Auth
	public Response following(final Long userId, final Long followingUserId, final String userAgent,
			final String oauthToken) throws ForbiddenException {
		return Response.ok(new Entity<Integer>(userService.isFollowing(userId, followingUserId))).build();
	}

	@Override
	//// @Auth
	public Response getGETSignedUrl(final String objectName, final String userAgent, final String oauthToken)
			throws IllegalArgumentException, SignatureException, IOException, CustomException,
			com.cz.googlecontentapi.exception.CustomException {
		return Response.ok(new Entity<String>(userService.getSignedUrl("GET", objectName))).build();
	}

	@Override
	//// @Auth
	public Response getPUTSignedUrl(final String objectName, final String userAgent, final String oauthToken)
			throws IllegalArgumentException, SignatureException, IOException, CustomException,
			com.cz.googlecontentapi.exception.CustomException {
		return Response.ok(new Entity<String>(userService.getSignedUrl("PUT", objectName))).build();
	}

	@Override
	//// @Auth
	public Response getDELETESignedUrl(final String objectName, final String userAgent, final String oauthToken)
			throws IllegalArgumentException, SignatureException, IOException, CustomException,
			com.cz.googlecontentapi.exception.CustomException {
		return Response.ok(new Entity<String>(userService.getSignedUrl("DELETE", objectName))).build();
	}

	@Override
	//// @Auth
	public Response getSessionUrl(final String objectName, final String contentType, final String userAgent,
			final String oauthToken) throws CertificateException, IOException, CustomException,
			com.cz.googlecontentapi.exception.CustomException {
		return Response.ok(new Entity<String>(userService.getSessionUrl(objectName, contentType))).build();
	}

	@Override
	@Auth
	public Response updateProfileImage(final Long userId, final ProfileImage profileImage, final String userAgent,
			final String oauthToken) {
		if (userService.updateProfileImage(userId, profileImage)) {
			return Response.noContent().build();
		}
		return Response.notModified().build();
	}

	@Override
	@Auth
	@Blocked
	public Response sendMentorRequest(final Long userId, final Long mentorId, final SendRequest info,
			final String userAgent, final String oauthToken) throws BadRequestException, ForbiddenException, NotModifiedException, ConflitException {
		if (userService.sendRequest(userId, mentorId, info)) {
			return Response.noContent().build();
		}
		return Response.notModified().build();
	}

	@Override
	@Auth
	public Response getAccessToken(final Long userId, final String deviceId, final String userAgent,
			final String oauthToken) {
		if (deviceId == null) {
			return Response.status(HttpURLConnection.HTTP_BAD_REQUEST).entity("DeviceId is mandatory in query peram")
					.build();
		}
		return Response.ok(new Entity<String>(userService.getAccessToken(userId, deviceId))).build();
	}

	@Override
	@Auth
	public Response getMyMentor(final Long userId, final String userAgent, final String oauthToken, final Long friendId,
			final Integer pageNo) {
		final Optional<List<User>> requests;
		if (friendId != null) {
			requests = userService.getMyMentor(friendId, pageNo);
		} else {
			requests = userService.getMyMentor(userId, pageNo);
		}
		if (requests.isPresent() && !requests.get().isEmpty()) {
			return Response.ok(new UserList(requests.get())).build();
		}
		return Response.noContent().build();
	}

	@Override
	@Auth
	public Response getMyMentee(final Long userId, final String userAgent, final String oauthToken, final Long friendId,
			final Integer pageNo) {
		final Optional<List<User>> requests;
		if (friendId != null) {
			requests = userService.getMyMentee(friendId, pageNo);
		} else {
			requests = userService.getMyMentee(userId, pageNo);
		}
		if (requests.isPresent() && !requests.get().isEmpty()) {
			return Response.ok(new UserList(requests.get())).build();
		}
		return Response.noContent().build();
	}

	@Override
	@Auth
	public Response listFollowers(final Long userId, final String userAgent, final String oauthToken,
			final Long freindId, Integer pageNo) {
		Optional<List<User>> requests;
		if (freindId != null) {
			requests = userService.listFollowers(freindId, pageNo);
		} else {
			requests = userService.listFollowers(userId, pageNo);
		}
		if (requests.isPresent() && !requests.get().isEmpty()) {
			return Response.ok(new UserList(requests.get())).build();
		}
		return Response.noContent().build();
	}

	@Override
	@Auth
	public Response listFollowing(final Long userId, final String userAgent, final String oauthToken,
			final Long freindId, final Integer pageNo) {
		Optional<List<User>> requests;
		if (freindId != null) {
			requests = userService.listFollowing(freindId, pageNo);
		} else {
			requests = userService.listFollowing(userId, pageNo);
		}
		if (requests.isPresent() && !requests.get().isEmpty()) {
			return Response.ok(new UserList(requests.get())).build();
		}
		return Response.noContent().build();
	}

	@Override
	@Auth
	public Response addOrUpdateSocialInterest(final Long userId, final String userAgent, final String oauthToken,
			final SocialInterestList interestList, final SocialSource socialSource) {
		userService.deleteAndAddUserSocialInterests(userId, interestList.getData(), socialSource);
		return Response.ok().build();
	}

	@Override
	@Auth
	public Response credit(final Long userId, final String userAgent, final String oauthToken, final Payment payment) {
		final Boolean isCredit = userService.credit(payment);
		if (isCredit) {
			return Response.ok().build();
		}
		return Response.notModified().build();

	}

	@Override
	@Auth
	public Response debit(final Long userId, final String userAgent, final String oauthToken, final Payment payment,
			final Long mentorId) {
		final Boolean isCredit = userService.debit(payment, mentorId);
		if (isCredit) {
			return Response.ok().build();
		}
		return Response.notModified().build();
	}

	@Override
	@Auth
	public Response getBalance(final Long userId, final String userAgent, final String oauthToken,
			final Long mentorId) {
		final Optional<Balance> balance = userService.getBalance(userId, mentorId);
		if (balance.isPresent()) {
			return Response.ok().entity(balance.get()).build();
		}
		return Response.noContent().build();
	}

	@Override
	@Auth
	public Response redeem(final Long userId, final String userAgent, final String oauthToken, final Redeem redeem) {
		final Boolean isCredit = userService.redeem(redeem);
		if (isCredit) {
			return Response.ok().build();
		}
		return Response.notModified().build();
	}

	@Override
	@Auth
	public Response feedback(final Long userId, final Feedback feedback, final String userAgent,
			final String oauthToken) {
		userService.feedback(userId, feedback);
		return Response.noContent().build();
	}

	@Override
	@Auth
	public Response blockUser(final Long userId, final Long friendId, final String userAgent, final String oauthToken) {
		if (userService.blockUser(userId, friendId) > 0) {
			userService.unFollow(userId, friendId);
			userService.unFollow(friendId, userId);
			userCache.addToBlockedUser(userId, friendId);
			return Response.ok().build();
		}
		return Response.notModified().build();
	}

	@Override
	@Auth
	public Response getBlockedUsers(final Long userId, final Integer pageNo, final String userAgent,
			final String oauthToken) {
		final Optional<List<User>> users = userService.getBlockedUsers(userId, pageNo);
		if (users.get().isEmpty()) {
			return Response.noContent().build();
		}
		return Response.ok(users.get()).build();
	}

	@Override
	@Auth
	public Response unBlockUser(final Long userId, final Long friendId, final String userAgent,
			final String oauthToken) {
		if (userService.unBlockUser(userId, friendId) > 0) {
			userCache.removedFromBlockedUser(userId, friendId);
			return Response.ok().build();
		}
		return Response.notModified().build();
	}

	@Override
	@Auth
	@Blocked
	public Response getFriendProfile(final Long userId, final Long friendId, final String userAgent,
			final String oauthToken) throws NotFoundException {
		return Response.ok(userService.getProfile(friendId)).build();
	}

	@Override
	@Auth
	public Response findFollowing(final Long userId, final String userAgent, final String oauthToken) {
		Collection<User> users = userService.findFollowing(userId);
		if (users.isEmpty()) {
			return Response.noContent().build();
		}
		return Response.ok(users).build();
	}

	@Override
	//// @Auth
	public Response getReviews(final Long userId, final Integer pageNo, final String userAgent,
			final String oauthToken) {
		final Optional<Collection<User>> users = userService.getReviews(userId, pageNo);
		if (users.isPresent() && !users.get().isEmpty()) {
			return Response.ok(users.get()).build();
		}
		return Response.noContent().build();
	}

	@Override
	@Auth
	public Response updateDeviceInfo(final Long userId, final DeviceInfo deviceInfo, final String userAgent,
			final String oauthToken) {
		userService.updateDeviceInfo(userId, DeviceInfo.createDeviceInfoWithUserAgent(userAgent, deviceInfo));
		return Response.ok().build();
	}

	@Override
	@Auth
	public Response logout(final Long userId, final String userAgent, final String oauthToken)
			throws UnAuthorizedException {
		if (userService.logout(userId, userAgent)) {
			return Response.ok().build();
		}
		return Response.notModified().build();
	}

	@Override
	public Response forgetPassword(final ForgetPassword forgetPassword)
			throws com.crater.mentorz.exception.CustomException {
		userService.forgetPassword(forgetPassword);
		return Response.ok().build();
	}

	@Override
	public Response resetPassword(final NewPassowrd newPassowrd) {
		final Boolean isValid = userService.resetPassword(newPassowrd);
		if (isValid) {
			return Response.ok().build();
		}
		return Response.status(Status.PRECONDITION_FAILED).build();
	}

	@Override
	@Auth
	public Response accountStatus(Long userId, String userAgent, String oauthToken) throws UnAuthorizedException {
		final Boolean status = userService.getAccountStatus(userId);
		final Map<String, Boolean> map = new HashMap<>();
		map.put("isInActive", status);
		return Response.ok().entity(map).build();
	}

	@Override
	public Response getCurrentTime() {
		final Long time = Instant.now().toEpochMilli();
		final Map<String, Long> map = new HashMap<>();
		map.put("current_time", time);
		return Response.ok().entity(map).build();
	}

	@Override
	@Auth
	public Response syncInterest(Long userId, Long time, String userAgent, String oauthToken)
			throws UnAuthorizedException {
		final List<Interest> interests = userService.syncInterest(time);
		return Response.ok(new InterestList(interests)).build();
	}

	@Override
	public Response saveUserAtEs(User user) {
		userService.saveUserDocument();
		return Response.noContent().build();
	}

	@Auth
	@Override
	public Response skipMentor(Long userId, Long mentorId, String userAgent, String oauthToken) {
		userService.skipMentor(userId, mentorId);
		return Response.noContent().build();
	}

	@Auth
	@Override
	public Response searchMentorByString(Long userId, String mentorName, String userAgent, String oauthToken) {
		final List<MentorSearch> mentors = userService.searchMentorByString(userId, mentorName);
		if (mentors.isEmpty()) {
			return Response.noContent().build();
		}
		return Response.ok(mentors).build();
	}

	@Auth
	@Override
	public Response addContacts(final PhoneContacts contacts, final Long userId, final Short isDelta)
			throws UnAuthorizedException, BadRequestException {
		userService.addContacts(contacts.getContacts(), userId, isDelta);
		return Response.noContent().build();
	}

	@Override
	public Response verifyUser(InternationalNumber number) throws NotFoundException {
		userService.verifyUser(number);
		return Response.noContent().build();
	}

	@Auth
	@Override
	public Response updateContact(InternationalNumber updatecontact, Long userId, String userAgent, String oauthToken)
			throws ConflitException {

		userService.updateContact(updatecontact, userId);
		return Response.noContent().build();
	}

	@Auth
	@Override
	public Response getContactList(Long userId, String userAgent, String oauthToken) {

		List<PhoneContact> contacts = userService.getContactsToInvite(userId);
		if (contacts.isEmpty()) {
			return Response.noContent().build();
		}
		return Response.ok(contacts).build();
	}

	@Override
	public Response updatePassword(UpdatePasswordV2 updatepassword) throws UnAuthorizedException, NotFoundException {
		userService.updatePassword(updatepassword);
		return Response.noContent().build();

	}

	@Override     
	public Response isNoExists(InternationalNumber number) {
		Optional<SocialUser> user= userService.getUserByNo(number);
		if (user.isPresent()) {
			return Response.ok(user.get()).build();
		}
		return Response.status(Status.NOT_FOUND).build();
	}

	@Override
	public Response getUser(Long userId) throws IllegalArgumentException, SignatureException, IOException,
			com.cz.googlecontentapi.exception.CustomException {
		UserData userdata = userService.getUser(userId);
		if (userdata!=null) {
			return Response.ok(userdata).build();
		} else {
			return Response.noContent().build();
		}
	}

}
