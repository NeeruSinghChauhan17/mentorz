package com.crater.mentorz.resources.impl;

import java.util.Collection;

import javax.ws.rs.GET;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.crater.mentorz.models.user.SubscribeUser;
import com.crater.mentorz.resources.SubscribeUserResource;
import com.crater.mentorz.service.SubscribeUserService;


@Component
public class SubscribeUserResouceImpl implements SubscribeUserResource {

	private final SubscribeUserService subscribeUserService;

	@Autowired
	public SubscribeUserResouceImpl(final SubscribeUserService subscribeUserService) {
		this.subscribeUserService = subscribeUserService;
	}

	@Override
	public Response subscribeUser(SubscribeUser subscribeUser) {
		subscribeUserService.subscribeUser(subscribeUser);
		return Response.ok().build();
	}

	@Override
	@GET
	public Collection<SubscribeUser> getAllSubscribedUser() {
		return null;
	}

}
