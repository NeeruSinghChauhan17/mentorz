/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.resources;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.hibernate.validator.constraints.NotBlank;

import com.crater.mentorz.annotation.Auth;
import com.crater.mentorz.annotation.Blocked;
import com.crater.mentorz.exception.ForbiddenException;
import com.crater.mentorz.models.mentor.BeAMentor;
import com.crater.mentorz.models.mentor.MentorFilter;


@Path("/mentorz/api/v2/{userId}/mentor")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public interface MentorResourceV2 extends Handler{

	@Auth
	@POST
	Response beAMentor(@PathParam("userId") final  Long userId, @NotNull @Valid  final BeAMentor mentor,
			@NotBlank @HeaderParam(HEADER_PARAM_USER_AGENT) final String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) final String oauthToken);
	
	@Auth
	@POST
	@Path("/find")
	Response getMentor(@PathParam("userId") final Long userId, @Valid final MentorFilter filter,
			@NotBlank @HeaderParam(HEADER_PARAM_USER_AGENT) final String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN)  final String oauthToken, 
			@DefaultValue("0") @QueryParam("offset")  final Integer offset,
			@DefaultValue("20") @QueryParam("limit")  final Integer limit);
	
	//@Auth
	@GET
	@Path("/{mentorId}/expertise")
	Response getMentorExpertise(@PathParam("userId") final Long userId,
			@NotBlank @HeaderParam(HEADER_PARAM_USER_AGENT) final String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN)  final String oauthToken,
			@PathParam("mentorId")  final Long  mentorId) throws ForbiddenException;
	
	@GET
	@Path("/{mentorId}/expertises")
	Response expertiseList(@PathParam("userId") Long userId,@PathParam("mentorId") Long  mentorId,@DefaultValue("0") @QueryParam("pageNo") Integer pageNo, @HeaderParam(HEADER_PARAM_USER_AGENT) String userAgent,
			 @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) String oauthToken,@QueryParam("interestId") Integer id,@QueryParam("parentId") Integer parentId);
	
	
	@Auth
	@GET
	@Path("/bio")
	Response getPublicBio(@PathParam("userId") Long userId,
			@NotBlank @HeaderParam(HEADER_PARAM_USER_AGENT) String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) String oauthToken) throws ForbiddenException;
	
	@Auth
	@GET
	@Path("/mentee/requests")
	Response getMenteeRequests(@PathParam("userId") Long userId,
			@DefaultValue("0") @QueryParam("pageNo") Integer pageNo,
			@NotBlank @HeaderParam(HEADER_PARAM_USER_AGENT) String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) String oauthToken);
	
	@Auth
	@GET
	@Path("/pending/requests")
	Response getPendingRequests(@PathParam("userId") Long userId,
			@DefaultValue("0") @QueryParam("pageNo") Integer pageNo,
			@NotBlank @HeaderParam(HEADER_PARAM_USER_AGENT) String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) String oauthToken);
	
	
	@Auth
	@POST
	@Blocked
	@Path("/accept/request/{menteeId}")
	Response addMentee(@PathParam("userId") Long userId, @PathParam("menteeId") Long menteeId,
			@NotBlank @HeaderParam(HEADER_PARAM_USER_AGENT) String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) String oauthToken)
			throws ForbiddenException;
	
	@GET
	@Path("/{mentorId}/request/status")
	Response getRequestStatus(@PathParam("userId") Long userId, @PathParam("mentorId") Long mentorId,
			@NotBlank @HeaderParam(HEADER_PARAM_USER_AGENT) String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) String oauthToken);
	
	@Auth
	@DELETE
	@Path("/delete/request/{menteeId}")                           //delete request by mentor
	Response deleteRequest(@PathParam("userId") Long userId, @PathParam("menteeId") Long menteeId,
			@NotBlank @HeaderParam(HEADER_PARAM_USER_AGENT) String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) String oauthToken);
	
	@Auth
	@DELETE
	@Path("/cancel/request/{mentorId}")                             //cancel request by user
	Response cancelPendingRequest(@PathParam("userId") Long userId,
			@PathParam("mentorId") Long mentorId,
			@NotBlank @HeaderParam(HEADER_PARAM_USER_AGENT) String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) String oauthToken);
	
	
	
}
