/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.resources;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.hibernate.validator.constraints.NotBlank;

import com.crater.mentorz.annotation.Auth;
import com.crater.mentorz.annotation.Blocked;
import com.crater.mentorz.annotation.PostBlocked;
import com.crater.mentorz.exception.ForbiddenException;
import com.crater.mentorz.models.post.AbuseType;
import com.crater.mentorz.models.post.Comment;
import com.crater.mentorz.models.post.PostV2;


@Path("/mentorz/api/v2")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public interface PostResourceV2 extends Handler{

	@Auth
	@PUT
	@Path("/{userId}/post")
	PostV2 addPost(@PathParam("userId") final Long userId, @NotNull  final PostV2 post,
			@NotBlank @HeaderParam(HEADER_PARAM_USER_AGENT) final String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) final String oauthToken);

	@Auth
	@POST
	@Path("/{userId}/post/{postId}/like")
	@PostBlocked
	Response like(@PathParam("userId") final Long userId,
			@PathParam("postId") final Long postId,
			@NotBlank @HeaderParam(HEADER_PARAM_USER_AGENT) final String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN)  final String oauthToken)
			throws ForbiddenException;

	@Auth
	@POST
	@Path("/{userId}/post/{postId}/view")
	Response view(@PathParam("userId") final Long userId, 
			@PathParam("postId") final Long postId,
			@NotBlank @HeaderParam(HEADER_PARAM_USER_AGENT) final String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) final String oauthToken)
			throws ForbiddenException;

	@Auth
	@POST
	@Path("/{userId}/post/{postId}/share")
	Response share(@PathParam("userId") final Long userId, 
			@PathParam("postId") final Long postId,
			@NotBlank @HeaderParam(HEADER_PARAM_USER_AGENT) final String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) final String oauthToken)
			throws ForbiddenException;

	@Auth
	@PUT
	@Path("/{userId}/post/{postId}/comment")
	Comment comment(@PathParam("userId") final Long userId, @PathParam("postId") final  Long postId,
			@NotNull @Valid final Comment comment,
			@NotBlank @HeaderParam(HEADER_PARAM_USER_AGENT) final  String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN)  final String oauthToken) throws ForbiddenException;

	@GET
	@Path("/{userId}/post")
	Response getPostByUserId(@PathParam("userId") final  Long userId,
			@DefaultValue("0") @QueryParam("pageNo") final Integer pageNo);

	@Auth
	@GET
	@Path("/{userId}/post/{postId}/comments")
	Response getComments(@PathParam("userId") final  Long userId, 
			@PathParam("postId") final  Long postId,
			@DefaultValue("0") @QueryParam("pageNo") final  Integer pageNo,
			@NotBlank @HeaderParam(HEADER_PARAM_USER_AGENT) final String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) final String oauthToken);
	
	@Auth
	@DELETE
	@Path("/{userId}/post/{postId}/comment/{commentId}")
	Response deleteComment(@PathParam("userId") final Long userId,
			@PathParam("postId") final Long postId, 
			@PathParam("commentId") final  Long commentId,
			@NotBlank @HeaderParam(HEADER_PARAM_USER_AGENT) final String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) final String oauthToken) throws ForbiddenException;
	
	@GET
	@Auth
	@Blocked
	@Path("/{userId}/post/friend/{friendId}")
	Response getFriendPostByUserId(@PathParam("userId") final Long userId, 
			@PathParam("friendId")  final Long friendId,
			@DefaultValue("0") @QueryParam("pageNo") final Integer pageNo);
	
	@POST
	@Auth
	@Path("/{userId}/post/{postId}/abuse")
	Response abusePost(@PathParam("userId") final Long userId,
			@PathParam("postId") final Long postId, 
			@DefaultValue("SPAM") @QueryParam("abuse_type")  final AbuseType abuseType,
			@NotBlank @HeaderParam(HEADER_PARAM_USER_AGENT)  final String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) final String oauthToken) throws ForbiddenException;
	
	@GET
	@Auth
	@Path("/{userId}/post/search")
	Response searchPostByInterest(@PathParam("userId") final Long userId,
			@QueryParam("interest") final List<Integer> interest,
			@DefaultValue("0") @QueryParam("pageNo") final Integer pageNo,
			@NotBlank @HeaderParam(HEADER_PARAM_USER_AGENT) final String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) final String oauthToken);
	
	@GET
	@Path("/post/{postId}")
	Response getPostByPostIdInShareOut(@PathParam("postId") final Long postId);               ///api used in mentorz-web-shareout
	
	@GET
	@Path("/{userId}/post/{postId}")
	Response getPostByPostId(@PathParam("userId") final Long userId,@PathParam("postId") final Long postId);
	
	@GET()
	@Path("/{userId}/post/{postId}/comment/{commentId}")
	Response getCommentByCommentId(@PathParam("userId") final Long userId,
			@PathParam("postId") final  Long postId,
			@PathParam("commentId")  final Long commentId,
			@NotBlank @HeaderParam(HEADER_PARAM_USER_AGENT) final  String userAgent,
			@NotBlank @HeaderParam(HEADER_PARAM_OAUTH_TOKEN) final String oauthToken);
}
