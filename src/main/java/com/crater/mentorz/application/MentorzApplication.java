package com.crater.mentorz.application;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.filter.RequestContextFilter;

import com.crater.mentorz.filter.CORSResponseFilter;
import com.crater.mentorz.healthcheck.MentorzHealthCheck;
import com.crater.mentorz.provider.CustomExceptionMapper;
import com.crater.mentorz.provider.filter.AdminAuthenticationFilter;
import com.crater.mentorz.provider.filter.AuthorizationRequestFilter;
import com.crater.mentorz.provider.filter.BlockedPostAuthenticationFilter;
import com.crater.mentorz.provider.filter.BlockedUserAuthenticationFilter;
import com.crater.mentorz.resources.AdminResource;
import com.crater.mentorz.resources.MentorResource;
import com.crater.mentorz.resources.MentorResourceV2;
import com.crater.mentorz.resources.MentorResourceV3;
import com.crater.mentorz.resources.PostResource;
import com.crater.mentorz.resources.PostResourceV2;
import com.crater.mentorz.resources.SubscribeUserResource;
import com.crater.mentorz.resources.UserResource;
import com.crater.mentorz.resources.UserResourceV2;
import com.crater.mentorz.resources.UserResourceV3;
import com.crater.mentorz.resources.impl.AdminResourceImpl;
import com.crater.mentorz.resources.impl.MentorResourceImpl;
import com.crater.mentorz.resources.impl.MentorResourceImplV2;
import com.crater.mentorz.resources.impl.MentorResourceImplV3;
import com.crater.mentorz.resources.impl.PostResourceImpl;
import com.crater.mentorz.resources.impl.PostResourceImplV2;
import com.crater.mentorz.resources.impl.SubscribeUserResouceImpl;
import com.crater.mentorz.resources.impl.UserResourceImpl;
import com.crater.mentorz.resources.impl.UserResourceImplV2;
import com.crater.mentorz.resources.impl.UserResourceImplV3;
import com.google.common.collect.Lists;
import com.mchange.v2.c3p0.ComboPooledDataSource;

import io.dropwizard.Application;
import io.dropwizard.lifecycle.Managed;
import io.dropwizard.setup.Environment;


public class MentorzApplication extends
		Application<MentorzApplicationConfiguration> implements Managed {
	private ClassPathXmlApplicationContext classPathXmlApplicationContext;

	public static void main(String[] args) throws Exception {
		new MentorzApplication().run(Lists.newArrayList("server", "mentorz.yaml")
				.toArray(new String[2]));
	}

	@Override
	public void run(MentorzApplicationConfiguration config, Environment env) throws Exception {
		classPathXmlApplicationContext = new ClassPathXmlApplicationContext(
				"spring/application-config.xml");

		/**
		 * Add Health Check Service
		 */
		final MentorzHealthCheck healthCheck = classPathXmlApplicationContext.getBean(MentorzHealthCheck.class);
		env.healthChecks().register("mentorz Health Check", healthCheck);
		/**
		 * Register Filter
		 */
		final AuthorizationRequestFilter authorizationFilter = classPathXmlApplicationContext
				.getBean(AuthorizationRequestFilter.class);
		
		final BlockedUserAuthenticationFilter blockedUserAuthenticationFilter = classPathXmlApplicationContext
				.getBean(BlockedUserAuthenticationFilter.class);
		
		final AdminAuthenticationFilter adminAuthenticationFilter = classPathXmlApplicationContext
				.getBean(AdminAuthenticationFilter.class);
		
		
		final BlockedPostAuthenticationFilter blockedPostAuthenticationFilter=classPathXmlApplicationContext
				.getBean(BlockedPostAuthenticationFilter.class);
		env.jersey().register(blockedPostAuthenticationFilter);
		
		env.jersey().register(authorizationFilter);
		env.jersey().register(blockedUserAuthenticationFilter);
		env.jersey().register(adminAuthenticationFilter);
		
		/**
		 * Register Exception Mapper
		 */
		env.jersey().register(CustomExceptionMapper.class);
		
		env.jersey().register(RequestContextFilter.class);
		final UserResource userResource = classPathXmlApplicationContext.getBean(UserResourceImpl.class);
		final UserResourceV2 userResource2 = classPathXmlApplicationContext.getBean(UserResourceImplV2.class);
		final UserResourceV3 userResource3 = classPathXmlApplicationContext.getBean(UserResourceImplV3.class);
		final PostResource postResource = classPathXmlApplicationContext.getBean(PostResourceImpl.class);
		final PostResourceV2 postResource1 = classPathXmlApplicationContext.getBean(PostResourceImplV2.class);
		final MentorResource mentorResource = classPathXmlApplicationContext.getBean(MentorResourceImpl.class);
		final MentorResourceV2 mentorResource1 = classPathXmlApplicationContext.getBean(MentorResourceImplV2.class);
		final MentorResourceV3 mentorResource2 = classPathXmlApplicationContext.getBean(MentorResourceImplV3.class);
		final SubscribeUserResource subscribeUserResource = classPathXmlApplicationContext.getBean(SubscribeUserResouceImpl.class);
		final AdminResource adminResource = classPathXmlApplicationContext.getBean(AdminResourceImpl.class);
		
		env.jersey().register(userResource);
		env.jersey().register(userResource2);
		env.jersey().register(userResource3);
		
		env.jersey().register(postResource);
		env.jersey().register(mentorResource);
		env.jersey().register(postResource1);
		env.jersey().register(mentorResource1);
		env.jersey().register(mentorResource2);
		env.jersey().register(subscribeUserResource);
		env.jersey().register(CORSResponseFilter.class);
		env.jersey().register(adminResource);
		env.lifecycle().manage(this);
	}

	@Override
	public void start() throws Exception {
		// Auto-generated method stub

	}

	@Override
	public void stop() throws Exception {
		final ComboPooledDataSource comboPooledDataSource = classPathXmlApplicationContext
				.getBean(ComboPooledDataSource.class);
		comboPooledDataSource.close();
		classPathXmlApplicationContext.close();
	}
}
