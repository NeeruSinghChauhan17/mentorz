package com.crater.mentorz.http;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.crater.mentorz.models.push.Notification;

public class HttpCallMediator {

	public static final String SERVER_URL = "http://localhost:1990/notification/api/v1/notify";

	private RestTemplate restTemplate;
	private static final Logger LOGGER = LoggerFactory.getLogger(HttpCallMediator.class);

	@Autowired
	public HttpCallMediator(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	public void sendPush(final Notification notification) {

		final HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.setContentType(MediaType.APPLICATION_JSON);
		final HttpEntity<Notification> entity = new HttpEntity<Notification>(notification, headers);
		try {
			final ResponseEntity<String> response = restTemplate.exchange(SERVER_URL, HttpMethod.POST, entity,
					String.class);
			if (response.getStatusCode() == HttpStatus.NO_CONTENT) {
			} else {
				LOGGER.error("==============ERROR WHILE SEND PUSH NOTIFICATIONS ,STATUS ============"
						+ response.getStatusCode());
			}

		} catch (Exception e) {
			LOGGER.error("==============EXCEPTION WHILE SEND PUSH NOTIFICATIONS ============" + e.getMessage());
		}

	}

}
