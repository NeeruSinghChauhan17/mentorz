package com.crater.mentorz.models.admin;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @Auther Harish Anuragi
 */

@JsonInclude(Include.NON_NULL)
public class User {

	private String name;
	private String password;
	private String token;

	@SuppressWarnings("unused")
	private User() {
		this(null, null, null);
	}

	public User(final String name, final String password, final String token) {
		this.name = name;
		this.password = password;
		this.token = token;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("password")
	public String getPassword() {
		return password;
	}

	@JsonProperty("auth_token")
	public String getToken() {
		return token;
	}
}
