package com.crater.mentorz.models.admin;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Rank {

	private final Integer rankCount;
	private final String  name;
	
	@SuppressWarnings("unused")
	private Rank(){
		this(null, null);
	}
	
	public Rank(final Integer rankCount, final String name) {
		this.rankCount = rankCount;
		this.name = name;
	}
	
	@JsonProperty("rank_count")
	public  Integer getRankCount() {
		return rankCount;
	}
	
	@JsonProperty("name")
	public  String getName() {
		return name;
	}
}
