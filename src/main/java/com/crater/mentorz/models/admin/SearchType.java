package com.crater.mentorz.models.admin;

/**
 *
 * @Auther Harish Anuragi
 */
public enum SearchType {

	ALL, ABUSED, BLOCKED, FEEDBACK;

	public static int getIntValue(SearchType searchType) {
		switch (searchType) {
		case ALL:
			return 0;
		case ABUSED:
			return 1;
		case BLOCKED:
			return 2;
		case FEEDBACK:
			return 3;
		default:
			return 0;
		}
	}

	public static SearchType getSearchType(Integer value) {
		switch (value) {
		case 0:
			return ALL;
		case 1:
			return ABUSED;
		case 2:
			return BLOCKED;
		case 3:
			return FEEDBACK;
		default:
			return ALL;
		}
	}
}
