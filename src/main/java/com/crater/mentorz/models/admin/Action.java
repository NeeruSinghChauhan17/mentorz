package com.crater.mentorz.models.admin;

/**
 * 
 * @author Harish Anuragi
 *
 */
public enum Action {
	ACTIVE, INACTIVE;

	public static int getIntValue(Action action) {
		switch (action) {
		case ACTIVE:
			return 0;
		case INACTIVE:
			return 1;
		default:
			return 0;
		}
	}

	public static Action getActionType(Integer value) {
		switch (value) {
		case 0:
			return ACTIVE;
		case 1:
			return INACTIVE;
		default:
			return ACTIVE;
		}
	}
}
