/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.models;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Harish Anuragi
 * 
 */
public class Entity<T> {

	private T value;

	@SuppressWarnings("unused")
	private Entity() {
		this(null);
	}

	public Entity(T value) {
		this.value = value;
	}

	@JsonProperty("value")
	public T getValue() {
		return value;
	}

}
