/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.models.post;


public enum MediaType {
	TEXT, AUDIO, VIDEO, IMAGE;

	public static int getIntValue(MediaType mediaType) {
		switch (mediaType) {
		case TEXT:
			return 0;
		case AUDIO:
			return 1;
		case VIDEO:
			return 2;
		case IMAGE:
			return 3;
		default:
			return 0;
		}
	}

	public static MediaType getMediaType(int value) {
		switch (value) {
		case 0:
			return TEXT;
		case 1:
			return AUDIO;
		case 2:
			return VIDEO;
		case 3:
			return IMAGE;
		default:
			return TEXT;
		}
	}
}
