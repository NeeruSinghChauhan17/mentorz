/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.models.post;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author Harish Anuragi
 *
 */
public class CommentList {

	private List<Comment> commentList;

	@SuppressWarnings("unused")
	private CommentList() {
		this(new ArrayList<Comment>());
	}

	public CommentList(List<Comment> commentList) {
		this.commentList = commentList;
	}

	@JsonProperty("comment_list")
	public List<Comment> getCommentList() {
		return commentList;
	}

}
