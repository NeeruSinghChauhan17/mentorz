/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.models.post;

import java.time.Instant;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Harish Anuragi
 * 
 */
@JsonInclude(Include.NON_NULL)
public class Comment {

	private long commentId;

	@NotBlank
	private String comment;

	private Long commentTime;

	private long userId;
	private String userName;
	private String lresId;
	private String hresId;

	@SuppressWarnings("unused")
	private Comment() {
		this(0, null, Instant.now().toEpochMilli(), 0, null, null, null);

	}

	public Comment(long commentId, String comment, Long commentTime, long userId, String userName, String lresId, String hresId) {
		this.commentId = commentId;
		this.comment = comment;
		this.commentTime = commentTime;
		this.userId = userId;
		this.userName = userName;
		this.lresId = lresId;
		this.hresId = hresId;
	}

	@JsonProperty("comment_id")
	public long getCommentId() {
		return commentId;
	}

	@JsonProperty("comment")
	public String getComment() {
		return comment;
	}

	@JsonProperty("comment_time")
	public Long getCommentTime() {
		return commentTime;
	}

	@JsonProperty("user_id")
	public long getUserId() {
		return userId;
	}

	@JsonProperty("name")
	public String getUserName() {
		return userName;
	}

	@JsonProperty("lres_id")
	public String getLresId() {
		return lresId;
	}

	@JsonProperty("hres_id")
	public String getHresId() {
		return hresId;
	}

	public static Comment createWithCommentId(final long commentId, final Comment comment, final long userId) {
		return new Comment(commentId, comment.getComment(), comment.getCommentTime(), userId, comment.getUserName(), comment.getLresId(),
				comment.getHresId());
	}
}
