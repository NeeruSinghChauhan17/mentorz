/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.models.post;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;


public class Content {

	@JsonIgnore
	private long contentId;

	@NotBlank
	private MediaType mediaType;

	private String lresId;

	private String hresId;

	private String description;

	@SuppressWarnings("unused")
	private Content() {
		this(0, null, null, null, null);
	}

	public Content(long contentId, MediaType mediaType, String lresId, String hresId, String description) {
		this.contentId = contentId;
		this.mediaType = mediaType;
		this.lresId = lresId;
		this.hresId = hresId;
		this.description = description;
	}

	@JsonProperty("content_id")
	public long getContentId() {
		return contentId;
	}

	@JsonProperty("media_type")
	public MediaType getMediaType() {
		return mediaType;
	}

	@JsonProperty("lres_id")
	public String getLresId() {
		return lresId;
	}

	@JsonProperty("hres_id")
	public String getHresId() {
		return hresId;
	}

	@JsonProperty("description")
	public String getDescription() {
		return description;
	}

	public static Content createWithContentId(final long contentId, final Content content) {
		return new Content(contentId, content.getMediaType(), content.getLresId(), content.getHresId(), content.getDescription());
	}

}
