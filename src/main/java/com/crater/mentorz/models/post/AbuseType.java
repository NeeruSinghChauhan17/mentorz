package com.crater.mentorz.models.post;
/**
 *
 * @Auther Harish Anuragi
 */
public enum AbuseType {
	INAPPROPRIATE_CONTENT, SPAM;

	public static int getIntValue(AbuseType abuseType) {
		switch (abuseType) {
		case INAPPROPRIATE_CONTENT:
			return 1;
		case SPAM:
			return 2;
		default:
			return 2;
		}
	}

	public static AbuseType getAbuseType(int value) {
		switch (value) {
		case 1:
			return INAPPROPRIATE_CONTENT;
		case 2:
			return SPAM;
		default:
			return SPAM;
		}
	}
}

