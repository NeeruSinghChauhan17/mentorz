/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.models.post;

import java.time.Instant;
import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonInclude(Include.NON_NULL)
public class Post {

	private long postId;
	private long userId;
	private int likeCount;
	private Long shareTime;
	private Boolean liked;
	private Content content;
	private ArrayList<Comment> comments;
	private int viewCount;
	private int shareCount;
	private int commentCount;
	private String name;
	private Integer abuseCount;
	private Boolean isArchived;

	@SuppressWarnings("unused")
	private Post() {
		this(0, 0, 0, Instant.now().toEpochMilli(), null, null, null, 0, 0, 0, null, null, null);
	}

	public Post(long postId, long userId, int likeCount, Long shareTime, Boolean liked, Content content, ArrayList<Comment> comments,
			int viewCount, int shareCount, int commentCount, String name, Integer abuseCount, Boolean isArchived) {
		this.postId = postId;
		this.userId = userId;
		this.likeCount = likeCount;
		this.shareTime = shareTime;
		this.liked = liked;
		this.content = content;
		this.comments = comments;
		this.viewCount = viewCount;
		this.shareCount = shareCount;
		this.commentCount = commentCount;
		this.name = name;
		this.abuseCount = abuseCount;
		this.isArchived = isArchived;
	}

	@JsonProperty("post_id")
	public long getPostId() {
		return postId;
	}

	@JsonProperty("user_id")
	public long getUserId() {
		return userId;
	}

	@JsonProperty("like_count")
	public int getLikeCount() {
		return likeCount;
	}

	@JsonProperty("share_time")
	public Long getShareTime() {
		return shareTime;
	}

	@JsonProperty("liked")
	public Boolean isLiked() {
		return liked;
	}

	@JsonProperty("content")
	public Content getContent() {
		return content;
	}

	@JsonProperty("comments")
	public ArrayList<Comment> getComments() {
		return comments;
	}

	@JsonProperty("view_count")
	public int getViewCount() {
		return viewCount;
	}

	@JsonProperty("share_count")
	public int getShareCount() {
		return shareCount;
	}

	@JsonProperty("comment_count")
	public int getCommentCount() {
		return commentCount;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}
	
	@JsonProperty("abuse_count")
	public Integer getAbuseCount() {
		return abuseCount;
	}

	@JsonProperty("is_archived")
	public Boolean getIsArchived() {
		return isArchived;
	}

	public static Post createWithPostIdAndUserId(final long postId, Post post, final long userId) {
		return new Post(postId, userId, post.getLikeCount(), post.getShareTime(), post.isLiked(), post.getContent(), post.getComments(),
				post.getViewCount(), post.getShareCount(), post.getCommentCount(), post.getName(), post.getAbuseCount(), post.getIsArchived());
	}

	public static Post createWithContent(final Content content, final Post post) {
		return new Post(post.getPostId(), post.getUserId(), post.getLikeCount(), post.getShareTime(), post.isLiked(), content,
				post.getComments(), post.getViewCount(), post.getShareCount(), post.getCommentCount(), post.getName(), post.getAbuseCount(), post.getIsArchived());
	}
}
