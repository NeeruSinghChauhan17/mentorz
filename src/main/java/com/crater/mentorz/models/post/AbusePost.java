package com.crater.mentorz.models.post;

import java.util.ArrayList;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @Auther Harish Anuragi
 */
@JsonInclude(Include.NON_NULL)
public class AbusePost extends Post {

	@Valid
	@NotNull
	private AbuseType abuseType;

	@SuppressWarnings("unused")
	private AbusePost() {
		this(0, 0, 0, 0l, null, null, null, 0, 0, 0, null, null, null, null);
	}

	public AbusePost(long postId, long userId, int likeCount, Long shareTime, Boolean liked, Content content, ArrayList<Comment> comments,
			int viewCount, int shareCount, int commentCount, String name, Integer abuseCount, Boolean isArchived, AbuseType abusetype) {
		super(postId, userId, likeCount, shareTime, liked, content, comments, viewCount, shareCount, commentCount, name, abuseCount, isArchived);
		this.abuseType = abusetype;
	}

	@JsonProperty("abuse_type")
	public AbuseType getAbuseType() {
		return abuseType;
	}
}
