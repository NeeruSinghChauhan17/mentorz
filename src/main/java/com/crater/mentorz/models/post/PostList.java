/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.models.post;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author Harish Anuragi
 *
 */
public class PostList {

	private List<Post> postList;

	@SuppressWarnings("unused")
	private PostList() {
		this(new ArrayList<Post>());
	}

	public PostList(List<Post> postList) {
		this.postList = postList;
	}

	@JsonProperty("post_list")
	public List<Post> getPostList() {
		return postList;
	}

}
