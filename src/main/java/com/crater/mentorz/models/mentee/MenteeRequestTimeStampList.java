package com.crater.mentorz.models.mentee;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MenteeRequestTimeStampList {
	
	private List<MenteeRequestTimeStamp> MenteeRequestTimeStamp;

	@SuppressWarnings("unused")
	private MenteeRequestTimeStampList() {
		this(new ArrayList<MenteeRequestTimeStamp>());
	}

	public MenteeRequestTimeStampList( List<MenteeRequestTimeStamp> MenteeRequestTimeStamp) {
		this.MenteeRequestTimeStamp = MenteeRequestTimeStamp;
	}

	@JsonProperty("request_list")
	public List<MenteeRequestTimeStamp> getUserList() {
		return MenteeRequestTimeStamp;
	}

}
