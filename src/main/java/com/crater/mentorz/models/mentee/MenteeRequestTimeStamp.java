package com.crater.mentorz.models.mentee;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.crater.mentorz.models.mentor.Expertise;
import com.crater.mentorz.models.user.DeviceInfo;
import com.crater.mentorz.models.user.Profile;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;


@JsonInclude(Include.NON_NULL)
public class MenteeRequestTimeStamp {

	private  final long userId;

	private  final String emailId;

	protected final String password;

	@Valid
	@NotNull(message = "device info can't be null or blank")
	private final DeviceInfo deviceInfo;

	@Valid
	@NotNull(message = "profile can't be null or blank")
	private final Profile profile;

	private final String authToken;
	
	private final List<Expertise> experties;
	
	private  final Boolean isAccountDeactivated;
	
	private final Boolean hasInterests;
	
	private final Boolean hasValues;
	
	private final Long  request_time_stamp;
	
	private final String comment;

	@SuppressWarnings("unused")
	private MenteeRequestTimeStamp() {
		this(0, null, null, null, null, null, null, null,null,null,null,null);
	}

	public MenteeRequestTimeStamp(final long userId,final String emailId, final String password,final DeviceInfo deviceInfo,final Profile profile,
			final String authToken,final List<Expertise> experties,final  Boolean isAccountDeactivated,final Boolean hasInterests,  
			final Boolean hasValues,final Long request_time_stamp,final String comment) {
		this.userId = userId;
		this.emailId = emailId;
		this.password = password;
		this.deviceInfo = deviceInfo;
		this.profile = profile;
		this.authToken = authToken;
		this.experties = experties;
		this.isAccountDeactivated = isAccountDeactivated;
		this.hasInterests=hasInterests;
		this.hasValues=hasValues;
		this.request_time_stamp=request_time_stamp;
		this.comment=comment;
	}

	@JsonProperty("user_id")
	public long getUserId() {
		return userId;
	}

	@JsonProperty("email_id")
	public String getEmailId() {
		return emailId;
	}

	@JsonProperty("password")
	public String getPassword() {
		return password;
	}

	@JsonProperty("device_info")
	public DeviceInfo getDeviceInfo() {
		return deviceInfo;
	}

	@JsonProperty("user_profile")
	public Profile getProfile() {
		return profile;
	}

	@JsonProperty("auth_token")
	public String getAuthToken() {
		return authToken;
	}

	@JsonProperty("experties")
	public List<Expertise> getExperties() {
		return experties;
	}
 
	@JsonProperty("comment")
	public String getComment() {
		return comment;
	}

	@JsonProperty("is_account_deactivated")
	public Boolean getIsAccountDeactivated() {
		return isAccountDeactivated;
	}

	
	@JsonProperty("has_interests")
	public  Boolean getHasInterests() {
		return hasInterests;
	}

	@JsonProperty("request_time_stamp")
	public Long getRequest_time_stamp() {
		return request_time_stamp;
	}

	@JsonProperty("has_values")
	public  Boolean getHasValues() {
		return hasValues;
	}
}
