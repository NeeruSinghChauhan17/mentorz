package com.crater.mentorz.models.mentor;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class MentorSearch {
	
	@JsonProperty("user_id")
	private final Long userId;
	
	@JsonProperty("name")
	private final String name;
	
	@JsonProperty("designation")
	private final String designation;
	
	@JsonProperty("lres_id")
	private final String lresId;
	
	@JsonProperty("hres_id")
	private final String hresId;
	
	@SuppressWarnings("unused")
	private MentorSearch(){
		this(null,null,null,null,null);
	}
	
	public MentorSearch(final Long userId,final String name,final String designation,final String lresId,
			final String hresId){
		this.userId=userId;
		this.name=name;
		this.designation=designation;
		this.lresId=lresId;
		this.hresId=hresId;
		
	}

	public Long getUserId() {
		return userId;
	}

	public String getName() {
		return name;
	}

	public String getDesignation() {
		return designation;
	}

	public String getLresId() {
		return lresId;
	}

	public String getHresId() {
		return hresId;
	}
	
	

}
