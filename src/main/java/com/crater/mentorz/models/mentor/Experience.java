/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.models.mentor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Harish Anuragi
 * 
 */
public class Experience {

	@Min(Long.MIN_VALUE)
	@Max(Long.MAX_VALUE)
	private long from;

	@Min(Long.MIN_VALUE)
	@Max(Long.MAX_VALUE)
	private long to;

	@NotBlank
	private String organisation;

	@NotBlank
	private String designation;

	@NotBlank
	private String location;

	@SuppressWarnings("unused")
	private Experience() {
		this(0, 0, null, null, null);
	}

	public Experience(long from, long to, String organisation, String designation, String location) {
		this.from = from;
		this.to = to;
		this.organisation = organisation;
		this.designation = designation;
		this.location = location;
	}

	@JsonProperty("from")
	public long getFrom() {
		return from;
	}

	@JsonProperty("to")
	public long getTo() {
		return to;
	}

	@JsonProperty("organization")
	public String getOrganisation() {
		return organisation;
	}

	@JsonProperty("designation")
	public String getDesignation() {
		return designation;
	}

	@JsonProperty("location")
	public String getLocation() {
		return location;
	}

}
