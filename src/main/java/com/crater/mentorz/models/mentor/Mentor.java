/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.models.mentor;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Harish Anuragi
 *
 */
public class Mentor {

	@NotEmpty
	@Valid
	private List<Expertise> experties;

	@NotNull
	@Valid
	private PublicBio publicBio;

	@SuppressWarnings("unused")
	private Mentor() {
		this(null, null);
	}

	public Mentor(List<Expertise> experties, PublicBio publicBio) {
		this.experties = experties;
		this.publicBio = publicBio;
	}

	@JsonProperty("experties")
	public List<Expertise> getExperties() {
		return experties;
	}

	@JsonProperty("public_bio")
	public PublicBio getPublicBio() {
		return publicBio;
	}

}
