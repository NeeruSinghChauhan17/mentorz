/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.models.mentor;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Min;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Harish Anuragi
 * 
 */
public class PublicBio {

	@NotEmpty
	@Valid
	private List<Experience> experiences;

	@NotEmpty
	@Valid
	private List<ProfessionalDetail> professionalDetails;

	@Min(0)
	private int price;

	@SuppressWarnings("unused")
	private PublicBio() {
		this(null, null, 0);
	}

	public PublicBio(List<Experience> experiences, List<ProfessionalDetail> professionalDetails, int price) {
		this.experiences = experiences;
		this.professionalDetails = professionalDetails;
		this.price = price;
	}

	@JsonProperty("experiences")
	public List<Experience> getExperiences() {
		return experiences;
	}

	@JsonProperty("professionalDetails")
	public List<ProfessionalDetail> getProfessionalDetails() {
		return professionalDetails;
	}

	@JsonProperty("price")
	public int getPrice() {
		return price;
	}

}
