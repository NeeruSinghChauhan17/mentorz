/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.models.mentor;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Harish Anuragi
 *
 */
@JsonInclude(Include.NON_NULL)
public class BeAMentor {

	@Min(Integer.MIN_VALUE)
	@Max(Integer.MAX_VALUE)
	private int expYears;

	@NotBlank
	private String organisation;

	@NotBlank
	private String designation;

	@NotBlank
	private String location;

	@NotEmpty
	@Valid
	private List<Expertise> experties;

	@Min(0)
	private int price;

	@SuppressWarnings("unused")
	private BeAMentor() {
		this(0, null, null, null, null, 0);
	}

	public BeAMentor(int expYears, String organisation, String designation, String location, List<Expertise> experties, int price) {
		this.expYears = expYears;
		this.organisation = organisation;
		this.designation = designation;
		this.location = location;
		this.experties = experties;
		this.price = price;
	}

	@JsonProperty("exp_years")
	public int getExpYears() {
		return expYears;
	}

	@JsonProperty("organization")
	public String getOrganisation() {
		return organisation;
	}

	@JsonProperty("designation")
	public String getDesignation() {
		return designation;
	}

	@JsonProperty("location")
	public String getLocation() {
		return location;
	}

	@JsonProperty("experties")
	public List<Expertise> getExperties() {
		return experties;
	}

	@JsonProperty("price")
	public int getPrice() {
		return price;
	}
}
