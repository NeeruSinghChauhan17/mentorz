/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.models.mentor;

import java.util.ArrayList;

import javax.validation.Valid;

import org.hibernate.validator.constraints.NotEmpty;

import com.crater.mentorz.models.user.Interest;
import com.fasterxml.jackson.annotation.JsonProperty;

public class MentorFilter {

	/*
	 * @Valid
	 * 
	 * @NotNull private ArrayList<Value> values;
	 */
	@NotEmpty
	@Valid
	private ArrayList<Interest> interests;

	private int minExp;

	private int maxExp;

	private int minPrice;

	private int maxPrice;

	private int minRating;

	private int maxRating;

	/*
	 * @JsonProperty("values") public ArrayList<Value> getValues() { return
	 * values; }
	 */

	@JsonProperty("interests")
	public ArrayList<Interest> getInterests() {
		return interests;
	}

	@JsonProperty("min_exp")
	public int getMinExp() {
		return minExp;
	}

	@JsonProperty("max_exp")
	public int getMaxExp() {
		return maxExp;
	}

	@JsonProperty("min_price")
	public int getMinPrice() {
		return minPrice;
	}

	@JsonProperty("max_price")
	public int getMaxPrice() {
		return maxPrice;
	}

	@JsonProperty("min_rating")
	public int getMinRating() {
		return minRating;
	}

	@JsonProperty("max_rating")
	public int getMaxRating() {
		return maxRating;
	}

}
