/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.models.mentor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class Expertise {

	@Min(Integer.MIN_VALUE)
	@Max(Integer.MAX_VALUE)
	private Integer expertiseId;

	
	//@NotBlank(message = "expertise can't be null")   no need in v1
	private String expertise;
	
	private final Integer parentId;
	private final Boolean hasChildren;
	private final Boolean isMyExpertise;

	@SuppressWarnings("unused")
	private Expertise() {
		this(0, null,null,false,false);
	}

	public Expertise(final Integer expertiseId,final String expertise,final Integer parentId,
			final Boolean hasChildren,final Boolean isMyExpertise) {
		this.expertiseId = expertiseId;
		this.expertise = expertise;
		this.parentId=parentId;
		this.hasChildren=hasChildren;
		this.isMyExpertise=isMyExpertise;
	}

	@JsonProperty("expertise_id")
	public Integer getExpertiseId() {
		return expertiseId;
	}

	@JsonProperty("expertise")
	public String getExpertise() {
		return expertise;
	}

	@JsonProperty("parent_id")
	public  Integer getParentId() {
		return parentId;
	}

	@JsonProperty("has_children")
	public  Boolean getHasChildren() {
		return hasChildren;
	}

	@JsonProperty("is_my_expertise")
	public  Boolean getIsMyExpertise() {
		return isMyExpertise;
	}

	public static Expertise createExpertiseWithMyInterest(final Expertise exts,final Boolean isMyExpertise) {
		
		return new Expertise(exts.getExpertiseId(), exts.getExpertise(), exts.getParentId(), exts.getHasChildren(), isMyExpertise);
	}
}
