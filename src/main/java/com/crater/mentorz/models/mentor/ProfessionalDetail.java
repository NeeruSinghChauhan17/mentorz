/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.models.mentor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonProperty;


public class ProfessionalDetail {

	@NotBlank
	private String course;

	@NotBlank
	private String school;

	@NotBlank
	private String university;

	@Min(Long.MIN_VALUE)
	@Max(Long.MAX_VALUE)
	private long completedIn;

	@Min(Integer.MIN_VALUE)
	@Max(Integer.MAX_VALUE)
	private int percentage;

	@SuppressWarnings("unused")
	private ProfessionalDetail() {
		this(null, null, null, 0, 0);
	}

	public ProfessionalDetail(String course, String school, String university, long completedIn, int percentage) {
		this.course = course;
		this.school = school;
		this.university = university;
		this.completedIn = completedIn;
		this.percentage = percentage;
	}

	@JsonProperty("course")
	public String getCourse() {
		return course;
	}

	@JsonProperty("school")
	public String getSchool() {
		return school;
	}

	@JsonProperty("university")
	public String getUniversity() {
		return university;
	}

	@JsonProperty("completed_in")
	public long getCompletedIn() {
		return completedIn;
	}

	@JsonProperty("percentage")
	public int getPercentage() {
		return percentage;
	}

}
