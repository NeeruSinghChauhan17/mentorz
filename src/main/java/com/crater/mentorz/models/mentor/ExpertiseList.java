/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.models.mentor;

import java.util.List;

import javax.validation.Valid;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author Harish Anuragi
 *
 */
public class ExpertiseList {

	@Valid
	@NotEmpty
	private List<Expertise> expertise;

	@SuppressWarnings("unused")
	private ExpertiseList() {
		this(null);
	}

	public ExpertiseList(List<Expertise> expertise) {
		this.expertise = expertise;
	}

	public List<Expertise> getExpertise() {
		return expertise;
	}

}
