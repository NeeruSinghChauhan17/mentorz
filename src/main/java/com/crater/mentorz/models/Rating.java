/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.models;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Harish Anuragi
 * 
 */

@JsonInclude(Include.NON_NULL)
public class Rating {

	@Valid
	@Min(value = 0) 
	@Max(value = 5)
	private Double rating;
	
	private String review;
	
	private Long time;

	@SuppressWarnings("unused")
	private Rating() {
		this(0.0, null, null);
	}

	public Rating(final Double rating, final String review, final Long time) {
		this.rating = rating;
		this.review = review;
		this.time = time;
	}

	@JsonProperty("rating")
	public Double getRating() {
		return rating;
	}

	@JsonProperty("review")
	public String getReview() {
		return review;
	}

	@JsonProperty("time")
	public Long getTime() {
		return time;
	}
}
