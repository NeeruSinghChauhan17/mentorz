/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.models.user;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Harish Anuragi
 * 
 */
public class UserList {
	private List<User> userList;

	@SuppressWarnings("unused")
	private UserList() {
		this(new ArrayList<User>());
	}

	public UserList(List<User> userList) {
		this.userList = userList;
	}

	@JsonProperty("user_list")
	public List<User> getUserList() {
		return userList;
	}
}
