/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.models.user;

import java.util.List;

import javax.validation.Valid;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Harish Anuragi
 *
 */
public class ValuesList {

	@Valid
	@NotEmpty
	private List<Value> values;

	@SuppressWarnings("unused")
	private ValuesList() {
		this(null);
	}

	public ValuesList(List<Value> values) {
		this.values = values;
	}

	@JsonProperty("values")
	public List<Value> getValues() {
		return values;
	}
}
