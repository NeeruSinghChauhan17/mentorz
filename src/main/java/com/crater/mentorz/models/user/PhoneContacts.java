package com.crater.mentorz.models.user;

import java.util.List;

import javax.validation.Valid;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;


@JsonInclude(Include.NON_NULL)
public class PhoneContacts {
	
	@Valid
	@NotEmpty
	private  List<PhoneContact> contacts;
	
	@SuppressWarnings("unused")
	private PhoneContacts(){
		this(null);
	}
	
	public PhoneContacts( List<PhoneContact> contacts){
	this.contacts=contacts;
	}

	
	@JsonProperty("phone_contacts")
	public List<PhoneContact> getContacts() {
		return contacts;
	}
	
	
	
	
	

}
