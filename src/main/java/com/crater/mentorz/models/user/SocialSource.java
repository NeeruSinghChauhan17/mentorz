package com.crater.mentorz.models.user;

public enum SocialSource {
	FACEBOOK, LINKEDIN;

	public static Integer getIntValue(SocialSource socialSource) {
		if(socialSource == null){
			return 1;
		}
		switch (socialSource) {
		case FACEBOOK:
			return 1;
		case LINKEDIN:
			return 2;
		default:
			return 1;
		}
	}
	
	public static SocialSource getSocialSourceType(Integer value) {
		switch (value) {
			case 1:
				return FACEBOOK;
			case 2:
				return LINKEDIN;
			default:
				return FACEBOOK;
		}
	}
}
