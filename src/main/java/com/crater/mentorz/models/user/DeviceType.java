package com.crater.mentorz.models.user;

/**
 * 
 * @author Harish Anuragi
 *
 */
public enum DeviceType {
	WEB, ANDROID, IOS, WINDOW, BLACKBERRY;

	public static int getIntValue(DeviceType deviceType) {
		switch (deviceType) {
		case WEB:
			return 0;
		case ANDROID:
			return 1;
		case IOS:
			return 2;
		case WINDOW:
			return 3;
		case BLACKBERRY:
			return 4;
		default:
			return 0;
		}
	}

	public static DeviceType getDeviceType(int value) {
		switch (value) {
		case 0:
			return WEB;
		case 1:
			return ANDROID;
		case 2:
			return IOS;
		case 3:
			return WINDOW;
		case 4:
			return BLACKBERRY;
		default:
			return WEB;
		}
	}
}
