package com.crater.mentorz.models.user;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @Auther Harish Anuragi
 */
public class Feedback {

	private Long userId;
	
	private String name;
	
	@NotNull
	@Min(1)
	@Max(10)
	private Integer recommendedCount;

	/*@Valid
	@NotBlank(message = "device info can't be null or blank")*/
	private String description;

	@SuppressWarnings("unused")
	private Feedback() {
		this(null, null, null, null);
	}

	public Feedback(Long userId, String name, Integer recommendedCount, String description) {
		this.userId = userId;
		this.name = name;
		this.recommendedCount = recommendedCount;
		this.description = description;
	}
	
	@JsonProperty("user_id")
	public Long getUserId() {
		return userId;
	}

	@JsonProperty("user_name")
	public String getName() {
		return name;
	}

	@JsonProperty("recommend_scale_count")
	public Integer getRecommendedCount() {
		return recommendedCount;
	}

	@JsonProperty("description")
	public String getDescription() {
		return description;
	}

}
