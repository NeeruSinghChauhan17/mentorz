package com.crater.mentorz.models.user;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author Harish Anuragi
 *
 */

@JsonInclude(Include.NON_NULL)
public class DeviceInfo {

	//@NotNull(message = "device token can't be null or blank")
	private String deviceToken;

	@Valid
	@NotNull
	private DeviceType deviceType;

	private String userAgent;

	@SuppressWarnings("unused")
	private DeviceInfo() {
		this(null, null, null);
	}

	public DeviceInfo(String deviceToken, DeviceType deviceType, String userAgent) {
		this.deviceToken = deviceToken;
		this.deviceType = deviceType;
		this.userAgent = userAgent;
	}

	@JsonProperty("device_token")
	public String getDeviceToken() {
		return deviceToken;
	}

	@JsonProperty("device_type")
	public DeviceType getDeviceType() {
		return deviceType;
	}

	public String getUserAgent() {
		return userAgent;
	}

	public static DeviceInfo createDeviceInfoWithUserAgentAndDeviceType(String userAgent, DeviceType deviceType, DeviceInfo deviceInfo) {
		return new DeviceInfo(deviceInfo.getDeviceToken(), deviceType, userAgent);
	}
	
	public static DeviceInfo createDeviceInfoWithUserAgent(String userAgent, DeviceInfo deviceInfo) {
		return new DeviceInfo(deviceInfo.getDeviceToken(),deviceInfo.getDeviceType(), userAgent);
	}


}
