package com.crater.mentorz.models.user;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ForgetPassword {

	@NotBlank(message = "emailId can't be null or blank")
	private String emailId;

	@SuppressWarnings("unused")
	private ForgetPassword(){
		this(null);
	}
	
	public ForgetPassword(String emailId) {
		this.emailId = emailId;
	}

	@JsonProperty("email_id")
	public  String getEmailId() {
		return emailId;
	}
	
	
}
