/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.models.user;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Interest {

	@Min(Integer.MIN_VALUE)
	@Max(Integer.MAX_VALUE)
	private final int interestId;
	private final String interest;
	private final Integer parentId;
	private final Boolean hasChildren;
	private final Boolean isMyInterest;

	@SuppressWarnings("unused")
	private Interest() {
		this(0, null,null,false,false);
	}

	public Interest(final int interestId, final String interest,final Integer parentId,final Boolean hasChildren,final Boolean isMyInterest) {
		this.interestId = interestId;
		this.interest = interest;
		this.parentId=parentId;
		this.hasChildren=hasChildren;
		this.isMyInterest=isMyInterest;
	}

	@JsonProperty("interest_id")
	public int getInterestId() {
		return interestId;
	}

	@JsonProperty("interest")
	public String getInterest() {
		return interest;
	}

	@JsonProperty("parent_id")
	public  Integer getParentId() {
		return parentId;
	}

	@JsonProperty("has_children")
	public  Boolean getHasChildren() {
		return hasChildren;
	}

	@JsonProperty("is_my_Interest")
	public  Boolean getIsMyInterest() {
		return isMyInterest;
	}
	
	public static Interest createInterestWithMyInterest(final Interest interest,Boolean isMyInterest ){
		return new Interest(interest.getInterestId(), interest.getInterest(), interest.getParentId(), interest.getHasChildren(), isMyInterest);
	}
	

}
