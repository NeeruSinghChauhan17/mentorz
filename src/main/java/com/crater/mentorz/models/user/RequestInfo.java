
package com.crater.mentorz.models.user;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RequestInfo {

	//@NotBlank(message = "info can't be null or blank")
	private String info;

	@SuppressWarnings("unused")
	private RequestInfo() {
		this(null);
	}

	public RequestInfo(String info) {
		this.info = info;
	}

	@JsonProperty("info")
	public String getInfo() {
		return info;
	}

}
