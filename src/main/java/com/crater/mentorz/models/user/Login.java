/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.models.user;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Harish Anuragi
 * 
 */
public class Login {

	//@NotBlank(message = "emailId can't be null or blank")
	private String emailId;
	
	private InternationalNumber number;

	@NotBlank(message = "password can't be null or blank")
	private String password;

	@NotNull
	private DeviceInfo deviceInfo;

	@SuppressWarnings("unused")
	private Login() {
		this(null, null, null,null);
	}

	public Login(String emailId, String password, DeviceInfo deviceInfo,InternationalNumber number) {
		this.emailId = emailId;
		this.password = password;
		this.deviceInfo = deviceInfo;
		this.number=number;
		
	}

	@JsonProperty("email_id")
	public String getEmailId() {
		return emailId;
	}

	@JsonProperty("password")
	public String getPassword() {
		return password;
	}

	@JsonProperty("device_info")
	public DeviceInfo getDeviceInfo() {
		return deviceInfo;
	}

	@JsonProperty("phone_number")
	public InternationalNumber getNumber() {
		return number;
	}

	public void setNumber(InternationalNumber number) {
		this.number = number;
	}
	
	
}
