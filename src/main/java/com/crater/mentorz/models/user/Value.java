/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.models.user;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Harish Anuragi
 * 
 */
public class Value {

	@Min(Integer.MIN_VALUE)
	@Max(Integer.MAX_VALUE)
	private int valueId;

	private String value;

	@SuppressWarnings("unused")
	private Value() {
		this(0, null);
	}

	public Value(int valueId, String value) {
		this.valueId = valueId;
		this.value = value;
	}

	@JsonProperty("value_id")
	public int getValueId() {
		return valueId;
	}

	@JsonProperty("value")
	public String getValue() {
		return value;
	}
}
