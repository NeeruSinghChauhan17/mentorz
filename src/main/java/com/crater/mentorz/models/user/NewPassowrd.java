package com.crater.mentorz.models.user;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonProperty;

public class NewPassowrd {

	@NotBlank(message = "token can't be null or blank")
	private final String token;
	
	@NotBlank(message = "new password can't be null or blank")
	private final String password;
	
	
	@SuppressWarnings("unused")
	private NewPassowrd(){
		this(null, null);
	}
	
	public NewPassowrd(String token, String newPassword) {
		this.token = token;
		this.password = newPassword;
	}
	
	
	@JsonProperty("token")
	public  String getToken() {
		return token;
	}

	@JsonProperty("password")
	public  String getPassword() {
		return password;
	}
	
	
	
	
}
