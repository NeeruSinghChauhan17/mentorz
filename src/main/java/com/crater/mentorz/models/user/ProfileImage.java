/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.models.user;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Harish Anuragi
 *
 */
public class ProfileImage {

	@NotBlank
	private String lresId;

	@NotBlank
	private String hresId;

	@SuppressWarnings("unused")
	private ProfileImage() {
		this(null, null);
	}

	public ProfileImage(String lresId, String hresId) {
		this.lresId = lresId;
		this.hresId = hresId;
	}

	@JsonProperty("lres_id")
	public String getLresId() {
		return lresId;
	}

	@JsonProperty("hres_id")
	public String getHresId() {
		return hresId;
	}
}
