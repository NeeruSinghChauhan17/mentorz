package com.crater.mentorz.models.user;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(value = Include.NON_NULL)
public class SubscribeUser {

	@JsonProperty("email")
	private final String email;

	// for Jax-B
	private SubscribeUser() {
		this(null);
	}

	private SubscribeUser(final String email) {
		this.email = email;
	}

	public String getEmail() {
		return email;
	}

	public static class Builder {

		private String email;

		public Builder withEmail(final String email) {
			this.email = email;
			return this;
		}

		public SubscribeUser build() {
			return new SubscribeUser(email);
		}

	}

}
