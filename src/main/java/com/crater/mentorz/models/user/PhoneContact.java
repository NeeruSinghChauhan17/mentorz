package com.crater.mentorz.models.user;

import javax.validation.Valid;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class PhoneContact {
	
@NotBlank(message="name can't be null or blank")
private final String name;

@Valid
private final InternationalNumber internationalNumber;


private final Boolean isInvited;


@SuppressWarnings("unused")
private PhoneContact(){
	this(null,null,null);
}

public PhoneContact(final String name,final InternationalNumber internationalNumber,final Boolean isInvited){
	this.name=name;
	this.internationalNumber=internationalNumber;
	this.isInvited=isInvited;
}

@JsonProperty("name")
public String getName() {
	return name;
}

@JsonProperty("phone_number")
public InternationalNumber getInternationalNumber() {
	return internationalNumber;
}

@JsonProperty("is_invited")
public Boolean getIsInvited() {
	return isInvited;
}



}
