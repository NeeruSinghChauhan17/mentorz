package com.crater.mentorz.models.user;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class UpdateContact {
	
@JsonProperty("old_number")
private final  InternationalNumber oldNo;

@JsonProperty("new_number")
private final InternationalNumber newNo;


@SuppressWarnings("unused")
private UpdateContact(){
	this(null,null);
}

public UpdateContact(final InternationalNumber oldNo,final InternationalNumber newNo){
	this.oldNo=oldNo;
	this.newNo=newNo;
    }

public InternationalNumber getOldNo() {
	return oldNo;
}

public InternationalNumber getNewNo() {
	return newNo;
}



}
