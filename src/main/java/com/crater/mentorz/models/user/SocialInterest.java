package com.crater.mentorz.models.user;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class SocialInterest {

	@NotBlank(message = "Invalid name")
	private String name;

	@NotBlank(message = "Invalid id")
	private String id;

	@SuppressWarnings("unused")
	private SocialInterest() {
		this(null, null);
	}

	public SocialInterest(String name, String id) {
		super();
		this.name = name;
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public String getId() {
		return id;
	}

	@Override
	public String toString() {
		return "SocialInterest [name=" + name + ", id=" + id + "]";
	}

}
