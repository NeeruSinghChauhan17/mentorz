/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.models.user;

import java.util.List;

import javax.validation.Valid;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Harish Anuragi
 *
 */
public class InterestList {

	@Valid
	@NotEmpty
	private List<Interest> interests;

	@SuppressWarnings("unused")
	private InterestList() {
		this(null);
	}

	public InterestList(List<Interest> interests) {
		this.interests = interests;
	}

	@JsonProperty("interests")
	public List<Interest> getInterests() {
		return interests;
	}

}
