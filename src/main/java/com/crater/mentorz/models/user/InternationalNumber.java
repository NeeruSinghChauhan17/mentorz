package com.crater.mentorz.models.user;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class InternationalNumber {
	
	@JsonProperty("cc")
    private final String cc; //91 or 1
	
	@JsonProperty("iso_alpha_2_cc") // iso_country_code
	private final String code;
	
	@JsonProperty("number")
	private final String phoneNumber;
	
	@SuppressWarnings("unused")
	private InternationalNumber(){
		this(null,null,null);
	}
	public InternationalNumber(final String cc,final String code, final String phoneNumber){
		this.cc=cc;
		this.code=code;
		this.phoneNumber=phoneNumber;
	}
	public String getCc() {
		return cc;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public String getCode() {
		return code;
	}
	
}
