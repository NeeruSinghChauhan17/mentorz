/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.models.user;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.crater.mentorz.models.mentor.Expertise;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Harish Anuragi
 *
 */
@JsonInclude(Include.NON_NULL)
public class UserData {

	private  final long userId;



	@Valid
	@NotNull(message = "profile can't be null or blank")
	private final Profile profile;

	
	private final String url;
	
	
	private final List<Expertise> experties;
	
	
	

	@SuppressWarnings("unused")
	private UserData() {
		this(0, null, null, null);
	}

	public UserData(final long userId,final Profile profile,
			final String url,final List<Expertise> experties) {
		this.userId = userId;
		
		this.profile = profile;
		this.url = url;
		this.experties = experties;
		
	}

	@JsonProperty("user_id")
	public long getUserId() {
		return userId;
	}

	

	@JsonProperty("user_profile")
	public Profile getProfile() {
		return profile;
	}


	@JsonProperty("value")
	public String getUrl() {
		return url;
	}

	@JsonProperty("experties")
	public List<Expertise> getExperties() {
		return experties;
	}
 
	public static UserData createWithProfileImageAndExpertise(final User user,String url,final List<Expertise> expertise){
		return new UserData(0L, user.getProfile(),url,  expertise);	
	}


	
	public static UserData createWithProfileImage(final User user,String url){
		return new UserData(0L, user.getProfile(),url, null);	
	}
	
}
