package com.crater.mentorz.models.user;

import java.util.ArrayList;

import javax.validation.Valid;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class SendRequest {
	@NotEmpty
	@Valid
	@JsonProperty("interests")
	private final  ArrayList<Interest> interests;
	
	@JsonProperty("comment")
	private  final String text;
	
	@SuppressWarnings("unused")
	private SendRequest(){
		this(null,null);
	}
	
	public SendRequest( final  ArrayList<Interest> interests,final String text){
		this.interests=interests;
		this.text=text;
	}

	public ArrayList<Interest> getInterests() {
		return interests;
	}

	public String getText() {
		return text;
	}
	
	
	
}
