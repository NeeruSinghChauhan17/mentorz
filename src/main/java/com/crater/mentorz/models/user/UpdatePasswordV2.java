package com.crater.mentorz.models.user;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class UpdatePasswordV2 {
	
	
	@Valid
	@NotNull(message = "phone number cant' be blank")
	@JsonProperty("phone_number")
	private InternationalNumber number;
	
	
	@Valid
	@NotNull(message = "password can't be blank")
	@JsonProperty("password")
	private String password;
	
	@JsonProperty("code")
	private String code;
	
	@JsonProperty("string")
	private String encrytString;
	
	
	
	@SuppressWarnings("unused")
	private UpdatePasswordV2(){
		this(null,null,null,null);
	}

	public UpdatePasswordV2(InternationalNumber number,String password,String code,String encrytString){
		this.number=number;
		this.password=password;
		this.code=code;
		this.encrytString= encrytString;
		
	}

	public InternationalNumber getNumber() {
		return number;
	}

	public String getPassword() {
		return password;
	}

	public String getCode() {
		return code;
	}

	public String getEncrytString() {
		return encrytString;
	}
	
	
	
	
	
}
