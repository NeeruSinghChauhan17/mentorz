/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.models.user;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.crater.mentorz.models.mentor.Expertise;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonInclude(Include.NON_NULL)
public class User {

	private  final long userId;

	private  final String emailId;

	
	
	protected final String password;
   
	@Valid
	@NotNull(message = "device info can't be null or blank")
	private final DeviceInfo deviceInfo;

	@Valid
	@NotNull(message = "profile can't be null or blank")
	private final Profile profile;

	private final String authToken;
	
	private final List<Expertise> experties;
	
	private  final Boolean isAccountDeactivated;
	
	private final Boolean hasInterests;
	
	private final Boolean hasValues;
	
	private final InternationalNumber number;
	
	private final Boolean isEmailVerified;
	private final Boolean isNumberVerified;
	
	

	@SuppressWarnings("unused")
	private User() {
		this(0, null, null, null, null, null, null, null,null,null,null,null,null);
	}

	public User(final long userId,final String emailId, final String password,
			final DeviceInfo deviceInfo,InternationalNumber number,final Profile profile,
			final String authToken,final List<Expertise> experties,final  
			Boolean isAccountDeactivated,final Boolean hasInterests,  
			final Boolean hasValues,Boolean isEmailVerified,Boolean isNumberVerified) {
		this.userId = userId;
		this.emailId = emailId;
		this.password = password;
		this.deviceInfo = deviceInfo;
		this.profile = profile;
		this.authToken = authToken;
		this.experties = experties;
		this.isAccountDeactivated = isAccountDeactivated;
		this.hasInterests=hasInterests;
		this.hasValues=hasValues;
		this.number=number;
		this.isEmailVerified=isEmailVerified;
		this.isNumberVerified=isNumberVerified;
	}

	

	@JsonProperty("user_id")
	public long getUserId() {
		return userId;
	}

	@JsonProperty("email_id")
	public String getEmailId() {
		return emailId;
	}

	@JsonProperty("password")
	public String getPassword() {
		return password;
	}

	@JsonProperty("device_info")
	public DeviceInfo getDeviceInfo() {
		return deviceInfo;
	}

	@JsonProperty("user_profile")
	public Profile getProfile() {
		return profile;
	}

	@JsonProperty("auth_token")
	public String getAuthToken() {
		return authToken;
	}

	@JsonProperty("experties")
	public List<Expertise> getExperties() {
		return experties;
	}
 
	@JsonProperty("is_account_deactivated")
	public Boolean getIsAccountDeactivated() {
		return isAccountDeactivated;
	}

	
	@JsonProperty("has_interests")
	public  Boolean getHasInterests() {
		return hasInterests;
	}

	@JsonProperty("has_values")
	public  Boolean getHasValues() {
		return hasValues;
	}

	@JsonProperty("phone_number")
	public InternationalNumber getNumber() {
		return number;
	}
	
	@JsonProperty("is_email_verified")
	public Boolean getIsEmailVerified() {
		return isEmailVerified;
	}

	@JsonProperty("is_number_verified")
	public Boolean getIsNumberVerified() {
		return isNumberVerified;
	}
	
	
	public static User createWithUserId(final long userId, final User user) {
		return new User(userId, user.getEmailId(), user.getPassword(), user.getDeviceInfo(), user.getNumber(),
				user.getProfile(), user.getAuthToken(), user.getExperties(), 
				user.getIsAccountDeactivated(),user.getHasInterests(),user.getHasValues(),null,null);
	}

	public static User createWithAuthToken(final String authToken, final User user) {
		return new User(user.getUserId(), user.getEmailId(), null,
				user.getDeviceInfo(), user.getNumber(),user.getProfile(), authToken, user.getExperties(), 
				user.getIsAccountDeactivated(),user.getHasInterests(),user.getHasValues(),null,user.getIsNumberVerified());
	}
	

	public static User createWithDeviceInfoAndUserAgent(final User user, final String userAgent) {
		return new User(user.getUserId(), user.getEmailId(), user.getPassword(), DeviceInfo.createDeviceInfoWithUserAgentAndDeviceType(
				userAgent, user.getDeviceInfo().getDeviceType(), user.getDeviceInfo()),user.getNumber(), user.getProfile(),
				user.getAuthToken(), user.getExperties(), user.getIsAccountDeactivated(),
				user.getHasInterests(),user.getHasValues(),null,null);
	}
	
	public static User createWithExpertise(final User user, final List<Expertise> expertise) {
		return new User(user.getUserId(), user.getEmailId(), user.getPassword(), 
				user.getDeviceInfo(),user.getNumber(), user.getProfile(), user.getAuthToken(), 
				expertise, user.getIsAccountDeactivated(),user.getHasInterests(),
				user.getHasValues(),null,null);
	}
	
	public static User createWithProfile(final User user, final Profile profile) {
		return new User(user.getUserId(), user.getEmailId(), user.getPassword(), 
				user.getDeviceInfo(), user.getNumber(),profile, user.getAuthToken(), user.getExperties(),
				user.getIsAccountDeactivated(),user.getHasInterests(),user.getHasValues(),null,null);
	}
	
	
	public static User createWithProfileImageAndExpertise(final User user,final List<Expertise> expertise){
		return new User(0L, null, null,null,null, user.getProfile(), null, expertise, null,null,null,null,null);	
	}

	
	
}
