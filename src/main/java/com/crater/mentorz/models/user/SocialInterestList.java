package com.crater.mentorz.models.user;

import java.util.List;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
;
@JsonInclude(Include.NON_NULL)
public class SocialInterestList {

	@NotNull(message = "interest list can't be null")
	List<SocialInterest> data;

	public List<SocialInterest> getData() {
		return data;
	}

}
