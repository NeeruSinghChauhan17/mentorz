/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.models.user;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonProperty;


public class SocialLogin {

	@NotBlank
	private String socialId;

	@NotNull
	private DeviceInfo deviceInfo;

	@NotBlank(message = "social source can't be null or blank")
	private String socialSource;

	public SocialLogin() {
		this(null, null, null);
	}

	public SocialLogin(String socialId, DeviceInfo deviceInfo, String socialSource) {
		this.socialId = socialId;
		this.deviceInfo = deviceInfo;
		this.socialSource = socialSource;
	}

	@JsonProperty("social_id")
	public String getSocialId() {
		return socialId;
	}

	@JsonProperty("device_info")
	public DeviceInfo getDeviceInfo() {
		return deviceInfo;
	}

	@JsonProperty("social_source")
	public String getSocialSource() {
		return socialSource;
	}

}
