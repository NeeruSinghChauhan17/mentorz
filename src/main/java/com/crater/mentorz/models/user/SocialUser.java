/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.models.user;

import java.util.List;

import org.hibernate.validator.constraints.NotBlank;

import com.crater.mentorz.models.mentor.Expertise;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonInclude(Include.NON_NULL)
public class SocialUser extends User {

	@NotBlank(message = "social id can't be null or blank")
	private String socialId;
   
	@NotBlank(message = "social source can't be null or blank")
	private String socialSource;

	@SuppressWarnings("unused")
	private SocialUser() {
		this(0, null, null, null, null, null, null, null, null, null,null,null,null);
	}

	public SocialUser(long userId, String emailId, String password, DeviceInfo deviceInfo, Profile profile, String authToken,
			List<Expertise> experties, Boolean isAccountDeactivated, String socialId, String socialSource,Boolean hasInterests,
			Boolean hasValues,InternationalNumber number) {
		super(userId, emailId, password, deviceInfo, number,profile, authToken, experties, isAccountDeactivated,hasInterests,hasValues,null,null);
		this.socialId = socialId;
		this.socialSource = socialSource;
	}

	@Override
	@JsonIgnore
	public String getPassword() {
		return null;
	}

	@JsonProperty("social_id")
	public String getSocialId() {
		return socialId;
	}

	@JsonProperty("social_source")
	public String getSocialSource() {
		return socialSource;
	}
	
	public static SocialUser createWithUserAndSocialId(final User user,final String socialId){
		return new SocialUser(user.getUserId(), null, null, null, null, null, null, null, socialId, null,null,null,
				new InternationalNumber(user.getNumber().getCc(),user.getNumber().getCode(),user.getNumber().getPhoneNumber()));	
	}
}
