/*

 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.models.user;

import java.util.ArrayList;

import org.hibernate.validator.constraints.NotBlank;

import com.crater.mentorz.models.Rating;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonInclude(Include.NON_NULL)
public class Profile {

	private ArrayList<Value> values;
	private ArrayList<Interest> interests;
	private String lresId;
	private String hresId;
	private int followers;
	private int following;
	private int mentors;
	private int mentees;
	private String basicInfo;
	private long birthDate;
	private int chargePrice;
	private int requests;
	private Integer exp;
	private String videoBioLres;
	private String videoBioHres;
	private Rating rating;
	
	@NotBlank(message = "user name can't be null or blank")
	private String name;
	
	private Integer blockCount;
	private String facebookUrl;
	private String linkedInURl;
	
	
	@SuppressWarnings("unused")
	private Profile() {
		this(null, null, null, null, 0, 0, 0, 0, null, 0, 0, null, 0, null, null, null, null, null,
				null,null);
	}

	public Profile(ArrayList<Value> values, ArrayList<Interest> interests, String lresId, String hresId, int followers, int following,
			int mentors, int mentees, String basicInfo, long birthDate, int chargePrice, String name, int requests, Integer exp,
			String videoBioLres, String videoBioHres, Rating rating, Integer blockCount,String facebookUrl,String linkedInURl
			) {
		
		
		this.values = values;
		this.interests = interests;
		this.lresId = lresId;
		this.hresId = hresId;
		this.followers = followers;
		this.following = following;
		this.mentors = mentors;
		this.mentees = mentees;
		this.basicInfo = basicInfo;
		this.birthDate = birthDate;
		this.chargePrice = chargePrice;
		this.name = name;
		this.requests = requests;
		this.exp = exp;
		this.videoBioLres = videoBioLres;
		this.videoBioHres = videoBioHres;	
		this.rating = rating;
		this.blockCount = blockCount;
		this.facebookUrl=facebookUrl;
		this.linkedInURl=linkedInURl;
		
		
	}

	
	@JsonProperty("experience")
	public Integer getExp() {
		return exp;
	}

	@JsonProperty("values")
	public ArrayList<Value> getValues() {
		return values;
	}

	@JsonProperty("birth_date")
	public long getBirthDate() {
		return birthDate;
	}

	@JsonProperty("interests")
	public ArrayList<Interest> getInterests() {
		return interests;
	}

	@JsonProperty("lres_id")
	public String getLresId() {
		return lresId;
	}

	@JsonProperty("hres_id")
	public String getHresId() {
		return hresId;
	}

	@JsonProperty("followers")
	public int getFollowers() {
		return followers;
	}

	@JsonProperty("following")
	public int getFollowing() {
		return following;
	}

	@JsonProperty("mentors")
	public int getMentors() {
		return mentors;
	}

	@JsonProperty("mentees")
	public int getMentees() {
		return mentees;
	}

	@JsonProperty("basic_info")
	public String getBasicInfo() {
		return basicInfo;
	}

	@JsonProperty("charge_price")
	public int getChargePrice() {
		return chargePrice;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("requests")
	public int getRequests() {
		return requests;
	}

	@JsonProperty("video_bio_lres")
	public String getVideoBioLres() {
		return videoBioLres;
	}

	@JsonProperty("video_bio_hres")
	public String getVideoBioHres() {
		return videoBioHres;
	}

	@JsonProperty("rating")
	public Rating getRating() {
		return rating;
	}
	
	@JsonProperty("block_count")
	public Integer getBlockCount() {
		return blockCount;
	}

	@JsonProperty("facebook_url")
	public String getFacebookUrl() {
		return facebookUrl;
	}

	@JsonProperty("linkedin_url")
	public String getLinkedInURl() {
		return linkedInURl;
	}
	

	public static Profile createWithRating(final Profile profile, final Rating rating) {
		return new Profile(profile.getValues(), profile.getInterests(), profile.getLresId(), profile.getHresId(), 
				profile.getFollowers(), profile.getFollowing(), profile.getMentors(), profile.getMentees(), 
				profile.getBasicInfo(), profile.getBirthDate(), profile.getChargePrice(), profile.getName(),
				profile.getRequests(), profile.getExp(), profile.getVideoBioLres(), profile.getVideoBioHres(), 
				rating, profile.getBlockCount(),profile.getFacebookUrl(),profile.getLinkedInURl());
	}

	
}
