/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.models.xperience;

/**
 * @author Harish Anuragi
 *
 */
public enum XperienceType {

	INTERNSHIP, PROJECT, JOB;

	public static int getIntValue(XperienceType jobType) {
		switch (jobType) {
		case INTERNSHIP:
			return 0;
		case PROJECT:
			return 1;
		case JOB:
			return 2;
		default:
			return 2;
		}
	}

	public static XperienceType getJobType(int value) {
		switch (value) {
		case 0:
			return INTERNSHIP;
		case 1:
			return PROJECT;
		case 2:
			return JOB;
		default:
			return JOB;
		}
	}
}
