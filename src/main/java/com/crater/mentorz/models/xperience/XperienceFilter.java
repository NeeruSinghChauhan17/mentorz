/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.models.xperience;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Harish Anuragi
 *
 */
public class XperienceFilter {

	@Min(4)
	@Max(10)
	private int workingHour;

	@NotBlank
	private String location;

	@NotBlank
	private String industry;

	@NotBlank
	private String function;

	@Min(0)
	@Max(50)
	private int experience;

	@Valid
	@NotNull
	private XperienceType xperienceType;

	@JsonProperty("working_hour")
	public int getWorkingHour() {
		return workingHour;
	}

	@JsonProperty("location")
	public String getLocation() {
		return location;
	}

	@JsonProperty("industry")
	public String getIndustry() {
		return industry;
	}

	@JsonProperty("function")
	public String getFunction() {
		return function;
	}

	@JsonProperty("experience")
	public int getExperience() {
		return experience;
	}

	@JsonProperty("xperience_type")
	public XperienceType getXperienceType() {
		return xperienceType;
	}

}
