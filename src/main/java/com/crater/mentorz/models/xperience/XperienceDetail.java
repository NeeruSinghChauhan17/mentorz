/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.models.xperience;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonProperty;

public class XperienceDetail {

	@NotBlank
	private String providedBy;

	@NotBlank
	private String title;

	@Min(0)
	@Max(24)
	private int workingHour;

	@NotBlank
	private String location;

	@NotBlank
	private String perks;

	@NotBlank
	private String industry;

	@NotBlank
	private String function;

	@Valid
	@NotNull
	private XperienceType xperienceType;

	@NotBlank
	private String detail;

	@Min(0)
	@Max(100)
	private int xperienceReqired;

	@JsonProperty("provided_by")
	public String getProvidedBy() {
		return providedBy;
	}

	@JsonProperty("title")
	public String getTitle() {
		return title;
	}

	@JsonProperty("working_hour")
	public int getWorkingHour() {
		return workingHour;
	}

	@JsonProperty("location")
	public String getLocation() {
		return location;
	}

	@JsonProperty("perks")
	public String getPerks() {
		return perks;
	}

	@JsonProperty("industry")
	public String getIndustry() {
		return industry;
	}

	@JsonProperty("function")
	public String getFunction() {
		return function;
	}

	@JsonProperty("xperience_type")
	public XperienceType getXperienceType() {
		return xperienceType;
	}

	@JsonProperty("detail")
	public String getDetail() {
		return detail;
	}

	@JsonProperty("xperience_required")
	public int getXperienceReqired() {
		return xperienceReqired;
	}

}
