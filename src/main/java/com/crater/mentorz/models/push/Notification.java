package com.crater.mentorz.models.push;

import java.util.List;
import java.util.Map;
import com.crater.mentorz.enums.NotificationMessageType;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Notification {

	private final Long from;

	private final List<Long> to;

	private final Long activityTime;

	private final NotificationMessageType notificationMessageType;
	
	private final String nickName;	
	
    private final Map<String,Object> customData;

    
    private final Boolean customPayload;
    
    
	@SuppressWarnings("unused")
	private Notification() {
		this(null, null, null, null, null, null,null);
	}

	public Notification(final Long from, final List<Long> to, final Long activityTime,
			 final NotificationMessageType notificationMessageType,
			 final String nickName,final Map<String,Object> customData,final Boolean customPayload) {
		this.from = from;
		this.to = to;
		this.activityTime = activityTime;
		this.notificationMessageType = notificationMessageType;
		this.nickName = nickName;
		this.customData=customData;
		this.customPayload=customPayload;
	}

	@JsonProperty("from")
	public Long getFrom() {
		return from;
	}

	@JsonProperty("to")
	public List<Long> getTo() {
		return to;
	}

	@JsonProperty("notification_time")
	public Long getActivityTime() {
		return activityTime;
	}

	@JsonProperty("notification_message_type")
	public NotificationMessageType getNotificationMessageType() {
		return notificationMessageType;
	}

	@JsonProperty("nick_name")
	public String getNickName() {
		return nickName;
	}


	@JsonProperty("custom_data")
	public synchronized Map<String, Object> getCustomData() {
		return customData;
	}

	@JsonProperty("is_custom_payload")
	public Boolean getCustomPayload() {
		return customPayload;
	} 

	

}
