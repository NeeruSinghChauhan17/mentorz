package com.crater.mentorz.models.push;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(value = Include.NON_NULL)
public class NotificationContent {

	@JsonProperty("to")
	private String to;

	@JsonProperty("notification")
	private Notification notification;

	@SuppressWarnings("unused")
	private NotificationContent() {
		this(null, null);
	}

	public NotificationContent(final String to, Notification notification) {
		this.to = to;
		this.notification = notification;
	}

	@JsonProperty("to")
	public String getTo() {
		return to;
	}
	
	@JsonProperty("notification")
	public Notification getNotification() {
		return notification;
	}

	@Override
	public String toString() {
		return "NotificationContent [to=" + to + ", notification="
				+ notification + "]";
	}


}
