package com.crater.mentorz.models.payment;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Redeem extends Payment {

	@JsonProperty("paypal_id")
	private final String paypalId;

	@SuppressWarnings("unused")
	private Redeem() {
		this(null, null, null);
	}

	public Redeem(Long userId, Double balance, final String paypalId) {
		super(userId, balance);
		this.paypalId = paypalId;
	}

	public String getPaypalId() {
		return paypalId;
	}

}
