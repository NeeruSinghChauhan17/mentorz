package com.crater.mentorz.models.payment;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Balance {

	@JsonProperty("charge_price")
	private final Integer chargePrice;

	@JsonProperty("available_balance")
	private final Double avilableBalance;

	@JsonProperty("credits")
	private final Double credit;

	@SuppressWarnings("unused")
	private Balance() {
		this(null, null, null);
	}

	public Balance(final Double avilableBalance, final Double credit, final Integer chargePrice) {
		this.avilableBalance = avilableBalance;
		this.credit = credit;
		this.chargePrice = chargePrice;
	}

	public Double getAvilableBalance() {
		return avilableBalance;
	}

	public Double getCredit() {
		return credit;
	}

}
