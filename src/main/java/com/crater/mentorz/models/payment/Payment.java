package com.crater.mentorz.models.payment;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Payment {

	@Min(value = Long.MIN_VALUE, message = "UserId Is Invalid")
	@Max(value = Long.MAX_VALUE, message = "UserId Is Invalid")
	@JsonProperty("user_id")
	private final Long userId;

	@JsonProperty("balance")
	private final Double balance;

	@SuppressWarnings("unused")
	private Payment() {
		this(null, null);
	}

	public Payment(final Long userId, final Double balance) {
		this.userId = userId;
		this.balance = balance;
	}

	public Long getUserId() {
		return userId;
	}

	public Double getBalance() {
		return balance;
	}

}
