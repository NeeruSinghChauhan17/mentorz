package com.crater.mentorz.mail;
import java.util.HashMap;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.velocity.app.VelocityEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.velocity.VelocityEngineUtils;

import com.crater.mentorz.models.user.SubscribeUser;



@Service
public class EmailServiceImpl{

	private static final Logger LOGGER = LoggerFactory.getLogger(EmailServiceImpl.class);

	private static final String UTF_8_ENCODING = "utf-8";

	private final JavaMailSender mailSender;

	private final VelocityEngine velocityEngine;

	private final String fromContactId;
	
	private final String adminMailId;
	/*private ClassPathResource logo;
	private ClassPathResource facebook;
	private ClassPathResource twitter;
	private ClassPathResource background;
	private ClassPathResource footer;
	private ClassPathResource offer;*/

	@Autowired
	public EmailServiceImpl(final JavaMailSender mailSender, final VelocityEngine velocityEngine,
			@Value("${email.from.contactId}") final String fromContactId,
			@Value("${email.from.adminMailId}") final String adminMailId) {
		this.mailSender = mailSender;
		this.velocityEngine = velocityEngine;
		this.fromContactId = fromContactId;
		this.adminMailId=adminMailId;
	}

	/*@PostConstruct
	public void loadResources() {
		logo = new ClassPathResource("img/logo.png");
		facebook = new ClassPathResource("img/icon-facebook.png");
		twitter = new ClassPathResource("img/icon-twitter.png");
		background = new ClassPathResource("img/background-img.jpg");
		footer = new ClassPathResource("img/footer-bg.png");
		offer = new ClassPathResource("img/offer-img.png");
	}*/

	public void sendWelcomeMail(final String email, String url) {
		try {
			final MimeMessage message = mailSender.createMimeMessage();
			final MimeMessageHelper helper = new MimeMessageHelper(message, true);
			helper.setSubject("Email verification mail");
			helper.setFrom(fromContactId);
			helper.setTo(email);
			final Map<String, Object> model = new HashMap<String, Object>();
			model.put("url", url);
			model.put("welcomeText", "Hi There!!");
			final String text = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "UserVerificationMail.vm", UTF_8_ENCODING, model);
			helper.setText(text, true);
			/*helper.addInline("logo", logo);
			helper.addInline("facebook", facebook);
			helper.addInline("twitter", twitter);
			helper.addInline("background", background);
			helper.addInline("footer", footer);
			helper.addInline("offer", offer);*/
			mailSender.send(message);
			LOGGER.info("=========Mail successsfully sent===========");
		} catch (final MessagingException e) {
			LOGGER.error("===============Error while Sending Email to {}============", e);
		}

	}	
	
	public void sendSubscribeMail(final SubscribeUser subscribeUser) {
		try {
			final MimeMessage message = mailSender.createMimeMessage();
			final MimeMessageHelper helper = new MimeMessageHelper(message, true);
			helper.setSubject("Subscription Confirmation Mail");
			helper.setFrom(fromContactId);

			helper.setTo(subscribeUser.getEmail());
			final Map<String, Object> model = new HashMap<String, Object>();
			model.put("welcomeText", "Hi There!!");
			final String text = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "UserSubscriptionMail.vm", UTF_8_ENCODING, model);
			helper.setText(text, true);
			mailSender.send(message);
		} catch (final MessagingException e) {
			LOGGER.error("Error Sending Email to {}", e, subscribeUser.getEmail());
		}

	}
	
	
	public void sendTransactionMail(final String userMailId,final Double amount) {
		try {
			final MimeMessage message = mailSender.createMimeMessage();
			final MimeMessageHelper helper = new MimeMessageHelper(message, true);
			helper.setSubject("Transaction Confirmation Mail");
			helper.setFrom(fromContactId);
			helper.setTo(adminMailId);
			helper.setCc(userMailId);
			final Map<String, Object> model = new HashMap<String, Object>();
			model.put("welcomeText", "Hi There!!");
			final String text ="Hi Admin, \n"+ userMailId+" has sent a request to credit "+amount+"$";
			helper.setText(text);
			mailSender.send(message);
		} catch (final MessagingException e) {
			LOGGER.error("Error Sending Email to {}", e, userMailId);
		}

	}
	
	public void sendResetToken(final String userMailId,final String token) {
		try {
			final MimeMessage message = mailSender.createMimeMessage();
			final MimeMessageHelper helper = new MimeMessageHelper(message, true);
			helper.setSubject("Forgot Password Mail");
			helper.setFrom(fromContactId);
			helper.setTo(userMailId);
			
			final Map<String, Object> model = new HashMap<String, Object>();
			model.put("token", token);
			final String text = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "ForgotPasswordMail.vm", UTF_8_ENCODING, model);
			helper.setText(text, true);
			
			mailSender.send(message);
		} catch (final MessagingException e) {
			LOGGER.error("Error Sending Email to {}", e, userMailId);
		}

	}
	
	/*public void sendMail(final String mail, String url){
	SendGrid sendgrid = new SendGrid("SG.bx_99_L7R9y7ttSmZB3nKw.nDmK_ZN3pJElgM6MmSEwPt-bsbxLh_lnK6J7Kt911bQ");

    SendGrid.Email email = new SendGrid.Email();
    email.addTo("harishanuragi@gmail.com");
    email.
    email.setFrom("hchandra@craterzone.com");
    email.setSubject("Hello World");
    //email.setText("My first email with SendGrid Java!");
    final Map<String, Object> model = new HashMap<String, Object>();
	model.put("url", url);
	model.put("welcomeText", "Hi There!!");
	final String text = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "UserVerificationMail.vm", UTF_8_ENCODING, model);
	email.setHtml(text);

    try {
      SendGrid.Response response = sendgrid.send(email);
      System.out.println(response.getMessage());
    }
    catch (SendGridException e) {
      System.err.println(e);
    }
}*/
}