/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.dao;

import java.util.Collection;
import java.util.List;

import com.crater.mentorz.exception.ForbiddenException;
import com.crater.mentorz.models.post.AbuseType;
import com.crater.mentorz.models.post.Comment;
import com.crater.mentorz.models.post.Post;
import com.crater.mentorz.models.post.PostV2;
import com.google.common.base.Optional;


public interface PostDAOV2 {
	PostV2 addPostWithContent(final Long userId, final PostV2 post);

	PostV2 addPostWithOutContent(final Long userId, final PostV2 post);

	void like(final Long postId, final Long userId) throws ForbiddenException;

	void view(final Long postId, final Long userId) throws ForbiddenException;

	void share(final Long postId, final Long userId) throws ForbiddenException;

	Comment addComment(final Long userId, final Long postId, final Comment comment) throws ForbiddenException;

	Optional<List<Post>> getPostByUserId(final Long userId, final Integer pageNo);

	Optional<List<Comment>> getCommentByPostId(final Long postId, final Integer pageNo);

	Optional<List<Post>> getBoard(final Integer pageNo, final Long userId);

	Integer deleteComment(final Long userId, final Long postId, final Long commentId);

	Optional<List<Post>> getFriendPostByUserId(final Long userId, final Long friendId, final Integer pageNo);

	void abusePost(final Long userId, final Long postId, final AbuseType abuseType) throws ForbiddenException;

	Optional<List<Post>> searchPost(final Long userId, final List<Integer> interest, final Integer pageNo);
	
	Long getUserIdByPost(final Long postId);
	
	Optional<Comment> getCommentByCommentId(final Long commentId);
	
	Optional<Post> getPostByPostIdInShareOut(final Long postId);

	Optional<Collection<PostV2>> getPostByPostId(Long postId);
	
}
