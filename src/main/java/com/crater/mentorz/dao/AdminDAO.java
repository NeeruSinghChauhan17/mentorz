package com.crater.mentorz.dao;

import java.util.List;

import com.crater.mentorz.models.admin.Action;
import com.crater.mentorz.models.admin.Rank;
import com.crater.mentorz.models.admin.User;
import com.crater.mentorz.models.post.Post;
import com.crater.mentorz.models.user.Feedback;
import com.crater.mentorz.models.user.Interest;
import com.google.common.base.Optional;

/**
 *
 * @Auther Harish Anuragi
 */
public interface AdminDAO {

	String login(final User user);

	Optional<List<com.crater.mentorz.models.user.User>> getUsers(final Integer pageNo);
	
	List<com.crater.mentorz.models.user.User> getMentors();

	Optional<List<Feedback>> getFeedback(final Integer pageNo);

	Integer saveInterest(final Interest interest);

	com.google.common.base.Optional<List<com.crater.mentorz.models.user.User>> getBlockedUsers(final Integer pageNo);

	Boolean updateUserAccountStatus(final Long userId, final Action action);

	Optional<List<Feedback>> searchFeedbackByUserName(final String userName, final Integer pageNo);

	Optional<List<com.crater.mentorz.models.user.User>> searchUsersByUserName(final String userName, final Integer pageNo);

	Optional<List<Post>> searchAbusedUsersByUserName(final String userName, final Integer pageNo);

	Optional<List<com.crater.mentorz.models.user.User>> searchBlockedUsersByUserName(final String userName, final Integer pageNo);

	Optional<List<Post>> getAbusedPost(final Integer pageNo);

	void updatePostStatus(final Long userId, final Long postId, final Action action);
	
	Integer getTotalUser();
	
	Integer getTotalMentor();
	
	Integer getTotalExpertise();
	
	Integer getTotalValues();
	
	List<Interest> getInterestsList();
	
	List<Rank> getExpertisesRank();
	
	List<Rank> getValuesRank();

	List<Rank> getInterestsRank();
}

