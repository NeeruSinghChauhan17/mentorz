/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.dao;

import java.util.Map;

import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

/**
 * 
 * @author Harish Anuragi
 *
 */
public interface BaseDAO {

	public abstract NamedParameterJdbcOperations getJdbcTemplate();

	public abstract String getQueryById(String id);

	public abstract int insert(String query, Map<String, Object> paramMap);

	public abstract int getRowCount(String query, SqlParameterSource source);
}