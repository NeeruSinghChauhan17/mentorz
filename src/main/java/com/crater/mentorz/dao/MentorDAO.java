/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.dao;

import java.util.List;
import java.util.Set;

import com.crater.mentorz.exception.ForbiddenException;
import com.crater.mentorz.models.mentee.MenteeRequestTimeStamp;
import com.crater.mentorz.models.mentor.BeAMentor;
import com.crater.mentorz.models.mentor.Expertise;
import com.crater.mentorz.models.mentor.MentorFilter;
import com.crater.mentorz.models.user.Interest;
import com.crater.mentorz.models.user.User;
import com.google.common.base.Optional;


public interface MentorDAO {

	void addMentor(final Long userId, final BeAMentor mentor) throws ForbiddenException;


	Optional<List<User>> getMentor(final Long userId, final MentorFilter filter, final Integer pageNo);

	List<Expertise> getMyExpertise(final Long userId);

	Optional<BeAMentor> getPublicBio(final Long userId);

	Optional<List<MenteeRequestTimeStamp>> getMenteeRequests(final Long userId, final Integer pageNo);

	Optional<List<MenteeRequestTimeStamp>> getPendingRequests(final Long userId, final Integer pageNo);

	void addMentee(final Long userId, final Long menteeId) throws ForbiddenException;

	Boolean isRequestAlreadySent(final Long userId, final Long mentorId);

	Boolean isMyMentor(final Long userId, final Long mentorId);

	Integer deleteRequest(final Long userId, final Long menteeId);

	Integer cancelPendingRequest(final Long userId, final Long mentorId);

	List<Expertise> getExpertiseById(final Long mentorId,final Integer interestId);

	List<Expertise> getExpertiseByParentId(final Integer parentId);

	List<Expertise> getExpertiseList(final Long mentorId);
	
	void updateExperties(final long userId, final List<Expertise> experties,final Integer added);
	
	boolean isValidMentee( final long userId , final long menteeId );

	List<Expertise> getExpertiseListByParentId(Integer parentId, Long mentorId);

	
	public void deleteExpertises(long userId);

	List<Expertise> getParentExpertises(Set<Integer> childExpertises);
	List<Interest> getParentInterestIds(Set<Integer> childExpertises);

	
	
}
