package com.crater.mentorz.dao.impl.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.crater.mentorz.models.mentee.MenteeRequestTimeStamp;
import com.crater.mentorz.models.user.Profile;

public class MenteeRequestTimeStampRowMapper  implements RowMapper<MenteeRequestTimeStamp>{

	
	@Override
	public MenteeRequestTimeStamp mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new MenteeRequestTimeStamp(rs.getLong("mentee_id"), null, null, null, new Profile(null, null, rs.getString("profile_image_url_lres"),
				rs.getString("profile_image_url_hres"), 0, 0, 0, 0, null, 0, 0,rs.getString("name")
				, 0, null, null, null, null, null,null,null), null, null, null,null,null,rs.getLong("request_time"),null);
	}

}
