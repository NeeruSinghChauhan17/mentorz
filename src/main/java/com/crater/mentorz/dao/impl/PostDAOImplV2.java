/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.dao.impl;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.crater.mentorz.dao.BaseDAO;
import com.crater.mentorz.dao.PostDAOV2;
import com.crater.mentorz.dao.impl.rowmapper.CommentRowMapper;
import com.crater.mentorz.dao.impl.rowmapper.PostRequestResultSetExtractor;
import com.crater.mentorz.dao.impl.rowmapper.PostRowMapper;
import com.crater.mentorz.enums.AccountStatus;
import com.crater.mentorz.exception.ForbiddenException;
import com.crater.mentorz.models.post.AbuseType;
import com.crater.mentorz.models.post.Comment;
import com.crater.mentorz.models.post.Content;
import com.crater.mentorz.models.post.MediaType;
import com.crater.mentorz.models.post.Post;
import com.crater.mentorz.models.post.PostV2;
import com.google.common.base.Optional;


@Repository
public class PostDAOImplV2 implements PostDAOV2 {

	private final PostRowMapper POST_ROW_MAPPER = new PostRowMapper();
	private final CommentRowMapper COMMENT_ROW_MAPPER = new CommentRowMapper();
	// private static final Logger LOGGER =  LoggerFactory.getLogger(PostDAOImpl.class);
	private static final ResultSetExtractor<Collection<PostV2>>  POST_RESULT_SET_EXTRACTOR = new PostRequestResultSetExtractor();
	private final BaseDAO baseDao;

	private static final String[] POST_ID_COLUMN_NAME = { "post_id" };
	private static final String[] POST_CONTENT_ID_COLUMN_NAME = { "post_content_id" };
	private static final String[] COMMENT_ID_COLUMN_NAME = { "comment_id" };
	private final int NO_OF_POSTS_PER_PAGE = 20;

	@Autowired
	public PostDAOImplV2(final BaseDAOFactory baseDaoFactory) {
		this.baseDao = checkNotNull(baseDaoFactory.createBaseDao("post.xml"), "Base Dao Cannot Be Null");
	}

	@Override
	public PostV2 addPostWithContent(final Long userId, final PostV2 post) {
		final PostV2 postWithId = savePostInfo(userId, post);
		final ArrayList<Content> content = savePostContent(post.getContent(), postWithId.getPostId());
		final PostV2 postWithContent = PostV2.createWithContent(content, postWithId);
		return postWithContent;
	}

	private ArrayList<Content> savePostContent(final ArrayList<Content> content, final long postId) {
		final String saveContentQuery = baseDao.getQueryById("saveContent");
		final KeyHolder keyHolder = new GeneratedKeyHolder();
		ArrayList<Content> contents=new ArrayList<Content>();
            for(Content con:content){
		     final SqlParameterSource source = new MapSqlParameterSource()
				.addValue("mediaType", MediaType.getIntValue(con.getMediaType()))
				.addValue("mediaUrlLres", con.getLresId())
				.addValue("mediaUrlHres", con.getHresId())
				.addValue("description", con.getDescription())
				.addValue("postId", postId);
		        baseDao.getJdbcTemplate().update(saveContentQuery, source, keyHolder, POST_CONTENT_ID_COLUMN_NAME);
		       final long contentId = keyHolder.getKey().intValue();
		      contents.add( Content.createWithContentId(contentId, con));
            }
		return contents;
	}
	
	
/*	
	try {
		final String deleteValuesQuery = baseDao.getQueryById("deleteValues");
		final String updateValuesQuery = baseDao.getQueryById("updateValues");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId);
		baseDao.getJdbcTemplate().update(deleteValuesQuery, source);
		final SqlParameterSource[] batch = new MapSqlParameterSource[values.size()];
		for (int index = 0; index < values.size(); index++) {
			final Value value = values.get(index);
			batch[index] = new MapSqlParameterSource().addValue("userId", userId).addValue("value_id", value.getValueId());
		}
		baseDao.getJdbcTemplate().batchUpdate(updateValuesQuery, batch);
	} catch (DataIntegrityViolationException e) {
		LOGGER.info("========VALUES CONTAINS INVALID ID'S============");
		//throw new ForbiddenException.Builder().build();
		
	}*/

	private PostV2 savePostInfo(final long userId, final PostV2 post) {
		final KeyHolder keyHolder = new GeneratedKeyHolder();
		final String saveVerseQuery = baseDao.getQueryById("savePost");
		final SqlParameterSource source = new MapSqlParameterSource()
				.addValue("userId", userId)
				.addValue("shareTime", post.getShareTime());
		baseDao.getJdbcTemplate().update(saveVerseQuery, source, keyHolder, POST_ID_COLUMN_NAME);
		final long postId = keyHolder.getKey().longValue();
		return PostV2.createWithPostIdAndUserId(postId, post, userId);
	}

	@Override
	public PostV2 addPostWithOutContent(final Long userId, final PostV2 post) {
		return savePostInfo(userId, post);
	}

	@Override
	public void like(final Long postId, final Long userId) throws DuplicateKeyException, ForbiddenException {
		addLikes(postId, userId);
		updateLikeCount(postId);
	}

	private void addLikes(final long postId, final long userId) throws DuplicateKeyException, ForbiddenException {
		try {
			final String updateReverseInfoQuery = baseDao.getQueryById("savePostLike");
			final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId).addValue("postId", postId);
			baseDao.getJdbcTemplate().update(updateReverseInfoQuery, source);
		} catch (DuplicateKeyException e) {
			throw e;
		} catch (DataIntegrityViolationException e) {
			throw new ForbiddenException.Builder().build();
		}
	}

	private void updateLikeCount(final long postId) {
		final String updateLikeCountQuery = baseDao.getQueryById("updateLikeCount");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("postId", postId);
		baseDao.getJdbcTemplate().update(updateLikeCountQuery, source);

	}

	@Override
	public void view(final Long postId, final Long userId) throws DuplicateKeyException, ForbiddenException {
		addViews(postId, userId);
		updateViewCount(postId);
	}

	@Override
	public void share(final Long postId, final Long userId) throws DuplicateKeyException, ForbiddenException {
		addShare(postId, userId);
		updateShareCount(postId);
	}

	private void addViews(final long postId, final long userId) throws DuplicateKeyException, ForbiddenException {
		try {
			final String updateReverseInfoQuery = baseDao.getQueryById("savePostView");
			final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId).addValue("postId", postId);
			baseDao.getJdbcTemplate().update(updateReverseInfoQuery, source);
		} catch (DuplicateKeyException e) {
			throw e;
		} catch (DataIntegrityViolationException e) {
			throw new ForbiddenException.Builder().build();
		}
	}

	private void updateViewCount(final long postId) {
		final String updateLikeCountQuery = baseDao.getQueryById("updateViewCount");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("postId", postId);
		baseDao.getJdbcTemplate().update(updateLikeCountQuery, source);

	}

	private void addShare(final long postId, final long userId) throws DuplicateKeyException, ForbiddenException {
		try {
			final String updateReverseInfoQuery = baseDao.getQueryById("savePostShare");
			final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId).addValue("postId", postId);
			baseDao.getJdbcTemplate().update(updateReverseInfoQuery, source);
		} catch (DuplicateKeyException e) {
			throw e;
		} catch (DataIntegrityViolationException e) {
			throw new ForbiddenException.Builder().build();
		}
	}

	private void updateShareCount(final long postId) {
		final String updateLikeCountQuery = baseDao.getQueryById("updateShareCount");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("postId", postId);
		baseDao.getJdbcTemplate().update(updateLikeCountQuery, source);

	}

	@Override
	public Comment addComment(final Long userId, final Long postId, final Comment comment) throws ForbiddenException {
		final Comment commentWithId = saveComment(userId, postId, comment);
		updateCommentCount(postId);
		return commentWithId;
	}

	private void updateCommentCount(final long postId) {
		final String updateLikeCountQuery = baseDao.getQueryById("updateCommentCount");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("postId", postId);
		baseDao.getJdbcTemplate().update(updateLikeCountQuery, source);
	}

	private Comment saveComment(final long userId, final long postId, final Comment comment) throws ForbiddenException {
		try {
			final String saveContentQuery = baseDao.getQueryById("saveComment");
			final KeyHolder keyHolder = new GeneratedKeyHolder();

			final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId).addValue("postId", postId)
					.addValue("comment", comment.getComment()).addValue("commentTime", comment.getCommentTime());
			baseDao.getJdbcTemplate().update(saveContentQuery, source, keyHolder, COMMENT_ID_COLUMN_NAME);
			final long contentId = keyHolder.getKey().intValue();
			return Comment.createWithCommentId(contentId, comment, userId);
		} catch (DataIntegrityViolationException e) {
			throw new ForbiddenException.Builder().build();
		}
	}

	@Override
	public Optional<List<Post>> getPostByUserId(final Long userId, final Integer pageNo) {
		final String getPostByUserIdQuery = baseDao.getQueryById("getUserPostsByUserId");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId)
				.addValue("offset", pageNo * NO_OF_POSTS_PER_PAGE).addValue("limit", NO_OF_POSTS_PER_PAGE);
		final List<Post> post = baseDao.getJdbcTemplate().query(getPostByUserIdQuery, source, POST_ROW_MAPPER);
		return Optional.fromNullable(post);
	}

	@Override
	public Optional<List<Comment>> getCommentByPostId(final Long postId, final Integer pageNo) {
		final String getPostByUserIdQuery = baseDao.getQueryById("getCommentByPostId");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("postId", postId)
				.addValue("offset", pageNo * NO_OF_POSTS_PER_PAGE).addValue("limit", NO_OF_POSTS_PER_PAGE);
		final List<Comment> comments = baseDao.getJdbcTemplate().query(getPostByUserIdQuery, source, COMMENT_ROW_MAPPER);
		return Optional.fromNullable(comments);
	}

	@Override
	public Optional<List<Post>> getBoard(final Integer pageNo, final Long userId) {
		final String getBoardByInterestsQuery = baseDao.getQueryById("getBoardByInterests");
		final SqlParameterSource source = new MapSqlParameterSource()
		.addValue("userId", userId)
		.addValue("status", AccountStatus.ACTIVE.getStatusCode())
		.addValue("offset", pageNo * NO_OF_POSTS_PER_PAGE)
		.addValue("limit", NO_OF_POSTS_PER_PAGE);
		final List<Post> comments = baseDao.getJdbcTemplate().query(getBoardByInterestsQuery, source, POST_ROW_MAPPER);
		return Optional.fromNullable(comments);
	}

	@Override
	public Integer deleteComment(final Long userId, final Long postId, final Long commentId) {
		final String deleteCommentQuery = baseDao.getQueryById("deleteCommentQuery");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId).addValue("commentId", commentId)
				.addValue("postId", postId);
		decreaseCommentCount(postId);
		return baseDao.getJdbcTemplate().update(deleteCommentQuery, source);
	}

	private void decreaseCommentCount(final long postId) {
		final String updateLikeCountQuery = baseDao.getQueryById("decreaseCommentCount");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("postId", postId);
		baseDao.getJdbcTemplate().update(updateLikeCountQuery, source);
	}

	@Override
	public Optional<List<Post>> getFriendPostByUserId(final Long userId, final Long friendId, final Integer pageNo) {
		final String getPostByUserIdQuery = baseDao.getQueryById("getFriendPostsByUserId");
		final SqlParameterSource source = new MapSqlParameterSource()
				.addValue("userId", userId).addValue("friendId", friendId)
				.addValue("status", AccountStatus.ACTIVE.getStatusCode())
				.addValue("offset", pageNo * NO_OF_POSTS_PER_PAGE)
				.addValue("limit", NO_OF_POSTS_PER_PAGE);
		final List<Post> post = baseDao.getJdbcTemplate().query(getPostByUserIdQuery, source, POST_ROW_MAPPER);
		return Optional.fromNullable(post);
	}

	@Override
	public void abusePost(final Long userId, final Long postId, final AbuseType abuseType) throws ForbiddenException {
		try {
			final String query = baseDao.getQueryById("abusePost");
			final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId)
					.addValue("postId", postId)
					.addValue("abuseType", AbuseType.getIntValue(abuseType));
			baseDao.getJdbcTemplate().update(query, source);
		} catch (DataIntegrityViolationException e) {
			throw new ForbiddenException.Builder().build();
		} 
	}

	@Override
	public Optional<List<Post>> searchPost(final Long userId, final List<Integer> interest, final Integer pageNo) {
		final String query = baseDao.getQueryById("searchPost");
		final SqlParameterSource source = new MapSqlParameterSource()
		.addValue("userId", userId)
		.addValue("interests", interest)
		.addValue("offset", pageNo * NO_OF_POSTS_PER_PAGE)
		.addValue("limit", NO_OF_POSTS_PER_PAGE);
		final List<Post> comments = baseDao.getJdbcTemplate().query(query, source, POST_ROW_MAPPER);
		return Optional.fromNullable(comments);
	}

	@Override
	public Long getUserIdByPost(final Long postId) {
		final String query = baseDao.getQueryById("getUserIdByPost");
		final SqlParameterSource source = new MapSqlParameterSource()
				.addValue("postId", postId);
		return baseDao.getJdbcTemplate().queryForObject(query, source,Long.class);
	}

	@Override
	public Optional<Comment> getCommentByCommentId(final Long commentId) {
		try {
			final String query = baseDao.getQueryById("getCommentByCommentId");
			final SqlParameterSource source = new MapSqlParameterSource().addValue("commentId", commentId);
			final Comment comment = baseDao.getJdbcTemplate().queryForObject(query, source, COMMENT_ROW_MAPPER);
			return Optional.fromNullable(comment);
		} catch (EmptyResultDataAccessException e) {
			return Optional.<Comment> absent();
		}

	}

	@Override
	public Optional<Post> getPostByPostIdInShareOut(final Long postId) {
		try {
			final String query = baseDao.getQueryById("getPostByPostIdShareOutV2");
			final SqlParameterSource source = new MapSqlParameterSource().addValue("postId", postId);
			final Post post = baseDao.getJdbcTemplate().queryForObject(query, source, POST_ROW_MAPPER);
			return Optional.fromNullable(post);
		} catch (EmptyResultDataAccessException e) {
			return Optional.<Post> absent();
		}
	}

	@Override
	public Optional<Collection<PostV2>> getPostByPostId(Long postId) {
		try {
			final String query = baseDao.getQueryById("getPostByPostIdV2");
			final SqlParameterSource source = new MapSqlParameterSource().addValue("postId", postId);
			final Collection<PostV2> post = baseDao.getJdbcTemplate().query(query, source, POST_RESULT_SET_EXTRACTOR);
			return Optional.fromNullable(post);
		} catch (EmptyResultDataAccessException e) {
			return  Optional.<Collection<PostV2>> absent();
		}
	}

	
}
