package com.crater.mentorz.dao.impl.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.crater.mentorz.models.post.Content;
import com.crater.mentorz.models.post.MediaType;
import com.crater.mentorz.models.post.PostV2;

public class PostRequestResultSetExtractor  implements ResultSetExtractor<Collection<PostV2>>{
	
	@Override
	public Collection<PostV2> extractData(ResultSet rs) throws SQLException, DataAccessException {
		final Map<Long, PostV2> userMap = new HashMap<Long, PostV2>();
		while (rs.next()) {
			final Long postId = rs.getLong("post_id");
			if (!userMap.containsKey(postId)) {
				userMap.put(postId, new PostV2(rs.getLong("post_id"),rs.getLong("user_id"),rs.getInt("like_count"), rs.getLong("share_time"),
						rs.getLong("liked") > 0 ? true : false,new ArrayList<Content>(),null, rs.getInt("view_count"),
								rs.getInt("share_count"), rs.getInt("comment_count"), rs.getString("name"), null, null));
			}
			
	      userMap.get(postId).getContent().add(new Content(rs.getLong("post_content_id"), MediaType.getMediaType(rs.getInt("media_type")), rs.getString("media_url_lres"),
					rs.getString("media_url_hres"), rs.getString("description")));
			
		}

		return userMap.values();
	}

}
