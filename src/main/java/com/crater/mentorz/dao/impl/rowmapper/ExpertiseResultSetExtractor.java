package com.crater.mentorz.dao.impl.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.crater.mentorz.models.mentor.Expertise;
import com.crater.mentorz.models.mentor.ExpertiseList;


public class ExpertiseResultSetExtractor implements ResultSetExtractor<ExpertiseList>{

	@Override
	public ExpertiseList extractData(ResultSet rs) throws SQLException, DataAccessException {
		final List<Expertise> expertise = new ArrayList<Expertise>();
		while(rs.next()) {
			expertise.add(new Expertise(rs.getInt("interest_id"), rs.getString("name"),rs.getInt("parent_id"),rs.getBoolean("has_children"),false));
		}
		return new ExpertiseList(expertise);
	}
	
}

