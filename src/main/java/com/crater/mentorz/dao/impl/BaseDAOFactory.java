/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.dao.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.stereotype.Component;

import com.crater.mentorz.dao.BaseDAO;

/**
 * 
 * @author Harish Anuragi
 *
 */
@Component
public class BaseDAOFactory {

	private static final Logger LOGGER = LoggerFactory.getLogger(BaseDAOFactory.class);

	private static final String DB_CLASS_PATH = "/db/";

	private final NamedParameterJdbcOperations jdbcTemplate;

	@Autowired
	public BaseDAOFactory(@Qualifier("jdbcTemplate") final NamedParameterJdbcOperations jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public BaseDAO createBaseDao(final String fileName) {
		final Properties properties = new Properties();
		BaseDAO baseDao = null;
		try {
			properties.loadFromXML(this.getClass().getResourceAsStream(DB_CLASS_PATH + "" + fileName));
			final Map<String, String> daoQueryMap = new HashMap<>();
			for (final String name : properties.stringPropertyNames()) {
				daoQueryMap.put(name, properties.getProperty(name));
			}
			baseDao = new BaseDAOImpl(daoQueryMap, jdbcTemplate);
		} catch (final IOException e) {
			LOGGER.error("Error while loading file {}", e, fileName);
		}
		return baseDao;

	}
}
