package com.crater.mentorz.dao.impl.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.crater.mentorz.models.mentor.Expertise;

public class ExpertiseListRowMapper implements RowMapper<Expertise> {

	@Override
	public Expertise mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		return new Expertise(rs.getInt("interest_id"), rs.getString("name"),rs.getInt("parent_id"),rs.getBoolean("has_children"),(rs.getLong("user_id")>0?true:false));
	}
}
