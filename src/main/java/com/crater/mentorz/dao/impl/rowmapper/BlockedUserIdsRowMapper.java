package com.crater.mentorz.dao.impl.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @Auther Harish Anuragi
 */
public class BlockedUserIdsRowMapper implements RowMapper<Map<Long, Long>>{

	@Override
	public Map<Long, Long> mapRow(ResultSet rs, int rowNum) throws SQLException {
		Map<Long, Long> user = new HashMap<>();
		user.put(rs.getLong("user_id"), rs.getLong("blocked_id"));
		return user;
	}

}

