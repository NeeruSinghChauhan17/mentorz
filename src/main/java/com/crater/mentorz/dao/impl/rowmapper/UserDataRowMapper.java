/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.dao.impl.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.crater.mentorz.models.user.InternationalNumber;
import com.crater.mentorz.models.user.Profile;
import com.crater.mentorz.models.user.User;

public class UserDataRowMapper implements RowMapper<User> {

	@Override
	public User mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new User(0L, null, null, null,null, 
				new Profile(null, null, rs.getString("profile_image_url_lres"),
				rs.getString("profile_image_url_hres"), 0, 0, 0,
				0, null, 0, 0, rs.getString("name"), 0,
				null, null, null, null, null,null,
				null), null,
				new ExpertiseResultSetExtractor().extractData(rs).getExpertise(),
				null,null,null,null,null);
	}

}
