package com.crater.mentorz.dao.impl.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.crater.mentorz.models.payment.Balance;

public class BalanceRowMapper implements RowMapper<Balance> {

	@Override
	public Balance mapRow(ResultSet rs, int rowNum) throws SQLException {

		return new Balance(rs.getDouble("balance_amount"), rs.getDouble("credits"), rs.getInt("charge_price"));
	}

}
