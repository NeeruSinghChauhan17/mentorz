package com.crater.mentorz.dao.impl.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.crater.mentorz.models.user.Profile;


public class ProfileRowMapper implements RowMapper<Profile>{

	@Override
	public Profile mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new Profile(null, null, rs.getString("profile_image_url_lres"),
				rs.getString("profile_image_url_hres"), 0, 0, 0, 0, rs.getString("basic_info"), 0, 0,
				rs.getString("name"), 0, null, rs.getString("video_bio_lres"),
				rs.getString("video_bio_hres"), null, null,null,null
				);
	}

}

