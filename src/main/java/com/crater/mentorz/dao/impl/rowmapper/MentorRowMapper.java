/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.dao.impl.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.crater.mentorz.models.user.Profile;
import com.crater.mentorz.models.user.User;


public class MentorRowMapper implements RowMapper<User> {

	@Override
	public User mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new User(rs.getLong("user_id"), null, null, null,null, new Profile(null, null, rs.getString("profile_image_url_lres"),
				rs.getString("profile_image_url_hres"), rs.getInt("followers"), 0, 0, 0, rs.getString("basic_info"), 0,
				rs.getInt("charge_price"), rs.getString("name"), 0, rs.getInt("exp"), rs.getString("video_bio_lres"), 
				rs.getString("video_bio_hres"), null, null,null,null), null, null, null,null,null,null,null);
	}

}
