package com.crater.mentorz.dao.impl.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.crater.mentorz.elasticsearch.model.req.Source;

public class AverageRatingRowMapper implements RowMapper<Source> {

	@Override
	public Source mapRow(ResultSet rs, int arg1) throws SQLException {
		
		return new Source(rs.getLong("rated_to"), null, 
				null,
				null,0,0,0,
				0,
				null,0,0,null,null,
				null,0,0,null,null,
				null,rs.getFloat("avg_rating"),null,0, null,null,null ,
				null,null,null,null,null,null);
	}
	

}
