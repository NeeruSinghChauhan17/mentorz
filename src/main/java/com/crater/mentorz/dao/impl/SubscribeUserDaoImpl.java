package com.crater.mentorz.dao.impl;

import static com.google.common.base.Preconditions.checkNotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import com.crater.mentorz.dao.BaseDAO;
import com.crater.mentorz.dao.SubscribeUserDao;
import com.crater.mentorz.dao.impl.rowmapper.SubscribeUserRowMapper;
import com.crater.mentorz.models.user.SubscribeUser;
import com.google.common.base.Optional;

@Repository
public class SubscribeUserDaoImpl implements SubscribeUserDao {

	private final BaseDAO baseDao;
	private static final SubscribeUserRowMapper USER_ROW_MAPPER = new SubscribeUserRowMapper();
	private static final Logger LOGGER = LoggerFactory.getLogger(SubscribeUserDaoImpl.class);

	@Autowired
	public SubscribeUserDaoImpl(final BaseDAOFactory baseDaoFactory) {
		this.baseDao = checkNotNull(baseDaoFactory.createBaseDao("subscribeUser.xml"), "Base Dao Cannot Be Null");

	}

	@Override
	public void saveSubscribedUser(final SubscribeUser subscribeUser) {
		LOGGER.debug("=======> Save Subscribed User ===================>" + subscribeUser.toString() + "=>");
		final String query = baseDao.getQueryById("subscribeUser");
		MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("email", subscribeUser.getEmail()).addValue(
				"status", 1);
		;
		baseDao.getJdbcTemplate().update(query, sqlParameterSource);
	}

	@Override
	public Optional<SubscribeUser> findUserByEmail(final String email) {
		LOGGER.debug("=======> Find User By Email =====================>" + email + "==========>");
		final String query = baseDao.getQueryById("findUserByEmail");
		SqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("email", email);
		try {
			SubscribeUser subscribeUser = baseDao.getJdbcTemplate().queryForObject(query, sqlParameterSource, USER_ROW_MAPPER);
			return Optional.fromNullable(subscribeUser);
		} catch (EmptyResultDataAccessException e) {
			return Optional.<SubscribeUser> absent();
		} catch (DataAccessException e) {
			LOGGER.error("=======> Error While Finding User By Email ====> " + e + "=============>");
			return Optional.<SubscribeUser> absent();
		}
	}

}
