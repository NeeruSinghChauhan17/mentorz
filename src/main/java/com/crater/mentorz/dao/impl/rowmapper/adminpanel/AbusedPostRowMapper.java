package com.crater.mentorz.dao.impl.rowmapper.adminpanel;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.crater.mentorz.models.post.Post;
import com.crater.mentorz.utils.StringUtil;

/**
 *
 * @Auther Harish Anuragi
 */
public class AbusedPostRowMapper implements RowMapper<Post>{

	@Override
	public Post mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new Post(rs.getLong("post_id"), rs.getLong("user_id"), 0, 0l, null, null, null, 0, 0, 0, StringUtil.textDecoderUTF8(rs.getString("name")), rs.getInt("abuse_count"), rs.getInt("post_status") > 0);
	}

}

