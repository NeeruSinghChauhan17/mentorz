package com.crater.mentorz.dao.impl.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.crater.mentorz.models.mentee.MenteeRequestTimeStamp;
import com.crater.mentorz.models.mentor.Expertise;
import com.crater.mentorz.models.user.Profile;


public class MenteePendingRequestExpertiseResultSetExtractor implements ResultSetExtractor<Collection<MenteeRequestTimeStamp>>{
	
	@Override
	public  Collection<MenteeRequestTimeStamp> extractData(ResultSet rs) throws SQLException, DataAccessException {
		final Map<Long, MenteeRequestTimeStamp> userMap = new HashMap<Long, MenteeRequestTimeStamp>();
		while (rs.next()) {
			final Long userId = rs.getLong("mentor_id");
			if (!userMap.containsKey(userId)) {
				userMap.put(userId, new MenteeRequestTimeStamp(userId,null,null,null,new Profile(null, null, rs.getString("profile_image_url_lres"),
						rs.getString("profile_image_url_hres"), 0, 0, 0, 0, null, 0, 0,rs.getString("name")
						, 0, null, null, null, null, null,null,null),null,new ArrayList<Expertise>(),null,null,null,rs.getLong("request_time"),rs.getString("comment")));
			}

			Integer expertise_id=rs.getInt("interest_id");
			userMap.get(userId).getExperties().add(new Expertise(expertise_id,rs.getString("interest"),
	        		   0,false ,false));
			
		}

		return userMap.values();
	}

}

