/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.dao.impl;

import static com.crater.mentorz.dao.impl.DefaultUtils.createUpdatingStringSupplier;
import static com.crater.mentorz.dao.impl.DefaultUtils.updateIfNotNull;
import static com.google.common.base.Preconditions.checkNotNull;

import java.sql.Date;
import java.time.Instant;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.crater.mentorz.dao.BaseDAO;
import com.crater.mentorz.dao.UserDAO;
import com.crater.mentorz.dao.UserDAOV3;
import com.crater.mentorz.dao.impl.rowmapper.AverageRatingRowMapper;
import com.crater.mentorz.dao.impl.rowmapper.BalanceRowMapper;
import com.crater.mentorz.dao.impl.rowmapper.BlockUserResultSetExtractor;
import com.crater.mentorz.dao.impl.rowmapper.BlockedUserIdsRowMapper;
import com.crater.mentorz.dao.impl.rowmapper.FriendProfileRowMapper;
import com.crater.mentorz.dao.impl.rowmapper.InterestIdRowMapper;
import com.crater.mentorz.dao.impl.rowmapper.InterestRowMapper;
import com.crater.mentorz.dao.impl.rowmapper.InterestsRowMapper;
import com.crater.mentorz.dao.impl.rowmapper.MenteeRequestResultSetExtractor;
import com.crater.mentorz.dao.impl.rowmapper.MentorExpertisesResultSetExtractor;
import com.crater.mentorz.dao.impl.rowmapper.MyInterestRowMapper;
import com.crater.mentorz.dao.impl.rowmapper.PhoneContactRowMapper;
import com.crater.mentorz.dao.impl.rowmapper.ProfileRowMapper;
import com.crater.mentorz.dao.impl.rowmapper.ProfileSummaryRowMapper;
import com.crater.mentorz.dao.impl.rowmapper.RatingResultSetExtractor;
import com.crater.mentorz.dao.impl.rowmapper.RatingRowMapper;
import com.crater.mentorz.dao.impl.rowmapper.SkippedMentorsResultSetExtractor;
import com.crater.mentorz.dao.impl.rowmapper.SourceResultSetExtractor;
import com.crater.mentorz.dao.impl.rowmapper.UserAccountRowMapperV2;
import com.crater.mentorz.dao.impl.rowmapper.UserContactsRowMapper;
import com.crater.mentorz.dao.impl.rowmapper.UserDataRowMapper;
import com.crater.mentorz.dao.impl.rowmapper.UserExistanceRowMapper;
import com.crater.mentorz.dao.impl.rowmapper.UserProfileImageRowMapper;
import com.crater.mentorz.dao.impl.rowmapper.UserProfileRowMapper;
import com.crater.mentorz.dao.impl.rowmapper.UserRowMapper;
import com.crater.mentorz.dao.impl.rowmapper.UserWithReviewRowMapper;
import com.crater.mentorz.dao.impl.rowmapper.ValuesRowMapper;
import com.crater.mentorz.elasticsearch.model.req.Source;
import com.crater.mentorz.enums.AccountStatus;
import com.crater.mentorz.exception.BadRequestException;
import com.crater.mentorz.exception.ConflitException;
import com.crater.mentorz.exception.ErrorEntity;
import com.crater.mentorz.exception.ForbiddenException;
import com.crater.mentorz.exception.NotFoundException;
import com.crater.mentorz.models.Rating;
import com.crater.mentorz.models.mentor.Expertise;
import com.crater.mentorz.models.payment.Balance;
import com.crater.mentorz.models.payment.Payment;
import com.crater.mentorz.models.payment.Redeem;
import com.crater.mentorz.models.user.DeviceInfo;
import com.crater.mentorz.models.user.DeviceType;
import com.crater.mentorz.models.user.Feedback;
import com.crater.mentorz.models.user.ForgetPassword;
import com.crater.mentorz.models.user.Interest;
import com.crater.mentorz.models.user.InternationalNumber;
import com.crater.mentorz.models.user.Login;
import com.crater.mentorz.models.user.PhoneContact;
import com.crater.mentorz.models.user.Profile;
import com.crater.mentorz.models.user.ProfileImage;
import com.crater.mentorz.models.user.SendRequest;
import com.crater.mentorz.models.user.SocialInterest;
import com.crater.mentorz.models.user.SocialSource;
import com.crater.mentorz.models.user.SocialUser;
import com.crater.mentorz.models.user.UpdatePasswordV2;
import com.crater.mentorz.models.user.User;
import com.crater.mentorz.models.user.Value;
import com.crater.mentorz.service.impl.ElasticSearchHelper;
import com.crater.mentorz.utils.GMTCalender;
import com.google.common.base.Optional;


@Repository
public class UserDAOImplV3 implements UserDAOV3 {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserDAO.class);
	private final BaseDAO baseDao;
	private static final int NO_OF_ROWS_PER_PAGE = 20;
	private final UserRowMapper USER_ROW_MAPPER = new UserRowMapper();
	private final UserAccountRowMapperV2 USER_ACCOUNT_ROW_MAPPER=new UserAccountRowMapperV2();
	private final UserProfileRowMapper USER_PROFILE_ROW_MAPPER = new UserProfileRowMapper();
	private final UserDataRowMapper USER_DATA_ROW_MAPPER=new UserDataRowMapper();
	private final ValuesRowMapper VALUE_ROW_MAPPER = new ValuesRowMapper();
	private final InterestRowMapper INTEREST_ROW_MAPPER = new InterestRowMapper();
	private final InterestsRowMapper INTERESTS_ROW_MAPPER = new InterestsRowMapper();
	private final MyInterestRowMapper MY_INTEREST_ROW_MAPPER=new MyInterestRowMapper();
	private static final String[] USER_ID_COLUMN_NAME = { "user_id" };
	private static final String[] MAPPING_ID_COLUMN_NAME = { "id" };
	private final UserProfileImageRowMapper USER_PROFILE_IMAGE_ROW_MAPPER = new UserProfileImageRowMapper();
	private final InterestIdRowMapper INTEREST_ID_ROW_MAPPER = new InterestIdRowMapper();
	private final int NO_OF_INTEREST_OR_VALUES_PER_PAGE = 50;
	private final UserExistanceRowMapper USER_EXISTANCE_ROW_MAPPER = new UserExistanceRowMapper();
	private final FriendProfileRowMapper FRIEND_PROFILE_ROW_MAPPER = new FriendProfileRowMapper();
	private final BalanceRowMapper BALANCE_ROW_MAPPER = new BalanceRowMapper();
	private final BlockedUserIdsRowMapper BLOCKED_USER_IDS_ROW_MAPPER = new BlockedUserIdsRowMapper();
	private final ProfileSummaryRowMapper PROFILE_SUMMARY_ROW_MAPPER = new ProfileSummaryRowMapper();
	private final ProfileRowMapper PROFILE_ROW_MAPPER = new ProfileRowMapper();
	private final RatingRowMapper RATING_ROW_MAPPER = new RatingRowMapper();
	private final RowMapper<User> USER_WITH_REVIEW_ROW_MAPPER = new UserWithReviewRowMapper();
	private static final ResultSetExtractor<Collection<Source>> RATING_RESULT_SET_EXTRACTOR=new RatingResultSetExtractor();
	private static final ResultSetExtractor<Collection<Source>> SOURCE_RESULT_EXTRACTOR = new SourceResultSetExtractor();
	private static final ResultSetExtractor<Collection<Source>> BLOCK_USER_RESULT_SET_EXTRACTOR = new BlockUserResultSetExtractor();
	private static final ResultSetExtractor<Collection<Source>> MENTEE_REQUEST_SENT_RESULT_SET_EXTRACTOR=new MenteeRequestResultSetExtractor();
	private static final AverageRatingRowMapper AVERAGE_RATING_ROW_MAPPER=new AverageRatingRowMapper();
	private static final PhoneContactRowMapper PHONE_CONTACTS_ROW_MAPPER=new PhoneContactRowMapper();
	private static final ResultSetExtractor<Collection<Source>>  SKIP_MENTORS_RESULT_SET_EXTRACTOR = new SkippedMentorsResultSetExtractor(); 
	private static final ResultSetExtractor<Collection<Source>>  MENTOR_EXPERTISES_RESULT_SET_EXTRACTOR = new MentorExpertisesResultSetExtractor();
	private static final UserContactsRowMapper USER_CONTACTS_ROW_MAPPER=new UserContactsRowMapper();
	private static final Integer VERIFIED_PHONE =1;
	@Autowired
	private ElasticSearchHelper esHelper;
	
	@Autowired
	public UserDAOImplV3(final BaseDAOFactory baseDaoFactory) {
		this.baseDao = checkNotNull(baseDaoFactory.createBaseDao("user.xml"), "Base Dao Cannot Be Null");

	}
	
	

	@Override
	public User save(final User user) throws ConflitException {
		final KeyHolder keyHolder = new GeneratedKeyHolder();
		final DeviceInfo deviceInfo = user.getDeviceInfo();
		final String saveUserQuery = baseDao.getQueryById("saveUserAccountInfoV3");
		try {
			baseDao.getJdbcTemplate().update(saveUserQuery, getAccountInfoParamMap(user), keyHolder, USER_ID_COLUMN_NAME);
		} catch (DataIntegrityViolationException e) {
			throw new ConflitException.Builder().entity(new ErrorEntity(3, "user already Registered")).build();
		}

		final long userId = keyHolder.getKey().intValue();
		final String saveProfileQuery = baseDao.getQueryById("saveUserProfileV3");       //save user(register)
	
		baseDao.insert(saveProfileQuery, getUserProfileParamMaps(userId, user));
		final String saveDeviceInfoQuery = baseDao.getQueryById("saveUserDeviceInfo");
		baseDao.insert(saveDeviceInfoQuery, getDeviceInfoParamMaps(userId, deviceInfo));
		return User.createWithUserId(userId, user);
	}

	private Map<String, Object> getUserProfileParamMaps(final long userId, final User usr) {
		final Map<String, Object> masterMap = new HashMap<>();
		masterMap.put("userId", userId);
		masterMap.put("profileImageLres", usr.getProfile().getLresId());
		masterMap.put("profileImageHres", usr.getProfile().getHresId());
		masterMap.put("basicInfo", usr.getProfile().getBasicInfo());
		masterMap.put("birthdate", usr.getProfile().getBirthDate());
		masterMap.put("name", usr.getProfile().getName());
		masterMap.put("videoBioLres", usr.getProfile().getVideoBioLres());
		masterMap.put("videoBioHres", usr.getProfile().getVideoBioHres());
		
		return masterMap;
	}

	private static final SqlParameterSource getAccountInfoParamMap(final User user) {

		final SqlParameterSource source = new MapSqlParameterSource()
				.addValue("userId", user.getUserId())
				.addValue("password", user.getPassword())
				.addValue("cc", user.getNumber().getCc())
				.addValue("areaCode", user.getNumber().getCode())
				.addValue("phoneNo",user.getNumber().getPhoneNumber())
				.addValue("registeredOn", GMTCalender.getInstance().getTime());
		return source;
	}

	private static final Map<String, Object> getDeviceInfoParamMaps(final long userId, 
			final DeviceInfo deviceInfo) {
		final Map<String, Object> masterMap = new HashMap<>();
		masterMap.put("userId", userId);
		masterMap.put("userAgent", deviceInfo.getUserAgent());
		masterMap.put("deviceToken", deviceInfo.getDeviceToken());
		masterMap.put("deviceType", DeviceType.getIntValue(deviceInfo.getDeviceType()));
		masterMap.put("timestamp", new Date(DateTime.now(DateTimeZone.UTC).getMillis()));
		return masterMap;
	}

	@Override
	public void verifyEmail(final Long userId) throws NotFoundException {
		final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId);
		final int updated = baseDao.getJdbcTemplate().update(baseDao.getQueryById("verifyEmailQuery"), source);
		if (updated == 0) {
			throw new NotFoundException.Builder().message("user not exists").build();
		}
	}

	@Override
	public void updateDeviceInfo(final Long userId, final DeviceInfo deviceInfo) {
		final String updateDeviceInfoQuery = baseDao.getQueryById("updateDeviceInfo");
		baseDao.getJdbcTemplate().update(updateDeviceInfoQuery, getDeviceInfoParamMaps(userId, deviceInfo));
	}

	@Override
	public Optional<User> getUserDetailsByEmail(final Login login) {
		try {
			final String getUserQuery = baseDao.getQueryById("getUserByEmailAndPasswordv3");
			final SqlParameterSource source = new MapSqlParameterSource()
					.addValue("emailId", login.getEmailId());
			final User user = baseDao.getJdbcTemplate().queryForObject(getUserQuery, source, USER_ACCOUNT_ROW_MAPPER);
			return Optional.fromNullable(user);
		} catch (EmptyResultDataAccessException e) {
			return Optional.<User> absent();
		}
	}
	@Override
	public Optional<User> getUserDetailsByPhone(InternationalNumber number) {
		try {
			final String getUserQuery = baseDao.getQueryById("getUserByPhonev3");
			final SqlParameterSource source = new MapSqlParameterSource()
					.addValue("cc", number.getCc())
					.addValue("areaCode",number.getCode())
					.addValue("phoneNo", number.getPhoneNumber());
			final User user = baseDao.getJdbcTemplate().queryForObject(getUserQuery, source, USER_ACCOUNT_ROW_MAPPER);
			return Optional.fromNullable(user);
		} catch (EmptyResultDataAccessException e) {
			return Optional.<User> absent();
		}
	}
	

	@Override
	public Optional<User> getUserDetailsByOnlyNumber(InternationalNumber no,String password) {
		try {
			final String getUserQuery = baseDao.getQueryById("getUserByOnlyPhoneAndPasswordv3");
			final SqlParameterSource source = new MapSqlParameterSource()
					.addValue("cc", no.getCc())
					.addValue("areaCode",no.getCode())
					.addValue("phoneNo", no.getPhoneNumber())
					.addValue("password", password);
			final User user = baseDao.getJdbcTemplate().queryForObject(getUserQuery, source, USER_ACCOUNT_ROW_MAPPER);
			return Optional.fromNullable(user);
		} catch (EmptyResultDataAccessException e) {
			return Optional.<User> absent();
		}
	}

	
	
	@Override
	public void updateValues(final Long userId, final List<Value> values) {
		try {
			final String deleteValuesQuery = baseDao.getQueryById("deleteValues");
			final String updateValuesQuery = baseDao.getQueryById("updateValues");
			final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId);
			baseDao.getJdbcTemplate().update(deleteValuesQuery, source);
			final SqlParameterSource[] batch = new MapSqlParameterSource[values.size()];
			for (int index = 0; index < values.size(); index++) {
				final Value value = values.get(index);
				batch[index] = new MapSqlParameterSource().addValue("userId", userId).addValue("value_id", value.getValueId());
			}
			baseDao.getJdbcTemplate().batchUpdate(updateValuesQuery, batch);
		} catch (DataIntegrityViolationException e) {
			LOGGER.info("========VALUES CONTAINS INVALID ID'S============");
			//throw new ForbiddenException.Builder().build();
			
		}
	}

	@Override
	public void updateInterests(final Long userId, final List<Interest> interests) {
		try {
			final String updateInterestsQuery = baseDao.getQueryById("updateInterests");
			final int size= interests.size();
			final SqlParameterSource[] batch = new MapSqlParameterSource[size];
			for (int index = 0; index < size; index++) {
				final Interest interest = interests.get(index);
				batch[index] = new MapSqlParameterSource().addValue("userId", userId).addValue("interest_id", interest.getInterestId());
			}
			baseDao.getJdbcTemplate().batchUpdate(updateInterestsQuery, batch);
		} catch (DataIntegrityViolationException e) {
			LOGGER.info("==========DUPLICATE INTEREST ID ENTRY IN INTEREST LIST===========");
		}
	}
	
	@Override
	public void deleteInterests(final Long userId) {
		final String deleteInterestsQuery = baseDao.getQueryById("deleteInterest");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId);
		baseDao.getJdbcTemplate().update(deleteInterestsQuery, source);
	}

	@Override
	public List<Integer> getInterests(final Long userId, final Integer pageNo) {
		final String listVersePBFriendsQuery = baseDao.getQueryById("interestsList");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId)
				.addValue("offset", pageNo * NO_OF_ROWS_PER_PAGE)
				.addValue("limit", NO_OF_ROWS_PER_PAGE);
		return baseDao.getJdbcTemplate().query(listVersePBFriendsQuery, source,INTEREST_ID_ROW_MAPPER);
	}

	@Override
	public List<Value> getValues(final Long userId, final Integer pageNo) {
		final String listVersePBFriendsQuery = baseDao.getQueryById("valuesList");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId)
				.addValue("offset", pageNo * NO_OF_ROWS_PER_PAGE)
				.addValue("limit", NO_OF_ROWS_PER_PAGE);
		return baseDao.getJdbcTemplate().query(listVersePBFriendsQuery, source, VALUE_ROW_MAPPER);
	}

	@Override
	public Boolean checkUserExistanceSocialId(final String socialId, final String socialSource) {
		final String query = baseDao.getQueryById("checkUserExistanceBySocialId" + socialSource);
		final SqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("socialId", socialId);
		return baseDao.getRowCount(query, sqlParameterSource) == 0 ? false : true;
	}
	

	@Override
	public boolean checkUserExistancePhoneNo(SocialUser socialUser) {
		final String query = baseDao.getQueryById("checkSocialUserExistanceByPhonev3");
		final SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
				.addValue("cc", socialUser.getNumber().getCc())
				.addValue("areaCode",socialUser.getNumber().getCode())
				.addValue("phoneNo", socialUser.getNumber().getPhoneNumber());
		return baseDao.getRowCount(query, sqlParameterSource) == 0 ? false : true;
	}


	@Override
	public void addSocialInfo(final Long userId, final String socialId, final String socialSource) {
		final String query = baseDao.getQueryById("addSocialInfo" + socialSource);
		final SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
				.addValue("userId", userId)
				.addValue("socialId", socialId);
		baseDao.getJdbcTemplate().update(query, sqlParameterSource);
	}

	@Override
	public Optional<User> getSocialUserDetails(final Long userId) {
		final String getUserQuery = baseDao.getQueryById("getUserByUserIdV3");
		final SqlParameterSource source = new MapSqlParameterSource()
				.addValue("userId", userId);
		final User user = baseDao.getJdbcTemplate().queryForObject(getUserQuery, source, USER_ACCOUNT_ROW_MAPPER);
		return Optional.fromNullable(user);
	}

	@Override
	public Optional<Long> getUserIdBySocialId(final String socialId, final String socialSource) {
		try {
			final String getUserQuery = baseDao.getQueryById("getUserIdBySocialId" + socialSource);
			final SqlParameterSource source = new MapSqlParameterSource().addValue("socialId", socialId);
			final Long userId = baseDao.getJdbcTemplate().queryForObject(getUserQuery, source, Long.class);
			return Optional.fromNullable(userId);
		} catch (EmptyResultDataAccessException e) {
			return Optional.<Long> absent();
		}
	}

	@Override
	public Optional<User> getProfile(final Long userId) {
		try {
			final String getUserProfileQuery = baseDao.getQueryById("getProfileByUserId");
			final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId)
					.addValue("status", AccountStatus.ACTIVE.getStatusCode());
			final User user = baseDao.getJdbcTemplate().queryForObject(getUserProfileQuery, source, USER_PROFILE_ROW_MAPPER);
			return Optional.fromNullable(user);
		} catch (EmptyResultDataAccessException e) {
			return Optional.<User> absent();
		}
	}

	@Override
	public void followUser(final Long userId, final Long followId) throws DuplicateKeyException, ForbiddenException {
		addFollow(userId, followId);
		updateFollowerCount(followId);
		updateFollowingCount(userId);
	  }

	private void addFollow(final long userId, final long followId) throws DuplicateKeyException, ForbiddenException {
		try {
			final String updateReverseInfoQuery = baseDao.getQueryById("saveFollow");
			final SqlParameterSource source = new MapSqlParameterSource()
					.addValue("userId", userId)
					.addValue("followId", followId);
			baseDao.getJdbcTemplate().update(updateReverseInfoQuery, source);
			/**
			 * Update Follower and Following Count At ES
			 */
			esHelper.updateFollowerCount(followId);
			esHelper.updateFollowingCount(userId);
		} catch (DuplicateKeyException e) {
			throw e;
		} catch (DataIntegrityViolationException e) {
			throw new ForbiddenException.Builder().build();
		}
	}

	private void updateFollowerCount(final long userId) {
		final String updateLikeCountQuery = baseDao.getQueryById("updateFollowerCount");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId);
		baseDao.getJdbcTemplate().update(updateLikeCountQuery, source);

	}

	private void updateFollowingCount(final long userId) {
		final String updateLikeCountQuery = baseDao.getQueryById("updateFollowingCount");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId);
		baseDao.getJdbcTemplate().update(updateLikeCountQuery, source);

	}

	@Override
	public Long getFollowers(final Long userId) {
		try {
			final String getFollowersQuery = baseDao.getQueryById("getFollowers");
			final SqlParameterSource source = new MapSqlParameterSource()
					.addValue("userId", userId);
			final Long followers = baseDao.getJdbcTemplate().queryForObject(getFollowersQuery, source, Long.class);
			return followers;
		} catch (EmptyResultDataAccessException e) {
			return 0L;
		}
	}

	@Override
	public Double getRating(final Long userId) {
		try {
			final String getRatingQuery = baseDao.getQueryById("getRating");
			final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId);
			final Double rating = baseDao.getJdbcTemplate().queryForObject(getRatingQuery, source, Double.class);
			return rating;
		} catch (EmptyResultDataAccessException e) {
			return 0.0;
		}
	}

	@Override
	public Optional<Profile> getProfileImage(final Long userId) {
		try {
			final String getProfileImageQuery = baseDao.getQueryById("getProfileImage");
			final SqlParameterSource source = new MapSqlParameterSource()
					.addValue("userId", userId);
			final Profile profile = baseDao.getJdbcTemplate().queryForObject(getProfileImageQuery, source, USER_PROFILE_IMAGE_ROW_MAPPER);
			return Optional.fromNullable(profile);
		} catch (EmptyResultDataAccessException e) {
			return Optional.<Profile> absent();
		}
	}

	@Override
	public List<Integer> getInterestIds(final Long userId) {
		final String listVersePBFriendsQuery = baseDao.getQueryById("getInterestIds");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId);
		return baseDao.getJdbcTemplate().query(listVersePBFriendsQuery, source, INTEREST_ID_ROW_MAPPER);
	}

	@Override
	public Integer updateProfile(final Long userId, final Profile profile) {
		/**
		 * Update profile At Es 
		 */
		esHelper.updateProfile(userId, profile);
		
		final String updateProfileQuery = baseDao.getQueryById("updateProfilev3");
		final Profile oldProfile = getMyProfile(userId).get();
		 final MapSqlParameterSource source = new MapSqlParameterSource();
		  source.addValue("userId", userId);
		  updateIfNotNull("lresImage", source, createUpdatingStringSupplier(profile.getLresId(), oldProfile.getLresId()));
		  updateIfNotNull("hresImage", source, createUpdatingStringSupplier(profile.getHresId(), oldProfile.getHresId()));
		  updateIfNotNull("info", source, createUpdatingStringSupplier(profile.getBasicInfo(), oldProfile.getBasicInfo()));
		  updateIfNotNull("name", source, createUpdatingStringSupplier(profile.getName(), oldProfile.getName()));
		  updateIfNotNull("videoBioLres", source, createUpdatingStringSupplier(profile.getVideoBioLres(), oldProfile.getVideoBioLres()));
		  updateIfNotNull("videoBioHres", source, createUpdatingStringSupplier(profile.getVideoBioHres(), oldProfile.getVideoBioHres()));
		  updateIfNotNull("facebookUrl", source, createUpdatingStringSupplier(profile.getFacebookUrl(), oldProfile.getFacebookUrl()));
		  updateIfNotNull("linkedInUrl", source, createUpdatingStringSupplier(profile.getLinkedInURl(), oldProfile.getLinkedInURl()));
		 
		return baseDao.getJdbcTemplate().update(updateProfileQuery, source);
	}

	public Optional<Profile> getMyProfile(final Long userId) {
		try {
			final String query = baseDao.getQueryById("profileQuery");
			final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId);
			final Profile profile = baseDao.getJdbcTemplate().queryForObject(query, source, PROFILE_ROW_MAPPER);
			return Optional.fromNullable(profile);
		} catch (EmptyResultDataAccessException e) {
			return Optional.<Profile> absent();
		}
	}
	
	@Override
	public void rate(final Long userId, final Long ratedUserId, final Rating rating) throws DuplicateKeyException, ForbiddenException {
		try {
			final Optional<Rating> oldRating = getRatingWithReview(userId, ratedUserId);
			final String updateRatingQuery = baseDao.getQueryById("saveUserRating");
			final MapSqlParameterSource source = new MapSqlParameterSource();
			if(oldRating.isPresent()) {
				updateIfNotNull("review", source, createUpdatingStringSupplier(rating.getReview(), oldRating.get().getReview()));
				
				 /**
				  * Delete old User Rating At Es and Then Add
				  */
				   esHelper.deleteOldUserRating(userId,ratedUserId,oldRating.get().getRating().intValue());
				   esHelper.updateUserRating(userId,ratedUserId,rating.getRating().intValue());
				 
			}else {
				source.addValue("review", rating.getReview());
				/**
				 * Update rating At ES
				 */
				
				esHelper.updateUserRating(userId,ratedUserId,rating.getRating().intValue());
			}
			source.addValue("userId", userId);
			source.addValue("ratedUserId", ratedUserId);
			source.addValue("rating", rating.getRating().intValue());
			source.addValue("time", Instant.now().toEpochMilli());
			baseDao.getJdbcTemplate().update(updateRatingQuery, source);
			
		} catch (DuplicateKeyException e) {
			throw e;
		} catch (DataIntegrityViolationException e) {
			throw new ForbiddenException.Builder().build();
		}
	}

	private Optional<Rating> getRatingWithReview(final Long userId, final Long ratedUserId) {
		try {
			final String query = baseDao.getQueryById("getRatingWithReview");
			final SqlParameterSource source = new MapSqlParameterSource()
			      .addValue("userId", userId)
			      .addValue("ratedUserId", ratedUserId);
			final Rating rating = baseDao.getJdbcTemplate().queryForObject(query, source, RATING_ROW_MAPPER);
			return Optional.fromNullable(rating);
		} catch (EmptyResultDataAccessException e) {
			return Optional.<Rating> absent();
		}
	}

	@Override
	public void updateSocialInfo(final Long userId, final Profile profile) {
		updateProfile(userId, profile);
	
	}

	@Override
	public List<Interest> getMyInterests(final Long userId) {
		final String listVersePBFriendsQuery = baseDao.getQueryById("getMyInterest");
		final SqlParameterSource source = new MapSqlParameterSource()
		.addValue("userId", userId);
		return baseDao.getJdbcTemplate().query(listVersePBFriendsQuery, source, MY_INTEREST_ROW_MAPPER);
	}

	@Override
	public List<Value> getValuesList(final Integer pageNo) {
		final String listVersePBFriendsQuery = baseDao.getQueryById("getDefaultValues");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("offset", pageNo * NO_OF_INTEREST_OR_VALUES_PER_PAGE)
				.addValue("limit", NO_OF_INTEREST_OR_VALUES_PER_PAGE);
		return baseDao.getJdbcTemplate().query(listVersePBFriendsQuery, source, VALUE_ROW_MAPPER);
	}

	@Override
	public Integer isFollowing(final Long userId, final Long followingUserId) {
		final String getCheckFollowingQuery = baseDao.getQueryById("getCheckFollowingQuery");
		final SqlParameterSource source = new MapSqlParameterSource()
				.addValue("userId", userId)
				.addValue("followingUserId",
				followingUserId);
		return baseDao.getJdbcTemplate().queryForObject(getCheckFollowingQuery, source, Integer.class);
	}

	@Override
	public Optional<User> isExists(final InternationalNumber number) {
		 String getUserQuery=null;
		 SqlParameterSource source =null;
		try {
			
			 if((number.getCc()!=null && number.getPhoneNumber()!=null) && (number.getCc()!="" && number.getPhoneNumber()!="")){
			 getUserQuery = baseDao.getQueryById("checkUserExistanceWithPhoneV3");	
			source = new MapSqlParameterSource()
					.addValue("cc",number.getCc())
					.addValue("areaCode", number.getCode())
					.addValue("phoneNo",number.getPhoneNumber());
			}
		
			
			final User user = baseDao.getJdbcTemplate().queryForObject(getUserQuery, source, USER_EXISTANCE_ROW_MAPPER);
			return Optional.fromNullable(user);
		} catch (EmptyResultDataAccessException e) {
			return Optional.<User> absent();
		}
	}

	@Override
	public void updateUser(final Long userId, User user, final Boolean isHavePassword) throws ConflitException {
		user = User.createWithUserId(userId, user);
		final DeviceInfo deviceInfo = user.getDeviceInfo();
		String queryString;
		if (isHavePassword) {
			queryString = "updateUserAccountInfowithoutpassword";
		} else {
			queryString = "updateUserAccountInfo";
		}
	
		final String updateUserQuery = baseDao.getQueryById(queryString);
		try {
			baseDao.getJdbcTemplate().update(updateUserQuery, getAccountInfoParamMap(user));
		} catch (DataIntegrityViolationException e) {
			throw new ConflitException.Builder().entity(new ErrorEntity(3, "email id already Registered")).
			build();
		}

		updateProfile(userId, user.getProfile());

		final String updateDeviceInfoQuery = baseDao.getQueryById("updateDeviceInfo");
		baseDao.insert(updateDeviceInfoQuery, getDeviceInfoParamMaps(userId, deviceInfo));

	}

	@Override
	public void updateSocialDetails(final Long userId, final String socialId, final String socialSource) {
		final String query = baseDao.getQueryById("updateSocialInfo" + socialSource);
		final SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
				.addValue("userId", userId).addValue("socialId", socialId);
		baseDao.getJdbcTemplate().update(query, sqlParameterSource);
		
		
	}

	@Override
	public Boolean isAlreadySocial(final Long userId) {
		final String query = baseDao.getQueryById("checksocialuserid");
		final SqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("userId", userId);
		return baseDao.getRowCount(query, sqlParameterSource) == 0 ? false : true;
	}

	@Override
	public Integer updateProfileImage(final Long userId, final ProfileImage profileImage) {
		/**
		 * update user profile image at Elasticsearch
		 */
	   esHelper.updateUserProfileImageAtEs(userId,profileImage);
		
		final String updateProfileQuery = baseDao.getQueryById("updateProfileImage");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId)
				.addValue("lresImage", profileImage.getLresId()).addValue("hresImage", profileImage.getHresId());
		return baseDao.getJdbcTemplate().update(updateProfileQuery, source);
	}

	@Override
	public Integer sendRequest(final Long userId, final Long mentorId,SendRequest request) throws ForbiddenException, ConflitException {
	
			final KeyHolder keyHolder = new GeneratedKeyHolder();
			final String sendRequestQuery = baseDao.getQueryById("sendRequestQueryV3");
			try {
				baseDao.getJdbcTemplate().update(sendRequestQuery, getAccountInfoParamMapSendRequest(userId,mentorId,request), keyHolder, 
						MAPPING_ID_COLUMN_NAME);
				return keyHolder.getKey().intValue();
			} catch (DataIntegrityViolationException e) {
				throw new ForbiddenException.Builder().build();
			}
			
	}
	
	

	private static final SqlParameterSource getAccountInfoParamMapSendRequest(Long userId,Long mentorId,SendRequest request) {

		final SqlParameterSource source = new MapSqlParameterSource()
				.addValue("userId", userId)
				.addValue("mentorId", mentorId)
				.addValue("comment", request.getText())
				.addValue("requestTime", Instant.now().toEpochMilli());
		return source;
	}
	

	@Override
	public Optional<List<User>> getMyMentor(final Long userId, final Integer pageNo) {
		try {
			final String getMyMentorListQuery = baseDao.getQueryById("getMyMentorList");
			final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId)
					.addValue("status", AccountStatus.ACTIVE.getStatusCode())
					.addValue("offset", pageNo * NO_OF_ROWS_PER_PAGE)
					.addValue("limit", NO_OF_ROWS_PER_PAGE);
			final List<User> user = baseDao.getJdbcTemplate().query(getMyMentorListQuery, source, FRIEND_PROFILE_ROW_MAPPER);
			return Optional.fromNullable(user);
		} catch (EmptyResultDataAccessException e) {
			return Optional.<List<User>> absent();
		}
	}

	@Override
	public Optional<List<User>> getMyMentee(final Long userId, final Integer pageNo) {
		try {
			final String getMyMenteeListQuery = baseDao.getQueryById("getMyMenteeList");
			final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId)
					.addValue("status", AccountStatus.ACTIVE.getStatusCode())
					.addValue("offset", pageNo * NO_OF_ROWS_PER_PAGE)
					.addValue("limit", NO_OF_ROWS_PER_PAGE);
			final List<User> user = baseDao.getJdbcTemplate().query(getMyMenteeListQuery, source, FRIEND_PROFILE_ROW_MAPPER);
			return Optional.fromNullable(user);
		} catch (EmptyResultDataAccessException e) {
			return Optional.<List<User>> absent();
		}
	}
	
	@Override
	public Optional<List<User>> listFollowers(final Long userId, final Integer pageNo) {
		try {
			final String getMyMenteeListQuery = baseDao.getQueryById("getFollowersList");
			final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId)
					.addValue("status", AccountStatus.ACTIVE.getStatusCode())
					.addValue("offset", pageNo * NO_OF_ROWS_PER_PAGE)
					.addValue("limit", NO_OF_ROWS_PER_PAGE);
			final List<User> user = baseDao.getJdbcTemplate().query(getMyMenteeListQuery, source, FRIEND_PROFILE_ROW_MAPPER);
			return Optional.fromNullable(user);
		} catch (EmptyResultDataAccessException e) {
			return Optional.<List<User>> absent();
		}
	}

	@Override
	public Optional<List<User>> listFollowing(final Long userId,final Integer pageNo) {
		try {
			final String getMyMenteeListQuery = baseDao.getQueryById("getFollowingList");
			final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId)
					.addValue("status", AccountStatus.ACTIVE.getStatusCode())
					.addValue("offset", pageNo * NO_OF_ROWS_PER_PAGE)
					.addValue("limit", NO_OF_ROWS_PER_PAGE);
			final List<User> user = baseDao.getJdbcTemplate().query(getMyMenteeListQuery, source, FRIEND_PROFILE_ROW_MAPPER);
			return Optional.fromNullable(user);
		} catch (EmptyResultDataAccessException e) {
			return Optional.<List<User>> absent();
		}
	}

	@Override
	public void deleteAndAddUserSocialInterests(final Long userId, final List<SocialInterest> interestList, final SocialSource socialSource) {
		deleteUserInterests(userId, socialSource);
		addUserInterests(userId, interestList, socialSource);
	}

	private void deleteUserInterests(final Long userId, final SocialSource socialSource) {
		final String deleteQuery = baseDao.getQueryById("deleteInterests");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId)
				.addValue("socialSource", SocialSource.getIntValue(socialSource));
		baseDao.getJdbcTemplate().update(deleteQuery, source);
	}

	private void addUserInterests(final Long userId, final List<SocialInterest> interestList, final SocialSource socialSource) {
		final String saveQuery = baseDao.getQueryById("addUserInterests");
		final SqlParameterSource[] sources = new MapSqlParameterSource[interestList.size()];
		for (int i = 0; i < interestList.size(); i++) {
			sources[i] = new MapSqlParameterSource()					
					.addValue("userId", userId)
					.addValue("interestId", interestList.get(i).getId())
					.addValue("interestName", interestList.get(i).getName())
					.addValue("socialSource", SocialSource.getIntValue(socialSource));
		}
		baseDao.getJdbcTemplate().batchUpdate(saveQuery, sources);
	}

	@Override
	public Boolean credit(final Payment payment) {
		final String query = baseDao.getQueryById("creditBalance");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", payment.getUserId()).addValue("amount",
				payment.getBalance());
		return baseDao.getJdbcTemplate().update(query, source) > 0 ? true : false;
	}

	@Override
	public Boolean debit(final Payment payment, final Boolean isCredit) {
		final String query = isCredit ? baseDao.getQueryById("debitBalanceFromCredits") : baseDao.getQueryById("debitBalanceFromAmount");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", payment.getUserId()).addValue("amount",
				payment.getBalance());
		return baseDao.getJdbcTemplate().update(query, source) > 0 ? true : false;
	}

	@Override
	public void creditIntoMentorAccount(final Long mentorId, final Double amount) {
		final String query = baseDao.getQueryById("creditBalanceToMentorAccount");
		final SqlParameterSource source = new MapSqlParameterSource()
				.addValue("mentorId", mentorId)
				.addValue("amount", amount);
		baseDao.getJdbcTemplate().update(query, source);
	}

	@Override
	public Optional<Balance> getBalance(final Long userId, final Long mentorId) {
		try {
			final String getProfileImageQuery = baseDao.getQueryById("getBalance");
			final SqlParameterSource source = new MapSqlParameterSource()
					.addValue("userId", userId).addValue("mentorId", mentorId);
			final Balance balance = baseDao.getJdbcTemplate().queryForObject(getProfileImageQuery, source, BALANCE_ROW_MAPPER);
			return Optional.fromNullable(balance);
		} catch (EmptyResultDataAccessException e) {
			return Optional.<Balance> absent();
		}
	}

	@Override
	public Boolean debit(final Balance payment, final Long userId, final Double requestAmount) {
		final String query = baseDao.getQueryById("debitBalanceAndCredits");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId)
				.addValue("amount", payment.getAvilableBalance())
				.addValue("credits", payment.getCredit() - (requestAmount - payment.getAvilableBalance()));
		return baseDao.getJdbcTemplate().update(query, source) > 0 ? true : false;
	}

	@Override
	public Boolean redeem(final Redeem redeem) {
		final String query = baseDao.getQueryById("reedemBalance");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", redeem.getUserId())
				.addValue("amount", redeem.getBalance()).addValue("paypalId", redeem.getPaypalId());
		return baseDao.getJdbcTemplate().update(query, source) > 0 ? true : false;
	}

	@Override
	public void feedback(final Long userId, final Feedback feedback) {
		try {
			final String query = baseDao.getQueryById("feedback");
			final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId)
					.addValue("recommendedCount", feedback.getRecommendedCount())
					.addValue("description", feedback.getDescription());
			baseDao.getJdbcTemplate().update(query, source);
		} catch (DataIntegrityViolationException e) {
			LOGGER.info("==============UNABLE TO ADD FEEDBACK==========");
		}
	}

	@Override
	public Integer blockUser(final Long userId, final Long blockUserId) {
		try {
			 
			final String query = baseDao.getQueryById("blockUserQuery");
			final SqlParameterSource source = new MapSqlParameterSource()
					.addValue("userId", userId).addValue("blockUserId",blockUserId);
			return baseDao.getJdbcTemplate().update(query, source);
		} catch (DataIntegrityViolationException e) {
			LOGGER.info("========= USER CONTAINS INVALID USER ID'S OR ALREADY EXISTS {} ===========", e);
			return 0;
		}
	}
	
	

	@Override
	public Integer unFollow(final Long userId, final Long unfollowUserId) {
		final String updateReverseInfoQuery = baseDao
				.getQueryById("deleteFollow");
		final SqlParameterSource source = new MapSqlParameterSource().addValue(
				"userId", userId).addValue("unFollowId", unfollowUserId);
		return baseDao.getJdbcTemplate().update(updateReverseInfoQuery, source);
	}

	@Override
	public Optional<List<User>> getBlockedUsers(final Long userId, final Integer pageNo) {
		final String query = baseDao.getQueryById("getBlockedUsers");

		final SqlParameterSource source = new MapSqlParameterSource()
				.addValue("userId", userId)
				.addValue("offset", pageNo * NO_OF_ROWS_PER_PAGE)
				.addValue("limit", NO_OF_ROWS_PER_PAGE);
		final List<User> users = baseDao.getJdbcTemplate().query(query, source,
				PROFILE_SUMMARY_ROW_MAPPER);
		return Optional.fromNullable(users);
	}

	@Override
	public Integer unBlockUser(final Long userId, final Long unBlockUserId) {
		try {
			
			/**
			 * Unblock User from Es
			 */
			esHelper.unblockUserAtEs(userId,unBlockUserId);
			final String query = baseDao.getQueryById("unBlockUserQuery");
			final SqlParameterSource source = new MapSqlParameterSource()
					.addValue("userId", userId).addValue("unBlockUserId",
							unBlockUserId);
			return baseDao.getJdbcTemplate().update(query, source);
		} catch (DataIntegrityViolationException e) {
			LOGGER.info("========= USER CONTAINS INVALID USER ID'S OR ALREDY EXISTS {} ===========", e);
			return 0;
		}
	}

	@Override
	public void deceraseFollowingCount(final Long userId) {
			/**
		 * Decrease  Following count at ES
		 */
		esHelper.decreaseFollowingCount(userId);
		
		final String query = baseDao.getQueryById("deceraseFollowingCount");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId);
		baseDao.getJdbcTemplate().update(query, source);
	}

	@Override
	public void deceraseFollowerCount(final Long userId) {
		/**
		 * Decrease follower  count at ES
		 */
		esHelper.decreaseFollowerCount( userId);
		
		final String query = baseDao.getQueryById("deceraseFollowerCount");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId);
		baseDao.getJdbcTemplate().update(query, source);
	}

	@Override
	public List<Map<Long, Long>> getBlockedUsersIds() {
		final String query = baseDao.getQueryById("findBlockedUserIds");
		final List<Map<Long, Long>> users = baseDao.getJdbcTemplate().query(query, BLOCKED_USER_IDS_ROW_MAPPER);
		return users;
	}

	@Override
	public Optional<Collection<User>> getReviews(final Long userId,final Integer pageNo) {
		final String query = baseDao.getQueryById("getReviews");
		final SqlParameterSource source = new MapSqlParameterSource()
				.addValue("userId", userId)
				.addValue("offset", pageNo * NO_OF_ROWS_PER_PAGE)
				.addValue("limit", NO_OF_ROWS_PER_PAGE);
		final Collection<User> users = baseDao.getJdbcTemplate().query(query, source, USER_WITH_REVIEW_ROW_MAPPER);
		return Optional.fromNullable(users);
	}

	@Override
	public Boolean checkAccountStatus(final String emailId,final InternationalNumber number,final String password) {
		try{
			String query=null;
			 SqlParameterSource source=null;
			if(emailId!=null && emailId!=""){
				 query=	baseDao.getQueryById("checkAccountStatus");
				 source = new MapSqlParameterSource()
							.addValue("emailId", emailId)
							.addValue("password",password);
							
			}
			else if(( number.getPhoneNumber()!=null) || ( number.getPhoneNumber()!="")){
				query=baseDao.getQueryById("checkAccountStatusByPhonev3");
				 source = new MapSqlParameterSource()
						    .addValue("cc",number.getCc())
						    .addValue("areaCode",number.getCode())
						    .addValue("phoneNo", number.getPhoneNumber())
							.addValue("password",password);
			}
		   return baseDao.getJdbcTemplate().queryForObject(query, source,Boolean.class);
		} catch(EmptyResultDataAccessException e){
			return false;
		}
	}
	
	@Override
	public Boolean checkSocialAccountStatus(final String emailId,final String password,
			final boolean isSocial,final String socialSource) {
		try{
			 String query =baseDao.getQueryById("getAccountStatusBySocialId"+socialSource );
			 SqlParameterSource  source = new MapSqlParameterSource()
							.addValue("password",password)
							.addValue("socialId", emailId);
		          return baseDao.getJdbcTemplate().queryForObject(query, source,Boolean.class);
		} catch(EmptyResultDataAccessException e){
			return false;
		}
	}
	
	@Override
	public String getNameByUserId(final Long userId) {
		final String query = baseDao.getQueryById("getNameByUserId");
		final SqlParameterSource source = new MapSqlParameterSource()
				.addValue("userId", userId);
		return baseDao.getJdbcTemplate().queryForObject(query, source,String.class);
	}

	@Override
	public Boolean logout(final Long userId,final String userAgent) {
		final String query = baseDao.getQueryById("deleteDeviceToken");
		final SqlParameterSource source = new MapSqlParameterSource()
				.addValue("userId", userId)
				.addValue("userAgent", userAgent);
		int n =baseDao.getJdbcTemplate().update(query, source);
		return n > 0 ? true : false;
	}

	@Override
	public void saveAndUpdateVerificationToken(final ForgetPassword forgetPassword,
			final String verificationToken) {
		final String query = baseDao.getQueryById("saveAndUpdateVerficationDetail");
		final SqlParameterSource source = new MapSqlParameterSource()
				.addValue("emailId", forgetPassword.getEmailId())
				.addValue("verificationToken", verificationToken)
				.addValue("createdTime", Instant.now().getEpochSecond());
		baseDao.getJdbcTemplate().update(query, source);
		
	}

	@Override
	public String verifyToken(final String token) {
		try {
			final String query = baseDao.getQueryById("checkValidToken");
			final SqlParameterSource source = new MapSqlParameterSource()
					.addValue("token", token);
			return baseDao.getJdbcTemplate().queryForObject(query, source,String.class);
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}

	@Override
	public void deleteToken(final String emailId) {
		final String query = baseDao.getQueryById("deleteTokensOfUser");
		final SqlParameterSource source = new MapSqlParameterSource()
				.addValue("emailId", emailId);
		baseDao.getJdbcTemplate().update(query, source);
		
	}

	@Override
	public Boolean resetPassword(final String newPassword,final String emailId) {
		final String query = baseDao.getQueryById("resetPassword");
		final SqlParameterSource source = new MapSqlParameterSource()
				.addValue("emailId", emailId)
				.addValue("newPassword",newPassword);
		return baseDao.getJdbcTemplate().update(query, source) >0 ? true : false;
	
	}



	@Override
	public void deleteFromPendingRequest(final Long userId,final Long mentorId) {
		/**
		 * 
		 * remove pending request
		 */
		esHelper.deleteAndCancelRequest(userId,mentorId);
		final String query = baseDao.getQueryById("deleteFromPendingRequest");
		final SqlParameterSource source = new MapSqlParameterSource()
				.addValue("menteeId", userId)
				.addValue("mentorId", mentorId);
		baseDao.getJdbcTemplate().update(query, source);

	}

	@Override
	public void deleteFromMenteeRequest(final Long userId,final  Long menteeId) {
		/**
		 * 
		 * remove mentee request
		 */
		esHelper.deleteAndCancelRequest(menteeId,userId);
		final String query = baseDao.getQueryById("deleteFromMenteeRequest");
		final SqlParameterSource source = new MapSqlParameterSource()
				.addValue("menteeId", menteeId)
				.addValue("mentorId", userId);
		baseDao.getJdbcTemplate().update(query, source);

	}

	@Override
	public void deleteFromMyMentor(final Long userId, final Long mentorId) {
		/**
		 * remove from my mentor
		 */
		esHelper.deleteAndCancelRequest(userId,mentorId);
		final String query = baseDao.getQueryById("deleteFromMyMentor");
		final SqlParameterSource source = new MapSqlParameterSource()
				.addValue("userId", userId)
				.addValue("mentorId",mentorId );
		baseDao.getJdbcTemplate().update(query, source);
		
	}

	@Override
	public void deleteFromMyMentee(final Long userId,final Long menteeId) {
		/**
		 * remove from my mentee
		 */
		esHelper.deleteAndCancelRequest(menteeId,userId);
		final String query = baseDao.getQueryById("deleteFromMyMentee");
		final SqlParameterSource source = new MapSqlParameterSource()
				.addValue("userId", menteeId)
				.addValue("menteeId", userId);
		baseDao.getJdbcTemplate().update(query, source);
		
	}

	@Override
	public boolean isBlockedUserOrBlockedBy(final Long userId,final Long blockedId) {
			final String query = baseDao.getQueryById("isBlockedUserOrBlockedBy");
			final SqlParameterSource source = new MapSqlParameterSource()
					.addValue("userId", userId).addValue("blockedId", blockedId);
			return baseDao.getJdbcTemplate().queryForObject(query, source,Boolean.class);
	}

	@Override
	public void deleteDevice(final Long userId) {
		final String query = baseDao.getQueryById("deleteDeviceInfoById");
		final SqlParameterSource source = new MapSqlParameterSource()
				.addValue("userId", userId);
		baseDao.getJdbcTemplate().update(query, source);
	}

	@Override
	public void deleteToken(final Long userId) {
		final String query = baseDao.getQueryById("deleteTokenInfoById");
		final SqlParameterSource source = new MapSqlParameterSource()
				.addValue("userId", userId);
		baseDao.getJdbcTemplate().update(query, source);
		
	}

	@Override
	public Boolean getAccountStatus(final Long userId) {
		final String query = baseDao.getQueryById("getAccountStatus");
		final SqlParameterSource source = new MapSqlParameterSource()
				.addValue("userId", userId);
		return baseDao.getJdbcTemplate().queryForObject(query, source,Boolean.class);
	}

	@Override
	public List<Interest> getInterestsById(final Integer interestId) {
		final String listVersePBFriendsQuery = baseDao.getQueryById("getInterestsById");
		final SqlParameterSource source = new MapSqlParameterSource()
				.addValue("interestId", interestId);
		return baseDao.getJdbcTemplate().query(listVersePBFriendsQuery, source, INTEREST_ROW_MAPPER);
	}

	@Override
	public List<Interest> getInterestsByParentId(final Integer parentId) {
		final String listVersePBFriendsQuery = baseDao.getQueryById("getInterestsByParentId");
		final SqlParameterSource source = new MapSqlParameterSource()
				.addValue("parentId", parentId);
		return baseDao.getJdbcTemplate().query(listVersePBFriendsQuery, source, INTEREST_ROW_MAPPER);
	}

	@Override
	public List<Interest> getRootInterests(final Integer pageNo) {
		final String listVersePBFriendsQuery = baseDao.getQueryById("getRootInterests");
		final SqlParameterSource source = new MapSqlParameterSource()
				.addValue("offset", pageNo * NO_OF_INTEREST_OR_VALUES_PER_PAGE)
				.addValue("limit", NO_OF_INTEREST_OR_VALUES_PER_PAGE);
		return baseDao.getJdbcTemplate().query(listVersePBFriendsQuery, source, INTEREST_ROW_MAPPER);
	}

	@Override
	public List<Interest> syncInterest(final Long time) {
		final String listVersePBFriendsQuery = baseDao.getQueryById("syncInterest");
		final SqlParameterSource source = new MapSqlParameterSource()
				.addValue("time", time);
		return baseDao.getJdbcTemplate().query(listVersePBFriendsQuery, source, INTEREST_ROW_MAPPER);
	}


	@Override
	public Collection<User> findFollowing(final Long userId) {
		final String query1 = baseDao.getQueryById("findFollowing");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId);
		return baseDao.getJdbcTemplate().query(query1, source,FRIEND_PROFILE_ROW_MAPPER);
		
	}

	@Override
	public Collection<Source> getUserDocument(final Integer offset,final Integer limit) {
		final String query1 = baseDao.getQueryById("get.user.documents");
		final SqlParameterSource source = new MapSqlParameterSource()
				.addValue("offset",offset).addValue("limit",limit);
	return baseDao.getJdbcTemplate().query(query1,source, SOURCE_RESULT_EXTRACTOR);
	}


     
	@Override
	public Collection<Source> getUserRating() {
		final String query1 = baseDao.getQueryById("get.user.rating");
	   return baseDao.getJdbcTemplate().query(query1, RATING_RESULT_SET_EXTRACTOR);
	}



	@Override
	public Collection<Source> getBlockedUsers() {
		final String query1 = baseDao.getQueryById("get.blocked.users");
		return baseDao.getJdbcTemplate().query(query1, BLOCK_USER_RESULT_SET_EXTRACTOR);
	}



	@Override
	public Collection<Source> getUserAverageRating() {
		final String query1 = baseDao.getQueryById("get.user.average.rating");
		   return baseDao.getJdbcTemplate().query(query1, AVERAGE_RATING_ROW_MAPPER);
	}

	@Override
	public Boolean  skipMentor(final Long userId,final Long mentorId) {
		try {
			final String skipMentorQuery = baseDao.getQueryById("skip.mentor");
			final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId).addValue("mentorId", mentorId);
			baseDao.getJdbcTemplate().update(skipMentorQuery, source);
			return true;
		} catch (DuplicateKeyException e) {
			LOGGER.error("==========DUPLICATE INSERTION==========");
			return false;
		} 
		catch (Exception e) {
		LOGGER.error("===========ERROR WHILE SKIP MENTOR=========="+ e);
		return false;
		}}



	@Override
	public Collection<Source> saveUserRequestSentMentors() {
		final String query1 = baseDao.getQueryById("get.request.sent.mentors");
		return baseDao.getJdbcTemplate().query(query1, MENTEE_REQUEST_SENT_RESULT_SET_EXTRACTOR);
	}



	@Override
	public List<Expertise> getParentExpertises(final Set<Integer> childExpertises) {
		    final String Query = baseDao.getQueryById("getParentExpertises");
			final SqlParameterSource source = new MapSqlParameterSource()
					.addValue("childExpertises",childExpertises);
		return baseDao.getJdbcTemplate().query(Query, source, INTERESTS_ROW_MAPPER  );
	}



	@Override
	public Collection<Source> getSkippedMentors() {
		final String query1 = baseDao.getQueryById("get.skipped.mentors");
		return baseDao.getJdbcTemplate().query(query1, SKIP_MENTORS_RESULT_SET_EXTRACTOR);
	}



	@Override
	public Collection<Source> getMentorExpertises(final Integer offset,final Integer limit) {
		final String query1 = baseDao.getQueryById("get.mentor.expertises");
		final SqlParameterSource source = new MapSqlParameterSource();
		return baseDao.getJdbcTemplate().query(query1,source, MENTOR_EXPERTISES_RESULT_SET_EXTRACTOR);
	}



	@Override
	public List<User> getListPhoneContacts(final User usr) {    //get Contacts to send Notifications(push)
		final String query1 = baseDao.getQueryById("get.phone.contactsv3");
		final SqlParameterSource source = new MapSqlParameterSource()
				.addValue("cc", usr.getNumber().getCc())
				.addValue("areaCode",usr.getNumber().getCode())
				.addValue("phone",usr.getNumber().getPhoneNumber());
		return baseDao.getJdbcTemplate().query(query1,source, PHONE_CONTACTS_ROW_MAPPER);
	}
	
	@Override
	public void addContacts(final List<PhoneContact> contacts, final Long userId) throws   BadRequestException{
		final String addContactsQuery = baseDao.getQueryById("add.phone.contactsv3");
		final SqlParameterSource[] batch = new MapSqlParameterSource[contacts.size()];
			try{
				for(int index=0;index<contacts.size();index++){
					final PhoneContact contact = contacts.get(index);
					  batch[index] = new MapSqlParameterSource()
							  .addValue("userId", userId)
							  .addValue("name", contact.getName())
							  .addValue("cc", contact.getInternationalNumber().getCc())
							  .addValue("areaCode", contact.getInternationalNumber().getCode())
							  .addValue("phone", contact.getInternationalNumber().getPhoneNumber());
					}
				   baseDao.getJdbcTemplate().batchUpdate(addContactsQuery, batch);
				}
			catch(DataIntegrityViolationException e){
				LOGGER.info("========Data integerity exception============");
			}
			catch(InvalidDataAccessApiUsageException e){
				throw new BadRequestException.Builder().entity(new ErrorEntity(2, "error message: "+e.getMessage())).build();	
			}
			catch(Exception e){
			LOGGER.info("ERROR WHILE ===========  ADD CONTACTS" + e );	
			}
	}
	
	@Override
	public void verifyUserWithPhoneNumer(InternationalNumber number){
		final String verifyUserQuery = baseDao.getQueryById("verify.user.with.phone.nov3");
		final SqlParameterSource source = new MapSqlParameterSource()
				 .addValue("cc", number.getCc())
				 .addValue("areaCode", number.getCode())
				 .addValue("phoneNo", number.getPhoneNumber())
				.addValue("verifiedPhone", VERIFIED_PHONE);
		baseDao.getJdbcTemplate().update(verifyUserQuery, source);	
	}

    @Override
	public void deleteOldContacts(final Long userId) {
		final String deleteOldContactsQuery = baseDao.getQueryById("delete.old.contacts");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId);
		baseDao.getJdbcTemplate().update(deleteOldContactsQuery, source);	
		
	}

	@Override
	public boolean isValidUser(InternationalNumber oldNo, Long userId) {
		
		final String query = baseDao.getQueryById("is.valid.userv3");
		final SqlParameterSource source = new MapSqlParameterSource()
				.addValue("userId", userId)
				.addValue("cc",oldNo.getCc())
				 .addValue("areaCode", oldNo.getCode())
				.addValue("phoneNo",oldNo.getPhoneNumber());
		return baseDao.getRowCount(query, source) == 0 ? false : true;
		
		
	}
	
	@Override
	public void updateContact(InternationalNumber no, Long userId){
		final String deleteOldContactsQuery = baseDao.getQueryById("update.contact.number");
		final SqlParameterSource source = new MapSqlParameterSource()
				.addValue("userId", userId)
				.addValue("cc",no.getCc())
				.addValue("areaCode",no.getCode())
				.addValue("phoneNo",no.getPhoneNumber());
		baseDao.getJdbcTemplate().update(deleteOldContactsQuery, source);	
	}
    
	@Override
	public List<PhoneContact> getContactsToInvite(Long userId) {
		final String query1 = baseDao.getQueryById("get.phone.contacts.by.user.id");
		final SqlParameterSource source = new MapSqlParameterSource()
				.addValue("userId",userId);
		return baseDao.getJdbcTemplate().query(query1,source, USER_CONTACTS_ROW_MAPPER);
	}

	
	public boolean isNoExists(InternationalNumber number){
		final String query = baseDao.getQueryById("is.valid.user.by.nov3");
		final SqlParameterSource source = new MapSqlParameterSource()
				.addValue("cc",number.getCc())
				.addValue("areaCode",number.getCode())
				.addValue("phoneNo",number.getPhoneNumber());
		return baseDao.getRowCount(query, source) == 0 ? false : true;
	}



	@Override
	public boolean updatePassword(UpdatePasswordV2 updatepassword) {
		final String query = baseDao.getQueryById("forget.update.password.v3");
		final SqlParameterSource source = new MapSqlParameterSource()
				.addValue("cc",updatepassword.getNumber().getCc())
				.addValue("areaCode",updatepassword.getNumber().getCode())
				.addValue("phoneNo",updatepassword.getNumber().getPhoneNumber())
				.addValue("password", updatepassword.getPassword());
		return baseDao.getJdbcTemplate().update(query, source) > 0 ? true : false;
	}



	@Override
	public boolean checkUserExistanceSocialId(SocialUser socialUser) throws NotFoundException {
		
	
		 SqlParameterSource source =null;
		 String query = null;
		if(socialUser.getSocialSource().equals("fb")){
			 query = baseDao.getQueryById("is.social.user.by.fb.id");
		 source = new MapSqlParameterSource()
				.addValue("fbSocialId",socialUser.getSocialId());
		}
		else if(socialUser.getSocialSource().equals("linkedin")){
			query = baseDao.getQueryById("is.social.user.by.linkedin.id");
			 source = new MapSqlParameterSource()
					.addValue("linkedinSocialId",socialUser.getSocialId() );
			
		}
		else{
			throw new NotFoundException.Builder().entity(new ErrorEntity(2, " social souce not exists")).build();
			
		}
		return baseDao.getRowCount(query, source) == 0 ? false : true;
	}



	@Override
	public boolean isSocialUserWithUserId(Long userId) {
		
		final String query = baseDao.getQueryById("is.social.user.by.user.id");
		final SqlParameterSource source = new MapSqlParameterSource()
				.addValue("userId",userId);
		return baseDao.getRowCount(query, source) == 0 ? false : true;
	}



	@Override
	public Optional<User> getUser(Long userId) {
		try {
			final String getUserProfileQuery = baseDao.getQueryById("getUserDataV3");
			final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId);
			final User user = baseDao.getJdbcTemplate().queryForObject(getUserProfileQuery, source, USER_DATA_ROW_MAPPER);
			return Optional.fromNullable(user);
		} catch (EmptyResultDataAccessException e) {
			return Optional.<User> absent();
		}
	}



	@Override
	public void addUserInterestInMentorExpertise(Integer mappingId, SendRequest request) {
		try {
			final String updateInterestsQuery = baseDao.getQueryById("add.user.interests.of.mentor.expertise.v3");
			final int size= request.getInterests().size();
			final SqlParameterSource[] batch = new MapSqlParameterSource[size];
			for (int index = 0; index < size; index++) {
				final Interest interest =  request.getInterests().get(index);
				batch[index] = new MapSqlParameterSource()
						.addValue("mappingId", mappingId)
						.addValue("interestId", interest.getInterestId());
			}
			baseDao.getJdbcTemplate().batchUpdate(updateInterestsQuery, batch);
		} catch (DataIntegrityViolationException e) {
			LOGGER.info("==========DUPLICATE INTEREST ID ENTRY IN INTEREST LIST===========");
		}
	}



	@Override
	public boolean isAlreadySentRequest(Long userId, Long mentorId) {
		final String getUserProfileQuery = baseDao.getQueryById("isAlreadySentRequest");
		final SqlParameterSource source = new MapSqlParameterSource()
				.addValue("userId", userId)
				.addValue("mentorId", mentorId)
				.addValue("userId", userId)
				.addValue("mentorId",mentorId);
		return baseDao.getJdbcTemplate().queryForObject(getUserProfileQuery, source, Integer.class)>0?true:false;
	}



	@Override
	public boolean isAlreadyMentor(Long userId, Long mentorId) {
		final String getUserProfileQuery = baseDao.getQueryById("isAlreadyMentor");
		final SqlParameterSource source = new MapSqlParameterSource()
				.addValue("userId", userId)
				.addValue("mentorId", mentorId)
				.addValue("userId", userId)
				.addValue("mentorId",mentorId);
		return baseDao.getJdbcTemplate().queryForObject(getUserProfileQuery, source, Integer.class)>0?true:false;
	}






	



		
	
	}



/*	@Override
	public boolean checkUserExistanceSocialId(SocialUser socialUser) {
		final String query = baseDao.getQueryById("is.valid.user.by.no");
		final SqlParameterSource source = new MapSqlParameterSource()
				.addValue("socialId",socialUser.);
		return baseDao.getRowCount(query, source) == 0 ? false : true;
		return false;
	}*/



	
	

