package com.crater.mentorz.dao.impl.rowmapper.adminpanel;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.crater.mentorz.models.Rating;
import com.crater.mentorz.models.user.Profile;
import com.crater.mentorz.models.user.User;
import com.crater.mentorz.utils.StringUtil;

public class MentorRowMapper  implements RowMapper<User>{
	
	@Override
	public User mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new User(rs.getLong("user_id"), null, null, null,null, new Profile(null, null, null, null, 0, 0, 0, 0, null, 0l, 0,StringUtil.textDecoderUTF8( rs.getString("name")), 0, 
				null, null, null, new Rating(rs.getDouble("rating"), null, null), 
				getOptionalCoulmnValue(rs,"block_count"),null,null), null, null, rs.getBoolean("account_status"),null,null,null,null);
	}
	
	private Integer getOptionalCoulmnValue(final ResultSet rs, final String columnName) {
		try{
			return rs.getInt(columnName);
		} catch (SQLException ex) {
			return null;
		}
	}

}
