package com.crater.mentorz.dao.impl.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.crater.mentorz.elasticsearch.model.req.Rating;
import com.crater.mentorz.elasticsearch.model.req.Source;

public class RatingResultSetExtractor implements ResultSetExtractor<Collection<Source>> {

	@Override
	public Collection<Source> extractData(ResultSet rs) throws SQLException, DataAccessException {
		final Map<Long, Source> userMap = new HashMap<Long, Source>();
		while (rs.next()) {
			final Long userId = rs.getLong("rated_to");
			if (!userMap.containsKey(userId)) {
				userMap.put(userId, new Source(userId, null, null, null, 0, 0, 0, 0, null, 0, 0, null, null, null, 0, 0,
						null, null, new ArrayList<Rating>(), 0.0f, null, null,null, null, null,null,null,
						null,null,null,null));
			}

			userMap.get(userId).getRating().add( new Rating(rs.getInt("rating"),rs.getLong("rated_by")));
		}

		return userMap.values();
	}
}
