/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.dao.impl.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.crater.mentorz.models.mentor.Expertise;


public class ExpertiseRowMapper implements RowMapper<Expertise> {

	@Override
	public Expertise mapRow(ResultSet rs, int rowNum) throws SQLException {

		return new Expertise(rs.getInt("interest_id"), rs.getString("name"),rs.getInt("parent_id"),rs.getBoolean("has_children"),false);
	}
}
