package com.crater.mentorz.dao.impl.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.crater.mentorz.elasticsearch.model.req.Source;

public class BlockUserResultSetExtractor  implements ResultSetExtractor<Collection<Source>>{
	
	@Override
	public Collection<Source> extractData(ResultSet rs) throws SQLException, DataAccessException {
		final Map<Long, Source> userMap = new HashMap<Long, Source>();
		while (rs.next()) {
			final Long userId = rs.getLong("user_id");
			if (!userMap.containsKey(userId)) {
				userMap.put(userId, new Source(userId, null, null, null, 0, 0, 0, 0, null, 0, 0, null, null, null, 0, 0,
						null, null, null, 0.0f, null, null, null,null, new HashSet<Long>(),null,null,null ,null,null,null));
			}

			userMap.get(userId).getBlockedIds().add( rs.getLong("blocked_id"));
		}

		return userMap.values();
	}

}
