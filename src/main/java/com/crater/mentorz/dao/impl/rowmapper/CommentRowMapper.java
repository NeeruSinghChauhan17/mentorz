/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.dao.impl.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.crater.mentorz.models.post.Comment;

/**
 * @author Harish Anuragi
 *
 */
public class CommentRowMapper implements RowMapper<Comment> {

	@Override
	public Comment mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new Comment(rs.getLong("comment_id"), rs.getString("comment"), rs.getLong("comment_time"), rs.getLong("user_id"),
				rs.getString("name"), rs.getString("profile_image_url_lres"), rs.getString("profile_image_url_hres"));
	}

}
