package com.crater.mentorz.dao.impl.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.crater.mentorz.models.Rating;
import com.crater.mentorz.models.user.Profile;
import com.crater.mentorz.models.user.User;


public class UserWithReviewRowMapper implements RowMapper<User>{

	@Override
	public User mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new User(rs.getLong("rated_by"), null, null, null,null, new Profile(null, null, rs.getString("profile_image_url_lres"), rs.getString("profile_image_url_hres"),
				0, 0, 0, 0, null, 0, 0, rs.getString("name"), 0, null, null, null, new Rating(rs.getDouble("rating")
						, rs.getString("review"), rs.getLong("time")), null,null,null), null, null, null,null,null,null,null);
	}
	

}

