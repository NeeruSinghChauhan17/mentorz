package com.crater.mentorz.dao.impl.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.crater.mentorz.models.user.Interest;

public class MyInterestRowMapper implements RowMapper<Interest> {

	@Override
	public Interest mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new Interest(rs.getInt("id"), rs.getString("name"),rs.getInt("parent_id"),rs.getBoolean("has_children"),true);
	}
}
