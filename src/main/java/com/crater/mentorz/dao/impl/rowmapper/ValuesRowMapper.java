/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.dao.impl.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.crater.mentorz.models.user.Value;

/**
 * @author Harish Anuragi
 *
 */
public class ValuesRowMapper implements RowMapper<Value> {

	@Override
	public Value mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new Value(rs.getInt("value_id"), rs.getString("value"));
	}
}
