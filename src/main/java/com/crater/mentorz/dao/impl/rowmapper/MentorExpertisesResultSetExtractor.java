package com.crater.mentorz.dao.impl.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.crater.mentorz.elasticsearch.model.req.Rating;
import com.crater.mentorz.elasticsearch.model.req.Source;
import com.crater.mentorz.models.mentor.Expertise;


public class MentorExpertisesResultSetExtractor implements ResultSetExtractor<Collection<Source>> {
	

	@Override
	public Collection<Source> extractData(ResultSet rs) throws SQLException, DataAccessException {
		
		final Map<Long, Source> userMap = new HashMap<Long, Source>();
	     while (rs.next()) {
			final Long userId = rs.getLong("user_id");
              if (!userMap.containsKey(userId)) {
            	userMap.put(userId, new Source(userId, null, 
				null,null,0,0,0,
				0,
				null,0,0,null,null,
				null,0,0,null,null,
				null,0.0f,null,null, new ArrayList<Expertise>(),
				new HashSet<Integer>(),
				null,null,null,new ArrayList<Expertise>(),
				new ArrayList<Integer>(),null,null));
			}
           Integer expertise=rs.getInt("interest_id");
         if(rs.getInt("added")==1)
          {  
           userMap.get(userId).getExpertise().add(new Expertise(expertise,rs.getString("expertise_name"),
        		   rs.getInt("parent_id"),rs.getInt("has_children")>0? true:false ,false));
		  }
         userMap.get(userId).getExpertiseIds().add(expertise);

		}
		
		
		return userMap.values();
		
	}
	

}
