package com.crater.mentorz.dao.impl.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.crater.mentorz.models.user.SubscribeUser;
import com.crater.mentorz.models.user.SubscribeUser.Builder;

public class SubscribeUserRowMapper implements RowMapper<SubscribeUser> {

	@Override
	public SubscribeUser mapRow(final ResultSet rs, final int rowNum) throws SQLException {
		final Builder subscribeUserBuilder = new SubscribeUser.Builder();
		return subscribeUserBuilder.withEmail(rs.getString("email")).build();
	}

}
