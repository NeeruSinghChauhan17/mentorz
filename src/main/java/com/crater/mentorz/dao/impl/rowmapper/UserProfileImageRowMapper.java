/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.dao.impl.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.crater.mentorz.models.user.Profile;

/**
 * @author Harish Anuragi
 *
 */
public class UserProfileImageRowMapper implements RowMapper<Profile> {
	@Override
	public Profile mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new Profile(null, null, rs.getString("profile_image_url_lres"), 
				rs.getString("profile_image_url_hres"), 0, 0, 0, 0, null, 0,
				0, null, 0, null, null, null, null, null,null,null);
	}
}
