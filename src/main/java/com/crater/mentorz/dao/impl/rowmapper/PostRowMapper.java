/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.dao.impl.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.crater.mentorz.models.post.Content;
import com.crater.mentorz.models.post.MediaType;
import com.crater.mentorz.models.post.Post;

public class PostRowMapper implements RowMapper<Post> {

	@Override
	public Post mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new Post(rs.getLong("post_id"), rs.getLong("user_id"), rs.getInt("like_count"), rs.getLong("share_time"),
				rs.getLong("liked") > 0 ? true : false,
				new Content(rs.getLong("post_content_id"), MediaType.getMediaType(rs.getInt("media_type")), rs.getString("media_url_lres"),
						rs.getString("media_url_hres"), rs.getString("description")), null, rs.getInt("view_count"),
				rs.getInt("share_count"), rs.getInt("comment_count"), rs.getString("name"), null, null);
	}

}
