/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.dao.impl.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.crater.mentorz.models.user.Interest;

/**
 * @author Harish Anuragi
 *
 */
public class InterestRowMapper implements RowMapper<Interest> {
	
	@Override
	public Interest mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new Interest(rs.getInt("id"), rs.getString("name"),rs.getInt("parent_id"),rs.getBoolean("has_children"),false);
	}
}
