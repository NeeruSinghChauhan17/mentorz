package com.crater.mentorz.dao.impl;

import static java.util.Objects.nonNull;
import static org.apache.commons.lang3.StringUtils.EMPTY;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;
import java.util.function.Supplier;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

public class DefaultUtils {

	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultUtils.class);
	
	private DefaultUtils() {
		
	}
	
	public static String defaultStringIfNull(final String object) {
		return defaultIfNull(object, StringUtils.EMPTY);
	}
	
	public static Date defaultDateIfInvalid(final String object) {
		try {
			return new SimpleDateFormat().parse(object);
		} catch(Exception e) {
			LOGGER.debug("unable to parse date "+ e);
			return new Date();
		}
	}

	public static Long defaultLongIfNull(final Long object) {
		return defaultIfNull(object, Long.valueOf(0));
	}

	public static Integer defaultIntegerIfNull(final Integer object) {
		return defaultIfNull(object, Integer.valueOf(0));
	}
	
	public static Double defaultDoubleIfNull(final Double object) {
		return defaultIfNull(object, Double.valueOf(0));
	}
	
	public static Double nullIfDefaultDouble(final Double value) {
		return nullIfDefault(value, Double.valueOf(0));
	}

	public static Boolean defaultBooleanIfNull(final Boolean object) {
		return defaultIfNull(object, Boolean.FALSE);
	}

	public static String nullIfDefaultString(final String value) {
		return nullIfDefault(value, StringUtils.EMPTY);
	}

	public static Integer nullIfDefaultInteger(final Integer value) {
		return nullIfDefault(value, Integer.valueOf(0));
	}

	public static Long nullIfDefaultLong(final Long value) {
		return nullIfDefault(value, Long.valueOf(0));
	}

	public static Boolean nullIfDefaultLong(final Boolean value) {
		return nullIfDefault(value, Boolean.FALSE);
	}

	public static <T> Supplier<T> createUpdatingSupplier(T newValue, T originalValue, T defaultValue) {
		return new NullValueCheckingSupplier<>(newValue, originalValue, defaultValue);
	}

	public static Supplier<String> createUpdatingStringSupplier(String newValue, String originalValue) {
		return createUpdatingSupplier(newValue, originalValue, EMPTY);
	}
	

	public static Supplier<String> createUpdatingStringSupplierWithDefaultNull(String newValue, String originalValue) {
		return createUpdatingSupplier(newValue, originalValue, null);
	}
	


	public static Supplier<Short> createUpdatingShortSupplier(Short newcc, Short oldcc) {
		return createUpdatingSupplier(newcc, oldcc,Short.valueOf((short) 0));
	}

	
	public static Supplier<Integer> createUpdatingIntegerSupplier(Integer newValue, Integer originalValue) {
		return createUpdatingSupplier(newValue, originalValue, Integer.valueOf(0));
	}

	public static Supplier<Long> createUpdatingLongSupplier(Long newValue, Long originalValue) {
		return createUpdatingSupplier(newValue, originalValue, Long.valueOf(0));
	}
	
	public static Supplier<Double> createUpdatingDoubleSupplier(Double newValue, Double originalValue) {
		return createUpdatingSupplier(newValue, originalValue, Double.valueOf(0));
	}

	public static Supplier<Boolean> createUpdatingBooleanSupplier(Boolean newValue, Boolean originalValue) {
		return createUpdatingSupplier(newValue, originalValue, Boolean.FALSE);
	}

	public static <T> T defaultIfNull(final T object, final T defaultObject) {
		if (Objects.isNull(object)) {
			return defaultObject;
		}
		return object;
	}

	public static <T> T nullIfDefault(final T value, final T defaultObject) {
		if (value.equals(defaultObject)) {
			return null;
		}
		return value;
	}

	public static <T> void updateIfNotNull(String columnName, MapSqlParameterSource sqlParameterSource,
			Supplier<T> supplier) {
		sqlParameterSource.addValue(columnName, supplier.get());
	}

	/**
	 * A supplier that supplies only non null values
	 * 
	 *
	 * @param <T>
	 *            the type of value to be supplied
	 */
	private static class NullValueCheckingSupplier<T> implements Supplier<T> {

		private final T newValue;

		private final T originalValue;

		private final T defaultValue;

		private NullValueCheckingSupplier(T newValue, T originalValue, T defaultValue) {
			this.newValue = newValue;
			this.originalValue = originalValue;
			this.defaultValue = defaultValue;
		}

		@Override
		public T get() {
			if (nonNull(newValue)) {
				return newValue;
			} else if (nonNull(originalValue)) {
				return originalValue;
			} else {
				return defaultValue;
			}
		}

	}

}
