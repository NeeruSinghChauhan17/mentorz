package com.crater.mentorz.dao.impl.rowmapper.adminpanel;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.crater.mentorz.models.admin.Rank;

public class RankRowMapper implements RowMapper<Rank> {

	@Override
	public Rank mapRow(ResultSet rs, int column) throws SQLException {
		
		return new Rank(rs.getInt("rank_count"), rs.getString("name"));
	}

}
