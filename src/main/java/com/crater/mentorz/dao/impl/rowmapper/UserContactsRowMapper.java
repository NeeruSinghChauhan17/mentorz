package com.crater.mentorz.dao.impl.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.RowMapper;

import com.crater.mentorz.models.user.InternationalNumber;
import com.crater.mentorz.models.user.PhoneContact;

public class UserContactsRowMapper  implements RowMapper<PhoneContact> {

	@Override
	public PhoneContact mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new PhoneContact(rs.getString("name"),
				new InternationalNumber(rs.getString("cc") == null ? StringUtils.EMPTY : rs.getString("cc"),
						rs.getString("area_code")==null?StringUtils.EMPTY :rs.getString("area_code"),
						rs.getString("phone_number") == null ? StringUtils.EMPTY : rs.getString("phone_number")),null);
	}
    

}
