package com.crater.mentorz.dao.impl.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.crater.mentorz.elasticsearch.model.req.Rating;
import com.crater.mentorz.elasticsearch.model.req.Source;
import com.crater.mentorz.models.mentor.Expertise;


public class SourceResultSetExtractor implements ResultSetExtractor<Collection<Source>> {
	

	@Override
	public Collection<Source> extractData(ResultSet rs) throws SQLException, DataAccessException {
		
		final Map<Long, Source> userMap = new HashMap<Long, Source>();
	     while (rs.next()) {
			final Long userId = rs.getLong("user_id");
              if (!userMap.containsKey(userId)) {
            	userMap.put(userId, new Source(userId, rs.getString("email_id"), 
				rs.getString("profile_image_url_lres"),
				rs.getString("profile_image_url_hres"),rs.getInt("followers"),rs.getInt("following"),rs.getInt("mentors"),
				rs.getInt("mentees"),
				rs.getString("basic_info"),rs.getLong("dob"),rs.getInt("charge_price"),rs.getString("org"),rs.getString("located_in"),
				rs.getString("position"),0,rs.getInt("exp"),rs.getString("video_bio_lres")==null?"":rs.getString("video_bio_lres"),
						rs.getString("video_bio_hres")==null?"":rs.getString("video_bio_hres"),
				new ArrayList<Rating>(),0.0f,rs.getString("name"),rs.getInt("account_status"), new ArrayList<Expertise>(),
				new HashSet<Integer>(),
				new HashSet<Long>(),new HashSet<Long>(),new HashSet<Long>(),new ArrayList<Expertise>(),
				new ArrayList<Integer>(),rs.getString("facebook_url")==null?"":rs.getString("facebook_url"),
						rs.getString("linkedin_url")==null?"":rs.getString("linkedin_url")	));
			}
        /*   Integer expertise=rs.getInt("interest_id");
         if(rs.getInt("added")==1)
          {  
           userMap.get(userId).getExpertise().add(new Expertise(expertise,rs.getString("expertise_name"),
        		   rs.getInt("parent_id"),rs.getInt("has_children")>0? true:false ,false));
		  }
         userMap.get(userId).getExpertiseIds().add(expertise);*/

		}
		
		
		return userMap.values();
		
	}
	

	}
