package com.crater.mentorz.dao.impl.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.crater.mentorz.models.Rating;

/**
 *
 * @Auther Harish Anuragi
 */
public class RatingRowMapper implements RowMapper<Rating>{

	@Override
	public Rating mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new Rating(rs.getDouble("rating"), rs.getString("review"), null);
	}

}

