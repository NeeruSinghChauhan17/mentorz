package com.crater.mentorz.dao.impl;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Collections;
import java.util.List;

import org.joda.time.Instant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.crater.mentorz.dao.AdminDAO;
import com.crater.mentorz.dao.BaseDAO;
import com.crater.mentorz.dao.impl.rowmapper.InterestRowMapper;
import com.crater.mentorz.dao.impl.rowmapper.adminpanel.AbusedPostRowMapper;
import com.crater.mentorz.dao.impl.rowmapper.adminpanel.FeedbackRowMapper;
import com.crater.mentorz.dao.impl.rowmapper.adminpanel.MentorRowMapper;
import com.crater.mentorz.dao.impl.rowmapper.adminpanel.RankRowMapper;
import com.crater.mentorz.dao.impl.rowmapper.adminpanel.UserAccountSummaryRowMapper;
import com.crater.mentorz.models.admin.Action;
import com.crater.mentorz.models.admin.Rank;
import com.crater.mentorz.models.admin.User;
import com.crater.mentorz.models.post.Post;
import com.crater.mentorz.models.user.Feedback;
import com.crater.mentorz.models.user.Interest;
import com.google.common.base.Optional;


@Repository
public class AdminDAOImpl implements AdminDAO {

	private final BaseDAO baseDao;
	private static final int NO_OF_ROWS_PER_PAGE = 20;
	private static final RowMapper<com.crater.mentorz.models.user.User> USER_ACCOUNT_SUMMARY_ROW_MAPPER = new UserAccountSummaryRowMapper();
	private static final RowMapper<com.crater.mentorz.models.user.User> MENTOR_ROW_MAPPER = new MentorRowMapper();
	
	private static final RowMapper<Feedback> FEEDBACK_ROW_MAPPER = new FeedbackRowMapper();
	private static final RowMapper<Post> ABUSED_POST_ROW_MAPPER = new AbusedPostRowMapper();
	private static final RowMapper<Rank> RANK_ROW_MAPPER = new RankRowMapper();
	private final InterestRowMapper INTEREST_ROW_MAPPER = new InterestRowMapper();
	private static final String[] SERVICE_ID_COLUMN_NAME = { "id" };

	@Autowired
	public AdminDAOImpl(final BaseDAOFactory baseDaoFactory) {
		this.baseDao = checkNotNull(baseDaoFactory.createBaseDao("admin.xml"), "Base Dao Cannot Be Null");

	}

	@Override
	public String login(final User user) {
		try {
			final String query = baseDao.getQueryById("getAdminUser");
			final SqlParameterSource source = new MapSqlParameterSource().addValue("name", user.getName());
			return baseDao.getJdbcTemplate().queryForObject(query, source, String.class);
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}

	@Override
	public Optional<List<com.crater.mentorz.models.user.User>> getUsers(final Integer pageNo) {
		try {
			final String query = baseDao.getQueryById("getAllUser");
			final SqlParameterSource source = new MapSqlParameterSource()
					.addValue("offset", pageNo * NO_OF_ROWS_PER_PAGE)
					.addValue("limit", NO_OF_ROWS_PER_PAGE);
			return Optional.fromNullable(baseDao.getJdbcTemplate().query(query, source, USER_ACCOUNT_SUMMARY_ROW_MAPPER));
		} catch (EmptyResultDataAccessException e) {
			return Optional.absent();
		}
	}
	
	@Override
	public List<com.crater.mentorz.models.user.User> getMentors() {
		try {
			final String query = baseDao.getQueryById("getAllMentors");
			final SqlParameterSource source = new MapSqlParameterSource();
			return  baseDao.getJdbcTemplate().query(query, source, MENTOR_ROW_MAPPER);
		} catch (EmptyResultDataAccessException e) {
			return Collections.emptyList();
		}
	}

	@Override
	public Optional<List<Feedback>> getFeedback(final Integer pageNo) {
		try {
			final String query = baseDao.getQueryById("getAllFeedbacks");
			final SqlParameterSource source = new MapSqlParameterSource().addValue("offset", pageNo * NO_OF_ROWS_PER_PAGE).addValue(
					"limit", NO_OF_ROWS_PER_PAGE);
			return Optional.fromNullable(baseDao.getJdbcTemplate().query(query, source, FEEDBACK_ROW_MAPPER));
		} catch (EmptyResultDataAccessException e) {
			return Optional.absent();
		}
	}

	@Override
	public Integer saveInterest(final Interest interest) {
		final String query = baseDao.getQueryById("addInterest");
		final KeyHolder keyHolder = new GeneratedKeyHolder();
		try {
			final SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
			.addValue("interest", interest.getInterest())
			.addValue("parentId", interest.getParentId())
			.addValue("hasChildren", interest.getHasChildren())
			.addValue("lastUpdated", Instant.now().getMillis());
			baseDao.getJdbcTemplate().update(query, sqlParameterSource,keyHolder,SERVICE_ID_COLUMN_NAME);
			return keyHolder.getKey().intValue();	
		} catch (DataIntegrityViolationException e) {
			return 0;
		}
	}

	@Override
	public List<Interest> getInterestsList() {
		final String listVersePBFriendsQuery = baseDao.getQueryById("getDefaultInterest");
		final SqlParameterSource source = new MapSqlParameterSource();
		return baseDao.getJdbcTemplate().query(listVersePBFriendsQuery, source, INTEREST_ROW_MAPPER);
	}
	
	@Override
	public Optional<List<com.crater.mentorz.models.user.User>> getBlockedUsers(final Integer pageNo) {
		try {
			final String query = baseDao.getQueryById("getAllBlockedUser");
			final SqlParameterSource source = new MapSqlParameterSource().addValue("offset", pageNo * NO_OF_ROWS_PER_PAGE).addValue(
					"limit", NO_OF_ROWS_PER_PAGE);
			return Optional.fromNullable(baseDao.getJdbcTemplate().query(query, source, USER_ACCOUNT_SUMMARY_ROW_MAPPER));
		} catch (EmptyResultDataAccessException e) {
			return Optional.absent();
		}
	}

	@Override
	public Boolean updateUserAccountStatus(final Long userId, final Action action) {
		final String query = baseDao.getQueryById("updateAccountStatus");
			final SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
			.addValue("userId", userId)
			.addValue("status", Action.getIntValue(action));
			return baseDao.getJdbcTemplate().update(query, sqlParameterSource) > 0;
	}

	@Override
	public Optional<List<Feedback>> searchFeedbackByUserName(final String userName, final Integer pageNo) {
		try {
			final String query = baseDao.getQueryById("searchFeedbacksByUserName");
			final SqlParameterSource source = new MapSqlParameterSource()
			        .addValue("name", "%"+userName+"%")
			        .addValue("offset", pageNo * NO_OF_ROWS_PER_PAGE)
			        .addValue("limit", NO_OF_ROWS_PER_PAGE);
			return Optional.fromNullable(baseDao.getJdbcTemplate().query(query, source, FEEDBACK_ROW_MAPPER));
		} catch (EmptyResultDataAccessException e) {
			return Optional.absent();
		}
	}

	@Override
	public Optional<List<com.crater.mentorz.models.user.User>> searchUsersByUserName(final String userName, final Integer pageNo) {
		try {
			final String query = baseDao.getQueryById("searchUserByName");
			final SqlParameterSource source = new MapSqlParameterSource()
			      .addValue("name", "%"+userName+"%")
			      .addValue("offset", pageNo * NO_OF_ROWS_PER_PAGE)
			      .addValue("limit", NO_OF_ROWS_PER_PAGE);
			return Optional.fromNullable(baseDao.getJdbcTemplate().query(query, source, USER_ACCOUNT_SUMMARY_ROW_MAPPER));
		} catch (EmptyResultDataAccessException e) {
			return Optional.absent();
		}

	}

	/* (non-Javadoc)
	 * @see com.crater.mentorz.dao.AdminDAO#searchAbusedUsersByUserName(java.lang.String, java.lang.Integer)
	 */
	@Override
	public Optional<List<Post>> searchAbusedUsersByUserName(final String userName, final Integer pageNo) {
		try {
			final String query = baseDao.getQueryById("searchAbusedPostsByUserName");
			final SqlParameterSource source = new MapSqlParameterSource()
			       .addValue("name", "%"+userName+"%")
			       .addValue("offset", pageNo * NO_OF_ROWS_PER_PAGE)
			       .addValue("limit", NO_OF_ROWS_PER_PAGE);
			return Optional.fromNullable(baseDao.getJdbcTemplate().query(query, source, ABUSED_POST_ROW_MAPPER));
		} catch (EmptyResultDataAccessException e) {
			return Optional.absent();
		}
	}

	@Override
	public Optional<List<com.crater.mentorz.models.user.User>> searchBlockedUsersByUserName(final String userName, final Integer pageNo) {
		try {
			final String query = baseDao.getQueryById("searchBlockedUserByUserName");
			final SqlParameterSource source = new MapSqlParameterSource()
			       .addValue("name", "%"+userName+"%")
			       .addValue("offset", pageNo * NO_OF_ROWS_PER_PAGE)
			       .addValue("limit", NO_OF_ROWS_PER_PAGE);
			return Optional.fromNullable(baseDao.getJdbcTemplate().query(query, source, USER_ACCOUNT_SUMMARY_ROW_MAPPER));
		} catch (EmptyResultDataAccessException e) {
			return Optional.absent();
		}
	}

	@Override
	public Optional<List<Post>> getAbusedPost(final Integer pageNo) {
		try {
			final String query = baseDao.getQueryById("getAbusedPosts");
			final SqlParameterSource source = new MapSqlParameterSource()
			       .addValue("offset", pageNo * NO_OF_ROWS_PER_PAGE)
			       .addValue("limit", NO_OF_ROWS_PER_PAGE);
			return Optional.fromNullable(baseDao.getJdbcTemplate().query(query, source, ABUSED_POST_ROW_MAPPER));
		} catch (EmptyResultDataAccessException e) {
			return Optional.absent();
		}
	}

	@Override
	public void updatePostStatus(final Long userId, final Long postId, final Action action) {
		final String query = baseDao.getQueryById("updatePostStatus");
		final SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
		.addValue("userId", userId)
		.addValue("postId", postId)
		.addValue("status", Action.getIntValue(action));
		baseDao.getJdbcTemplate().update(query, sqlParameterSource);
	}

	@Override
	public Integer getTotalUser() {
		final String query = baseDao.getQueryById("totalUsers");
		final SqlParameterSource sqlParameterSource = new MapSqlParameterSource();
		return baseDao.getJdbcTemplate().queryForObject(query, sqlParameterSource, Integer.class);
	}

	@Override
	public Integer getTotalMentor() {
		final String query = baseDao.getQueryById("totalMentors");
		final SqlParameterSource sqlParameterSource = new MapSqlParameterSource();
		return baseDao.getJdbcTemplate().queryForObject(query, sqlParameterSource, Integer.class);
	}

	@Override
	public Integer getTotalExpertise() {
		final String query = baseDao.getQueryById("getTotalExpertise");
		final SqlParameterSource sqlParameterSource = new MapSqlParameterSource();
		return baseDao.getJdbcTemplate().queryForObject(query, sqlParameterSource, Integer.class);
	}

	@Override
	public Integer getTotalValues() {
		final String query = baseDao.getQueryById("getTotalValues");
		final SqlParameterSource sqlParameterSource = new MapSqlParameterSource();
		return baseDao.getJdbcTemplate().queryForObject(query, sqlParameterSource, Integer.class);
	}

	@Override
	public List<Rank> getExpertisesRank() {
		try {
			final String query = baseDao.getQueryById("getExpertisesRank");
			final SqlParameterSource source = new MapSqlParameterSource();
			return baseDao.getJdbcTemplate().query(query, source, RANK_ROW_MAPPER);
		} catch (EmptyResultDataAccessException e) {
			return Collections.emptyList();
		}
	}

	@Override
	public List<Rank> getValuesRank() {
		try {
			final String query = baseDao.getQueryById("getValuesRank");
			final SqlParameterSource source = new MapSqlParameterSource();
			return baseDao.getJdbcTemplate().query(query, source, RANK_ROW_MAPPER);
		} catch (EmptyResultDataAccessException e) {
			return Collections.emptyList();
		}
	}

	@Override
	public List<Rank> getInterestsRank() {
		try {
			final String query = baseDao.getQueryById("getInterestsRank");
			final SqlParameterSource source = new MapSqlParameterSource();
			return baseDao.getJdbcTemplate().query(query, source, RANK_ROW_MAPPER);
		} catch (EmptyResultDataAccessException e) {
			return Collections.emptyList();
		}
	}

}
