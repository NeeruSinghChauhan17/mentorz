package com.crater.mentorz.dao.impl.rowmapper.adminpanel;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.crater.mentorz.models.user.Feedback;
import com.crater.mentorz.utils.StringUtil;

/**
 *
 * @Auther Harish Anuragi
 */
public class FeedbackRowMapper implements RowMapper<Feedback>{

	@Override
	public Feedback mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new Feedback(rs.getLong("user_id"), StringUtil.textDecoderUTF8(rs.getString("name")), rs.getInt("recommend_scale_count"),StringUtil.textDecoderUTF8( rs.getString("description")));
	}

}

