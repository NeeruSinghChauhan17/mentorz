/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.dao.impl;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import com.crater.mentorz.dao.BaseDAO;
import com.crater.mentorz.dao.MentorDAO;
import com.crater.mentorz.dao.impl.rowmapper.ExpertiseListRowMapper;
import com.crater.mentorz.dao.impl.rowmapper.ExpertiseRowMapper;
import com.crater.mentorz.dao.impl.rowmapper.InterestRowMapper;
import com.crater.mentorz.dao.impl.rowmapper.IsValidMenteeRowMapper;
import com.crater.mentorz.dao.impl.rowmapper.MenteeRequestTimeStampRowMapper;
import com.crater.mentorz.dao.impl.rowmapper.MentorRowMapper;
import com.crater.mentorz.dao.impl.rowmapper.MyExpertiseRowMapper;
import com.crater.mentorz.dao.impl.rowmapper.PendingRequestTimeStampRowMapper;
import com.crater.mentorz.dao.impl.rowmapper.PublicBioRowMapper;
import com.crater.mentorz.enums.AccountStatus;
import com.crater.mentorz.exception.ForbiddenException;
import com.crater.mentorz.models.mentee.MenteeRequestTimeStamp;
import com.crater.mentorz.models.mentor.BeAMentor;
import com.crater.mentorz.models.mentor.Expertise;
import com.crater.mentorz.models.mentor.MentorFilter;
import com.crater.mentorz.models.user.Interest;
import com.crater.mentorz.models.user.User;
import com.google.common.base.Optional;


@Repository
public class MentorDAOImpl implements MentorDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(MentorDAO.class);
	private static final int NO_OF_RESULTS_PER_PAGE = 20;
	private final BaseDAO baseDao;
    private final MentorRowMapper MENTOR_ROW_MAPPER = new MentorRowMapper();
	private final ExpertiseRowMapper EXPERTISE_ROW_MAPPER = new ExpertiseRowMapper();
	private final MyExpertiseRowMapper MY_EXPERTISE_ROW_MAPPER = new MyExpertiseRowMapper();
	private final PublicBioRowMapper PUBLIC_BIO_ROW_MAPPER = new PublicBioRowMapper();
	private final InterestRowMapper INTEREST_ROW_MAPPER = new InterestRowMapper();
	private final ExpertiseListRowMapper EXPERTISE_LIST_ROW_MAPPER=new ExpertiseListRowMapper();
	//private final MenteeRequestRowMapper MENTEE_REQUESTS_ROW_MAPPER = new MenteeRequestRowMapper();
	private final MenteeRequestTimeStampRowMapper MENTEE_REQUESTS_TIMESTAMP_ROW_MAPPER=new  MenteeRequestTimeStampRowMapper();
	private final PendingRequestTimeStampRowMapper PENDING_REQUEST_TIMESTAMP_ROW_MAPPER=new PendingRequestTimeStampRowMapper();
	//private final PendingRequestRowMapper PENDING_REQUEST_ROW_MAPPER = new PendingRequestRowMapper();
	private final IsValidMenteeRowMapper IS_VALID_MENTEE_ROWMAPPER = new IsValidMenteeRowMapper();
	
	@Autowired
	public MentorDAOImpl(final BaseDAOFactory baseDaoFactory) {
		this.baseDao = checkNotNull(baseDaoFactory.createBaseDao("mentor.xml"), "Base Dao Cannot Be Null");

	}

	@Override
	public void addMentor(final Long userId, final BeAMentor mentor) throws ForbiddenException {
		addOrUpdateMentor(userId, mentor);
		//updateExperties(userId, mentor.getExperties());
	}

	private void addOrUpdateMentor(final long userId, final BeAMentor mentor) throws ForbiddenException {
		try {
			final String updateMentorQuery = baseDao.getQueryById("addOrUpdateMentorDetails");
			final SqlParameterSource source = new MapSqlParameterSource()
					.addValue("userId", userId).addValue("price", mentor.getPrice())
					.addValue("org", mentor.getOrganisation())
					.addValue("location", mentor.getLocation())
					.addValue("position", mentor.getDesignation())
					.addValue("expYear", mentor.getExpYears());
			baseDao.getJdbcTemplate().update(updateMentorQuery, source);
		} catch (DataIntegrityViolationException e) {
			LOGGER.info("unable to be a montor {}",e);
			throw new ForbiddenException.Builder().build();
		}
	}
	@Override
	public void updateExperties(final long userId, final List<Expertise> experties,Integer added)  {
		try {
			final String insertExpertiseQuery = baseDao.getQueryById("insertExpertise");
			final SqlParameterSource[] batch = new MapSqlParameterSource[experties.size()];
			for (int index = 0; index < experties.size(); index++) {
				final Expertise expertise = experties.get(index);
				batch[index] = new MapSqlParameterSource().addValue("userId", userId).addValue("added", added).
						addValue("interestId", expertise.getExpertiseId());
				
			}
			baseDao.getJdbcTemplate().batchUpdate(insertExpertiseQuery, batch);
		} catch (DataIntegrityViolationException e) {
			LOGGER.error("expertise contains invalid id's");
		}
	}

	public void deleteExpertises(long userId){
		final String deleteExpertiseQuery = baseDao.getQueryById("deleteExpertise");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId);
		baseDao.getJdbcTemplate().update(deleteExpertiseQuery, source);
	}
	@Override
	public Optional<List<User>> getMentor(final Long userId, final MentorFilter filter, final Integer pageNo) {
		try {
			String getMentorQuery;
			SqlParameterSource source;
			List<String> list = new ArrayList<>();
			for (int i = 0; i < filter.getInterests().size(); i++) {
				list.add(Integer.toString(filter.getInterests().get(i).getInterestId()));
			}

			if (filter.getMaxExp() != 0 && filter.getMinExp() != 0 && filter.getMinRating() != 0 && filter.getMaxRating() != 0
					&& filter.getMinPrice() != 0 && filter.getMaxPrice() != 0) {
				getMentorQuery = baseDao.getQueryById("getMentorViaPriceAndRatingAndExp");
				source = getMentorWithRatingAndExpAndPriceParamMap(filter, list, userId, pageNo);
			} else if (filter.getMaxExp() != 0 && filter.getMinExp() != 0 && filter.getMinRating() != 0 && filter.getMaxRating() != 0) {
				getMentorQuery = baseDao.getQueryById("getMentorViaExpAndRating");
				source = getMentorWithRatingAndExpParamMap(filter, list, userId, pageNo);
			} else if (filter.getMinRating() != 0 && filter.getMaxRating() != 0 && filter.getMinPrice() != 0 && filter.getMaxPrice() != 0) {
				getMentorQuery = baseDao.getQueryById("getMentorViaPriceAndRating");
				source = getMentorWithRatingAndPriceParamMap(filter, list, userId, pageNo);
			} else if (filter.getMaxExp() != 0 && filter.getMinExp() != 0 && filter.getMinPrice() != 0 && filter.getMaxPrice() != 0) {
				getMentorQuery = baseDao.getQueryById("getMentorViaPriceAndExp");
				source = getMentorWithExpAndPriceParamMap(filter, list, userId, pageNo);
			} else if (filter.getMaxExp() != 0 && filter.getMinExp() != 0) {
				getMentorQuery = baseDao.getQueryById("getMentorViaExp");
				source = getMentorWithExpParamMap(filter, list, userId, pageNo);
			} else if (filter.getMinRating() != 0 && filter.getMaxRating() != 0) {
				getMentorQuery = baseDao.getQueryById("getMentorViaRating");
				source = getMentorWithRatingParamMap(filter, list, userId, pageNo);
			} else if (filter.getMinPrice() != 0 && filter.getMaxPrice() != 0) {
				getMentorQuery = baseDao.getQueryById("getMentorViaPrice");
				source = getMentorWithPriceParamMap(filter, list, userId, pageNo);
			} else {
				getMentorQuery = baseDao.getQueryById("getMentorViaInterest");
				source = getMentorWithInterestParamMap(list, userId, pageNo);
			}
			final List<User> user = baseDao.getJdbcTemplate().query(getMentorQuery, source, MENTOR_ROW_MAPPER);
			return Optional.fromNullable(user);
		} catch (EmptyResultDataAccessException e) {
			return Optional.<List<User>> absent();
		}
	}

	private static final SqlParameterSource getMentorWithInterestParamMap(final List<String> interestList, final Long userId, final Integer pageNo) {
		final SqlParameterSource source = new MapSqlParameterSource().addValue("interestList", interestList)
				.addValue("userId", userId)
				.addValue("status", AccountStatus.ACTIVE.getStatusCode())
				.addValue("offset", pageNo * NO_OF_RESULTS_PER_PAGE)
				.addValue("limit", NO_OF_RESULTS_PER_PAGE);
		return source;
	}

	private static final SqlParameterSource getMentorWithPriceParamMap(final MentorFilter filter, final List<String> interestList, final Long userId, final Integer pageNo) {
		final SqlParameterSource source = new MapSqlParameterSource().addValue("interestList", interestList)
				.addValue("minPrice", filter.getMinPrice()).addValue("maxPrice", filter.getMaxPrice())
				.addValue("userId", userId)
				.addValue("status", AccountStatus.ACTIVE.getStatusCode())
				.addValue("offset", pageNo * NO_OF_RESULTS_PER_PAGE)
				.addValue("limit", NO_OF_RESULTS_PER_PAGE);
		return source;
	}

	private static final SqlParameterSource getMentorWithRatingParamMap(final MentorFilter filter, final List<String> interestList, final Long userId, final Integer pageNo) {
		final SqlParameterSource source = new MapSqlParameterSource().addValue("interestList", interestList)
				.addValue("minRating", filter.getMinRating()).addValue("maxRating", filter.getMaxRating())
				.addValue("userId", userId)
				.addValue("status", AccountStatus.ACTIVE.getStatusCode())
				.addValue("offset", pageNo * NO_OF_RESULTS_PER_PAGE)
				.addValue("limit", NO_OF_RESULTS_PER_PAGE);
		return source;
	}

	private static final SqlParameterSource getMentorWithExpParamMap(final MentorFilter filter, final List<String> interestList, final Long userId, final Integer pageNo) {
		final SqlParameterSource source = new MapSqlParameterSource().addValue("interestList", interestList)
				.addValue("minExp", filter.getMinExp()).addValue("maxExp", filter.getMaxExp())
				.addValue("userId", userId)
				.addValue("status", AccountStatus.ACTIVE.getStatusCode())
				.addValue("offset", pageNo * NO_OF_RESULTS_PER_PAGE)
				.addValue("limit", NO_OF_RESULTS_PER_PAGE);
		return source;
	}

	private static final SqlParameterSource getMentorWithExpAndPriceParamMap(final MentorFilter filter, final List<String> interestList, final Long userId, final Integer pageNo) {
		final SqlParameterSource source = new MapSqlParameterSource().addValue("interestList", interestList)
				.addValue("minPrice", filter.getMinPrice()).addValue("maxPrice", filter.getMaxPrice())
				.addValue("minExp", filter.getMinExp()).addValue("maxExp", filter.getMaxExp())
				.addValue("userId", userId)
				.addValue("status", AccountStatus.ACTIVE.getStatusCode())
				.addValue("offset", pageNo * NO_OF_RESULTS_PER_PAGE)
				.addValue("limit", NO_OF_RESULTS_PER_PAGE);
		return source;
	}

	private static final SqlParameterSource getMentorWithRatingAndPriceParamMap(final MentorFilter filter, final List<String> interestList, final Long userId, final Integer pageNo) {
		final SqlParameterSource source = new MapSqlParameterSource().addValue("interestList", interestList)
				.addValue("minPrice", filter.getMinPrice()).addValue("maxPrice", filter.getMaxPrice())
				.addValue("minRating", filter.getMinRating()).addValue("maxRating", filter.getMaxRating())
				.addValue("userId", userId)
				.addValue("status", AccountStatus.ACTIVE.getStatusCode())
				.addValue("offset", pageNo * NO_OF_RESULTS_PER_PAGE)
				.addValue("limit", NO_OF_RESULTS_PER_PAGE);
		return source;
	}

	private static final SqlParameterSource getMentorWithRatingAndExpParamMap(final MentorFilter filter, final List<String> interestList, final Long userId, final Integer pageNo) {
		final SqlParameterSource source = new MapSqlParameterSource().addValue("interestList", interestList)
				.addValue("minExp", filter.getMinExp()).addValue("maxExp", filter.getMaxExp()).addValue("minRating", filter.getMinRating())
				.addValue("maxRating", filter.getMaxRating())
				.addValue("userId", userId)
				.addValue("status", AccountStatus.ACTIVE.getStatusCode())
				.addValue("offset", pageNo * NO_OF_RESULTS_PER_PAGE)
				.addValue("limit", NO_OF_RESULTS_PER_PAGE);
		return source;
	}

	private static final SqlParameterSource getMentorWithRatingAndExpAndPriceParamMap(final MentorFilter filter,
			final List<String> interestList, final Long userId, final Integer pageNo) {
		final SqlParameterSource source = new MapSqlParameterSource().addValue("interestList", interestList)
				.addValue("minExp", filter.getMinExp()).addValue("maxExp", filter.getMaxExp()).addValue("minRating", filter.getMinRating())
				.addValue("maxRating", filter.getMaxRating()).addValue("minPrice", filter.getMinPrice())
				.addValue("maxPrice", filter.getMaxPrice())
				.addValue("userId", userId)
				.addValue("status", AccountStatus.ACTIVE.getStatusCode())
				.addValue("offset", pageNo * NO_OF_RESULTS_PER_PAGE)
				.addValue("limit", NO_OF_RESULTS_PER_PAGE);
		return source;
	}

	@Override
	public List<Expertise> getMyExpertise(final Long userId) {
		final String listVersePBFriendsQuery = baseDao.getQueryById("expertiseList");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId);
		return baseDao.getJdbcTemplate().query(listVersePBFriendsQuery, source, MY_EXPERTISE_ROW_MAPPER);
	}

	@Override
	public Optional<BeAMentor> getPublicBio(final Long userId) {
		try {
			final String getPublicBioQuery = baseDao.getQueryById("getPublicBio");
			final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId);
			final BeAMentor publicBio = baseDao.getJdbcTemplate().queryForObject(getPublicBioQuery, source, PUBLIC_BIO_ROW_MAPPER);
			return Optional.fromNullable(publicBio);
		} catch (EmptyResultDataAccessException e) {
			return Optional.<BeAMentor> absent();
		}
	}

	@Override
	public Optional<List<MenteeRequestTimeStamp>> getMenteeRequests(final Long userId, final Integer pageNo) {
		try {
		final String getMenteeRequestsQuery = baseDao.getQueryById("getMenteeRequests");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId)
				.addValue("offset", pageNo * NO_OF_RESULTS_PER_PAGE).addValue("limit", NO_OF_RESULTS_PER_PAGE);
		final List<MenteeRequestTimeStamp> user = baseDao.getJdbcTemplate().query(getMenteeRequestsQuery, source,MENTEE_REQUESTS_TIMESTAMP_ROW_MAPPER);
			return Optional.fromNullable(user);
		} catch (EmptyResultDataAccessException e) {
			return Optional.<List<MenteeRequestTimeStamp>> absent();
		}
	}

	@Override
	public void addMentee(final Long userId, final Long menteeId) throws DuplicateKeyException, ForbiddenException {
		addMyMentee(userId, menteeId);
		addMymentor(userId, menteeId);
		updateMenteeCount(userId);
		updateMentorCount(menteeId);
		removeRequest(userId, menteeId);
	}

	private int removeRequest(final Long userId, final Long menteeId) {
		final String removeRequestQuery = baseDao.getQueryById("removeRequest");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId).addValue("menteeId", menteeId);
		return baseDao.getJdbcTemplate().update(removeRequestQuery, source);
	}

	private void updateMentorCount(final Long menteeId) {
		final String updateMentorCountQuery = baseDao.getQueryById("updateMentorCount");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", menteeId);
		baseDao.getJdbcTemplate().update(updateMentorCountQuery, source);
	}

	private void updateMenteeCount(final Long userId) {
		final String updateMenteeCountQuery = baseDao.getQueryById("updateMenteeCount");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId);
		baseDao.getJdbcTemplate().update(updateMenteeCountQuery, source);
	}

	private void addMymentor(final Long userId, final Long menteeId) throws ForbiddenException {
		try {
			final String addMentorQuery = baseDao.getQueryById("addMentor");
			final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", menteeId).addValue("mentorId", userId);
			baseDao.getJdbcTemplate().update(addMentorQuery, source);
		} catch (DuplicateKeyException e) {
			throw new ForbiddenException.Builder().build();
		} catch (DataIntegrityViolationException e) {
			throw new ForbiddenException.Builder().build();
		}
	}

	private void addMyMentee(final Long userId, final Long menteeId) throws ForbiddenException {
		try {
			final String addMenteeQuery = baseDao.getQueryById("addMentee");
			final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId).addValue("menteeId", menteeId);
			baseDao.getJdbcTemplate().update(addMenteeQuery, source);
		} catch (DuplicateKeyException e) {
			throw new ForbiddenException.Builder().build();
		} catch (DataIntegrityViolationException e) {
			throw new ForbiddenException.Builder().build();
		}
	}

	@Override
	public Boolean isRequestAlreadySent(final Long userId, final Long mentorId) {
		try {
			final String ckeckMenteeRequestsQuery = baseDao.getQueryById("ckeckMenteeRequests");
			final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId).addValue("mentorId", mentorId);
			return baseDao.getJdbcTemplate().queryForObject(ckeckMenteeRequestsQuery, source, Integer.class) > 0 ? true : false;
		} catch (EmptyResultDataAccessException e) {
			return false;
		}
	}

	@Override
	public Boolean isMyMentor(final Long userId, final Long mentorId) {
		try {
			final String getMyMentorQuery = baseDao.getQueryById("getMyMentor");
			final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId).addValue("mentorId", mentorId);
			return baseDao.getJdbcTemplate().queryForObject(getMyMentorQuery, source, Integer.class) > 0 ? true : false;
		} catch (EmptyResultDataAccessException e) {
			return false;
		}
	}
     
	@Override
	public Integer deleteRequest(final Long userId, final Long menteeId) {
		return removeRequest(userId, menteeId);
	}

	@Override
	public Optional<List<MenteeRequestTimeStamp>> getPendingRequests(final Long userId, final Integer pageNo) {
		try {
			final String getMenteeRequestsQuery = baseDao.getQueryById("getPendingRequests");
			final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId)
					.addValue("offset", pageNo * NO_OF_RESULTS_PER_PAGE).addValue("limit", NO_OF_RESULTS_PER_PAGE);
			final List<MenteeRequestTimeStamp> user = baseDao.getJdbcTemplate().query(getMenteeRequestsQuery, source, PENDING_REQUEST_TIMESTAMP_ROW_MAPPER);
			return Optional.fromNullable(user);
		} catch (EmptyResultDataAccessException e) {
			return Optional.<List<MenteeRequestTimeStamp>> absent();
		}
	}

	@Override
	public Integer cancelPendingRequest(final Long userId, final Long mentorId) {
		final String removeRequestQuery = baseDao.getQueryById("removePendingRequest");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId).addValue("mentorId", mentorId);
		return baseDao.getJdbcTemplate().update(removeRequestQuery, source);
	}

	@Override
	public List<Expertise> getExpertiseById(final Long mentorId,final Integer interestId) {
		final String listVersePBFriendsQuery = baseDao.getQueryById("getExpertiseById");
		final SqlParameterSource source = new MapSqlParameterSource()
				.addValue("interestId", interestId)
				.addValue("mentorId",mentorId);
		return baseDao.getJdbcTemplate().query(listVersePBFriendsQuery, source, EXPERTISE_LIST_ROW_MAPPER);
	}

	@Override
	public List<Expertise> getExpertiseByParentId(Integer parentId) {
		final String listVersePBFriendsQuery = baseDao.getQueryById("getExpertiseByParentId");
		final SqlParameterSource source = new MapSqlParameterSource()
				.addValue("parentId", parentId);
		return baseDao.getJdbcTemplate().query(listVersePBFriendsQuery, source, EXPERTISE_ROW_MAPPER);
	}

	@Override
	public List<Expertise> getExpertiseList(final Long mentorId) {
		final String listVersePBFriendsQuery = baseDao.getQueryById("getExpertiseList");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", mentorId).addValue("parentId", 0);
		return baseDao.getJdbcTemplate().query(listVersePBFriendsQuery, source, EXPERTISE_LIST_ROW_MAPPER);
	}

	@Override
	public boolean isValidMentee(long userId, long menteeId) {
		try {
			final String getMenteeRequestsQuery = baseDao.getQueryById("isValidMantee");
			
			final SqlParameterSource source = new MapSqlParameterSource()
					.addValue("userId", userId)
					.addValue("menteeId", menteeId);
		 long userId1	= baseDao.getJdbcTemplate().queryForObject(getMenteeRequestsQuery, source, IS_VALID_MENTEE_ROWMAPPER);	
	     return true;	
		} catch (EmptyResultDataAccessException e) {
			LOGGER.error("No information is available for mentee"+menteeId);	
		}
   return false;
	}
    
	@Override
	public List<Expertise> getExpertiseListByParentId(Integer parentId,
			Long mentorId) {
		final String listVersePBFriendsQuery = baseDao.getQueryById("getExpertiseListByParentId");
		final SqlParameterSource source = new MapSqlParameterSource()
				.addValue("parentId", parentId)
				.addValue("userId", mentorId);
		return baseDao.getJdbcTemplate().query(listVersePBFriendsQuery, source, EXPERTISE_LIST_ROW_MAPPER);
	}

	
	@Override
	public List<Expertise> getParentExpertises(Set<Integer> childExpertises) {
		final String Query = baseDao.getQueryById("getParentExpertises");
		final SqlParameterSource source = new MapSqlParameterSource()
				.addValue("childExpertises",childExpertises);
				
		return baseDao.getJdbcTemplate().query(Query, source, EXPERTISE_ROW_MAPPER );
	}

	@Override
	public List<Interest> getParentInterestIds(Set<Integer> childExpertises) {
		final String Query = baseDao.getQueryById("getParentInterests");
		final SqlParameterSource source = new MapSqlParameterSource()
				.addValue("childExpertises",childExpertises);
				
		return baseDao.getJdbcTemplate().query(Query, source, INTEREST_ROW_MAPPER );
	}

	
}
