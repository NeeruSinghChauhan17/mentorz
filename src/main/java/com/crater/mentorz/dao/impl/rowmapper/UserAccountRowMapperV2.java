/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.dao.impl.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.RowMapper;

import com.crater.mentorz.models.user.InternationalNumber;
import com.crater.mentorz.models.user.Profile;
import com.crater.mentorz.models.user.User;


public class UserAccountRowMapperV2 implements RowMapper<User> {

	@Override
	public User mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new User(rs.getLong("user_id"), rs.getString("email_id"), rs.getString("password"), null,
				new InternationalNumber(rs.getString("cc") == null ? StringUtils.EMPTY : rs.getString("cc"),
						rs.getString("area_code")==null?StringUtils.EMPTY :rs.getString("area_code"),
						rs.getString("phone_number") == null ? StringUtils.EMPTY : rs.getString("phone_number")), new Profile(null, null,
				rs.getString("profile_image_url_lres"), rs.getString("profile_image_url_hres"), 0, 0, 0, 0,rs.getString("basic_info"),
				rs.getLong("dob"), 0, rs.getString("name"), 0, null, rs.getString("video_bio_lres"), rs.getString("video_bio_hres"),
				null, null,null,null),
				null, null, rs.getInt("account_status")==1?true:false,rs.getBoolean("has_interests"),rs.getBoolean("has_values"),
						rs.getInt("email_verified")==0?false:true,
						rs.getInt("phone_number_verified")==0?false:true);
	}

}
