/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.dao.impl.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.crater.mentorz.models.mentor.BeAMentor;

/**
 * @author Harish Anuragi
 * 
 */
public class PublicBioRowMapper implements RowMapper<BeAMentor> {

	@Override
	public BeAMentor mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new BeAMentor(rs.getInt("exp_years"), rs.getString("org"), rs.getString("position"), rs.getString("located_in"), null,
				rs.getInt("charge_price"));
	}

}
