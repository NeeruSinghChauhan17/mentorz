/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.dao;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import com.crater.mentorz.elasticsearch.model.req.Source;
import com.crater.mentorz.exception.ConflitException;
import com.crater.mentorz.exception.ForbiddenException;
import com.crater.mentorz.exception.NotFoundException;
import com.crater.mentorz.models.Rating;
import com.crater.mentorz.models.mentor.Expertise;
import com.crater.mentorz.models.payment.Balance;
import com.crater.mentorz.models.payment.Payment;
import com.crater.mentorz.models.payment.Redeem;
import com.crater.mentorz.models.user.DeviceInfo;
import com.crater.mentorz.models.user.Feedback;
import com.crater.mentorz.models.user.ForgetPassword;
import com.crater.mentorz.models.user.Interest;
import com.crater.mentorz.models.user.Login;
import com.crater.mentorz.models.user.Profile;
import com.crater.mentorz.models.user.ProfileImage;
import com.crater.mentorz.models.user.SocialInterest;
import com.crater.mentorz.models.user.SocialSource;
import com.crater.mentorz.models.user.User;
import com.crater.mentorz.models.user.Value;
import com.google.common.base.Optional;


public interface UserDAO {

	User save(final User user) throws ConflitException;

	void verifyEmail(final Long userId) throws NotFoundException;

	void updateDeviceInfo(final Long userId, final DeviceInfo deviceInfo);

	Optional<User> getUserDetails(final Login login);

	Optional<User> getSocialUserDetails(final Long userId);

	void updateValues(final Long userId, final List<Value> values);

	void updateInterests(final Long userId, final List<Interest> interests);
	
	void deleteInterests(final Long userId);

	List<Integer> getInterests(final Long userId, final Integer pageNo);

	List<Value> getValues(final Long userId, final Integer pageNo);

	void addSocialInfo(final Long userId, final String socialId, final String socialSource);

	Boolean checkUserExistanceSocialId(final String socialId, final String socialSource);

	Optional<Long> getUserIdBySocialId(final String socialId, final String socialSource);

	Optional<User> getProfile(final Long userId);

	void followUser(final Long userId, final Long followId) throws ForbiddenException;

	Long getFollowers(final Long userId);

	Double getRating(final Long userId);

	Optional<Profile> getProfileImage(final Long userId);

	List<Integer> getInterestIds(final Long userId);

	Integer updateProfile(final Long userId, final Profile profile);

	void rate(final Long userId, final Long ratedUserId, final Rating rating) throws ForbiddenException;

	void updateSocialInfo(final Long userId, final Profile profile);

	List<Interest> getMyInterests(final Long userId);
	
	List<Interest> getRootInterests(final Integer pageNo);
	
	List<Interest> getInterestsById(final Integer interestId);
	
	List<Interest> getInterestsByParentId(final Integer parentId);

	List<Value> getValuesList(final Integer pageNo);

	Integer isFollowing(final Long userId, final Long followingUserId);

	Optional<User> isExists(final String emailId);

	void updateUser(final Long userId, final User createWithDeviceInfoAndUserAgent, final Boolean isHavePassword) throws ConflitException;

	void updateSocialDetails(final Long userId, final String socialId, final String socialSource);

	Boolean isAlreadySocial(final Long userId);

	Integer updateProfileImage(final Long userId, final ProfileImage profileImage);

	Integer sendRequest(final Long userId, final Long mentorId) throws ForbiddenException;

	Optional<List<User>> getMyMentor(final Long userId, final Integer pageNo);

	Optional<List<User>> getMyMentee(final Long userId, final Integer pageNo);

	void deleteAndAddUserSocialInterests(final Long userId, final List<SocialInterest> interestList, final SocialSource socialSource);

	Boolean credit(final Payment payment);

	Boolean redeem(final Redeem redeem);

	Boolean debit(final Payment payment, final Boolean isCredit);

	Boolean debit(final Balance payment, final Long userId, final Double requestAmount);

	Optional<Balance> getBalance(final Long userId, final Long mentorId);

	void creditIntoMentorAccount(final Long mentorId, final Double amount);

	void feedback(final Long userId, final Feedback feedback);

	Integer blockUser(final Long userId, final Long blockUserId);

	Integer unFollow(final Long userId, final Long unfollowUserId);

	Optional<List<User>> getBlockedUsers(final Long userId, final Integer pageNo);

	Integer unBlockUser(final Long userId, final Long unBlockUserId);

	void deceraseFollowingCount(final Long userId);

	void deceraseFollowerCount(final Long userId);

	List<Map<Long, Long>> getBlockedUsersIds();

	Optional<Collection<User>> getReviews(final Long userId, final Integer pageNo);
	
	Boolean checkAccountStatus(final String emailId,final String password,final boolean isSocial,final String socialSource);
	
	String getNameByUserId(final Long userId);
	
	Boolean logout(final Long userId,final String userAgent);
	
	void saveAndUpdateVerificationToken(final ForgetPassword forgetPassword,final String verificationToken);
	
	String verifyToken(final String token);
	
	void deleteToken(final String emailId);
	
	Boolean resetPassword(final String newPassword,final String emailId);
	
	void deleteFromPendingRequest(final Long userId,final Long mentorId);
	
	void deleteFromMenteeRequest(final Long userId,final Long menteeId);
	
	void deleteFromMyMentor(final Long userId,final Long mentorId);
	
	void deleteFromMyMentee(final Long userId,final Long menteeId);
	
	boolean isBlockedUserOrBlockedBy(final Long userId,final Long blockedId);
	
    void deleteDevice(final Long userId);
	
	void deleteToken(final Long userId);
	
	Boolean getAccountStatus(final Long userId);

	List<Interest> syncInterest(Long time);

    Collection<User> findFollowing(Long userId);

	Collection<Source> getUserDocument(Integer offset,Integer limit);

	Collection<Source> getUserRating();

	Collection<Source> getBlockedUsers();

	Collection<Source> getUserAverageRating();

	Optional<List<User>> listFollowers(Long userId, Integer pageNo);

	Optional<List<User>> listFollowing(Long userId, Integer pageNo);
	Boolean skipMentor(Long userId, Long mentorId);

	Collection<Source> saveUserRequestSentMentors();

	List<Expertise> getParentExpertises(Set<Integer> childExpertises);

	Collection<Source> getSkippedMentors();

	Collection<Source> getMentorExpertises(Integer offset, Integer limit);

	List<User> getListPhoneContacts(User usr);

	
}
