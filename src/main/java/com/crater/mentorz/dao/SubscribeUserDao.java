package com.crater.mentorz.dao;

import com.crater.mentorz.models.user.SubscribeUser;
import com.google.common.base.Optional;

public interface SubscribeUserDao {

	void saveSubscribedUser(final SubscribeUser subscribeUser);

	Optional<SubscribeUser> findUserByEmail(final String email);
}
