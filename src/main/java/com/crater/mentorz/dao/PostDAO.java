/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.mentorz.dao;

import java.util.List;

import com.crater.mentorz.exception.ForbiddenException;
import com.crater.mentorz.models.post.AbuseType;
import com.crater.mentorz.models.post.Comment;
import com.crater.mentorz.models.post.Post;
import com.google.common.base.Optional;

/**
 * @author Harish Anuragi
 * 
 */
public interface PostDAO {
	Post addPostWithContent(final Long userId, final Post post);

	Post addPostWithOutContent(final Long userId, final Post post);

	void like(final Long postId, final Long userId) throws ForbiddenException;

	void view(final Long postId, final Long userId) throws ForbiddenException;

	void share(final Long postId, final Long userId) throws ForbiddenException;

	Comment addComment(final Long userId, final Long postId, final Comment comment) throws ForbiddenException;

	Optional<List<Post>> getPostByUserId(final Long userId, final Integer pageNo);

	Optional<List<Comment>> getCommentByPostId(final Long postId, final Integer pageNo);

	Optional<List<Post>> getBoard(final Integer pageNo, final Long userId);

	Integer deleteComment(final Long userId, final Long postId, final Long commentId);

	Optional<List<Post>> getFriendPostByUserId(final Long userId, final Long friendId, final Integer pageNo);

	void abusePost(final Long userId, final Long postId, final AbuseType abuseType) throws ForbiddenException;

	Optional<List<Post>> searchPost(final Long userId, final List<Integer> interest, final Integer pageNo);
	
	Optional<Long> getUserIdByPost(final Long postId);
	
	Optional<Comment> getCommentByCommentId(final Long commentId);
	
	Optional<Post> getPostByPostId(final Long postId);
	
}
