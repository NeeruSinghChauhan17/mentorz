package com.crater.mentorz.elasticsearch.model.res;

import com.crater.mentorz.elasticsearch.model.req.AvgRating;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Aggregations {
	
	@JsonProperty("avg_rating")
private final AvgRating avgRating;
	
	@SuppressWarnings("unused")
	private Aggregations(){
		this(null);
	}
	
  public Aggregations(AvgRating avgRating){
		this.avgRating=avgRating;
	}

  public AvgRating getAvgRating() {
	return avgRating;
  }
	
	
	
	
}
