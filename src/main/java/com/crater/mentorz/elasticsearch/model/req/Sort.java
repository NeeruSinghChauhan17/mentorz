
package com.crater.mentorz.elasticsearch.model.req;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class Sort {

	
	
	@JsonProperty("like_count")
	private final LikeCount likeCount;
	

  
    @SuppressWarnings("unused")
	private Sort() {
    	this(null);
    }

    public Sort(final LikeCount likeCount) {
		
		this.likeCount = likeCount;
		
	}


    
  

	public LikeCount getLikeCount() {
		return likeCount;
	}

	@Override
	public String toString() {
		return "Sort ["+" likeCount="
				+ likeCount + "]";
	}

}