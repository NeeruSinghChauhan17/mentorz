package com.crater.mentorz.elasticsearch.model.req;

public class Rating {

	private Integer rating;
	
	private Long ratedByUserId;
	
	private Rating(){
		this(null,null);
	}
	
	public Rating(Integer rating,Long ratedByUserId){
		this.rating=rating;
		
		this.ratedByUserId=ratedByUserId;
	}
	
	
	public Integer getRating() {
		return rating;
	}

	

	public Long getRatedByUserId() {
		return ratedByUserId;
	}
}
