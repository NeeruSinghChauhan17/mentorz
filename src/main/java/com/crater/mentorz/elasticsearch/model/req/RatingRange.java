package com.crater.mentorz.elasticsearch.model.req;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RatingRange {


	@JsonProperty("gte")
	private Integer gte;
	
	@JsonProperty("lte")
	private Integer lte;
	
	@SuppressWarnings("unused")
	private RatingRange(){
		this(null,null);
	}

	public RatingRange(Integer gte,Integer lte){
		this.gte=gte;
		this.lte=lte;
	
	}

	
	
	public static RatingRange withMaxAndMin(Integer lte,Integer gte){
		return new RatingRange(gte,lte);
		
	}
	

	public Integer getGte() {
		return gte;
	}

	public Integer getLte() {
		return lte;
	}

	
}
