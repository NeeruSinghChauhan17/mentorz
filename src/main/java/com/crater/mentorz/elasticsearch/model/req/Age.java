
package com.crater.mentorz.elasticsearch.model.req;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class Age {

    @JsonProperty("from")
    private Integer from;
    
    @JsonProperty("to")
    private Integer to;
    
   
    /**
     * No args constructor for use in serialization
     * 
     */
    @SuppressWarnings("unused")
	private Age() {
    	this(null, null);
    }

    /**
     * 
     * @param gt
     * @param lt
     */
    public Age(Integer from, Integer to) {
        this.from = from;
        this.to = to;
    }

	public Integer getFrom() {
		return from;
	}

	public Integer getTo() {
		return to;
	}

	@Override
	public String toString() {
		return "Age [from=" + from + ", to=" + to + "]";
	}

   

    

}