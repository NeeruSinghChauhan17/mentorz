package com.crater.mentorz.elasticsearch.model.req;

import java.util.List;
import java.util.Set;

import com.crater.mentorz.models.mentor.Expertise;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class Params {
	
	

    @JsonProperty("lres_id")
	private final String lresId;
	
	@JsonProperty("hres_id")
	private final String hresId;
	
	@JsonProperty("blocked_users")
	private final Long blocked_id;
	
	@JsonProperty("status")
	private final Integer status;
	
	@JsonProperty("name")
	private final String name;
	
	@JsonProperty("basic_info")
	private final String userInfo;
	
	@JsonProperty("video_bio_lres")
	private final String videoBioLres;
	
	@JsonProperty("video_bio_hres")
	private final String videoBioHres;
	
	@JsonProperty("charge_price")
	private final Integer price;
	
	@JsonProperty("org")
	private final String org;
	
	@JsonProperty("located_in")
	private final String location;
	 
	@JsonProperty("position")
	private final String position;
	
	@JsonProperty("exp")
	private final Integer exp;
	
	@JsonProperty("expertises")
	private final List<Expertise> expertises;
	
	@JsonProperty("expertise_ids")
	private final  Set<Integer> expertiseIds;
	
	@JsonProperty("rating")
	private final Integer rating;
	


	@JsonProperty("ratings")
	private final Rating ratings;
	
	
	@JsonProperty("avg_rating")
	private final Float avgRating;
	
	@JsonProperty("facebook_url")
	private final String facebookUrl;
	
	@JsonProperty("linkedin_url")
	private final String linkedInUrl;
	
	
	@JsonProperty("skipped_mentors")
	private final Long mentorId;
	
	@JsonProperty("request_sent_mentors")
	private final Long mentorId1;
	
	@JsonProperty("parent_expertises")
	private final  Set<Expertise> parentExpertises;
	
	
	@JsonProperty("parent_expertise_ids")
	private final  Set<Integer> parentExpertiseIds;
	@SuppressWarnings("unused")
	private Params(){
		this(null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
				null,null,null,null,null);
	}
	
	
	public Params(String lresId,String hresId,Long blockedId,Integer status,String name,String userInfo,String videoBioLres,
			String videoBioHres,	final Integer price,final String org,final String location,final String position,
			final Integer exp,final List<Expertise> expertises,final Set<Integer> expertiseIds,Integer rating,Rating ratings,
			Float avgRating,String facebookUrl,String linkedInUrl,Long mentorId,Long mentorId1,
			Set<Expertise> parentExpertises,Set<Integer> parentExpertiseIds){
		this.lresId=lresId;
		this.hresId=hresId;
		this.blocked_id=blockedId;
		this.status=status;
		this.name=name;
		this.userInfo=userInfo;
		this.videoBioLres=videoBioLres;
		this.videoBioHres=videoBioHres;
		this.price=price;
		this.org=org;
		this.location=location;
		this.position=position;
		this.exp=exp;
		this.expertises=expertises;
		this.expertiseIds=expertiseIds;
		this.rating=rating;
		this.ratings=ratings;
		this.avgRating=avgRating;
		this.facebookUrl=facebookUrl;
		this.linkedInUrl=linkedInUrl;
		this.mentorId=mentorId;
		this.mentorId1=mentorId1;
		this.parentExpertises=parentExpertises;
		this.parentExpertiseIds=parentExpertiseIds;
		
	}

	
	public static Params withLresId(String lresId){
		return new Params( lresId,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null
				,null,null,null,null,null,null,null);
	}

	public static Params withHresId(String hresId){
		return new Params( null,hresId,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null
				,null,null,null,null,null,null,null);	
	}
	
	public static Params withStatus(Integer status){
		return new Params( null,null,null,status,null,null,null,null,null,null,null,null,null,null,null,null,
				null,null,null,null,null,null,null,null);
	}
	
	public static Params withPrice(Integer price){
		return new Params(null,null,null,null,null,null,null,null,price,null,null,null,null,null,null,null,
				null,null,null,null,null,null,null,null);
	}
	
	
	public static Params withOrg(String org){
		return new Params(null,null,null,null,null,null,null,null,null,org,null,null,null,null,null,null,null,
				null,null,null,null,null,null,null);
	}
	public static Params withLocation(String location){
		return new Params(null,null,null,null,null,null,null,null,null,null,location,null,null,null,null,null,
				null,null,null,null,null,null,null,null);
	}
	public static Params withPosition(String position){
		return new Params(null,null,null,null,null,null,null,null,null,null,null,position,null,null,null,null,
				null,null,null,null,null,null,null,null);
	}
	public static Params withExp(Integer exp){
		return new Params(null,null,null,null,null,null,null,null,null,null,null,null,exp,null,null,null,null,
				null,null,null,null,null,null,null);
	}
	public static Params withExpertise(List<Expertise> expertises){
		return new Params(null,null,null,null,null,null,null,null,null,null,null,null,null,expertises,null,null,
				null,null,null,null,null,null,null,null);
	}

	
	public static Params withBlockedId(Long blockedId){
		return new Params(null,null,blockedId,null,null,null,null,null,null,null,null,null,null,null,null,null,
				null,null,null,null,null,null,null,null);
	}
	
	public static Params withName(String name){
		return new Params(null,null,null,null,name,null,null,null,null,null,null,null,null,null,null,null,null,
				null,null,null,null,null,null,null);
	}
	public static Params withBasicInfo(String info){
		return new Params(null,null,null,null,null,info,null,null,null,null,null,null,null,null,null,null,null,
				null,null,null,null,null,null,null);
	}
	public static Params withVideoBioLres(String vedioBioLres){
		return new Params(null,null,null,null,null,null,vedioBioLres,null,null,null,null,null,null,null,null,null,
				null,null,null,null,null,null,null,null);
	}
	public static Params withVedioBioHres(String vedioBioHres){
		return new Params(null,null,null,null,null,null,null,vedioBioHres,null,null,null,null,null,null,null,null,
				null,null,null,null,null,null,null,null);
	}
	
	public static Params withRating(Integer rating){
		
		return new Params(null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,rating,null,
				null,null,null,null,null,null,null);
	}
	
	public static Params withAvgRating(Float avgRating){
		
		return new Params(null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
				avgRating,null,null,null,null,null,null);
	}
	
    public static Params withExpertiseIds(Set<Integer> expertiseIds){
		
		return new Params(null,null,null,null,null,null,null,null,null,null,null,null,null,null,expertiseIds,null,
				null,null,null,null,null,null,null,null);
	}
	
	public static Params withRatings(Rating ratings) {
		
		return new Params(null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,ratings,
				null,null,null,null,null,null,null);
	}
   public static Params withFacebookURl(String facbookUrl) {
		
		return new Params(null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
				null,facbookUrl,null,null,null,null,null);
	}

   public static Params withLinkedInUrl(String linkedInUrl) {
	
	return new Params(null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
			null,linkedInUrl,null,null,null,null);
     }
   public static Params withMentorID(Long mentorId) {
		
		return new Params(null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
				null,null,null,mentorId,null,null,null);
	     }
  
   public static Params withRequestSentMentorID(Long mentorId1) {
		
		return new Params(null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
				null,null,null,null,mentorId1,null,null);
	     }
   
   public static Params withParentExpertises(Set<Expertise> parentExpertises) {
		
		return new Params(null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
				null,null,null,null,null,parentExpertises,null);
	     }
   
   public static Params withParentExpertiseIds(Set<Integer> parentExpertiseIds) {
		
 		return new Params(null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
 				null,null,null,null,null,null,parentExpertiseIds);
 	     }

	public String getHresId() {
		return hresId;
	}


	public Long getBlocked_id() {
		return blocked_id;
	}


	public String getName() {
		return name;
	}


	public String getUserInfo() {
		return userInfo;
	}


	public String getVideoBioLres() {
		return videoBioLres;
	}


	public String getVideoBioHres() {
		return videoBioHres;
	}


	public Integer getStatus() {
		return status;
	}


	public String getLresId() {
		return lresId;
	}


	public Integer getPrice() {
		return price;
	}


	public String getOrg() {
		return org;
	}


	public String getLocation() {
		return location;
	}


	public String getPosition() {
		return position;
	}


	public Integer getExp() {
		return exp;
	}


	public List<Expertise> getExpertises() {
		return expertises;
	}
	
	
}