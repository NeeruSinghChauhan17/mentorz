package com.crater.mentorz.elasticsearch.model.req;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class Location {

	@JsonProperty("lat")
	private final Double lat;
	
	@JsonProperty("lon")
	private final Double lon;
	
	@SuppressWarnings("unused")
	private Location(){
		this(null, null);
	}
	
	public Location(final Double lat, final Double lon) {
		this.lat = lat;
		this.lon = lon;
	}

	public Double getLat() {
		return lat;
	}

	public Double getLon() {
		return lon;
	}

	@Override
	public String toString() {
		return "Location [lat=" + lat + ", lon=" + lon + "]";
	}
	
}
