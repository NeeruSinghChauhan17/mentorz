package com.crater.mentorz.elasticsearch.model.req;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class AvgRating {
	
	
	@JsonProperty("avg")
	private final Avg avg;
	
	@JsonProperty("value")
	private final Float value;
	
	public Float getValue() {
		return value;
	}

	@SuppressWarnings("unused")
	private AvgRating(){
		this(null,null);
	}
	
	public AvgRating(Avg avg,Float value){
		this.avg=avg; 
		this.value=value;
	}

	
	public static AvgRating withAvg(Avg avg){
		return new AvgRating(avg,null);
	}
	public Avg getAvg() {
		return avg;
	}
	
	
	
}
