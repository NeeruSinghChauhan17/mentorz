package com.crater.mentorz.elasticsearch.model.req;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class LikeCount {
	
	@JsonProperty("order")
	private final String order;
	
	@SuppressWarnings("unused")
	private LikeCount(){
		this(null);
	}
	public LikeCount(final String order) {
		this.order = order;
	}
	
	public String getOrder() {
		return order;
	}

	@Override
	public String toString() {
		return "LikeCount [order=" + order + "]";
	}
}
