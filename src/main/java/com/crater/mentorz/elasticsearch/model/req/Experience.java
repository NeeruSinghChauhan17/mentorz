package com.crater.mentorz.elasticsearch.model.req;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class Experience {
	
	
	private Integer gte;
	private Integer lte;
	@SuppressWarnings("unused")
	private Experience(){
		this(null,null);
	}

	public Experience(Integer gte,Integer lte){
		this.gte=gte;
		this.lte=lte;
	}

	public static Experience withMaxAndMin(Integer lte,Integer gte){
		return new Experience(gte,lte);
	}
	public Integer getGte() {
		return gte;
	}

	public Integer getLte() {
		return lte;
	}

}
