package com.crater.mentorz.elasticsearch.model.req;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Field {

	@JsonProperty("rating")
	private final String rating;
	
	@SuppressWarnings("unused")
	private Field(){
		this(null);
	}
	
	public Field(String rating){
	this.rating=rating;	
	}

	public static Field withRating(String rating){
		return new Field(rating);
	}
	
	public String getRating() {
		return rating;
	}
	
	
	
	
}
