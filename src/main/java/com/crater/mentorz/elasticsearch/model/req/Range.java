
package com.crater.mentorz.elasticsearch.model.req;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class Range {

   
    
    
	@JsonProperty("charge_price")
    private final ChargePrice chargePrice;
    
	@JsonProperty("exp")
    private final Experience exp;
    
	@JsonProperty("avg_rating")
	private RatingRange avgRating;
	
    
   
    /**
     * No args constructor for use in serialization
     * 
     */
    @SuppressWarnings("unused")
	private Range() {
    	this(null,null,null);
    }

  

	public Range(final ChargePrice chargePrice,final Experience exp ,RatingRange avgRating) {
       
        this.chargePrice=chargePrice;
        this.exp=exp;
        this.avgRating=avgRating;
    }

  public static Range withChargePrice(ChargePrice chargePrice){
	  return  new Range(chargePrice,null,null);
  }
 
  public static Range withAvgRating(RatingRange avgrating){
	  return new Range(null,null,avgrating);
  }

  
  public static Range withExperience(final Experience exp){
	  return new Range(null,exp,null);
  }
  
  
  public static Range withExperienceAndPrice(final Experience exp,ChargePrice chargePrice){
	  return new Range(chargePrice,exp,null);
	  
  }
  
  public static Range withRatingAndPrice(RatingRange rating,ChargePrice price){
	  return new Range(price,null,rating);
	  
  }
  public static Range withRatingAndExperience(RatingRange rating,Experience exp){
	  return new Range(null,exp,rating);
	  
  }
  
  public static Range withRatingAndExperienceAndPrice(RatingRange rating,Experience exp,ChargePrice chargePrice){
	 return new Range(chargePrice,exp,rating); 
  }
	public Experience getExp() {
		return exp;
	}

	@Override
	public String toString() {
		return "Range [ChargePrice=" + chargePrice 
				+ "]";
	}

	

   

	public RatingRange getAvgRating() {
		return avgRating;
	}



	public ChargePrice getChargePrice() {
		return chargePrice;
	}


}