package com.crater.mentorz.elasticsearch.model.req;

public class ChargePrice {
	
	private Integer gte;
	private Integer lte;
	@SuppressWarnings("unused")
	private ChargePrice(){
		this(null,null);
	}

	public ChargePrice(Integer gte,Integer lte){
		this.gte=gte;
		this.lte=lte;
	}
	
	public static ChargePrice withMaxAndMin(Integer lte,Integer gte){
		return new ChargePrice(gte,lte);
	}

	public Integer getGte() {
		return gte;
	}

	public Integer getLte() {
		return lte;
	}
	
	
	
}
