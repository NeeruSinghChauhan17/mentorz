
package com.crater.mentorz.elasticsearch.model.req;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonInclude(Include.NON_NULL)
public class ElasticSearch {

	@JsonProperty("query")
    private final Query query;
	
	@JsonProperty("filter")
	private final Filter filter;
	
	@JsonProperty("sort")
	private final List<Sort> sort ;
	
	@JsonProperty("_source")
	private final List<String> source;
	
	@JsonProperty("aggs")
	private final Aggs aggs;
	
	@SuppressWarnings("unused")
	private ElasticSearch() {
		 this(null, null, null,null,null);
	 }
	 
    public ElasticSearch(final Query query, final Filter filter, final List<Sort> sort, final List<String> source,Aggs aggs) {
		super();
		this.query = query;
		this.filter = filter;
		this.sort = sort;
		this.source=source;
		this.aggs=aggs;
	}

	public static ElasticSearch withQuery(final Query query) {
		return new ElasticSearch(query,null,null,null,null);
    }
    
    public static ElasticSearch withQueryFilter(final Query query,final Filter filter) {
    	return new ElasticSearch(query,filter,null,null,null);
    }
    
    public static ElasticSearch withQueryAndSource(final Query query,final List<String> source) {
    	return new ElasticSearch(query,null,null,source,null);
    }
    
    public static ElasticSearch withQuerySort(final Query query,final List<Sort> sort) {
    	return new ElasticSearch(query,null,sort,null,null);
    }
    public static ElasticSearch withSort(final List<Sort> sort) {
    	return new ElasticSearch(null, null, sort,null,null);
    }
    public static ElasticSearch withFilterSort(final Filter filter,final List<Sort> sort) {
    	return new ElasticSearch(null, filter, sort,null,null);
    }
    
    public static ElasticSearch withQueryFilterSort(final Query query,final Filter filter,final List<Sort> sort) {
    	return new ElasticSearch(query,filter,sort,null,null);
    }
    
    public static ElasticSearch withQueryAndSort(final Query query,final List<Sort> sort) {
    	return new ElasticSearch(query,null,sort,null,null);
    }
    
 
    
    public static ElasticSearch withQueryAndAggs(final Query query,final Aggs aggs) {
    	return new ElasticSearch(query,null,null,null,aggs);
    }
    
	public Query getQuery() {
		return query;
	}

	public Filter getFilter() {
		return filter;
	}

	public List<Sort> getSort() {
		return sort;
	}

	public List<String> getSource() {
		return source;
	}

	@Override
	public String toString() {
		return "ElasticSearch [query=" + query + ", filter=" + filter
				+ ", sort=" + sort + ", source=" + source + "]";
	}
    
   

}