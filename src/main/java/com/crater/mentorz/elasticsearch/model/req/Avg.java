package com.crater.mentorz.elasticsearch.model.req;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Avg {

	
	
	@JsonProperty("field")
	private final String field;
	
	
	@SuppressWarnings("unused")
	private Avg(){
		this(null);
	}
	
	
	public Avg(String field){
		this.field=field;
		
	}
	
	public static Avg withField(String field){
		return new Avg(field);
	}


	public String getField() {
		return field;
	}
}
