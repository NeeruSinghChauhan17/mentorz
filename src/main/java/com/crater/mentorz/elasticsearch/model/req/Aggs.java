package com.crater.mentorz.elasticsearch.model.req;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class Aggs {
 
	@JsonProperty("avg_rating")
	private final AvgRating averageRating;
	
	@SuppressWarnings("unused")
	private Aggs(){
		this(null);
	}
	
	public Aggs(AvgRating averageRating){
		this.averageRating=averageRating;
	}

    public static Aggs withAverageRating(AvgRating averageRating){
		return new Aggs( averageRating);
	}
	public AvgRating getAverageRating() {
		return averageRating;
	}
	
	
}

