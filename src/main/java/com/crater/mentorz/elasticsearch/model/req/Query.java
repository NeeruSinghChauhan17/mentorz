
package com.crater.mentorz.elasticsearch.model.req;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class Query {


	@JsonProperty("filtered")
    private Filtered filtered;
	
	@JsonProperty("bool")
	private final Bool bool;
	
	@JsonProperty("range")
	private final Range range;
	
	@JsonProperty("prefix")
	private Prefix prefix;

	
    @SuppressWarnings("unused")
	private Query() {
    	this(null, null,null,null);
    }
    
    public Query(Filtered filtered, Bool bool, Range range,Prefix prefix) {
		this.filtered=filtered;
		this.bool = bool;
		this.range = range;
		this.prefix=prefix;
		
	}

	public static Query withBool(final Bool bool) {
       return new Query(null, bool, null,null);
    }
     
	
	public static Query withRangeAndBool(Range range,Bool bool){
		return new Query(null,bool,range,null);
	}
   
    public static Query withFiltered(final Filtered filtered) {
    	 return new Query(filtered, null, null,null);
    }
    
    public static Query withRange(final Range range) {
   	 return new Query(null, null, range,null);
   }
    
    public static Query withPrefix(final Prefix prefix) {
      	 return new Query(null, null,null,prefix);
      }
    
   
       @Override
	public String toString() {
		return "Query [  bool=" + bool + ", range="
				+ range + "]";
	}

	public Bool getBool() {
		return bool;
	}

	public Range getRange() {
		return range;
	}

	public Filtered getFiltered() {
		return filtered;
	}
	

}