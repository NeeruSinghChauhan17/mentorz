
package com.crater.mentorz.elasticsearch.model.req;
import java.util.List;
import java.util.Set;
import org.hibernate.validator.constraints.NotBlank;

import com.crater.mentorz.models.mentor.Expertise;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class Source {
     
	

	@JsonProperty("user_id")
    private final Long userId;
	
	@JsonProperty("email_id")
	private final String emailId;
	

	@JsonProperty("lres_id")
	private String lresId;
	
	@JsonProperty("hres_id")
	private String hresId;
	
	@JsonProperty("followers")
	private int followers;
	
	@JsonProperty("following")
	private int following;
	
	@JsonProperty("mentors")
	private int mentors;
	
	@JsonProperty("mentees")
	private int mentees;
	
	@JsonProperty("basic_info")
	private String basicInfo;
	
	@JsonProperty("birth_date")
	private long birthDate;
	
	@JsonProperty("charge_price")
	private int chargePrice;
	
	@JsonProperty("org")
	private String org;
	
	@JsonProperty("located_in")
	private String locatedIn;
	
	@JsonProperty("position")
	private String position;
	
	@JsonProperty("requests")
	private int requests;
	
	@JsonProperty("exp")
	private Integer exp;
	
	@JsonProperty("video_bio_lres")
	private String videoBioLres;
	
	@JsonProperty("video_bio_hres")
	private String videoBioHres;
	
	@JsonProperty("ratings")
	private List<Rating> rating;
	
	@JsonProperty("avg_rating")
	private Float averageRating;

	@NotBlank(message = "user name can't be null or blank")
	private String name;
	
	@JsonProperty("status")
	private final Integer accountStatus;
	
	@JsonProperty("expertises")
     private final List<Expertise> expertise;
	
	@JsonProperty("expertise_ids")
	private final Set<Integer> expertiseIds;
	@JsonProperty("blocked_users")
    private final Set<Long> blockedIds;
	
	@JsonProperty("skipped_mentors")
    private final Set<Long> skippedMentors;
	
	@JsonProperty("request_sent_mentors")
	private final Set<Long> requestSentMentors;
	
	@JsonProperty("parent_expertises")
	private final  List<Expertise> parentExpertises;
	
	
	@JsonProperty("parent_expertise_ids")
	private final  List<Integer> parentExpertiseIds;
	
	
	@JsonProperty("facebook_url")
	private final String facebookUrl;
	
	@JsonProperty("linkedin_url")
	private final String linkedInURl;
	
    @SuppressWarnings("unused")
	private Source() {
	 this(null,null,null,null,0,0,0,0,null,0,0,null,null,null,0,null,null,null,null,null,null,null,
			 null,null,null, null,null,null,null,null,null);  
    }
    
    public Source(final Long userId ,final String emailId,final String lresId,final  String hresId,final int followers,final int following,
    		final int mentors ,final int mentees,final String basicInfo,final long birthDate,final int chargePrice,final String org,
    		final String locatedIn,final String position,final int requests,final Integer exp,final String videoBioLres,
    		final String videoBioHres,final List<Rating> rating,final Float averageRating,final	String name,final Integer accountStatus,
    		final List<Expertise> expertise,Set<Integer> expertiseIds,final Set<Long> blockedIds,Set<Long> skippedMentors,
    		Set<Long> requestSentMentors,List<Expertise> parentExpertises,List<Integer> parentExpertiseIds,
    		final String facebookUrl,final String linkedInUrl) {
		
		this.userId = userId;
		this.emailId=emailId;
		this.lresId=lresId;
		this.hresId=hresId;
		this.followers=followers;
		this.following=following;
		this.mentors=mentors;
		this.mentees=mentees;
		this.basicInfo=basicInfo;
		this.birthDate=birthDate;
		this.chargePrice=chargePrice;
		this.org=org;
		this.locatedIn=locatedIn;
		this.position=position;
		this.requests=requests;
		this.exp=exp;
		this.videoBioLres=videoBioLres;
		this.videoBioHres=videoBioHres;
	    this.rating=rating;
	    this.averageRating=averageRating;
	     this.name=name;
	
		this.accountStatus=accountStatus;
		this.expertise=expertise;
		this.expertiseIds=expertiseIds;
		this.blockedIds=blockedIds;
		this.skippedMentors=skippedMentors;
		this.requestSentMentors=requestSentMentors;
		this.parentExpertises=parentExpertises;
		this.parentExpertiseIds=parentExpertiseIds;
		this.facebookUrl=facebookUrl;
		this.linkedInURl=linkedInUrl;
	}
   


	public String getFacebookUrl() {
		return facebookUrl;
	}

	public String getLinkedInURl() {
		return linkedInURl;
	}

	public List<Expertise> getParentExpertises() {
		return parentExpertises;
	}

	public Set<Long> getRequestSentMentors() {
		return requestSentMentors;
	}

	public Set<Long> getSkippedMentors() {
		return skippedMentors;
	}

	public List<Integer> getParentExpertiseIds() {
		return parentExpertiseIds;
	}

	public Float getAverageRating() {
		return averageRating;
	}

	public Long getUserId() {
		return userId;
	}

	public String getEmailId() {
		return emailId;
	}

	public String getLresId() {
		return lresId;
	}

	public String getHresId() {
		return hresId;
	}

	public int getFollowers() {
		return followers;
	}

	public int getFollowing() {
		return following;
	}

	public int getMentors() {
		return mentors;
	}

	public int getMentees() {
		return mentees;
	}

	public String getBasicInfo() {
		return basicInfo;
	}

	public long getBirthDate() {
		return birthDate;
	}

	public int getChargePrice() {
		return chargePrice;
	}

	public String getOrg() {
		return org;
	}

	public String getLocatedIn() {
		return locatedIn;
	}

	public String getPosition() {
		return position;
	}

	public int getRequests() {
		return requests;
	}

	public Integer getExp() {
		return exp;
	}

	public String getVideoBioLres() {
		return videoBioLres;
	}

	public String getVideoBioHres() {
		return videoBioHres;
	}

	public List<Rating> getRating() {
		return rating;
	}

	public String getName() {
		return name;
	}

	public Integer getAccountStatus() {
		return accountStatus;
	}

	public List<Expertise> getExpertise() {
		return expertise;
	}

	public Set<Integer> getExpertiseIds() {
		return expertiseIds;
	}
	public Set<Long> getBlockedIds() {
		return blockedIds;
	}





	
	
    
}
