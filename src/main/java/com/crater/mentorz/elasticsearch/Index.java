package com.crater.mentorz.elasticsearch;

public class Index {
	
	public static final String MENTORZ = "mentorz";
	
	public static final String MATCHSTIX_ADMIN = "matchstix_admin";
	
	static class Type{
		public static final String USERS="users";
		public static final String PREMIUM="premium";
	}

}
